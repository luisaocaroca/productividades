<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.


return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Sistema Productividades',
        'onBeginRequest'=>array('LoginSession','verificarSession'),
        'language'=>'es',
        'sourceLanguage'=>'en',
        'charset'=>'utf-8',
	// preloading 'log' component
	'preload'=>array('log'),
        'aliases' => array(
             'bootstrap' => 'ext.bootstrap',
        ),
        'theme'=>'bootstrap',        
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
                'ext.yii-mail.YiiMailMessage',
                'ext.giix-components.*', // giix components
                'application.extensions.encrypter.Encrypter',
                'application.extensions.PHPExcel',
                'ext.wrest.*',
                'application.extensions.crontab.*',
                'bootstrap.behaviors.*',
                'bootstrap.helpers.*',
                'bootstrap.widgets.*'
                
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(                    
                        'generatorPaths' => array('bootstrap.gii'),
			'class'=>'system.gii.GiiModule',
                        
                        
                        'generatorPaths' => array(
                                'ext.giix-core', // giix generators
                        ),
                        
			'password'=>'ltoledo1206',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),

		'backup'
	),

	// application components
	'components'=>array(
                 'bootstrap' => array(
                    'class' => 'bootstrap.components.BsApi'
                ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
                        'class' => 'WebUser',
                        'loginUrl'=>array('site/index'),
		),

		// uncomment the following to enable URLs in path-format
                'urlManager'=>array(
                    'urlFormat'=>'path',
                    'rules'=>array(
                        //rest url patterns
                        array('api/<model>/delete', 'pattern'=>'api/<model:\w+>/<_id:\d+>', 'verb'=>'DELETE'),
                        array('api/<model>/update', 'pattern'=>'api/<model:\w+>/<_id:\d+>', 'verb'=>'PUT'),
                        array('api/<model>/list', 'pattern'=>'api/<model:\w+>', 'verb'=>'GET'),
                        array('api/<model>/get', 'pattern'=>'api/<model:\w+>/<_id:\d+>', 'verb'=>'GET'),
                        array('api/<model>/create', 'pattern'=>'api/<model:\w+>', 'verb'=>'POST'),
                    ),
                ),
            
                'mail' => array(
                        'class' => 'ext.yii-mail.YiiMail',
                        'transportType'=>'smtp',
                        'transportOptions'=>array(
                                'host'=>'ssl://smtp.gmail.com',
                                'username'=>'ltoledo@impsys-am.com',
                                'password'=>'ltoledo',
                                'port'=>'465',                       
                        ),
                        'viewPath' => 'application.views.mail',             
                ),
                'Smtpmail'=>array(
                    'class'=>'application.extensions.smtpmail.PHPMailer',
                    'Host'=>"ssl://smtp.gmail.com",
                    'Username'=>'ltoledo@impsys-am.com',
                    'Password'=>'ltoledo',
                    'Mailer'=>'smtp',
                    'Port'=>465,
                    'SMTPAuth'=>true,
                    //'SMTPSecure' => 'tls',
                ),
            
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,
                        'urlSuffix'=>'.html',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
                 
                 */
		
                /*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
                 * */
                 
		// uncomment the following to use a MySQL database

                'db'=>array(
                        'connectionString' => 'mysql:host=localhost;dbname=prod_db',
                        'emulatePrepare' => true,
                        //'username' => 'prod_u',
                        //'password' => 'Buitwatoex20Ye',
                        'username' => 'root',
                        'password' => 'ltoledo1206',
                        'charset' => 'utf8',
                        'enableParamLogging'=>true
                ),

            
                'coreMessages'=>array(
                    'basePath'=>'protected/messages'
                ),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
                        //'class' => 'AppErrorHandler',
			'errorAction'=>'site/error',
                        
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error,trace,info',
                                        
				),
//                                array(
//                                    'class'=>'CWebLogRoute',
//                                    'levels'=>'error,trace,info',
//                                    'showInFireBug' => false,
//                                    'ignoreAjaxInFireBug' => false,
//                                ),
				// uncomment the following to show log messages on web pages
				
//				array(
//					'class'=>'CWebLogRoute',
//				),
				
			),
		),
                'format'=>array(
                    'class'=>'application.components.Formatter',
                ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'ltoledo@impsys-am.com',
	),
);