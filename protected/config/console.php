<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Consola Sistema Productividades',
    
        // autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
                'ext.yii-mail.YiiMailMessage',
                'ext.giix-components.*', // giix components
                'application.extensions.encrypter.Encrypter',
                'application.extensions.PHPExcel',
                'ext.wrest.*',
                'application.extensions.crontab.*',
	),

	// preloading 'log' component
	'preload'=>array('log'),

	// application components
	'components'=>array(
                /*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
                 * 
                 */
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=prod_db',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'ltoledo1206',
			'charset' => 'utf8',
                        'enableParamLogging'=>true
		),
                'mail' => array(
                            'class' => 'ext.yii-mail.YiiMail',
                            'transportType'=>'smtp',
                            'transportOptions'=>array(
                                    'host'=>'ssl://smtp.gmail.com',
                                    'username'=>'ltoledo@impsys-am.com',
                                    'password'=>'ltoledo',
                                    'port'=>'465',                       
                            ),
                            'viewPath' => 'application.views.mail',             
                    ),
                    'Smtpmail'=>array(
                        'class'=>'application.extensions.smtpmail.PHPMailer',
                        'Host'=>"ssl://smtp.gmail.com",
                        'Username'=>'ltoledo@impsys-am.com',
                        'Password'=>'ltoledo',
                        'Mailer'=>'smtp',
                        'Port'=>465,
                        'SMTPAuth'=>true,
                        //'SMTPSecure' => 'tls',
                    ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error,trace,info',
                                        
				),
//                                array(
//                                    'class'=>'CWebLogRoute',
//                                    'levels'=>'error,trace,info',
//                                    'showInFireBug' => false,
//                                    'ignoreAjaxInFireBug' => false,
//                                ),
				// uncomment the following to show log messages on web pages
				
//				array(
//					'class'=>'CWebLogRoute',
//				),
				
			),
		),
	),
    
);