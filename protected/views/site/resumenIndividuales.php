

<script>

function verDetalleCentroCostoMes(periodo){
    
    mostrarLoader();
    
    var url = "<?php echo Yii::app()->createUrl('site/detalleMesIndividual');?>";
    $.ajax({
        url : url,
        type: "POST",
        data: {"vl_periodo":periodo},
        success: function(response) {
              //alert(response);
              $('#detalleMes').html(response);
              $('#loading-indicator').hide();
        },
        error:  function (error) {
            var output = '';
            for (property in error) {
              output += property + ': ' + error[property]+'; ';
            }
            alert(output);
            $('#loading-indicator').hide();
        }
    });
    e.preventDefault();
}
    
</script>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'site-form-consulta-form',
	
));?>
    



<div class="row">

    
    <div class="col-md-1">
        <span class="glyphicon glyphicon-calendar"></span>
        <b>Año</b> <br>

        <?php echo $form->dropDownList($model,'anio_id', CHtml::listData($anios,'id','value'),
                                                               array("class"=>"form-control","style"=>"min-width:120px;width:120px;"));?>
    </div>
    <div class="col-md-2">
        <br>
        <button class="btn btn-info" type="submit" id="actualiza">
            <span class=" glyphicon glyphicon-search"></span>
        </button>
    </div>

</div>
    <br>
    
<div class="row">
    <div class="col-md-4">

        <?php
        $this->beginWidget('bootstrap.widgets.BsPanel', array(
            'title' => '<span class="glyphicon glyphicon-dashboard"></span>  Resumen Anual',
            'type' => BsHtml::PANEL_TYPE_PRIMARY
        ));
        ?>

            <div class="row">
                <div class="col-md-12">
                    <?php

                $this->Widget('ext.highcharts.HighchartsWidget', array(
                   'id'=>"chartDetalleMeses",
                   'options'=>array(
                      'title' => array('text' => 'Detalle Año ' . $anio_id),
                      'xAxis' => array(
                           'labels' => array(
                              'rotation' => 0,
                              'useHTML'=> true
                              //'align'=>'right',
                           ),
                         'categories' => $datos_grafico_detalle_anio["categorias"]
                      ),
                      'yAxis' => array(
                         'title' => array('text' => '$ Productividades Pagadas')
                      ),      
                      'series' => $datos_grafico_detalle_anio["datos"],
                      'chart' => array(
                         'type' => 'column',
                         'height'=>400,
                         //'width'=>500,
                      ), 
                      //'legend' => $legend, 
                      'plotOptions' => array(
                         'column' => array('stacking'=> 'normal',
                                           'cursor' => 'pointer',
                                           'point'  => array('events'=>array('click'=>'js:function(){
                                                                var drilldown = this.drilldown;
                                                                if (drilldown) { // drill down                                                    
                                                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                                                } else { // restore
                                                                    setChart(name, categories, data);
                                                                }

                                                            }') )              
                                          ),

                      ),   
                      'theme' => 'grid',              
                      'credits' => array('enabled' => false),
                      'tooltip' => array(
                                'formatter' => 'js:function(){var point = this.point,
                                                                      s = this.x + "<br>";
                                                                      s += "<b><span style=\"color:"+this.series.color+"\">"+this.series.name +"</span></b>";
                                                                      s += ":<b> $ "+ number_format(this.y) +"</b><br>";



                                                                return s; }'
                              ),
                   )
                ));





                ?>
                </div>
            </div>
        
        <?php
        $this->endWidget();
        ?>

    </div>
    
    <div class="col-md-8">
        
        <div id="detalleMes">
            <?php
            $this->beginWidget('bootstrap.widgets.BsPanel', array(
                'title' => '<span class="glyphicon glyphicon-dashboard"></span>  Detalle Mes '  . $periodo->nombre_periodo,
                'type' => BsHtml::PANEL_TYPE_PRIMARY
            ));
            ?>


                    <?php

                                $this->widget('zii.widgets.grid.CGridView',
                                              array('dataProvider' => $resumenEstado["dataProvider"],
                                                    'rowCssClassExpression'=>'$data["color"]',
                                                    'columns'=>$resumenEstado["columnas"],
                                                    'tagName'=>'Resumen Período' )
                                             );


                                ?>
                    <br>

                    <?php

                            $datos_grilla = $datos_grafico_centro_costo["datos_grilla"] ;
                            $this->widget('zii.widgets.grid.CGridView',
                                          array('dataProvider' => $datos_grilla["dataProvider"],
                                                'columns'=>$datos_grilla["columnas"]                          

                                              )
                                         );

                            ?>

            <?php
            $this->endWidget();
            ?>
            </div>
    </div>
    
</div>
    
    
<?php
$this->endWidget();
?>


