<?php

$this->breadcrumbs = array(
	GxHtml::valueEx($model),
        'Datos Usuario'
);

?>



<script type="text/javascript">
    
    $(document).ready(function() {  

        
        //BOTON DE GUARDAR PASO 1
        $( "#editarMail" ).button().click(function() {
            $("#editarMailUsuario").css("display", "block"); 
            $("#editarMail").css("display", "none"); 
        });    
    });            

</script>
    
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'InformacionPersonaForm',
	'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
        'action' => $this->createUrl('site/datosUsuario'),
        'htmlOptions'=>array("class"=>"form-group")
));?>


<div class="row">
    <div class="col-md-6">
        <?php
        $flashMessages = Yii::app()->user->getFlashes();
        if ($flashMessages) {
            foreach($flashMessages as $key => $message) {
                echo BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, BsHtml::bold('Realizado !') . $message);

                //echo '<div class="alert-success flash-' . $key . '">' . $message . "</div>";
            }
        }
        ?>


        <?php
        $this->beginWidget('bootstrap.widgets.BsPanel', array(
            'title' => '<span class="glyphicon glyphicon-user"></span>  Datos Usuario',
            'type' => BsHtml::PANEL_TYPE_PRIMARY
        ));
        ?>

        <table  class="items table table-striped" >
        <tr>
                <td class="td" width="100px">Usuario</td>
                <td class="td1"  width="400px"><?php echo Yii::app()->user->nombreCompleto;?></td>
        </tr>
        <tr>
                <td class="td">Perfil</td>
                <td class="td1"><?php echo Yii::app()->user->perfil;?></td>
        </tr>

        <tr>
                <td class="td">RUT</td>
                <td class="td1"><?php echo Yii::app()->user->rut;?></td>
        </tr>

        <tr>
                <td class="td">Email</td>
                <td class="td1" width="400px">
                    <?php echo Yii::app()->user->mail;?>

                    <div id="editarMailUsuario" style="display: none">
                        <br>
                        <?php echo $form->textField($model,'mail',
                                    array(
                                    'style'=>'width:200px;',
                                    'class'=>'form-control',
                                    'value'=>Yii::app()->user->mail
                                    )); ?>
                        <?php echo $form->error($model,'mail'); ?>

                        <button class="btn btn-sm btn-success" id="grabarMail">Grabar Mail</button>
                    </div>

                    <button class=" btn btn-sm btn-info" id="editarMail" type="button">Editar Mail</button>
                </td>
        </tr>


        </table>
        <?php
        $this->endWidget();
        ?>

    </div>
</div>





<?php $this->endWidget(); ?>