<?php
/**
  * Some text describing what the view does goes here
  * @uses string $dataProvider The data provider for this model
  */
?>

<?php
$this->breadcrumbs=array(
	'Productividades',
	'Globales',
);


$ancho_pagina = Yii::app()->session['ancho_pagina']-100;
?>



<script type="text/javascript">
    

    $(document).ready(function() {   
        
        $('input[type="checkbox"]').change(function () {
            mostrarLoader();
            var name = $(this).val();
            var check = $(this).attr('checked');
            
            if(check){
                //alert("Chequeado");
                $('form#ConsultasForm').submit();
            }else{
                //alert("No Chequeado");   
                $('form#ConsultasForm').submit();
            }
            
        });
        
        
        $("#exportarExcel").click(function(){
            
           var url = "<?php echo  $this->createUrl("grillaProductividades/exportarExcel",array());?>";
           //alert(url)
           location.href = url ;

       });
      
        
        
    });    
    
 
    function radioChange(value){
        var disabled_c1 = false;
        var disabled_c2 = false;
        var disabled_c3 = false;

        if(value!=1){
            disabled_c1 = true;
        }    

        if(value!=2){
            disabled_c2 = true;
        }

        if(value!=3){
            disabled_c3 = true;
        }

        $("#ConsultasForm_anio_id").attr("disabled", disabled_c1);
        $("#ConsultasForm_mes_id").attr("disabled", disabled_c2);
        $("#ConsultasForm_anio_mes_id").attr("disabled", disabled_c2);
        $("#ConsultasForm_mes_desde_id").attr("disabled", disabled_c3);
        $("#ConsultasForm_anio_desde_id").attr("disabled", disabled_c3);
        $("#ConsultasForm_mes_hasta_id").attr("disabled", disabled_c3);
        $("#ConsultasForm_anio_hasta_id").attr("disabled", disabled_c3);
    }
    

    
</script>




<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ConsultasForm'
	
));?>


<div class="panel-group" id="accordion">
    <div class="panel panel-info" id="panel1">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-target="#collapseOne" 
                   href="#collapseOne">
                    <span class="glyphicon glyphicon-filter"></span> Busqueda Productividades
                    
                </a>
            </h4>

        </div>
        <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
            
                <div class="row">
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo $form->radioButton($model,   'tipo_consulta', 
                                                                    array('value'=>1,
                                                                          'checked'=>true,
                                                                          'onchange' => 'radioChange(this.value);')); ?>
                                Por Año
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo $form->dropDownList($model,'anio_id', CHtml::listData($anios,'id','value'),
                                                                array('disabled'=>$disabled_c1 ,"class"=>"form-control","style"=>"min-width:80px;width:80px;"));?>
                            </div>
                        </div>
                    </div>  

                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo $form->radioButton($model,   'tipo_consulta', 
                                                                    array('value'=>2,
                                                                          'uncheckValue'=>null,
                                                                          'onchange' => 'radioChange(this.value);')); ?>
                                Por Mes
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-inline">
                                    <?php echo $form->dropDownList($model,'mes_id', CHtml::listData($meses,'id','label'),array('disabled'=>$disabled_c2  ,"class"=>"form-control","style"=>"min-width:80px;"));?>

                                    <?php echo $form->dropDownList($model,'anio_mes_id', CHtml::listData($anios,'id','value'),array('disabled'=>$disabled_c2  ,"class"=>"form-control","style"=>"min-width:80px;"));?>
                                </div>
                            </div>
                        </div>
                    </div> 


                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo $form->radioButton($model,   'tipo_consulta', 
                                                                    array('value'=>3,
                                                                          'uncheckValue'=>null,
                                                                          'onchange' => 'radioChange(this.value);')); ?>
                                Por Período
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">   
                                    <div class="col-md-2">
                                        <label style="min-width: 80px">Desde:</label> 
                                    </div>
                                    <div class="col-md-10">
                                       <div class="form-inline">                                               
                                        <?php echo $form->dropDownList($model,'mes_desde_id', CHtml::listData($meses,'id','label'),array('disabled'=>$disabled_c3 ,"class"=>"form-control","style"=>"min-width:80px;" ));?>
                                        <?php echo $form->dropDownList($model,'anio_desde_id', CHtml::listData($anios,'id','value'),array('disabled'=>$disabled_c3  ,"class"=>"form-control","style"=>"min-width:80px;"));?>
                                       </div>
                                    </div>



                                </div>

                                <div class="row">
                                    <div class="col-md-2">
                                        <label style="min-width: 80px">Hasta:</label> 
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-inline">
                                            <?php echo $form->dropDownList($model,'mes_hasta_id', CHtml::listData($meses,'id','label'),array('disabled'=>$disabled_c3  ,"class"=>"form-control","style"=>"min-width:80px;"));?>
                                            <?php echo $form->dropDownList($model,'anio_hasta_id', CHtml::listData($anios,'id','value'),array('disabled'=>$disabled_c3  ,"class"=>"form-control","style"=>"min-width:80px;"));?>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <br>

                        <div class="form-inline">
                            <button type="submit" class="btn btn-success" ><span class=" glyphicon glyphicon-search"></span> Buscar</button>
                            <button id="exportarExcel" type="button" class="btn btn-info"><span class="glyphicon glyphicon-export"></span> Exportar Excel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>


<?php $titulo ="Listado Productividades"; ?>

<?php if(isset($periodo_fin)&&!is_null($periodo_fin)){
    $titulo .= " <b>desde ". $periodo->nombrePeriodo ." al ". $periodo_fin->nombrePeriodo . "</b>"; 
}else{
    $titulo .= " <b>Período ". $periodo->nombrePeriodo . "</b>"; 
}
?>



<div class="row">
    
    <div class="col-md-12">
        
        <div class="row">
            <div class="col-md-12">
                <?php echo $form->errorSummary($model); ?>
            </div>
        </div>
        


        <?php if($valido){?>

            <div class="row">
                <div class="col-md-12">
                    <?php 
                    $es_academico=false;
                    if($model->tipo_personas==1){
                        $es_academico=true;        
                    }?>

                    <div id="lista_productividades" style="overflow: auto">
                        <?php $this->widget('ext.productividades.LProductividadesWidget',
                                            array(
                                                'area_id'=>$area_id,
                                                'es_academico'=>$es_academico,
                                                'periodo_id'=>$periodo_id,
                                                'periodo_fin_id'=>$periodo_fin_id,
                                                'es_consulta'=>true,
                                                'width'=>3000,
                                                'model'=>$model,
                                                'detalle_por_tipo_productividad'=>$mostrar_detalle_tipo_productividad,
                                                'mostrar_positivos'=>$mostrar_positivos,
                                                'form'=>$form,
                                                'titulo'=>$titulo

                                                )
                                            ); ?>
                    </div>
                </div>
            </div>
            



        <?php }?>
    </div>
    
</div>




<?php $this->endWidget(); ?>

<br>
<br>
