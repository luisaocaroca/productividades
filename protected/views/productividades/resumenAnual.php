<h3>Productividad <?php echo $persona ?> <b>Año <?php echo $anio_id?></b></h3>


<?php 
    $total=0;
    foreach($periodos as $periodo){
        $total = $total  + $periodo["monto"];
    }
?>

<?php
$this->widget('bootstrap.widgets.BsGridView', array(
	'id' => 'listado-grid',
	'dataProvider' => $dataProvider,
        'type' => BsHtml::GRID_TYPE_CONDENSED 
        . ' ' . BsHtml
        ::GRID_TYPE_BORDERED . ' ' . BsHtml
        ::GRID_TYPE_STRIPED,
	'columns'=>array(    

                    array(
                        'name'=>'periodo',
                        'header'=>'Período',
                        'value'=>'$data["periodo"]->nombrePeriodo',   
                        'footer'=>"<b>Total</b>"
                    ),            
                    array(
                        'name'=>'monto',
                        'header'=>'Monto',
                        'value'=>'"$ " .Yii::app()->format->formatNumber($data["monto"])',          
                        'footer'=>"<b> $ " . Yii::app()->format->formatNumber($total) . "</b>"
                    ),
                    array(
                        'class'=>'bootstrap.widgets.BsButtonColumn',
                        'template'=>'{view}',
                        'buttons'=>array(
                            'view'=>array(
                                'url'=>'Yii::app()->createUrl("productividades/individuales",
                                        array("mes_id"=>$data["periodo"]->mes,
                                              "anio_mes_id"=>$data["periodo"]->anio,
                                              "tipo_consulta"=>2
                                               ))',
                            ),

                        )
                    ),  
                   


            ),

));


