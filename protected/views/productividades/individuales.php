<?php
/**
 * Muestra las productividades individuales de una personas
 * @uses CActiveDataProvider $dataProvider The data provider for this model
 * @uses User $model The user model
 * @uses ConsultasForm $model objeto que representa el formulario
 * @uses array $anios Listado de años 
 * @uses array $meses Listado de meses
 * @uses int $anio_id Año seleccionado en el form
 * @uses bool $disabled_c1 Si el primer combo debe o no estar habilitado
 * @uses bool $disabled_c2 Si el segundo combo debe o no estar habilitado
 * @uses bool $disabled_c3 Si el tercer combo debe o no estar habilitado
 * @uses string $html despliega el resultado de la consulta (datos HTML de la vista)
                                       
*/


$this->breadcrumbs=array(
	'Productividades',
	'Individuales / Buscar',
);

?>

<script>
    
    $(function () {
         
         $(window).resize(function(){
            if ($(window).width() >= 800){  
              $('.panel-collapse').addClass('in');
            }
            if ($(window).width() <= 800){  
              $('.panel-collapse').removeClass('in');
            }
          });
          
        $("#exportarExcel").click(function(){

           var url = "<?php echo  $this->createUrl("productividades/exportarExcel",array());?>";
           //alert(url)
           location.href = url ;

       });


   });   
function radioChange(value){
    var disabled_c1 = false;
    var disabled_c2 = false;
    var disabled_c3 = false;
    
    if(value!=1){
        disabled_c1 = true;
    }    
    
    if(value!=2){
        disabled_c2 = true;
    }
    
    if(value!=3){
        disabled_c3 = true;
    }
    
    $("#ConsultasForm_anio_id").attr("disabled", disabled_c1);
    $("#ConsultasForm_mes_id").attr("disabled", disabled_c2);
    $("#ConsultasForm_anio_mes_id").attr("disabled", disabled_c2);
    $("#ConsultasForm_mes_desde_id").attr("disabled", disabled_c3);
    $("#ConsultasForm_anio_desde_id").attr("disabled", disabled_c3);
    $("#ConsultasForm_mes_hasta_id").attr("disabled", disabled_c3);
    $("#ConsultasForm_anio_hasta_id").attr("disabled", disabled_c3);
}

function limpiar(){
    $("#ConsultasForm_limpiar").val(1);
    $('form#productividad-form-consulta-form').submit();


}


exportarExcel
</script>


    
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'productividad-form-consulta-form',
        'action'=>Yii::app()->createUrl('/productividades/individuales'),
	
));



?>


<div class="panel-group" id="accordion">
    <div class="panel panel-info" id="panel1">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-target="#collapseOne" 
                   href="#collapseOne">
                    <span class="glyphicon glyphicon-filter"></span> Busqueda Productividades
                    
                </a>
            </h4>

        </div>
        <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
            
                <div class="row">
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo $form->radioButton($model,   'tipo_consulta', 
                                                                    array('value'=>1,
                                                                          'checked'=>true,
                                                                          'onchange' => 'radioChange(this.value);')); ?>
                                Por Año
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo $form->dropDownList($model,'anio_id', CHtml::listData($anios,'id','value'),
                                                                array('disabled'=>$disabled_c1 ,"class"=>"form-control","style"=>"min-width:80px;width:80px;"));?>
                            </div>
                        </div>
                    </div>  

                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo $form->radioButton($model,   'tipo_consulta', 
                                                                    array('value'=>2,
                                                                          'uncheckValue'=>null,
                                                                          'onchange' => 'radioChange(this.value);')); ?>
                                Por Mes
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-inline">
                                    <?php echo $form->dropDownList($model,'mes_id', CHtml::listData($meses,'id','label'),array('disabled'=>$disabled_c2  ,"class"=>"form-control","style"=>"min-width:80px;"));?>

                                    <?php echo $form->dropDownList($model,'anio_mes_id', CHtml::listData($anios,'id','value'),array('disabled'=>$disabled_c2  ,"class"=>"form-control","style"=>"min-width:80px;"));?>
                                </div>
                            </div>
                        </div>
                    </div> 


                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo $form->radioButton($model,   'tipo_consulta', 
                                                                    array('value'=>3,
                                                                          'uncheckValue'=>null,
                                                                          'onchange' => 'radioChange(this.value);')); ?>
                                Por Período
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">   
                                    <div class="col-md-2">
                                        <label style="min-width: 80px">Desde:</label> 
                                    </div>
                                    <div class="col-md-10">
                                       <div class="form-inline">                                               
                                        <?php echo $form->dropDownList($model,'mes_desde_id', CHtml::listData($meses,'id','label'),array('disabled'=>$disabled_c3 ,"class"=>"form-control","style"=>"min-width:80px;" ));?>
                                        <?php echo $form->dropDownList($model,'anio_desde_id', CHtml::listData($anios,'id','value'),array('disabled'=>$disabled_c3  ,"class"=>"form-control","style"=>"min-width:80px;"));?>
                                       </div>
                                    </div>



                                </div>

                                <div class="row">
                                    <div class="col-md-2">
                                        <label style="min-width: 80px">Hasta:</label> 
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-inline">
                                            <?php echo $form->dropDownList($model,'mes_hasta_id', CHtml::listData($meses,'id','label'),array('disabled'=>$disabled_c3  ,"class"=>"form-control","style"=>"min-width:80px;"));?>
                                            <?php echo $form->dropDownList($model,'anio_hasta_id', CHtml::listData($anios,'id','value'),array('disabled'=>$disabled_c3  ,"class"=>"form-control","style"=>"min-width:80px;"));?>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <br>

                        <div class="form-inline">
                            <button type="submit" class="btn btn-success" ><span class=" glyphicon glyphicon-search"></span> Buscar</button>
                            <button id="exportarExcel" type="button" class="btn btn-info"><span class="glyphicon glyphicon-export"></span> Exportar Excel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>


<?php echo $form->hiddenField($model,'limpiar',array('type'=>"hidden","value"=>0)); ?>


<?php echo $form->errorSummary($model); ?>


<?php $this->endWidget(); ?>

<div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">
                <span class="glyphicon glyphicon-list"></span> Resultado Productividades
            </h4>

        </div>
        <div class="panel-body">
            <?php echo $html_consulta;?>

        </div>
    
</div>

</div>

