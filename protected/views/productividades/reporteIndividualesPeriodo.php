    
<table style="width: 100% !important; vertical-align: middle !important;margin-bottom:0em;">
    <tr>

        <td>
            <h3>
                Detalle Productividad <?php echo $persona ?>
                <?php if(isset($periodo_fin)){?>
                    desde <b><?php echo $periodo->nombrePeriodo?></b> al <b><?php echo $periodo_fin->nombrePeriodo?></b> 
                <?php }else{?>
                Período <b><?php echo $periodo->nombrePeriodo?> </b>
                <?php }?>
            </h3>

        </td>
    </tr>
</table>



    
<?php 
$total=0;
//foreach($detalle as $prod){
//    if( $prod->estado_id==ProdEstado::APROBADO){
//        $total = $total  + $prod->monto;
//    }
//}
?>
<hr>
    <h4>
        Total Productividades Aprobadas Período: <b>$<?php echo Yii::app()->format->formatNumber($model->total)?></b>
    </h4>


<?php

$this->widget('bootstrap.widgets.BsGridView', array(
    'id'=>'vacr-grid',    
    'dataProvider'=>$detalle,
    'type' => BsHtml::GRID_TYPE_CONDENSED 
    . ' ' . BsHtml
    ::GRID_TYPE_BORDERED . ' ' . BsHtml
    ::GRID_TYPE_STRIPED,
    'summaryText'=>'',
    'ajaxUpdate'=>true, 
    'rowCssClassExpression'=>'$data->color',
    'columns'=>array(    
        array(
            'name'=>'periodo_id',
            'header'=>'Período',
            'value'=>'$data->periodo'
        ),     
        array(
            'name'=>'cc_id',
            'header'=>'Centro Costo',
            'value'=>'$data->cc'
        ),         
        array(
            'name'=>'tipo_prod_id',
            'header'=>'Tipo Productividad',
            'value'=>'$data->tipoProd'
        ),  
        array(
            'name'=>'personaCarga',
            'header'=>'Cargado Por',
            'value'=>'$data->personaCarga'
        ),  
        array(
            'name'=>'estado_id',
            'header'=>'Estado',
            'value'=>'$data->estado'
        ),
        array(
            'name'=>'numero_cuotas',
            'header'=>'Cuotas',
            'value'=>'$data->cuota_actual ." de ". $data->numero_cuotas'
        ),
        array(
            'name'=>'monto',
            'header'=>'Monto',
            'value'=>'"$" . Yii::app()->format->formatNumber($data->monto)'
        ),
        'observaciones',
    ),
));
    

