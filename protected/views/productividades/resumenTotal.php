<h3>Productividad <?php echo $persona ?></b></h3>


<?php 
    $total=0;
    foreach($periodos as $periodo){
        $total = $total  + $periodo["monto"];
    }
?>


<?php
$this->widget('bootstrap.widgets.BsGridView', array(
	'id' => 'listado-grid',
	'dataProvider' => $dataProvider,
         'type' => BsHtml::GRID_TYPE_CONDENSED 
        . ' ' . BsHtml
        ::GRID_TYPE_BORDERED . ' ' . BsHtml
        ::GRID_TYPE_STRIPED,
	'columns'=>array(    

                    array(
                        'name'=>'anio',
                        'header'=>'Anio',
                        'value'=>'$data["anio"]', 
                        'footer'=>"<b>Total</b>"
                    ),            
                    array(
                        'name'=>'monto',
                        'header'=>'Monto',
                        'value'=>'"$ " . Yii::app()->format->formatNumber($data["monto"])',
                        'footer'=>"<b> $ " . Yii::app()->format->formatNumber($total) . "</b>"
                    ),
                    array(
                        'class'=>'bootstrap.widgets.BsButtonColumn',
                        'template'=>'{view}',
                        'buttons'=>array(
                            'view'=>array(
                                'url'=>'Yii::app()->createUrl("productividades/individuales",
                                     array("anio_id"=>$data["anio"],
                                           "tipo_consulta"=>1
                                            ))',
                            ),

                        )
                    ),  
                   


            ),

));


