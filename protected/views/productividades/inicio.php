<?php
/* @var $this ProductividadesController */

$this->breadcrumbs=array(
	'Productividades',
);
?>
<h1>Menú Productividades</h1>

<p>
<ul>
    <li>
        <?php 
            echo CHtml::link(
                        "Productividades Individuales",
                        Yii::app()->createUrl("productividades/individuales")
                );

        ?>
    </li> 
    <li>
        <?php 
            echo CHtml::link(
                        "Listado Productividades",
                        Yii::app()->createUrl("productividades/globales")
                );

        ?>
    </li> 
</ul>
</p>
