<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<script>
      
    
    function exportar(){
        $("#exportarForm").submit();
    }
</script>
<?php

$ancho_pagina = Yii::app()->session['ancho_pagina']-200;

?>

<form id="exportarForm" name="exportarForm" action="<?php echo $this->createUrl('productividades/exportarExcelDetallePersona')?>">
    <input type="hidden" id="persona_id" name="persona_id" value="<?php echo $persona->persona_id?>"/>
    <input type="hidden" id="periodo_id" name="periodo_id" value="<?php echo $periodo->periodo_id?>"/>
    <input type="hidden" id="periodo_fin_id" name="periodo_fin_id" value="<?php echo $periodo_fin->periodo_id?>"/>
</form>

<div class="modal-dialog modal-info">
    <div class="modal-content">

        <div class="modal-header" style="background-color:#DEDEDE  ">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">
                <span class= "glyphicon glyphicon-time"></span>
                Historial Productividad <?php echo $persona ?></h4>
        
        </div>
        
          
        <div class="modal-body">


            <div class="row">
                <div class="col-md-10">
                    <h3><?php echo $persona ?>
                        <?php if(!is_null($periodo_fin)){
                            $periodo_fin_id = $periodo_fin->periodo_id;
                            ?>
                            desde <b><?php echo $periodo->nombrePeriodo?></b> al <b><?php echo $periodo_fin->nombrePeriodo?></b> 
                        <?php }else{
                            $periodo_fin_id=null;
                            ?>
                            Período <b><?php echo $periodo->nombrePeriodo?> </b>
                        <?php }?>
                    </h3>
                </div>

                <div class="col-md-2 text-right">
                    <?php
                        $imghtml=CHtml::image(Yii::app()->baseUrl."/images/Excel-64.png","Exportar",array("title"=>"Exportar Excel"));

                        echo CHtml::link(
                                                $imghtml,
                                                "",
                                                array('onclick' => " exportar()",
                                                      'style'=>'cursor: pointer; text-decoration: underline;',
                                                      )
                                        );


                        ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <hr>
                    <h4>
                        Total Productividades Período: <b>$<?php echo Yii::app()->format->formatNumber($model->total)?></b>
                    </h4>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">


                    

                <div class="row">
                    <div class="col-md-12">
                    <?php

                    $this->widget('bootstrap.widgets.BsGridView', array(
                        'id'=>'detalle-prod',    
                        'dataProvider'=>$detalle,
                        'ajaxUpdate'=>true, 
                        'type' => BsHtml::GRID_TYPE_CONDENSED 
                        . ' ' . BsHtml
                        ::GRID_TYPE_BORDERED . ' ' . BsHtml
                        ::GRID_TYPE_STRIPED,
                        'rowCssClassExpression'=>'$data->color',
                        'columns'=>array(    
                            array(
                                'name'=>'periodo_id',
                                'header'=>'Período',
                                'value'=>'$data->periodo'
                            ),     

                            array(
                                'name'=>'cc_id',
                                'header'=>'Centro Costo',
                                'value'=>'$data->cc'
                            ), 
                            array(
                                'name'=>'tipo_prod_id',
                                'header'=>'Tipo Productividad',
                                'value'=>'$data->tipoProd'
                            ),             
                            array(
                                'name'=>'estado_id',
                                'header'=>'Estado',
                                'value'=>'$data->estado'
                            ),
                            array(
                                'name'=>'numero_cuotas',
                                'header'=>'Cuotas',
                                'value'=>'$data->cuota_actual ." de ". $data->numero_cuotas'
                            ),
                            array(
                                'name'=>'monto',
                                'header'=>'Monto',
                                'value'=>'"$" . Yii::app()->format->formatNumber($data->monto)'
                            ),

                            array(
                                'name'=>'total_monto_asignado',
                                'header'=>'Total Pago',
                                'value'=>'"$" . Yii::app()->format->formatNumber($data->total_monto_asignado)'
                            ),
                            'observaciones',
                        ),
                    ));

                ?>
                    </div>
                </div>
                        <?php 
                        //IMPRIME RESULTADO
                        echo $resultado_html;?>


                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
        </div>
        
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->    
            