<?php

$ancho_pagina = Yii::app()->session['ancho_pagina']-200;

?>



<div class="modal-dialog modal-info">
    <div class="modal-content">

        <div class="modal-header" style="background-color:#DEDEDE  ">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">
                <span class= "glyphicon glyphicon-list"></span>
                Detalle Coordinaciones <?php echo $persona ?></h4>
        
        </div>
        
          
        <div class="modal-body">
            <div class="row" >
                <div class="col-md-10">
                    
                </div>
                
            </div>
            
            <div class="row" >
                <div class="col-md-12">

                    <div class="form" style="overflow: auto">


                        <table class="table table-bordered">
                            <tr>
                                <td class="tsubheader"></td>            
                                <td class="tsubheader">Tipo Productividad</td>
                                <td class="tsubheader">Centro Costo</td>
                                <td class="tsubheader">Monto</td>
                                <td class="tsubheader">Detalle Coordinación</td>
                            </tr>

                        <?php 
                            $total=0;
                            foreach($detalle as $coordinacion){
                                $total = $total  + $coordinacion->monto;
                                $class="tregistro";

                                ?>
                            <tr>

                                <td class="<?php echo $class?>" align="center" style="width:90px">
                                    <div class="col-md-4">
                                    
                                        <h3>
                                            <?php 

                                                echo CHtml::link(
                                                                        '<span class="glyphicon glyphicon-edit "></span>',
                                                                        Yii::app()->createUrl("coordinaciones/editarCoordinacion",
                                                                                                         array("coordinacion_id"=>$coordinacion->coordinacion_id,
                                                                                                               "persona_id"=>$persona->persona_id))
                                                                );


                                                ?>
                                        </h3>
                                    </div>
                                    <div class="col-md-4">
                                    
                                        <h3>
                                            <?php 
                                                echo CHtml::link(
                                                                        '<span class="glyphicon glyphicon-remove " style="color: red"></span>',
                                                                        "",
                                                                        array( 'style'=>'cursor: pointer; text-decoration: underline;',
                                                                                'onclick'=>'{eliminarCoordinacion("'.

                                                                                        Yii::app()->createUrl("coordinaciones/borrarCoordinacion",
                                                                                                               array("coordinacion_id"=>$coordinacion->coordinacion_id,
                                                                                                                     "persona_id"=>$persona->persona_id))

                                                                                        .'"); 
                                                                                        }'
                                                                        )
                                                                );


                                            ?>
                                        </h3>    
                                    </div>
                                </td>
                                <td class="<?php echo $class?>"><?php echo $coordinacion->tipoProd->nombre_tipo_prod?></td>
                                <td class="<?php echo $class?>"><?php echo $coordinacion->cc?></td>
                                <td class="<?php echo $class?>"><?php echo Yii::app()->format->formatNumber($coordinacion->monto)?></td>
                                <td class="<?php echo $class?>"><?php echo $coordinacion->descripcion?></td>
                            </tr>

                        <?php }?>
                            <tr>
                                <td colspan="1"></td>
                                <td class="tregistrototalcol">
                                    <b>Total</b>
                                </td>
                                <td class="tregistrototalcol">
                                    <b><?php echo Yii::app()->format->formatNumber($total)?></b>
                                </td>
                            </tr>
                        </table>
                    </div>


                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
        </div>
        
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->    
            