<table class="tproductividades">    
    <tr>
        <td class="tsubheader"></td>
        <td class="tsubheader" width="200px">Persona</td>
            <?php foreach($tipos_productividades as $tipo_productividad){?>
                <td class="tsubheader"><?php echo $tipo_productividad;?></td>
            <?php }?>    
        <td class="tsubheader">Total</td>
    </tr>
    <?php foreach($salida_personas as $salida){
        $class="tregistro";
        $class_persona = "tregistropersona";
        ?>
        <tr>
            <td class="<?php echo $class?>">
                <?php 
                $imghtml=CHtml::image(Yii::app()->baseUrl."/images/Search_32.png","Ver Detalle",array("width"=>20));
                echo CHtml::ajaxLink($imghtml,Yii::app()->createUrl('coordinaciones/detalleCoordinaciones'),
                                        array('type'=>'POST',
                                              'data'=>array('persona_id'=>$salida["persona"]->persona_id,'ajax'=>'ajax'),
                                              'update'=>'#preview',
                                              'complete'=>'afterAjax'
                                        )
                                    ,array('id' =>'coordinacion_' . $salida["persona"]->persona_id)
                                    );

                ?>

            </td>

            <td class="<?php echo $class_persona?>">
                <?php echo $salida["persona"]?>
            </td>
            <?php 
                $total=0;
                foreach($salida["listado_coorinaciones"] as $coordinacion){
                    $total=$total+$coordinacion["monto"];
                    ?>
                <td class="<?php echo $class?>">
                    <?php echo Yii::app()->format->formatNumber($coordinacion["monto"]); ?>
                </td>
            <?php }?>
            <td class="tregistrototal">
                <?php echo Yii::app()->format->formatNumber($total);?>
            </td>

        </tr>
    <?php }?>

</table>