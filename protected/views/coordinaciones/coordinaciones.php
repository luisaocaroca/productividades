<?php
/* @var $this IngresoProductividadesController */

$this->breadcrumbs=array(
	'Ingreso Productividades',
	'Coordinaciones',
);

$ancho_pagina = Yii::app()->session['ancho_pagina']-140;
?>

<script>

    $(document).ready(function(){
        <?php $disabled=false;

        if($traspasada_productividad){
            $disabled=true;
        }
        
        if($disabled){
            ?>
            $('#modificar_productividades').attr("disabled", true);
            <?php         
        }?>
    });


    function afterAjax()
    {
        $.fancybox({
                href : '#preview',
                scrolling : 'no',
                transitionIn : 'fade',
                transitionOut : 'fade', 
                //check the fancybox api for additonal config to add here   
                onClosed: function() { 
                    $('#preview').html(''); 
                } //empty the preview div
        });
    }

    var _updatePaymentComment_url;
    function eliminarCoordinacion(_url)
    {
        if(window.confirm("¿Esta seguro(a) que desea borrar la coordinacion?")){
            
            // If its a string then set the global variable, if its an object then don't set
            if (typeof(_url)=='string')
                    _updatePaymentComment_url=_url;

            jQuery.ajax({
                    url: _updatePaymentComment_url,
                    'data':$(this).serialize(),
                    'type':'POST',
                    'success':function(data)
                            {
                                //alert("entro")
                                //alert(data)
                                $('#preview').html(data);
                                
                                <?php
                                    echo CHtml::ajax(array(
                                    'id'=>'actualiza_listado',
                                    'type'=>'POST',
                                    'url'=>Yii::app()->createUrl('coordinaciones/listadoCoordinaciones'),
                                    'update'=>'#lista_coordinaciones',
                            //      'data'=>'js:jQuery(this).serialize()',  // only send element name, not whole form
                                    ));
                                ?>
                                                    


                            } ,
                    'cache':false});
                
                
            return false;
        }

    }
    
    var _updatePaymentComment_url;
    function eliminarCoordinacion(_url)
    {
            
            // If its a string then set the global variable, if its an object then don't set
            if (typeof(_url)=='string')
                    _updatePaymentComment_url=_url;

            jQuery.ajax({
                    url: _updatePaymentComment_url,
                    'data':$(this).serialize(),
                    'type':'POST',
                    'success':function(data)
                            {
                                //alert("entro")
                                //alert(data)
                                $('#preview').html(data);
                                
                                <?php
                                    echo CHtml::ajax(array(
                                    'id'=>'actualiza_listado',
                                    'type'=>'POST',
                                    'url'=>Yii::app()->createUrl('coordinaciones/listadoCoordinaciones'),
                                    'update'=>'#lista_coordinaciones',
                            //      'data'=>'js:jQuery(this).serialize()',  // only send element name, not whole form
                                    ));
                                ?>
                                                    


                            } ,
                    'cache':false});
                
                
            return false;

    }
    
</script>

<div class="row">
    <div class="col-md-12">
        <?php
            $flashMessages = Yii::app()->user->getFlashes();
            if ($flashMessages) {
                foreach($flashMessages as $key => $message) {
                    if($key=="error"){
                        echo BsHtml::alert(BsHtml::ALERT_COLOR_ERROR, BsHtml::bold('Atención!') . ' ' .  $message);
                    }

                    if($key=="success"){
                        echo BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, BsHtml::bold('Atención!') . ' ' .  $message);
                    }


                }
            }?>
    </div>
</div>


<div class="row">
    <div class="col-md-12">


        <?php
            $this->beginWidget('bootstrap.widgets.BsPanel', array(
            'title' => '<span class="glyphicon glyphicon-new-window"></span> <b>Ingresar/Actualizar Coordinacion</b>',
            'type' => BsHtml::PANEL_TYPE_PRIMARY,
        ));
            ?>


        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'coordinaciones-form-ingreso-form',
                'enableAjaxValidation'=>false,
                'enableClientValidation'=>false,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
                'action' => $this->createUrl('coordinaciones/coordinaciones')
        ));?>

            
        <div class="row">
            <div class="col-md-12">

                <div class="row">                    
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group <?php if($form->error($model,'persona_id')){ ?> has-error has-feedback <?php }?>">
                                    <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'persona_id'); ?></label>

                                    <?php 
                                    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                          'attribute'=>'nombre_persona',
                                          'model'=>$model,
                                          'sourceUrl'=>array('coordinaciones/academicos'),
                                          'name'=>'nombre_persona',
                                          'id'=>'CoordinacionesForm_nombre_persona', 
                                          'options'=>array(
                                            'minLength'=>'2',
                                            'showAnim'=>'fold',
                                            'select'=>"js: function(event, ui) {
                                                $('#CoordinacionesForm_persona_id').val(ui.item['persona_id']);
                                            }",
                                            'change'=>"js: function(event, ui) {
                                                            if (!ui.item) {
                                                               $('#CoordinacionesForm_persona_id').val('');
                                                               $('#CoordinacionesForm_nombre_persona').val('');
                                                            }
                                                          }",
                                          ),
                                          'htmlOptions'=>array(
                                            'size'=>40,
                                            'maxlength'=>45,
                                            'class'=>'form-control'
                                          ),

                                    )); ?> 
                                    
                                    <?php if($form->error($model,'persona_id')){ ?>
                            
                                        <span class="red"><?php echo $form->error($model,'persona_id'); ?></span>
                                        <br>
                                    <?php }?>
                                </div>
                                
                                <?php echo $form->hiddenField($model,'persona_id',array('type'=>"hidden")); ?>
                            </div>
                            <div class="col-md-1">
                                <br>
                                <?php
                                //create a link
                                $imghtml=CHtml::image(Yii::app()->baseUrl.'/images/Add-male-user-32.png');
                                
                                /*
                                echo CHtml::ajaxLink($imghtml,Yii::app()->createUrl('prodPersona/crearPersona'),
                                array('type'=>'POST', 'update'=>'#preview', 'complete'=>'afterAjax'));

                                //put fancybox on page
                                $this->widget('application.extensions.fancybox.EFancyBox', 
                                                 array(
                                                    'target' => '#various2',
                                                    'config' => array(
                                                        'scrolling' => 'yes',
                                                        'titleShow' => true,
                                                        'width'=>300
                                                    )
                                                ));*/
                                
                                
                                echo BsHtml::ajaxLink($imghtml, Yii::app()->createAbsoluteUrl('prodPersona/crearPersona'), array(
                                        'cache' => true,
                                        'type' => 'POST',
                                        'success' => 'js:function(data){
                                                    
                                                    $(".modal-body").html(data);
                                                    $("#crear_persona_modal").modal("show");
                                                }'
                                    ), array(
                                        //'icon' => BsHtml::GLYPHICON_BELL
                                    ));
                                
                                ?>  
                            </div>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group <?php if($form->error($model,'monto')){ ?> has-error has-feedback <?php }?>">
                            <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'monto'); ?></label>
                            <?php echo $form->textField($model,'monto',array('class'=>'form-control')); ?>
                            
                            <?php if($form->error($model,'monto')){ ?>
                            
                            <span class="red"><?php echo $form->error($model,'monto'); ?></span>
                            <br>
                        <?php }?>
                        </div>
                    </div>
                </div>
                    
                <div class="row">                      
                     <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group <?php if($form->error($model,'tipo_prod_id')){ ?> has-error has-feedback <?php }?>">
                                    
                                    <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'tipo_prod_id'); ?></label>
                                

                                    <?php echo $form->dropDownList($model,'tipo_prod_id', CHtml::listData($tipos_productividades,'tipo_prod_id','nombre_tipo_prod'),
                                                    array(
                                                        'empty' => 'Seleccionar Tipo..',
                                                        'class'=>'form-control',
                                                        'ajax' => array(
                                                                'type'=>'POST', //request type
                                                                'url'=>CController::createUrl('coordinaciones/centroCostos'), //url to call.
                                                                //Style: CController::createUrl('currentController/methodToCall')
                                                                'update'=>'#CoordinacionesForm_cc_id', //selector to update
                                                                //'data'=>'$(this).serialize();',
                                                                //leave out the data key to pass all form values through
                                                                'beforeSend' => 'function(){                                                        
                                                                    $("#cargando_cc").addClass("ui-autocomplete-loading");
                                                                }',
                                                                'complete' => 'function(){       
                                                                    $("#cargando_cc").attr("class","");
                                                                }',
                                                            )
                                                    ));?>
                                    
                                    <?php if($form->error($model,'tipo_prod_id')){ ?>
                            
                                        <span class="red"><?php echo $form->error($model,'tipo_prod_id'); ?></span>
                                        <br>
                                    <?php }?>

                                </div>
                            </div>
                            <div class="col-md-1 text-right">
                                <div id="cargando_tipos_prod">
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group <?php if($form->error($model,'cc_id')){ ?> has-error has-feedback <?php }?>">
                                    <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'cc_id'); ?></label>

                                    <?php echo $form->dropDownList($model,'cc_id', CHtml::listData($centros_costos,'cc_id','nombreCompleto'),
                                        array(
                                            'class'=>'form-control',
                                        )
                                        );?>
                                    
                                    <?php if($form->error($model,'cc_id')){ ?>
                            
                                        <span class="red"><?php echo $form->error($model,'cc_id'); ?></span>
                                        <br>
                                    <?php }?>
                                </div>
                            </div>
                            <div class="col-md-1 text-right" >
                                <div id="cargando_cc">
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                          
                   
                    
                    
                    
                </div>
                <div class="row">                    
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-10">
                            <?php echo $form->labelEx($model,'descripcion'); ?>
                                <?php echo $form->textArea($model,"descripcion",array('class'=>'form-control')); ?>      
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-md-11">
                                <b><?php echo $form->error($model,"descripcion"); ?></b>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <br>
                <div class="row">                    
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-saved"></span> Crear/Actualizar</button>
                    </div>
                    
                </div>

              

            </div>
        </div>

        <?php $this->endWidget(); ?>
    

    </div>
</div>

<?php
$this->endWidget();
?>

<?php
$form_modificacion = $this->beginWidget('CActiveForm', array(
    'id' => 'productividad-form-consulta-form',
    'action' => $this->createUrl('coordinaciones/aplicarPeriodoAbiertos')
        ));
?>
    
<div class="row">
    <div class="col-md-12">


        <?php 

        echo CHtml::submitButton('Aplicar Modificaciones Productividades',array('id'=>'modificar_productividades' ,'class'=>'btn btn-warning')); ?>

    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">

        <div id="lista_coordinaciones" style="overflow:auto">    

            <table class=" table table-bordered">    
                <thead>
                <tr>
                    <th colspan="2" width="1%"></th>
                    <th>Centro Costo</th>
                    <?php foreach($centros_costos_coordinaciones as $centro_costo){?>
                        <th class="tsubheader" colspan="<?php echo count($centro_costo->prodTipoProductividads);?>" width="160px">
                            <?php echo $centro_costo;?>                
                        </th>
                    <?php }?>

                </tr>                
                <tr>
                    <td class="tsubheader" width="1%"></td>
                    <td class="tsubheader" width="300px">Persona</td>
                    <td class="tsubheader"  width="80px">Total</td>            
                        <?php foreach($centros_costos_coordinaciones as $centro_costo){?>
                            <?php foreach($centro_costo->prodTipoProductividads as $tipo_productividad){?>
                                <td class="tsubheader"><?php echo $tipo_productividad;?></td>
                            <?php }?>    
                        <?php }?>    

                </tr>
                </thead>
                
                <tbody>
                <?php foreach($salida_personas as $salida){
                    $class="tregistro";
                    $class_persona = "tregistropersona";
                    ?>
                    <tr>
                        <td class="<?php echo $class_persona?>">
                            <?php 
                            
                            echo  CHtml::ajaxLink("<span class='glyphicon glyphicon-search'></span>", Yii::app()->createAbsoluteUrl('coordinaciones/detalleCoordinaciones'), 
                                    array('type'=>'POST',
                                          'cache' => true,
                                            
                                          'data'=>array('persona_id'=>$salida["persona"]->persona_id,'ajax'=>'ajax'),
                                        
                                          'success' => 'js:function(data){                                                    
                                                    $("#modalDetalle2").html(data).modal();
                                                    $("#loading-indicator").hide();
                                                }'
                                      ),
                                  
                                    array(
                                        'id' =>'coordinacion_' . $salida["persona"]->persona_id
                                        //'icon' => BsHtml::GLYPHICON_BELL
                                    ));
                             /*
                            //$imghtml=CHtml::image(Yii::app()->baseUrl."/images/Search_32.png","Ver Detalle",array("width"=>20));
                            echo CHtml::ajaxLink("<span class='glyphicon glyphicon-search'></span>",Yii::app()->createUrl('coordinaciones/detalleCoordinaciones'),
                                                    array('type'=>'POST',
                                                          'data'=>array('persona_id'=>$salida["persona"]->persona_id,'ajax'=>'ajax'),
                                                          //'update'=>'#preview',
                                                          'complete'=>'afterAjax'
                                                    ),
                                                    array('id' =>'coordinacion_' . $salida["persona"]->persona_id)
                                                );
                                            */
                            ?>

                        </td>

                        <td class="<?php echo $class_persona?>" width="200px">
                            <?php echo $salida["persona"]?>
                        </td>
                        <?php 
                            $total=0;
                            foreach($salida["listado_coorinaciones"] as $coordinacion){
                                $total=$total+$coordinacion["monto"];
                            }
                        ?>
                        <td class="tregistrototal">
                            $ <?php echo Yii::app()->format->formatNumber($total);?>
                        </td>                
                        <?php 
                            foreach($salida["listado_coorinaciones"] as $coordinacion){  ?>
                            <td class="<?php echo $class?>" width="200px">
                                $ <?php echo Yii::app()->format->formatNumber($coordinacion["monto"]); ?>
                            </td>
                        <?php }?>


                    </tr>
                <?php }?>
                </tbody>    
                
                <tfoot>
                <?php 
                    $total_totales=0;
                    foreach($totales_coordinacion as $total){
                        $total_totales = $total_totales + $total;

                    }
                ?>
                    
                <tr class="tregistrototalFila">
                    <td class="tregistrototalFila"><br></td>
                    <td class="tregistrototalFila"><b>Totales</b></b>
                    <td class="tregistrototalFila">
                        <b><?php echo "$ ". Yii::app()->format->formatNumber($total_totales);?></b>
                    </td>
                    <?php 
                        $total_totales=0;
                        foreach($totales_coordinacion as $total){?>
                        <td class="tregistrototalFila">
                            <b>
                                <?php 
                                $total_totales = $total_totales + $total;
                                echo "$ ".  Yii::app()->format->formatNumber($total);?>
                            </b>
                        </td>
                    <?php }?>

                </tr>

                </tfoot>
            </table>
        </div>
    
    </div>
</div>
<br>
<?php $this->endWidget(); ?>    
    
    
<div style="display:none; width: 600px">
    <div id="preview">
    <!-- space here will be updated by ajax -->
    </div>
</div>    

