<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('persona_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->persona_id), array('view', 'id' => $data->persona_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nombres')); ?>:
	<?php echo GxHtml::encode($data->nombres); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('apellido_paterno')); ?>:
	<?php echo GxHtml::encode($data->apellido_paterno); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('apellido_materno')); ?>:
	<?php echo GxHtml::encode($data->apellido_materno); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('rut')); ?>:
	<?php echo GxHtml::encode($data->rut); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dv')); ?>:
	<?php echo GxHtml::encode($data->dv); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('email')); ?>:
	<?php echo GxHtml::encode($data->email); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('usuario_intranet_id')); ?>:
	<?php echo GxHtml::encode($data->usuario_intranet_id); ?>        
        <br />
	<?php echo GxHtml::encode($data->getAttributeLabel('area_id')); ?>:
	<?php echo GxHtml::encode($data->area); ?>        
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('telefono')); ?>:
	<?php echo GxHtml::encode($data->telefono); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('rol_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->rol)); ?>
	<br />
	*/ ?>

</div>