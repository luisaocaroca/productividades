<?php
/* @var $this ProdPersonaController */
/* @var $model ProdPersona */
/* @var $form CActiveForm */

?>


<div class="form-group" >

    
    <br>
    
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'prod-admin-persona-crearPersona-form',
        'enableAjaxValidation'=>false,
    )); ?>
    <?php //echo $form->errorSummary($model); ?>
    
    <div class="row"> 
        <div class="col-md-4"> 
            <div class="form-group <?php if($form->error($model,'rut')){ ?> has-error has-feedback <?php }?>">
                <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'rut'); ?></label>
            </div>
            
        </div>
        
        <div class="col-md-3">
            <div class="form-group <?php if($form->error($model,'rut')){ ?> has-error has-feedback <?php }?>">
            <?php echo $form->textField($model,'rut',
                        array(  'size'=>10, 
                                'maxlength'=>8,
                                'style'=>'width:140px;',
                                'class'=>'form-control'
                            )); ?>
            </div>
            
            
        </div>
        <div class="col-md-1">
            <div class="form-group <?php if($form->error($model,'dv')){ ?> has-error has-feedback <?php }?>">
                <?php echo $form->textField($model, 'dv',
                            array('size'=>1, 
                                  'class'=>'form-control',
                                  'style'=>'width:20px;',
                                  'maxlength'=>1)); ?>
            </div>
            
            
        </div>
        <div class="col-md-5">

            
            
        </div>
    </div>
    
    <?php if($form->error($model,'rut') || $form->error($model,'dv') ){ ?>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-8">
            <div class="form-group <?php if($form->error($model,'rut')){ ?> has-error has-feedback <?php }?>">
                <label class="control-label" for="inputError2"><?php echo $form->error($model,'rut'); ?></label>
            </div>
            
            <div class="form-group <?php if($form->error($model,'dv')){ ?> has-error has-feedback <?php }?>">
                <label class="control-label" for="inputError2"><?php echo $form->error($model,'dv'); ?></label>
            </div>
        </div>
    </div>    
    <?php }?>    
    
    <div class="row">
        <div class="col-md-4">
            <div class="form-group <?php if($form->error($model,'nombres')){ ?> has-error has-feedback <?php }?>">
                <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'nombres'); ?></label>
            </div>
        </div>
        
        <div class="col-md-8">
            <div class="form-group <?php if($form->error($model,'nombres')){ ?> has-error has-feedback <?php }?>">
                <?php echo $form->textField($model,'nombres',array('class'=>'form-control')); ?>
            </div>
        </div>
    </div>
    
    <?php if($form->error($model,'nombres')){ ?>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="form-group <?php if($form->error($model,'nombres')){ ?> has-error has-feedback <?php }?>">
                    <label class="control-label" for="inputError2"><?php echo $form->error($model,'nombres'); ?></label>
                </div>
            </div>
        </div>    
    <?php }?>
    
    <div class="row">
        <div class="col-md-4">
            <div class="form-group <?php if($form->error($model,'apellido_paterno')){ ?> has-error has-feedback <?php }?>">
                <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'apellido_paterno'); ?></label>
            </div>
        </div>
        
        <div class="col-md-8">
            <div class="form-group <?php if($form->error($model,'apellido_paterno')){ ?> has-error has-feedback <?php }?>">
                <?php echo $form->textField($model,'apellido_paterno',array('class'=>'form-control')); ?>
            </div>
        </div>
        
    </div>
    
    <?php if($form->error($model,'apellido_paterno')){ ?>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="form-group <?php if($form->error($model,'apellido_paterno')){ ?> has-error has-feedback <?php }?>">
                    <label class="control-label" for="inputError2"><?php echo $form->error($model,'apellido_paterno'); ?></label>
                </div>
            </div>
        </div>
    <?php }?>
    
    <div class="row">
        <div class="col-md-4">
            <div class="form-group <?php if($form->error($model,'apellido_materno')){ ?> has-error has-feedback <?php }?>">
                <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'apellido_materno'); ?></label>
            </div>
        </div>
        
        <div class="col-md-8">
           <?php echo $form->textField($model,'apellido_materno',array('class'=>'form-control')); ?>
        </div>
        
    </div>
    
    
    <?php if($form->error($model,'apellido_materno')){ ?>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-8">
            <div class="form-group <?php if($form->error($model,'apellido_materno')){ ?> has-error has-feedback <?php }?>">
                <label class="control-label" for="inputError2"><?php echo $form->error($model,'apellido_materno'); ?></label>
            </div>
        </div>
    </div>
    <?php }?>
    
    <div class="row">
        <div class="col-md-4">
            <div class="form-group <?php if($form->error($model,'email')){ ?> has-error has-feedback <?php }?>">
                <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'email'); ?></label>
            </div>
        </div>
        
        <div class="col-md-4">
            <div class="form-group <?php if($form->error($model,'email')){ ?> has-error has-feedback <?php }?>">
                <?php echo $form->textField($model,'email',array('class'=>'form-control')); ?>
            </div>
        </div>
    </div>
    
    <?php if($form->error($model,'email')){ ?>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="form-group <?php if($form->error($model,'email')){ ?> has-error has-feedback <?php }?>">
                    <label class="control-label" for="inputError2"><?php echo $form->error($model,'email'); ?></label>
                </div>
            </div>
        </div>    
    <?php }?>
    
    <div class="row">
        <div class="col-md-4">
            <div class="form-group <?php if($form->error($model,'telefono')){ ?> has-error has-feedback <?php }?>">
                <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'telefono'); ?></label>
            </div>
        </div>
        
        <div class="col-md-4">
            <div class="form-group <?php if($form->error($model,'telefono')){ ?> has-error has-feedback <?php }?>">
                <?php echo $form->textField($model,'telefono',array('class'=>'form-control')); ?>
            </div>
        </div>
        
    </div>
    
    
    <?php if($form->error($model,'telefono')){ ?>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-6">
                <div class="form-group <?php if($form->error($model,'telefono')){ ?> has-error has-feedback <?php }?>">
                    <label class="control-label" for="inputError2"><?php echo $form->error($model,'telefono'); ?></label>
                </div>
            </div>
        </div>        
    <?php }?>
    
    <div class="row">
        <div class="col-md-4">
            <div class="form-group <?php if($form->error($model,'bo_academico')){ ?> has-error has-feedback <?php }?>">
                <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'bo_academico'); ?></label>
            </div>
        </div>
        
        <div class="col-md-8">
            <div class="form-group <?php if($form->error($model,'bo_academico')){ ?> has-error has-feedback <?php }?>">
                <?php echo $form->radioButtonList($model, 'bo_academico',array(1=>'Académico', 0=>'No Académico'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>
            </div>
        </div>
        
    </div>
    
    <?php if($form->error($model,'bo_academico')){ ?>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="form-group <?php if($form->error($model,'bo_academico')){ ?> has-error has-feedback <?php }?>">
                    <label class="control-label" for="inputError2">
                        <?php echo $form->error($model,'bo_academico'); ?>
                    </label>
                </div>
            </div>
        </div>        
    <?php }?>
    
    
    <div class="row">
        <div class="col-md-12">
            <?php
                    echo CHtml::ajaxSubmitButton(
                            'Crear', $this->createUrl("prodPersona/crearPersona"),array(),array("class"=>"btn btn-success")
                    );
                     ?>
            
        </div>
    </div>
    

<?php $this->endWidget(); ?>

    
</div><!-- form -->