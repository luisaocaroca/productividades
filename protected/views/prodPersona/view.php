<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->persona_id)),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'persona_id',
'nombres',
'apellido_paterno',
'apellido_materno',
'rut',
'dv',
'email',
'telefono',
array(
        'name' => 'rol',
        'type' => 'raw',
        'value' => $model->rol !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->rol)), array('prodRol/view', 'id' => GxActiveRecord::extractPkValue($model->rol, true))) : null,
        ),
array(
        'name' => 'area',
        'type' => 'raw',
        'value' => $model->area !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->area)), array('prodArea/view', 'id' => GxActiveRecord::extractPkValue($model->area, true))) : null,
        ),            
'username',            
	),
    
)); ?>

