<?php
/* @var $this ProdPersona2Controller */
/* @var $model ProdPersona */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>


    <div class="row">
        <div class="col-md-6">
            <?php echo $form->textFieldControlGroup($model,'persona_id'); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?php echo $form->textFieldControlGroup($model,'nombres',array('maxlength'=>150)); ?>
        </div>
        <div class="col-md-4">
            <?php echo $form->textFieldControlGroup($model,'apellido_paterno',array('maxlength'=>150)); ?>
        </div>
        <div class="col-md-4">
            <?php echo $form->textFieldControlGroup($model,'apellido_materno',array('maxlength'=>150)); ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-3">
            <?php echo $form->textFieldControlGroup($model,'rut'); ?>
        </div>
        <div class="col-md-1">
            <?php echo $form->textFieldControlGroup($model,'dv',array('maxlength'=>1)); ?>
        </div>
    </div>
            
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->textFieldControlGroup($model,'email',array('maxlength'=>200)); ?>
        </div>
        <div class="col-md-6">
            <?php echo $form->textFieldControlGroup($model,'telefono',array('maxlength'=>50)); ?>
        </div>
    </div>
    
    
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->labelEx($model,'rol_id'); ?>
            <?php echo $form->dropDownList($model, 'rol_id', GxHtml::listDataEx(ProdRol::model()->findAllAttributes(null, true))); ?>
        </div>
        <div class="col-md-6">
            <?php echo $form->textFieldControlGroup($model,'username',array('maxlength'=>15)); ?>
        </div>
    </div>
    

    

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Buscar',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
