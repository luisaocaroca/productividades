
<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'prod-persona-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>

    <?php echo $form->errorSummary($model); ?>

    
    <div class="row">
        <div class="col-md-8">
            
            
            <div class="row">
                <div class="col-md-12">
                    <?php echo $form->textFieldControlGroup($model,'nombres',array('maxlength'=>150)); ?>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->textFieldControlGroup($model,'apellido_paterno',array('maxlength'=>150)); ?>
                </div>

                <div class="col-md-6">
                    <?php echo $form->textFieldControlGroup($model,'apellido_materno',array('maxlength'=>150)); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <?php echo $form->textFieldControlGroup($model,'rut'); ?>
                </div>

                <div class="col-md-1">
                    <?php echo $form->textFieldControlGroup($model,'dv',array('maxlength'=>1)); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">

                    <?php echo $form->textFieldControlGroup($model,'email',array('maxlength'=>200)); ?>
                </div>

                <div class="col-md-6">
                    <?php echo $form->textFieldControlGroup($model,'telefono',array('maxlength'=>50)); ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'bo_activo'); ?>
                    <?php echo $form->radioButtonList($model, 'bo_activo',array(1=>'Activo', 0=>'Inactivo'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>
                    <?php echo $form->error($model,'bo_activo'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->labelEx($model,'area_id'); ?>
                    <?php echo $form->dropDownList($model, 'area_id', GxHtml::listDataEx(ProdArea::getAreas())); ?>
                    <?php echo $form->error($model,'area_id'); ?>
                </div>
            </div><!-- row -->
            <br>
            
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'bo_academico'); ?>
                    <?php echo $form->radioButtonList($model, 'bo_academico',array(1=>'Académico', 0=>'No Académico'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>
                    <?php echo $form->error($model,'bo_academico'); ?>
                </div>
                <div class="col-md-6">
                    
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'rol_id'); ?>
                    <?php echo $form->dropDownList($model, 'rol_id', GxHtml::listDataEx(ProdRol::getRoles())); ?>
                    <?php echo $form->error($model,'rol_id'); ?>
                </div>

                <div class="col-md-6">
                    <?php echo $form->textFieldControlGroup($model,'username',array('maxlength'=>15)); ?>
                </div>
            </div>
        </div>
    </div>

    <?php echo BsHtml::submitButton('Guardar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>

<?php $this->endWidget(); ?>
