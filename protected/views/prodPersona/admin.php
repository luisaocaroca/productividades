<?php
/* @var $this ProdPersona2Controller */
/* @var $model ProdPersona */


$this->breadcrumbs=array(
	'Prod Personas'=>array('index'),
	'Administrador',
);

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-list','label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#prod-persona-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<?php echo BsHtml::pageHeader(Yii::t('app', 'Manage'),GxHtml::encode($model->label(2))) ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo BsHtml::button('Busqueda Avanzada',array('class' =>'search-button', 'icon' => BsHtml::GLYPHICON_SEARCH,'color' => BsHtml::BUTTON_COLOR_PRIMARY), '#'); ?></h3>
    </div>
    <div class="panel-body">
        <p>
            Se puede adicionalmente ingresar un operador de comparación (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
                &lt;&gt;</b>
            o <b>=</b>) al inicio de cada valor  a buscar..
        </p>

        <div class="search-form" style="display:none">
            <?php $this->renderPartial('_search',array(
                'model'=>$model,
            )); ?>
        </div>
        <!-- search-form -->

        <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'prod-persona-grid',
			'dataProvider'=>$model->search(),
                        'rowCssClassExpression'=>'$data->color()',
			'filter'=>$model,
			'columns' => array(
                                'persona_id',		
                                array(
                                    'name'=>'nombre_completo',
                                    'header'=>'Nombre Completo',
                                    'value'=>'$data->getNombre_completo()'
                                ),
                                array(
                                    'name'=>'rut_completo',
                                    'header'=>'Rut',
                                    'value'=>'$data->rut_completo()'
                                ),
                                array(
                                    'name'=>'email',
                                    'header'=>'Email',
                                    'value'=>'$data->email'
                                ),
                                array(
                                    'name'=>'bo_academico',
                                    'header'=>'Tipo Persona',
                                    'value'=>'$data->getTipoPersona()',
                                    'filter'=>CHtml::listData(array(array("id"=>1,"value"=>"Académico"), array("id"=>0,"value"=>"No Académico")),'id','value')
                                ),
                                array(
                                        'name'=>'rol_id',
                                        'value'=>'GxHtml::valueEx($data->rol)',
                                        'filter'=>GxHtml::listDataEx(ProdRol::model()->findAllAttributes(null, true)),
                                      ),
                                array(
                                        'name'=>'area_id',
                                        'value'=>'GxHtml::valueEx($data->area)',
                                        'filter'=>GxHtml::listDataEx(ProdArea::model()->findAllAttributes(null, true)),
                                      ),      
                                array(
                                    'name'=>'bo_activo',
                                    'header'=>'Activo',
                                    'value'=>'$data->activo()',
                                    'filter'=>CHtml::listData(array(array("id"=>1,"value"=>"Activo"), array("id"=>0,"value"=>"Inactivo")),'id','value')
                                ),
                                /*
                                'email',
                                'telefono',
                                array(
                                                'name'=>'rol_id',
                                                'value'=>'GxHtml::valueEx($data->rol)',
                                                'filter'=>GxHtml::listDataEx(ProdRol::model()->findAllAttributes(null, true)),
                                                ),
                                */
                                array(
                                    'class'=>'bootstrap.widgets.BsButtonColumn',
                                    'template'=>'{view} {update}',
                                    'afterDelete'=>'function(link,success,data){ $("#statusMsg").html(data); }',
                                ),
                        ),
        )); ?>
    </div>
</div>



