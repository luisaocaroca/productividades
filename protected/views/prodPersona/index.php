<?php

$this->breadcrumbs = array(
	ProdPersona::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . ProdPersona::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . ProdPersona::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(ProdPersona::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 