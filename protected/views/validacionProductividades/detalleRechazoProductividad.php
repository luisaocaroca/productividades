<?php

$ancho_pagina = Yii::app()->session['ancho_pagina']-200;

?>

<script type="text/javascript">
$(document).ready(function(){

        // powerful jquery ! Clicking on the checkbox 'checkAll' change the state of all checkbox  
        $('#aprobar_detalle_todo').click(function () {
            //alert("entro")            
            
            $("input[type=checkbox]").each(function() {                
                
                if($(this).attr("name").substr(0, 17)=="ProdProductividad"){
                    
                    //ProdProductividad[0][aprueba]
                    str = $(this).attr("name");
                    var n=str.split("[");
                    var tipo_check = n[2].substr(0, 7);
                    
                    if(tipo_check=="aprueba"){
                        $(this).attr('checked', true);
                    }

//                    if(tipo_check=="rechaza"){
//                        $(this).attr('checked', false);
//                    }
                }
            });
    
        });
                

    });


function send()
{
    mostrarLoader();
    var data=$("#productividad-cambio-estado-form").serialize();

    
    
    //alert($("#ProdProductividad_0_observaciones").val());
 
    $.ajax({
        type: 'POST',
        url: '<?php echo $this->createUrl('validacionProductividades/cambiarEstadoDetalle'); ?>',
        data:data,
        success:function(data){
            //alert(data); 
            //
            //$('.modal-body').html(data); 
            $('#modalDetalle').html(data); 
            <?php
                echo CHtml::ajax(array(
                'id'=>'actualiza_listado',
                'type'=>'POST',
                'url'=>Yii::app()->createUrl('validacionProductividades/actualizaListaProductividades',
                                                           array("es_academico"=>$es_academico,
                                                                 "periodo_id"=>$periodo->periodo_id,
                                                                 "area_id"=>$area_id)),
                'update'=>'#lista_productividades',
        //      'data'=>'js:jQuery(this).serialize()',  // only send element name, not whole form
                ));
            ?>
           
           jQuery("#productividades").trigger("reloadGrid");
            $('#loading-indicator').hide();                
                                
          },
        error: function(data) { // if error occured
            alert(data.responseText);

                console.log(data);
          },

        dataType:'html'
       });

   }
   
   function verHistorial(){
        
        var data=$("#productividad-cambio-estado-form").serialize();
        
        mostrarLoader();

        $.ajax({
            type: 'POST',
            url: '<?php echo $this->createUrl('productividades/historiaProductividadesPersona'); ?>',
            data:data,
            success:function(data){
                
                //alert(data);
                
                $('#modalDetalle2').html(data).modal();
                //$('#preview').html(data); 
                $('#loading-indicator').hide();
                
                //$( "#modalDetalle2" ).remove();
                
                
              },
            error: function(data) { // if error occured
                  alert(data.responseText);
                  
                  console.log(data);
             },

            dataType:'html'
        });

    }

   </script>

<div class="modal-dialog modal-info">
    <div class="modal-content">
         <?php $form=$this->beginWidget('CActiveForm', array(
                                 'id'=>'productividad-cambio-estado-form',
                                 'action' => $this->createUrl('validacionProductividades/cambiarEstadoDetalle')
                         ));?>

                             <?php echo $form->hiddenField($model, "periodo_id", array('type' => "hidden", "value" => $periodo->periodo_id)); ?>
                             <?php echo $form->hiddenField($model, "rol_id", array('type' => "hidden", "value" => $rol_id)); ?>
                             <?php echo $form->hiddenField($model, "persona_id", array('type' => "hidden", "value" => $persona->persona_id)); ?>
                             <?php echo $form->hiddenField($model, "es_academico", array('type' => "hidden", "value" => $es_academico)); ?> 
                            <?php echo $form->hiddenField($model, "area_id", array('type' => "hidden", "value" => $area_id)); ?> 

        <div class="modal-header" style="background-color:#DEDEDE  ">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">
                <span class= "glyphicon glyphicon-save"></span>
                Cambio estado Productividad <?php echo $persona ?></h4>
        
        </div>
        
        <div class="modal-body">
            <div class="row">
                 <div class="col-md-12">

                     <div class="row" >
                         <div class="col-md-10">
                             <h4>Período <b><?php echo $periodo->nombrePeriodo?> </b></h4>
                         </div>

                         <div class="col-md-2 text-right">

                             <?php

                             $imghtmlHist=CHtml::image(Yii::app()->baseUrl."/images/URL History-48.png","Historial",array());

                             echo CHtml::link(
                                                     $imghtmlHist,
                                                     "",
                                                     array(  'id'=>"historial",
                                                             'style'=>'cursor: pointer; text-decoration: underline;',
                                                             'onclick'=>'verHistorial()'
                                                     )
                                             );
                             ?>



                             <br>
                             Historial    
                         </div>

                     </div>

                     <div class="row">
                         <div class="col-md-12">
                             <?php
                                 $flashMessages = Yii::app()->user->getFlashes();
                                 if ($flashMessages) {
                                     foreach($flashMessages as $key => $message) {
                                         if($key=="error"){
                                             echo BsHtml::alert(BsHtml::ALERT_COLOR_ERROR, BsHtml::bold('Atención!') . ' ' .  $message);
                                         }

                                         if($key=="success"){
                                             echo BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, BsHtml::bold('Atención!') . ' ' .  $message);
                                         }


                                     }
                                 }?>
                         </div>
                     </div>

                     <div class="row" >
                         <div class="col-md-12" style="overflow: auto">

                             <table class="table table-hover">
                                 <thead>
                                 <tr>
                                     <th class="tsubheader">
                                         <?php

                                         $imghtml=CHtml::image(Yii::app()->baseUrl."/images/Ok-32.png","Aprobar Todo",array("width"=>20));

                                         echo CHtml::link(
                                                                 $imghtml,
                                                                 "",
                                                                 array(  'id'=>"aprobar_detalle_todo",
                                                                         'style'=>'cursor: pointer; text-decoration: underline;',
                                                                 )
                                                         );
                                         ?>

                                     </th>
                                     <!--
                                     <td class="tsubheader">
                                         <?php
                                         /* SE SACA EL RECHAZO, SE DEJA SOLO DOS ESTADOS: PENDIENTE, APROBADO
                                         $imghtml=CHtml::image("images/Delete_32.png","Rechazar Todo",array("width"=>20));

                                         echo CHtml::link(
                                                                 $imghtml,
                                                                 "",
                                                                 array(  'id'=>"rechazar_detalle_todo",
                                                                         'style'=>'cursor: pointer; text-decoration: underline;',
                                                                 )
                                                         );
                                          * */

                                         ?>

                                     </td>
                                     -->

                                     <td class="tsubheader">Centro Costo</td>
                                     <td class="tsubheader">Tipo Productividad</td>
                                     <td class="tsubheader">Persona Carga</td>
                                     <td class="tsubheader">Cuotas</td>
                                     <td class="tsubheader">Monto Couta</td>
                                     <td class="tsubheader">Total Pago</td>
                                     <td class="tsubheader">Estado</td>
                                     <td class="tsubheader">Área</td>
                                     <td class="tsubheader">Encargado</td>
                                     <td class="tsubheader">Fecha Encargado</td>
                                     <td class="tsubheader">Jefe Admin.</td>
                                     <td class="tsubheader">Fecha Jefe Admin.</td>
                                     <td class="tsubheader">Observaciones</td>
                                 </tr>
                                 </thead>
                                 <tbody>

                             <?php 
                                 $total=0;
                                 $i=0;

                                 foreach($detalle as $i=>$prod) {
                                     $total = $total  + $prod->monto;
                                     $class="tregistro";
                                     
                                     
                                     
                                     
                                     $checked_disabled = false;
                                     $checked_aprobado = false;
                                     
                                     if($prod->estado_id==ProdEstado::APROBADO){
                                         $checked_aprobado = true;
                                         $checked_disabled = true;
                                         $class="tregistroaprobado";
                                     }
                                     
                                     if($prod->estado_id==ProdEstado::APROBADO_ENCARGADO && $rol_id == ProdRol::ROL_ENCARGADO){
                                         $checked_aprobado = true;
                                         $checked_disabled = true;
                                         $class="tregistroencargado";
                                     }
                                     
                                     if($prod->estado_id==ProdEstado::APROBADO_ENCARGADO && $rol_id == ProdRol::ROL_JEFE_ADMINISTRATIVO){
                                         $class="tregistroencargado";
                                     }
                                     
                                     
                                     if($prod->estado_id==ProdEstado::PENDIENTE && $rol_id == ProdRol::ROL_ENCARGADO){
                                         //$checked_rechazado = true;
                                         $class="tregistroapendiente";
                                         
                                     }
                                     
                                     if($prod->estado_id==ProdEstado::PENDIENTE && $rol_id == ProdRol::ROL_JEFE_ADMINISTRATIVO){
                                         //$checked_rechazado = true;
                                         $checked_disabled = true;
                                         $class="tregistroapendiente";
                                         
                                     }



                                 ?>
                                 <tr>
                                     <td class="<?php echo $class?>">
                                         <?php
                                         $name_check = "[$i]aprueba";                
                                         echo CHtml::activeCheckBox($prod,$name_check,array("checked"=>$checked_aprobado,"onclick"=>"clickedVal(this)","disabled"=>$checked_disabled) ); 
                                         ?>
                                     </td>
                                     <!--
                                     <td class="<?php echo $class?>">
                                         <?php
                                         /* SE SACA EL RECHAZO, SE DEJA SOLO DOS ESTADOS: PENDIENTE, APROBADO
                                         $name_check = "[$i]rechaza";                
                                         echo CHtml::activeCheckBox($prod,$name_check,array("checked"=>$checked_rechazado,"onclick"=>"clickedRechazo(this)") ); 
                                          * 
                                          */
                                         ?>
                                     </td>
                                     -->

                                     <td class="<?php echo $class?>"><?php echo $prod->cc?></td>
                                     <td class="<?php echo $class?>"><?php echo $prod->tipoProd->nombre_tipo_prod?></td>
                                     <td class="<?php echo $class?>"><?php echo $prod->personaCarga?></td>
                                     <td class="<?php echo $class?>"><?php echo $prod->cuota_actual?>/<?php echo $prod->numero_cuotas?></td>
                                     <td class="<?php echo $class?>"><?php echo Yii::app()->format->formatNumber($prod->monto)?></td>
                                     <td class="<?php echo $class?>"><?php echo Yii::app()->format->formatNumber($prod->total_monto_asignado)?></td>
                                     <td class="<?php echo $class?>"><?php echo $prod->estado?></td>
                                     <td class="<?php echo $class?>"><?php echo $prod->area?></td>
                                     <td class="<?php echo $class?>"><?php echo $prod->personaEncargado?></td>
                                     <td class="<?php echo $class?>"><?php echo $prod->fecha_encargado?></td>
                                     <td class="<?php echo $class?>"><?php echo $prod->personaValida?></td>
                                     <td class="<?php echo $class?>"><?php echo $prod->fecha_valida?></td>
                                     <td class="<?php echo $class?>">               
                                         <?php echo $form->textArea($prod,"[$i]observaciones",array('class'=>'form-control','style'=>'width:250px')); ?>
                                         <div class="column">
                                             <b><?php echo $form->error($prod,"[$i]observaciones"); ?></b>
                                         </div>
                                     </td>



                                 </tr>
                                 </tbody>
                                 <?php 
                                     $i++;
                                 }?>

                                 <tfoot>
                                 <tr>
                                     <td colspan="4">
                                     </td>
                                     <td class="tregistrototalcol">
                                         <b>Total</b>
                                     </td>
                                     <td class="tregistrototalcol">
                                         <b><?php echo Yii::app()->format->formatNumber($total)?></b>
                                     </td>
                                 </tr>
                                 </tfoot>
                             </table>

                         </div>

                     </div>

                </div>
            </div>
        </div>
       
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
            <?php echo CHtml::Button('Guardar',array('onclick'=>'send();','class'=>'btn btn-success')); ?> 
        </div>
        
        <?php $this->endWidget(); ?>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->    


