<?php
/* @var $this ValidacionProductividadesController */

$this->breadcrumbs = array(
    'Validación Productividades',
    'Encargado Área',
);

Yii::app()->clientScript->registerScript('search', "

$('.search-form-2').toggle();
$('.search-button-2').click(function(){
        $('.search-form-2').toggle();
        return false;
});


");



$ancho_pagina = Yii::app()->session['ancho_pagina']-140;
?>


<script type="text/javascript">

    
    function actualizaPeriodo(){
        mostrarLoader();
        $('form#ValidacionForm').submit();
    }
    
    function guardar(){
        mostrarLoader();
       $("form#ValidacionForm").attr("action", "<?php echo $this->createUrl('validacionProductividades/cambiarEstadoEncargadoTodos')?>");
       $("form#ValidacionForm").submit(); 
    }
    
</script>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'ValidacionForm',
    'action' => $this->createUrl('validacionProductividades/validaEncargado')
        ));




?>

<div class="row">
    <div class="col-xs-12">

        
        <div class="row">
            <div class="col-xs-12">
                <?php
                    $flashMessages = Yii::app()->user->getFlashes();
                    if ($flashMessages) {
                        foreach($flashMessages as $key => $message) {
                            if($key=="error"){
                                echo BsHtml::alert(BsHtml::ALERT_COLOR_ERROR, BsHtml::bold('Atención!') . ' ' .  $message);
                            }

                            if($key=="success"){
                                echo BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, BsHtml::bold('Atención!') . ' ' .  $message);
                            }


                        }
                    }?>
            </div>
            
        </div>



        <div class="row">
            <div class="col-xs-2">

                
                <?php echo $form->labelEx($model, 'periodo_id'); ?>
                <?php
                echo $form->dropDownList($model, 
                                        'periodo_id', 
                                         CHtml::listData($periodos, 'periodo_id', 'nombre_periodo'), 
                                         array('style'=>'',
                                               'onchange' => '{actualizaPeriodo();}',
                                               'class'=>'form-control'
                        )
                );
                ?>

                <?php echo $form->error($model, 'periodo_id'); ?>
                
                
            </div>
            <div class="col-xs-2">
                <?php if($es_administrador){?>
                    <?php echo $form->labelEx($model,'area_id'); ?>
                    <?php echo $form->dropDownList($model,
                                                   'area_id', 
                                                   CHtml::listData($listado_areas,'area_id','nm_area'),
                                                   array('onchange'=>'{actualizaPeriodo();}',
                                                         'class'=>'form-control',
                                                         'style'=>'')
                                                    );?>


                    <?php echo $form->error($model,'area_id'); ?>
                <?php }else{?>
                    <?php echo $form->hiddenField($model,'area_id',array('type'=>"hidden","value"=>$area_seleccionada_id)); ?>
                <?php }?>
            </div>
            
        </div>
        
        
        


        <br>
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-info" id="panel1">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-target="#collapseOne" 
                               href="#collapseOne">
                                <span class="glyphicon glyphicon-chevron-up"></span> Resumen Productividades

                            </a>
                        </h4>

                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-6">
                                <?php

                                $this->widget('zii.widgets.grid.CGridView',
                                              array('dataProvider' => $resumenEstado["dataProvider"],
                                                    'rowCssClassExpression'=>'$data["color"]',
                                                    'columns'=>$resumenEstado["columnas"],
                                                    'tagName'=>'Resumen Período' )
                                             );


                                ?>
                                </div>

                                <div class="col-md-6">
                                <?php

                                $this->widget('zii.widgets.grid.CGridView',
                                              array('dataProvider' => $resumenCentroCosto["dataProvider"],
                                                    'columns'=>$resumenCentroCosto["columnas"],
                                                    'tagName'=>'Resumen Período' )
                                             );


                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <br>
        <div class="row">
            <div class="col-md-10 text-right">
                
            </div>
            <div class="col-md-2 text-right">
                
                <div class="row">
                    <div class="col-md-4">
                        <?php if($puede_validar){?>
                        <?php 

                                echo CHtml::imageButton(Yii::app()->baseUrl."/images/Save-64.png", array('onclick'=>'guardar()',
                                                                                                         'title'=>'Guardar',
                                                                                                         'alt'=>'Guardar'
                                                                                                        ));
                        }
                        ?>
                    </div>
                    
                    <div class="col-md-4">
                        <?php 
                        $imghtml=CHtml::image(Yii::app()->baseUrl."/images/Excel-64.png","Exportar",array("title"=>"Exportar Excel"));

                        echo CHtml::link(
                                $imghtml,
                                "",
                                array('submit' => array('grillaProductividades/exportarExcel'),
                                      'style'=>'cursor: pointer; text-decoration: underline;',
                                      )
                        );

                        ?>
                    </div>
                    
                    
                    <div class="col-md-4">
                        <?php
                        
                        $imghtml2=CHtml::image(Yii::app()->baseUrl."/images/reload_64.png","Reload",array('title'=>'Actualizar Página',
                                                                                                                  'alt'=>'Actualizar Página'));

                        echo CHtml::link(
                                            $imghtml2,
                                            "",
                                            array(  'onclick' => 'actualizaPeriodo()',
                                                    'style'=>'cursor: pointer; text-decoration: underline;',
                                            )
                                    );
                        ?>
                    </div>
                </div>
                
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">

                <div id="lista_productividades" style="overflow: auto">
                    

                    <?php


                    if($model->tipo_personas==1){
                        $es_academico=true;
                        $width=1300;
                    }else{
                        $es_academico=false;
                        $width=1000;
                    }

                   
                    $es_validacion = true;
                    $periodo_cerrado = false;

                    if(!$puede_validar){
                        //$es_validacion = false;
                        $periodo_cerrado = true;
                    }


                    $this->widget(  'ext.productividades.LProductividadesWidget', array(
                                    'area_id' => $area_seleccionada_id,
                                    'es_academico' => $es_academico,
                                    'periodo_id' => $periodo_id,
                                    'es_validacion_encargado' => true,
                                    'periodo_cerrado' => $periodo_cerrado,
                                    'model' => $model,
                                    'width' => 3000,
                                    'detalle_por_tipo_productividad'=>$mostrar_detalle_tipo_productividad,
                                    'form'=>$form,
                                    'model'=>$model
                                    )
                    );
                    ?>
                    </div>


            </div>
        </div>
        
        
     
    </div>
</div>


<?php $this->endWidget(); ?>    


