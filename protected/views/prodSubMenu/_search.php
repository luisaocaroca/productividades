<?php
/* @var $this ProdSubMenu2Controller */
/* @var $model ProdSubMenu */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'sub_menu_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'menu_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'nombre_menu',array('maxlength'=>200)); ?>
    <?php echo $form->textFieldControlGroup($model,'link',array('maxlength'=>500)); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Buscar',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
