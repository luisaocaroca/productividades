<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('sub_menu_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->sub_menu_id), array('view', 'id' => $data->sub_menu_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('menu_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->menu)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('nombre_menu')); ?>:
	<?php echo GxHtml::encode($data->nombre_menu); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('link')); ?>:
	<?php echo GxHtml::encode($data->link); ?>
	<br />

</div>