<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'prod-sub-menu-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'field_with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

        <div class="row">
            <div class="col-md-5">
                
                <?php echo $form->labelEx($model,'menu_id'); ?>
		<?php echo $form->dropDownList($model, 'menu_id', GxHtml::listDataEx(ProdMenu::model()->findAllAttributes(null, true)), array("class"=>"form-control")); ?>
		<?php echo $form->error($model,'menu_id'); ?>
                
                <?php echo $form->labelEx($model,'nombre_menu'); ?>
		<?php echo $form->textField($model, 'nombre_menu', array('maxlength' => 200,"class"=>"form-control")); ?>
		<?php echo $form->error($model,'nombre_menu'); ?>
                
                <?php echo $form->labelEx($model,'link'); ?>
		<?php echo $form->textField($model, 'link', array('maxlength' => 500,"class"=>"form-control")); ?>
		<?php echo $form->error($model,'link'); ?>
                
                <br>
                <?php echo $form->labelEx($model,'bo_activo'); ?>
                <br>
                <?php echo $form->radioButtonList($model, 'bo_activo',array(1=>'Activo', 0=>'Inactivo'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>
                <?php echo $form->error($model,'bo_activo'); ?>
            </div>
        </div>
        
        <br>
        
        <div class="row">
            <div class="col-md-6">
                <?php echo BsHtml::submitButton('Guardar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
            </div>
        </div>
        
        
        <br>
        
        <div class="row">
            <div class="col-md-6">
                <h2><?php echo GxHtml::encode($model->getRelationLabel('prodRols')); ?></h2>
                
                <?php echo $form->checkBoxList($model, 'prodRols', GxHtml::encodeEx(GxHtml::listDataEx(ProdRol::model()->findAllAttributes(null, true)), false, true),array('separator'=>'<br>','labelOptions'=>array('style'=>'display:inline'))); ?>
            </div>
        </div>
		

<?php
$this->endWidget();
?>
</div><!-- form -->