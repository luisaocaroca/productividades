<?php

$this->breadcrumbs = array(
        'Mantenedores'=>array('/administrador/mantenedores'),       
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#prod-sub-menu-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php echo BsHtml::pageHeader(Yii::t('app', 'Manage'),GxHtml::encode($model->label(2))) ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo BsHtml::button('Busqueda Avanzada',array('class' =>'search-button', 'icon' => BsHtml::GLYPHICON_SEARCH,'color' => BsHtml::BUTTON_COLOR_PRIMARY), '#'); ?></h3>
    </div>
    <div class="panel-body">
        <p>
            Se puede adicionalmente ingresar un operador de comparación (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
                &lt;&gt;</b>
            o <b>=</b>) al inicio de cada valor  a buscar..
        </p>

        <div class="search-form" style="display:none">
            <?php $this->renderPartial('_search',array(
                'model'=>$model,
            )); ?>
        </div>
        <!-- search-form -->

        <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'prod-sub-menu-grid',
			'dataProvider'=>$model->search(),
                        'rowCssClassExpression'=>'$data->color()',
			'filter'=>$model,
			'columns'=>array(
        		'sub_menu_id',
                        'menu_id',
                        'nombre_menu',
                        'link',
                        array(
                            'name'=>'bo_activo',
                            'header'=>'Activo',
                            'value'=>'$data->activo()',
                            'filter'=>CHtml::listData(array(array("id"=>1,"value"=>"Activo"), array("id"=>0,"value"=>"Inactivo")),'id','value')
                        ),    
				array(
                                    'class'=>'bootstrap.widgets.BsButtonColumn',
                                    'template'=>'{view} {update}',
				),
			),
        )); ?>
    </div>
</div>




