
<?php
/* @var $this ProdTipoProductividad2Controller */
/* @var $model ProdTipoProductividad */
/* @var $form BSActiveForm */
?>



<div class="row">
    <div class="col-md-12">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

        <?php echo $form->label($model, 'tipo_prod_id'); ?>
        <?php echo $form->textField($model, 'tipo_prod_id', array("class"=>"form-control")); ?>

        <?php echo $form->label($model, 'nombre_tipo_prod'); ?>
        <?php echo $form->textField($model, 'nombre_tipo_prod', array('maxlength' => 100,"class"=>"form-control")); ?>

        <?php echo $form->label($model, 'descripcion'); ?>
        <?php echo $form->textField($model, 'descripcion', array('maxlength' => 200,"class"=>"form-control")); ?>
        
        <br>
        <?php echo $form->label($model, 'acadamico'); ?>
        <?php echo $form->radioButtonList($model, 'acadamico',array(1=>'Si', 0=>'No'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>
        <br>
        <?php echo $form->label($model, 'no_academico'); ?>
        <?php echo $form->radioButtonList($model, 'no_academico',array(1=>'Si', 0=>'No'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>
        <br>
        <?php echo $form->label($model, 'es_coordinacion'); ?>
        <?php echo $form->radioButtonList($model, 'es_coordinacion',array(1=>'Si', 0=>'No'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>

        <br>
        <?php echo BsHtml::submitButton('Buscar',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>


<?php $this->endWidget(); ?>

    </div>
</div><!-- search-form -->
