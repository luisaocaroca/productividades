

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'prod-tipo-productividad-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'field_with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
                    <div class="col-md-4">
                        <?php echo $form->labelEx($model,'nombre_tipo_prod'); ?>
                        <?php echo $form->textField($model, 'nombre_tipo_prod', array('maxlength' => 100,"class"=>"form-control")); ?>
                        <?php echo $form->error($model,'nombre_tipo_prod'); ?>

                        <?php echo $form->labelEx($model,'descripcion'); ?>
                        <?php echo $form->textField($model, 'descripcion', array('maxlength' => 200,"class"=>"form-control")); ?>
                        <?php echo $form->error($model,'descripcion'); ?>
                    </div>
                    
                    <div class="col-md-4">
                        
                        
                        <div class="row">
                            <div class="col-md-5">
                                <?php echo $form->labelEx($model,'acadamico'); ?>
                            </div>
                            <div class="col-md-7">
                                <?php echo $form->radioButtonList($model, 'acadamico',array(1=>'Si', 0=>'No'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>
                                <?php echo $form->error($model,'acadamico'); ?>
                            </div>
                        </div>
                        
                        
                        <div class="row">
                            <div class="col-md-5">
                                <?php echo $form->labelEx($model,'no_academico'); ?>
                            </div>
                            <div class="col-md-7">
                                <?php echo $form->radioButtonList($model, 'no_academico',array(1=>'Si', 0=>'No'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>
                                <?php echo $form->error($model,'no_academico'); ?>
                            </div>
                        </div>
                        
                        
                        <div class="row">
                            <div class="col-md-5">
                                <?php echo $form->labelEx($model,'es_coordinacion'); ?>
                            </div>
                            <div class="col-md-7">
                                <?php echo $form->radioButtonList($model, 'es_coordinacion',array(1=>'Si', 0=>'No'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>
                                <?php echo $form->error($model,'es_coordinacion'); ?>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-5">
                                <?php echo $form->labelEx($model,'bo_activo'); ?>
                            </div>
                            <div class="col-md-7">
                                <?php echo $form->radioButtonList($model, 'bo_activo',array(1=>'Activo', 0=>'Inactivo'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>
                                <?php echo $form->error($model,'bo_activo'); ?>

                            </div>
                        </div>
                        
                        
                    </div>
                </div><!-- row -->
                
                <br>
                
                <div class="row">
                    <div class="col-md-6">
                        <?php echo BsHtml::submitButton('Guardar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
                    </div>
                </div>
                
              

<?php
$this->endWidget();
?>
