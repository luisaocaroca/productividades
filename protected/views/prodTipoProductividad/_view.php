<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_prod_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->tipo_prod_id), array('view', 'id' => $data->tipo_prod_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nombre_tipo_prod')); ?>:
	<?php echo GxHtml::encode($data->nombre_tipo_prod); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('descripcion')); ?>:
	<?php echo GxHtml::encode($data->descripcion); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('acadamico')); ?>:
	<?php echo GxHtml::encode($data->acadamico); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('no_academico')); ?>:
	<?php echo GxHtml::encode($data->no_academico); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('es_coordinacion')); ?>:
	<?php echo GxHtml::encode($data->es_coordinacion); ?>
	<br />

</div>