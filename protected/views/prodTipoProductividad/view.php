<?php

$this->breadcrumbs = array(
        'Mantenedores'=>array('/administrador/mantenedores'),   
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->tipo_prod_id)),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<div class="row">
    <div class="col-md-6">
        <?php $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'attributes' => array(
                    'tipo_prod_id',
                    'nombre_tipo_prod',
                    'descripcion',
                    'nombre_es_academico',
                    'nombre_es_no_academico',
                    'nombre_es_coordinacion',
                ),
        )); ?>
    </div>
</div>
