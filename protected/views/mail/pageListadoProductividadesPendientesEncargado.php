<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Listado Productividades Pendientes Validación Encargado</title>
        <style type="text/css">

        .theader{



            color: white;    
            background-color:  #4498c7;
            text-align: center;  
            font-weight: bold;
        }

        .tsubheader{

            color: white;
            background-color:  #4498c7;
            text-align: center;   
            font-weight: bold;


        }

        tr.red
        {
                background: #E5F1F4;
                color:red;
                font-weight: bold;
        }
        tr.normal
        {
                background: #F8F8F8;
        }

        </style>
    </head>
    <body>
           

            <table style="vertical-align: middle !important;margin-bottom:0em !important; " border="0">
                <tr>
                    <td width="100%">
                        <h3>Productividades Pendientes Área <?php echo $area->nm_area?></h3>
                    </td>
                </tr>

            </table>   
            <hr>
            <h4>
                Total Productividades Período: <b>$<?php echo Yii::app()->format->formatNumber($model->total)?></b>
            </h4>

            <table cellspacing="2" cellpading="2" style="width:100%;padding-right: 2px;    padding-left: 2px;    padding-bottom: 2px;    padding-top: 2px;    border-spacing:2px !important;    font-size: 12px;">    
                <tr>
                    <td style="color: white;background-color:  #4498c7;text-align: center;font-weight: bold;">Período</td>
                    <td style="color: white;background-color:  #4498c7;text-align: center;font-weight: bold;">Persona</td>
                    <td style="color: white;background-color:  #4498c7;text-align: center;font-weight: bold;">Estado</td>
                    <td style="color: white;background-color:  #4498c7;text-align: center;font-weight: bold;">Centro Costo</td>
                    <td style="color: white;background-color:  #4498c7;text-align: center;font-weight: bold;">Tipo Productividad</td>
                    <td style="color: white;background-color:  #4498c7;text-align: center;font-weight: bold;">Monto</td>
                    <td style="color: white;background-color:  #4498c7;text-align: center;font-weight: bold;">Persona Carga</td>
                    <td style="color: white;background-color:  #4498c7;text-align: center;font-weight: bold;">Observaciones</td>

                </tr>
                
                <?php 
                $ultimo_periodo="";
                $total=0;
                foreach($detalle->data as $data){ ?>
                    <?php if($ultimo_periodo!=$data->periodo && $ultimo_periodo!=""){?>
                        <tr>
                            <td colspan="5"></td>
                            <td><b>Total</b></td>
                            <td><b><?php echo "$" . Yii::app()->format->formatNumber($total)?></b></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="8"><hr></td>
                        </tr>
                        <?php 
                        $total = 0 ;                   
                    }
                    
                    $total = $total + $data->monto;
                    $style = "";
                    
                    if($data->estado->estado_id == 0){
                        $style = "      background-color: #fffbf4;
                                    padding-right: 2px;
                                    padding-left: 2px;
                                    padding-bottom: 2px;
                                    padding-top: 2px;
                                    color: #DD0000;
                                    font-style: normal;
                                    font-family: Verdana,Arial,Helvetica,sans-serif;

                                    text-align: left;
                                    vertical-align: middle;
                                    font-size: 10px;";
                    }
                    
                    
                    if($data->estado->estado_id == 1){
                        $style = "background-color: #F8F8F8;
                                    padding-right: 2px;
                                    padding-left: 2px;
                                    padding-bottom: 2px;
                                    padding-top: 2px;
                                    color: #ffb703;
                                    font-style: normal;
                                    font-family: Verdana,Arial,Helvetica,sans-serif;

                                    text-align: left;
                                    vertical-align: middle;
                                    font-size: 10px;
                                    font-weight: bold;";
                    }
                    
                    
                    if($data->estado->estado_id == 2){
                        $style = "background-color: #fffbf4;
                                padding-right: 2px;
                                padding-left: 2px;
                                padding-bottom: 2px;
                                padding-top: 2px;
                                color: #336600;
                                font-style: normal;
                                font-family: Verdana,Arial,Helvetica,sans-serif;

                                text-align: left;
                                vertical-align: middle;
                                font-size: 11px;";
                    }
                    ?>
                    
                    <tr>
                        
                        <td style="<?php echo $style?>">
                             <?php echo $data->periodo?>
                        </td>
                        <td style="<?php echo $style?>">
                             <?php echo $data->persona?>
                        </td>
                        <td style="<?php echo $style?>">
                             <?php echo $data->estado?>
                        </td>
                        <td style="<?php echo $style?>">
                             <?php echo $data->cc?>
                        </td>
                        <td style="<?php echo $style?>">
                             <?php echo $data->tipoProd?>
                        </td>
                        <td style="<?php echo $style?>">
                             <?php echo "$" . Yii::app()->format->formatNumber($data->monto)?>
                        </td>
                        <td style="<?php echo $style?>">
                             <?php echo $data->personaCarga?>
                        </td>
                        <td style="<?php echo $style?>">
                             <?php echo $data->observaciones?>
                        </td>
                    </tr>
                    
                    
                <?php 
                
                $ultimo_periodo =  $data->periodo;
                }?>
                    
                    <tr>
                        <td colspan="5"></td>
                        <td><b>Total</b></td>
                        <td><b><?php echo "$" . Yii::app()->format->formatNumber($total)?></b></td>
                        <td></td>
                    </tr>      
            </table>
            <br>
            
            <h2>Para ingresar al sistema y validar estas productividades presione <a href="<?php echo $url?>">Aquí</a></h2>
           
           

    </body>
</html>