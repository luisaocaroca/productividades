<?php 
$ancho_pagina = Yii::app()->session['ancho_pagina']-140;
?>

<div class="form">

<div style=" position: absolute; left: 1090px; top:230px">
<?php
    $imghtml=CHtml::image(Yii::app()->baseUrl."/images/Excel-64.png","Exportar",array("title"=>"Exportar Excel"));
    
    echo CHtml::link($imghtml, "javascript:;", array(
        'style' => 'cursor: pointer;',
        "onclick" => "exportarExcel(); return false;"
    ));

?>

</div>  


    
<h2>Listado Productividades
    <?php if(!is_null($periodo_fin->periodo_id)){?>
        desde <?php echo $periodo->nombrePeriodo?> al <?php echo $periodo_fin->nombrePeriodo?> 
    <?php }else{?>
    Período <?php echo $periodo->nombrePeriodo?> 
    <?php }?>
    
</h2>

<div id="listado_academicos" style="width:<?php echo $ancho_pagina?>px;height: 450px; overflow: auto">
<table class="tproductividades" style="width: 2000px !important;"> 
    <tr>
        <td colspan="4"><b>ACADEMICOS</b></td>
        <td></td>
        <td class="theader" colspan="<?php echo count($centros_costo_academicos)?>">Centros Costo</td>
    </tr>
    <tr>
        <td class="tsubheader" width="10px"></td>
        <td class="tsubheader" width="100px">Apellido Paterno</td>
        <td class="tsubheader" width="100px">Apellido Materno</td>
        <td class="tsubheader" width="100px">Nombres</td>
        <td class="tsubheader"  width="80px">Total</td>
        <?php foreach($centros_costo_academicos as $centro_costo){?>
            <td class="tsubheadercc" width="120px"> <?php echo $centro_costo;?></td>
        <?php }?>
        
    </tr>
    
    <?php foreach($salida_academicos["salida_personas"] as $persona){
        $class="tregistro";
        $class_persona = "tregistropersona";
        ?>
        
    <tr>
        <td class="<?php echo $class?>">
            <?php 
            $imghtml=CHtml::image(Yii::app()->baseUrl."/images/Search_32.png","Ver Detalle",array("width"=>20));            
            echo CHtml::ajaxLink($imghtml,Yii::app()->createUrl('productividades/detalleProductividadPersona'),
                                        array('type'=>'POST',
                                              'data'=>array('persona_id'=>$persona["persona"]->persona_id,
                                                            'periodo_id'=>$periodo->periodo_id,
                                                            'periodo_fin_id'=>$periodo_fin->periodo_id,
                                                            'ajax'=>'ajax'),
                                              'update'=>'#preview',
                                              'complete'=>'afterAjax'
                                        ),
                                        array('id' =>'coordinacion_' . $persona["persona"]->persona_id));            

                    
            

            ?>
        </td>
        
        <td class="<?php echo $class_persona?>">
            <?php echo $persona["persona"]->apellido_paterno?>
        </td>
        <td class="<?php echo $class_persona?>">
            <?php echo $persona["persona"]->apellido_materno?>
        </td>
        <td class="<?php echo $class_persona?>">
            <?php echo $persona["persona"]->nombres?>
        </td>
        <?php 
            $total=0;
            foreach($persona["listado_productividades"] as $detalle_productividad){
                $total=$total+$detalle_productividad["monto"];
            }
        ?>
        <td class="tregistrototal">
            <?php echo Yii::app()->format->formatNumber($total);?>
        </td>
        <?php 
            foreach($persona["listado_productividades"] as $detalle_productividad){
                ?>
                <td class="<?php echo $class?>">
                    <?php echo Yii::app()->format->formatNumber($detalle_productividad["monto"]); ?>
                </td>
        <?php }?>
        
    </tr>
    
    
    <?php }?>    
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td  class="tregistrototalFila"><b>Totales</b></td>
        <?php 
            $total_totales=0;
            foreach($salida_academicos["totales_columna"] as $total){
                    $total_totales = $total_totales + $total;
            
            
            }
       ?>
        
            
        <td class="tregistrototalFila">
            <b><?php echo Yii::app()->format->formatNumber($total_totales);?></b>
        </td>            
        <?php 
            foreach($salida_academicos["totales_columna"] as $total){?>
            <td class="tregistrototalFila">
                <b>
                    <?php 
                    echo  Yii::app()->format->formatNumber($total);?>
                </b>
            </td>
        <?php }?>
        
    </tr>

</table>
</div>
    
<br>
   
<div id="listado_no_academicos" style="width:<?php echo $ancho_pagina?>px; height: 450px; overflow: auto">
<table class="tproductividades" style="width: 2000px !important;"> 
    <tr>
        <td colspan="4"><b>NO ACADEMICOS</b></td>
        <td></td>
        <td class="theader" colspan="<?php echo count($centros_costo_no_academicos)?>">Centros Costo</td>
    </tr>    
    <tr>
        <td class="tsubheader" width="10px"></td>
        <td class="tsubheader" width="100px">Apellido Paterno</td>
        <td class="tsubheader" width="100px">Apellido Materno</td>
        <td class="tsubheader" width="100px">Nombres</td>
        <td class="tsubheader" width="80px">Total</td>
        <?php foreach($centros_costo_academicos as $centro_costo){?>
            <td class="tsubheadercc" width="120px"> <?php echo $centro_costo;?></td>
        <?php }?>
        
    </tr>
    
    <?php foreach($salida_no_academicos["salida_personas"] as $persona){
        $class="tregistro";
        $class_persona = "tregistropersona";
        ?>
        
    <tr>        
        <td class="<?php echo $class?>">
            <?php 
            $imghtml=CHtml::image(Yii::app()->baseUrl."/images/Search_32.png","Ver Detalle",array("width"=>20));
            echo CHtml::ajaxLink($imghtml,Yii::app()->createUrl('productividades/detalleProductividadPersona'),
                                        array('type'=>'POST',
                                              'data'=>array('persona_id'=>$persona["persona"]->persona_id,
                                                            'periodo_id'=>$periodo->periodo_id,
                                                            'periodo_fin_id'=>$periodo_fin->periodo_id,
                                                            'ajax'=>'ajax'),
                                              'update'=>'#preview',
                                              'complete'=>'afterAjax'
                                        ),
                                        array('id' =>'coordinacion_' . $persona["persona"]->persona_id));        

                    
            

            ?>
        </td>
        <td class="<?php echo $class_persona?>">
            <?php echo $persona["persona"]->apellido_paterno?>
        </td>
        <td class="<?php echo $class_persona?>">
            <?php echo $persona["persona"]->apellido_materno?>
        </td>
        <td class="<?php echo $class_persona?>">
            <?php echo $persona["persona"]->nombres?>
        </td>
        <?php 
            $total=0;
            foreach($persona["listado_productividades"] as $detalle_productividad){
                $total=$total+$detalle_productividad["monto"];
            }
        ?>
        <td class="tregistrototal">
            <?php echo Yii::app()->format->formatNumber($total);?>
        </td>
        <?php 
            foreach($persona["listado_productividades"] as $detalle_productividad){
            ?>
                <td class="<?php echo $class?>">
                    <?php echo Yii::app()->format->formatNumber($detalle_productividad["monto"]); ?>
                </td>
        <?php }?>
        
    </tr>
    
    
    <?php }?>    
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td  class="tregistrototalFila"><b>Totales</b></td>
            
        <?php 
            $total_totales=0;
            foreach($salida_no_academicos["totales_columna"] as $total){
                $total_totales = $total_totales + $total;
            }
        ?>
        <td class="tregistrototalFila">
            <b><?php echo Yii::app()->format->formatNumber($total_totales);?></b>
        </td>
        <?php 
            foreach($salida_no_academicos["totales_columna"] as $total){?>
            <td class="tregistrototalFila">
                <b>
                    <?php 
                    echo  Yii::app()->format->formatNumber($total);?>
                </b>
            </td>
        <?php }?>        
    </tr>

</table>
</div>
    
</div>