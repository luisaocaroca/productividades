<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('historial_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->historial_id), array('view', 'id' => $data->historial_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('productividad_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->productividad)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('periodo_id')); ?>:
	<?php echo GxHtml::encode($data->periodo_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('persona_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->persona)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('persona_modifica_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->personaModifica)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('observaciones')); ?>:
	<?php echo GxHtml::encode($data->observaciones); ?>
	<br />

</div>