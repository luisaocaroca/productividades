<?php

$this->breadcrumbs = array(
        'Mantenedores'=>array('/administrador/mantenedores'),
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	//array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	//array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	//array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->historial_id)),
	//array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->historial_id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h3><b>Detalle Productividad</b></h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
array(
			'name' => 'periodo',
			'type' => 'raw',
			'value' => $model->periodo !== null ? $model->periodo : null,
			),
            
array(
			'name' => 'persona',
			'type' => 'raw',
			'value' => $model->persona !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->persona)), array('prodPersona/view', 'id' => GxActiveRecord::extractPkValue($model->persona, true))) : null,
			), 

array(
			'name' => 'centro_costo',
			'type' => 'raw',
			'value' => $model->productividad !== null ? $model->productividad->cc  : null,
			),
array(
			'name' => 'tipo_productividad',
			'type' => 'raw',
			'value' => $model->productividad !== null ? $model->productividad->tipoProd  : null,
			),            
            
array(
			'name' => 'monto',
			'type' => 'raw',
			'value' => $model->productividad !== null ? "$ ".$model->productividad->monto  : null,
			),            
array(
			'name' => 'numero_cuotas',
			'type' => 'raw',
			'value' => $model->productividad !== null ? $model->productividad->numero_cuotas  : null,
			),  
array(
			'name' => 'cuota_actual',
			'type' => 'raw',
			'value' => $model->productividad !== null ? $model->productividad->cuota_actual  : null,
			),  
array(
			'name' => 'total_monto_asignado',
			'type' => 'raw',
			'value' => $model->productividad !== null ? "$ ".  $model->productividad->total_monto_asignado  : null,
			),   
array(
			'name' => 'observaciones',
			'type' => 'raw',
			'value' => $model->productividad !== null ? $model->productividad->observaciones  : null,
			),      
array(
			'name' => 'area',
			'type' => 'raw',
			'value' => $model->productividad !== null ? $model->productividad->area  : null,
			),   
array(
			'name' => 'personaEncargado',
			'type' => 'raw',
			'value' => $model->productividad !== null ? $model->productividad->personaEncargado  : null,
			), 
array(
			'name' => 'fechaEncargado',
			'type' => 'raw',
			'value' => $model->productividad !== null ? $model->productividad->fechaEncargado  : null,
			),             
            
array(
			'name' => 'personaValida',
			'type' => 'raw',
			'value' => $model->productividad !== null ? $model->productividad->personaValida  : null,
			),             
array(
			'name' => 'fechaValida',
			'type' => 'raw',
			'value' => $model->productividad !== null ? $model->productividad->fechaValida  : null,
			),  
	),
)); ?>

<br>


<h3><b>Log Modificaciones</b></h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'historial_id',

array(
                        'label'=> 'Persona Modifica',
			'name' => 'personaModifica',
			'type' => 'raw',
			'value' => $model->personaModifica !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->personaModifica)), array('prodPersona/view', 'id' => GxActiveRecord::extractPkValue($model->personaModifica, true))) : null,
			),
array(
			'name' => 'observaciones',
			'type' => 'raw'
			),
            

	),
)); ?>

