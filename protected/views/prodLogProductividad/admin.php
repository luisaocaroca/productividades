<?php

$this->breadcrumbs = array(
        'Mantenedores'=>array('/administrador/mantenedores'),
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

//$this->menu = array(
//		array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
//		//array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
//	);

Yii::app()->clientScript->registerScript('search', "
$('.search-form').toggle();
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('prod-log-productividad-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo BsHtml::button('Busqueda Avanzada',array('class' =>'search-button', 'icon' => BsHtml::GLYPHICON_SEARCH,'color' => BsHtml::BUTTON_COLOR_PRIMARY), '#'); ?></h3>
    </div>
    <div class="panel-body">
        

        

        <p>
        Opcionalmente se puede ingresar los operadores (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) al inicio de cada casilla de búsqueda.
        </p>

        <div class="search-form">
        <?php $this->renderPartial('_search', array(
                'model' => $model,
        )); ?>
        </div><!-- search-form -->
        
        

        <div id="lista">
            
        <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id' => 'prod-log-productividad-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns' => array(
                        'historial_id',
                        array(
                                        'name'=>'periodo_id',
                                        'value'=>'GxHtml::valueEx($data->periodo)',
                                        'filter'=>GxHtml::listDataEx(ProdPeriodo::model()->findAllAttributes(null, true)),
                                        ),
                        array(
                                        'name'=>'persona_id',
                                        'value'=>'GxHtml::valueEx($data->persona)',
                                        'filter'=>GxHtml::listDataEx(ProdPersona::getPersonas())
                                        ),
                        array(
                                        'name'=>'persona_modifica_id',
                                        'value'=>'GxHtml::valueEx($data->personaModifica)',
                                        'filter'=>GxHtml::listDataEx(ProdPersona::getPersonas())
                                        ),
                        array(
                            'name'=>'fecha_modificacion',
                            'header'=>'Fecha',
                            //'value'=>'date("d M Y",strtotime($data["work_date"]))'
                            'value'=>'Yii::app()->dateFormatter->format("dd MMM yyyy H:mm",strtotime($data->fecha_modificacion))'
                        ),
                         /*array(
                            'name'=>'observaciones',
                            'type'=>'raw',
                            ),  */ 

                        array(
                            'class'=>'bootstrap.widgets.BsButtonColumn',
                                'template'=>'{update} {view}',
                                'buttons'=>array(
                                                'update'=>array(
                                                                'visible'=>'false',
                                                        ),
                                                'view'=>array(
                                                                'visible'=>'true',
                                                        ),
                                                'delete'=>array(
                                                                'visible'=>'false',
                                                        ),
                                ),
                       ),

                      ),  
        )); ?>
            
            
        
        </div>
    </div>
    
    
</div>    
    