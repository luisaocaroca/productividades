<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
            <div class="col-md-6">
		<?php echo $form->label($model, 'periodo_id'); ?>
		<?php echo $form->textField($model, 'periodo_id',array("class"=>"form-control")); ?>
            </div>
	</div>

	<div class="row">
            <div class="col-md-6">
		<?php echo $form->label($model, 'persona_id'); ?>
		<?php echo $form->dropDownList($model, 'persona_id', GxHtml::listDataEx(ProdPersona::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'Todos'),"class"=>"form-control" )); ?>
	
            </div>
        </div>
	<div class="row">
            <div class="col-md-6">
		<?php echo $form->label($model, 'persona_modifica_id'); ?>
		<?php echo $form->dropDownList($model, 'persona_modifica_id', GxHtml::listDataEx(ProdPersona::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'Todos'),"class"=>"form-control")); ?>
	
            </div>
        </div>
        <br>
	<div class="row">
            <div class="col-md-6">
            <?php echo BsHtml::submitButton('Buscar',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
            </div>
        </div>
<?php $this->endWidget(); ?>

</div><!-- search-form -->
