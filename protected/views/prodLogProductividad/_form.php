<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'prod-log-productividad-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'field_with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'productividad_id'); ?>
		<?php echo $form->dropDownList($model, 'productividad_id', GxHtml::listDataEx(ProdProductividad::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'productividad_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'periodo_id'); ?>
		<?php echo $form->textField($model, 'periodo_id'); ?>
		<?php echo $form->error($model,'periodo_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'persona_id'); ?>
		<?php echo $form->dropDownList($model, 'persona_id', GxHtml::listDataEx(ProdPersona::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'persona_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'persona_modifica_id'); ?>
		<?php echo $form->dropDownList($model, 'persona_modifica_id', GxHtml::listDataEx(ProdPersona::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'persona_modifica_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'observaciones'); ?>
		<?php echo $form->textField($model, 'observaciones', array('maxlength' => 500)); ?>
		<?php echo $form->error($model,'observaciones'); ?>
		</div><!-- row -->

<table>
    <tr>
    </tr>
</table>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->