<script>
$(document).ready(function(){
                
        $('#log_modificaciones').click(function () {
            //alert("historial");   
            mostrarLoader();
            var data=$("#productividad-cambio-estado-form").serialize();
            
            $.ajax({
                type: 'POST',
                url: '<?php echo $this->createUrl('ingresoProductividades/getLogModificaciones'); ?>',
                data:{
                    'persona_id':'<?php echo $persona->persona_id ?>',
                    'periodo_id':'<?php echo $periodo->periodo_id ?>'
                },
                success:function(data){
                    //alert(data); 
                    //$('#preview').html(data); 
                    $('#modalDetalle2').html(data).modal();
                    $('#loading-indicator').hide();


                  },
                error: function(data) { // if error occured
                    alert(data.responseText);
                  
                    console.log(data);
                 },

                dataType:'html'
            });
    
        });
});        
</script>

<div class="modal-dialog modal-info">
    <div class="modal-content">

        <div class="modal-header" style="background-color:#DEDEDE  ">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">
                <span class= "glyphicon glyphicon-list"></span>
                Detalle Productividad <?php echo $persona ?></h4>
        
        </div>
        
          
        <div class="modal-body">
            <div class="row" >
                <div class="col-md-10">
                    <h4>Período <b><?php echo $periodo->nombrePeriodo?> </b></h4>
                </div>

                <div class="col-md-2 text-right">

                    <?php
                    $imghtml=CHtml::image(Yii::app()->baseUrl."/images/URL History-48.png","Log Modificaciones",array("title"=>"Log Modificaciones"));

                        echo CHtml::link(
                                                $imghtml,
                                                "",
                                                array(  'id'=>"log_modificaciones",
                                                        'style'=>'cursor: pointer; text-decoration: underline;',
                                                )
                                        );



                    ?>
                    <br>
                    Log Cambios 
                </div>

            </div>

       
        <div class="row" >
            <div class="col-md-12" style="overflow: auto">            

                <table class="table table-bordered">
                    <tr>
                        <td class="tsubheader"></td>            
                        <td class="tsubheader">Tipo Productividad</td>
                        <td class="tsubheader">Centro Costo</td>
                        <td class="tsubheader">Persona Carga</td>
                        <td class="tsubheader">Cuotas</td>
                        <td class="tsubheader">Monto Cuota</td>
                        <td class="tsubheader">Total Pago</td>
                        <td class="tsubheader">Estado</td>
                        <td class="tsubheader">Área</td>
                        <td class="tsubheader">Encargado</td>
                        <td class="tsubheader">Fecha Encargado</td>
                        <td class="tsubheader">Jefe Admin.</td>
                        <td class="tsubheader">Fecha Jefe Admin.</td>
                        <td class="tsubheader">Observaciones</td>
                    </tr>
                    
                    <?php 
                        $total=0;
                        foreach($detalle as $prod){
                            $total = $total  + $prod->monto;
                            $class="tregistro";
                            
                            $class_persona = "tregistropersona";
                            $class_total = "tregistrototal";

                            if($prod->estado_id==ProdEstado::PENDIENTE){
                                $class="tregistroapendiente";
                                $class_total ="tregistroatotalpendiente";
                                $class_persona ="tregistroapersonapendiente";
                            }

                            if($prod->estado_id==ProdEstado::APROBADO_ENCARGADO){
                                $class="tregistroencargado";
                                $class_total ="tregistrototalencargado";
                                $class_persona ="tregistropersonaencargado";


                            }

                            if($prod->estado_id==ProdEstado::APROBADO){
                                $class="tregistroaprobado";
                                $class_total ="tregistrototalaprobado";
                                $class_persona ="tregistropersonaaprobado";
                            }


                       
                            ?>
                        <tr>
                            <td class="<?php echo $class?>" align="center">
                                
                                <div class="col-md-4">
                                    
                                    <h3>

                                    <?php 
                                    if($prod->estado_id==ProdEstado::PENDIENTE && $prod->personaCarga->persona_id == Yii::app()->user->personaId){

                                        $imghtml=CHtml::image(Yii::app()->baseUrl."/images/Edit-32.png","Editar Productividad",array("width"=>25,"title"=>"Editar Productividad"));

                                        echo CHtml::link(
                                                                '<span class="glyphicon glyphicon-edit "></span>',
                                                                
                                                                Yii::app()->createUrl("ingresoProductividades/editarProductividad",
                                                                                                 array("productividad_id"=>$prod->productividad_id,
                                                                                                       "persona_id"=>$persona->persona_id,
                                                                                                       "area_id"=>$prod->area_id,
                                                                                                       "periodo_id"=>$prod->periodo_id)),
                                                                 array( 'style'=>'cursor: pointer; text-decoration: underline;',
                                                                    'title'=>'Editar Productividad'
                                                                     )                                       
                                                        );

                                    ?>                    
                                    </h3>
                                </div>
                                <div class="col-md-4">
                                    <h3 >
                                <?php
                                    //$imghtml=CHtml::image(Yii::app()->baseUrl."/images/Delete_32.png","Borrar Productividad",array("width"=>20,"title"=>"Borrar Productividad"));
                    //                echo CHtml::link($imghtml,Yii::app()->createUrl('ingresoProductividades/borrarProductividad',array(
                    //                                     "productividad_id"=>$prod->productuvidad_id)));

                                    echo CHtml::link(
                                                            '<span class="glyphicon glyphicon-remove " style="color: red"></span>',
                                                            "",
                                                            array( 'style'=>'cursor: pointer; text-decoration: underline;',
                                                                    'title'=>'Eliminar Productividad',
                                                                    'onclick'=>'{eliminarProductividad("'.

                                                                            Yii::app()->createUrl("ingresoProductividades/borrarProductividad",
                                                                                                   array("productividad_id"=>$prod->productividad_id,
                                                                                                         "persona_id"=>$persona->persona_id,
                                                                                                         "periodo_id"=>$prod->periodo_id))

                                                                            .'"); 
                                                                            }'
                                                            )
                                                    );
                                }


                                if($prod->personaCarga->persona_id != Yii::app()->user->personaId){
                                    $class = "tregistroOtro";
                                }


                                ?>
                                    </h3>
                                </div>
                            </td>
                            <td class="<?php echo $class?>"><?php echo $prod->tipoProd->nombre_tipo_prod?></td>
                            <td class="<?php echo $class?>"><?php echo $prod->cc?></td>
                            <td class="<?php echo $class?>"><?php echo $prod->personaCarga?></td>
                            <td class="<?php echo $class?>"><?php echo $prod->cuota_actual?>/<?php echo $prod->numero_cuotas?></td>
                            <td class="<?php echo $class?>"><?php echo Yii::app()->format->formatNumber($prod->monto)?></td>
                            <td class="<?php echo $class?>"><?php echo Yii::app()->format->formatNumber($prod->total_monto_asignado)?></td>
                            <td class="<?php echo $class?>"><?php echo $prod->estado?></td>
                            <td class="<?php echo $class?>"><?php echo $prod->area?></td>
                            <td class="<?php echo $class?>"><?php echo $prod->personaEncargado?></td>
                            <td class="<?php echo $class?>"><?php echo $prod->fecha_encargado?></td>
                            <td class="<?php echo $class?>"><?php echo $prod->personaValida?></td>
                            <td class="<?php echo $class?>"><?php echo $prod->fecha_valida?></td>
                            <td class="<?php echo $class?>">
                                <textarea rows="2" style="width: 250px" readonly="true" disabled="true"><?php echo $prod->observaciones?></textarea>                
                            </td>

                        </tr>

                    <?php }?>
                        <tr>
                            <td colspan="4"></td>
                            <td class="tregistrototalcol">
                                <b>Total</b>
                            </td>
                            <td class="tregistrototalcol">
                                <b><?php echo Yii::app()->format->formatNumber($total)?></b>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
        </div>
        
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->    
            