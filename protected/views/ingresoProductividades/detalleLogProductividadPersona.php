<script>
    
    $(document).ready(function(){

        $('#volver').click(function () {
            //alert("historial");   
            
            var data=$("#productividad-cambio-estado-form").serialize();
            
            $.ajax({
                type: 'POST',
                url: '<?php echo $this->createUrl('ingresoProductividades/detalleProductividadPersona'); ?>',
                data:{
                    'persona_id':'<?php echo $persona->persona_id ?>',
                    'periodo_id':'<?php echo $periodo->periodo_id ?>'
                },
                success:function(data){
                    //alert(data); 
                    $('#preview').html(data); 


                  },
                error: function(data) { // if error occured
                      alert("Error occured.please try again");
                      alert(data);
                 },

                dataType:'html'
            });
    
        });
    });        
        
</script>

<div class="modal-dialog modal-info">
    <div class="modal-content">

        <div class="modal-header" style="background-color:#DEDEDE  ">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">
                <span class= "glyphicon glyphicon-lock"></span>
                Log Modificaciones Productividades <?php echo $persona ?></h4>
        
        </div>
        
        

        <div class="modal-body">
            <div class="row" >
                <div class="col-md-12">            

                    <h4>Período <b><?php echo $periodo->nombrePeriodo?> </b></h4>
                </div>
                
            </div>
            <div class="row" >
                <div class="col-md-12" style="overflow: auto">            

                    <table class="table table-bordered">
                    <tr>
                        <td class="tsubheader">Historial</td>
                        <td class="tsubheader">Persona Modifica</td>
                        <td class="tsubheader">Fecha</td>
                        <td class="tsubheader">Observaciones</td>
                    </tr>

                <?php 
                    foreach($listadoLogProductividades as $logProd){

                        $class="tregistro";

                        ?>
                    <tr>
                        <td class="<?php echo $class?>"><?php echo $logProd->historial_id?></td>
                        <td class="<?php echo $class?>"><?php echo $logProd->personaModifica?></td>
                        <td class="<?php echo $class?>"><?php echo Yii::app()->dateFormatter->format("dd MMM yyyy H:mm",strtotime($logProd->fecha_modificacion));?></td>
                        <td class="<?php echo $class?>"><?php echo $logProd->observaciones?></td>

                    </tr>

                <?php }?>

                </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
        </div>
        
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->    
            