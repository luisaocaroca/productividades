<?php
/* @var $this IngresoProductividadesController */

$this->breadcrumbs=array(
	'Ingreso Productividades',
);
?>
<h1>Menú Ingreso Productividades</h1>

<p>
<ul>
    <li>
        <?php 
            echo CHtml::link(
                        "Formulario Ingreso Productividades",
                        Yii::app()->createUrl("ingresoProductividades/carga")
                );

        ?>
    </li> 
    <li>
        <?php 
            echo CHtml::link(
                        "Coordinaciones",
                        Yii::app()->createUrl("coordinaciones/coordinaciones")
                );

        ?>
    </li> 
</ul>
</p>
