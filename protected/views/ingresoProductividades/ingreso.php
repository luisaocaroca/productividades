<?php
/* @var $this ProductividadFormController */
/* @var $model ProductividadForm */
/* @var $form CActiveForm */


?>
<?php

$this->breadcrumbs = array(
        'Ingreso Productividades',
	Yii::t('app', 'Carga Datos Área'),
);

$ancho_pagina = Yii::app()->session['ancho_pagina']-140;
?>

<script type="text/javascript">


    function afterAjax()
    {
        $.fancybox({
                href : '#preview',
                scrolling : 'no',
                transitionIn : 'fade',
                transitionOut : 'fade', 
                //check the fancybox api for additonal config to add here   
                onClosed: function() { 
                    $('#preview').html(''); 
                } //empty the preview div
        });
    }
    
    var _updatePaymentComment_url;
    function eliminarProductividad(_url)
    {
        if(window.confirm("¿Esta seguro(a) que desea borrar la productividad?")){
            
            // If its a string then set the global variable, if its an object then don't set
            if (typeof(_url)=='string')
                    _updatePaymentComment_url=_url;

            jQuery.ajax({
                    url: _updatePaymentComment_url,
                    'data':$(this).serialize(),
                    'type':'POST',
                    'success':function(data)
                            {
                                //alert("entro")
                                //alert(data)
                                $('#preview').html(data);
                                
                                <?php
                                    echo CHtml::ajax(array(
                                    'id'=>'actualiza_listado',
                                    'type'=>'POST',
                                    'url'=>Yii::app()->createUrl('ingresoProductividades/actualizaListaProductividades',
                                                                               array("es_academico"=>true,
                                                                                     "periodo_id"=>$periodo_id)),
                                    'update'=>'#lista_productividades_academicos',
                            //      'data'=>'js:jQuery(this).serialize()',  // only send element name, not whole form
                                    ));
                                ?>
                                                    
                                <?php
                                    echo CHtml::ajax(array(
                                    'id'=>'actualiza_listado',
                                    'type'=>'POST',
                                    'url'=>Yii::app()->createUrl('ingresoProductividades/actualizaListaProductividades',
                                                                               array("es_academico"=>false,
                                                                                     "periodo_id"=>$periodo_id)),
                                    'update'=>'#lista_productividades_no_academicos',
                            //      'data'=>'js:jQuery(this).serialize()',  // only send element name, not whole form
                                    ));
                                ?>
                                

                            } ,
                    'cache':false});
                
                
            return false;
        }

    }
    
    function actualizaPeriodo(){
        $("#ProductividadForm_actualiza_periodo").val(true);
        $('form#ProductividadForm').submit();
        
    
    }

    
    function actualizaCentroCosto(persona_id){
//        alert(persona_id)
        jQuery.ajax({
            url: "<?php echo $this->createUrl('ingresoProductividades/centroCostosPersonas')?>",
            data: { 
                persona_id: persona_id,
                area_id: "<?php echo $area_seleccionada_id?>"
            },
            'type':'POST',
            'success':function(data)
                    {
                        //alert("entro")
                        //alert(data)
                        $('#ProductividadForm_cc_id').html(data);



                    } ,
            'beforeSend':function(){                                                        
                $("#cargando_cc").addClass("ui-autocomplete-loading");
            },
            'complete':function(){       
                $("#cargando_cc").attr("class","");
            },                    
            'error':function(data){       
                alert(data);
            },          
            'cache':false});
    }
    
  

</script>

<div class="row">
    <div class="col-md-12">

        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'ProductividadForm',
                'enableAjaxValidation'=>false,
                'enableClientValidation'=>false,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
                'action' => $this->createUrl('ingresoProductividades/ingreso')
        ));?>



        <?php echo $form->hiddenField($model,'actualiza_periodo',array('type'=>"hidden","value"=>false)); ?>
        <?php echo $form->hiddenField($model,'editar_productividad',array('type'=>"hidden","value"=>$editar)); ?>

        <?php //echo $form->errorSummary($model); ?>

        <div class="row">
            <div class="col-xs-2">
                
                <?php echo $form->labelEx($model,'periodo_id'); ?>
                <?php echo $form->dropDownList($model,
                                               'periodo_id', 
                                               CHtml::listData($periodos,'periodo_id','nombre_periodo'),
                                               array('onchange'=>'{actualizaPeriodo();}',
                                                     'class'=>'form-control',
                                                     'style'=>'')
                                                );?>
                   

                <?php echo $form->error($model,'periodo_id'); ?>
            </div>
            
            <div class="col-xs-2">
                <?php if($es_administrador){?>
                    <?php echo $form->labelEx($model,'area_id'); ?>
                    <?php echo $form->dropDownList($model,
                                                   'area_id', 
                                                   CHtml::listData($listado_areas,'area_id','nm_area'),
                                                   array('onchange'=>'{actualizaPeriodo();}',
                                                         'class'=>'form-control',
                                                         'style'=>'')
                                                    );?>


                    <?php echo $form->error($model,'area_id'); ?>
                <?php }else{?>
                    <?php echo $form->hiddenField($model,'area_id',array('type'=>"hidden","value"=>$area_seleccionada_id)); ?>
                <?php }?>
            </div>
        </div>
        
        <br>   
        
        <div class="row">
            <div class="col-md-12">
                <?php
                    $flashMessages = Yii::app()->user->getFlashes();
                    if ($flashMessages) {
                        foreach($flashMessages as $key => $message) {
                            if($key=="error"){
                                echo BsHtml::alert(BsHtml::ALERT_COLOR_ERROR, BsHtml::bold('Atención!') . ' ' .  $message);
                            }

                            if($key=="success"){
                                echo BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, BsHtml::bold('Atención!') . ' ' .  $message);
                            }


                        }
                    }?>
            </div>
        </div>
        
        
        <?php if($puede_modificar){?>
        
        <?php
            $this->beginWidget('bootstrap.widgets.BsPanel', array(
            'title' => '<span class="glyphicon glyphicon-new-window"></span> <b>Ingresar Productividad</b>',
            'type' => BsHtml::PANEL_TYPE_PRIMARY,
        ));
            ?>



        <div class="row">
            <div class="col-md-12">

                
                <div class="row">                    
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group <?php if($form->error($model,'persona_id')){ ?> has-error has-feedback <?php }?>">
                                    <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'persona_id'); ?></label>
                                    
                                     <?php 
                                     $this->widget('zii.widgets.jui.CJuiAutoComplete', array(                       
                                           'attribute'=>'nombre_persona',
                                           'model'=>$model,
                                           'sourceUrl'=>array('ingresoProductividades/personaList'),
                                           'name'=>'nombre_persona',
                                           'id'=>'ProductividadForm_nombre_persona', 
                                           'options'=>array(
                                             'minLength'=>'1',
                                             'showAnim'=>'fold',
                                             'select'=>"js: function(event, ui) {
                                                 actualizaCentroCosto(ui.item['persona_id']);



                                                 $('#ProductividadForm_persona_id').val(ui.item['persona_id']);
                                             }",
                                             'change'=>"js: function(event, ui) {
                                                             if (!ui.item) {
                                                                $('#ProductividadForm_persona_id').val('');
                                                                $('#ProductividadForm_nombre_persona').val('');
                                                             }
                                                           }",
                                           ),
                                           'htmlOptions'=>array(
                                             //'size'=>40,
                                            // 'maxlength'=>45,
                                             'disabled'=>$editar,
                                             'class'=>'form-control',
                                           ),

                                     )); ?> 
                                    
                                    <?php if($form->error($model,'persona_id')){ ?>
                            
                                        <span class="red"><?php echo $form->error($model,'persona_id'); ?></span>
                                        <br>
                                    <?php }?>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <br>
                                <?php
                                //create a link
                                $imghtml=CHtml::image(Yii::app()->baseUrl.'/images/Add-male-user-32.png');
                                /*
                                echo CHtml::ajaxLink($imghtml,Yii::app()->createUrl('prodPersona/crearPersona'),
                                array('type'=>'POST', 'update'=>'#preview', 'complete'=>'afterAjax'));

                                //put fancybox on page
                                $this->widget('application.extensions.fancybox.EFancyBox', 
                                                 array(
                                                    'target' => '#various2',
                                                    'config' => array(
                                                        'scrolling' => 'yes',
                                                        'titleShow' => true,
                                                        'width'=>300
                                                    )
                                                ));*/
                                
                                    echo BsHtml::ajaxLink($imghtml, Yii::app()->createAbsoluteUrl('prodPersona/crearPersona'), array(
                                        'cache' => true,
                                        'type' => 'POST',
                                        'success' => 'js:function(data){
                                                    
                                                    $(".modal-body").html(data);
                                                    $("#crear_persona_modal").modal("show");
                                                }'
                                    ), array(
                                        //'icon' => BsHtml::GLYPHICON_BELL
                                    ));
                                    ?>
                            </div>

                        </div>
                        
                        <?php echo $form->hiddenField($model,'persona_id',array('type'=>"hidden")); ?>

                    </div>

                    
                    <div class="col-md-3">
                        <div class="form-group <?php if($form->error($model,'monto')){ ?> has-error has-feedback <?php }?>">
                            <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'monto'); ?></label>
                            <?php echo $form->textField($model,'monto',array('class'=>'form-control')); ?>
                            
                        </div>
                        
                        <?php if($form->error($model,'monto')){ ?>
                            
                            <span class="red"><?php echo $form->error($model,'monto'); ?></span>
                            <br>
                        <?php }?>
                        
                    </div>
                    
                    
                    <div class="col-md-1">
                        <div class="form-group <?php if($form->error($model,'numero_pagos')){ ?> has-error has-feedback <?php }?>">
                            <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'numero_pagos'); ?></label>
                            <?php echo $form->textField($model,'numero_pagos',
                                        array(
                                        'readonly'=>$editar,
                                        'class'=>'form-control'
                                        )); ?>
                            
                            <?php if($form->error($model,'numero_pagos')){ ?>
                            
                                <span class="red"><?php echo $form->error($model,'numero_pagos'); ?></span>
                                <br>
                            <?php }?>

                        </div>
                    </div>
                    
                </div>
                <div class="row">                      
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group <?php if($form->error($model,'cc_id')){ ?> has-error has-feedback <?php }?>">
                                    <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'cc_id'); ?></label>

                                    <?php echo $form->dropDownList($model,'cc_id', CHtml::listData($centros_costos,'cc_id','nombreCompleto'),
                                        array(
                                        'empty' => 'Seleccionar Tipo..',
                                        'disabled'=>$editar,
                                        'class'=>'form-control',
                                        'ajax' => array(
                                                            'type'=>'POST', //request type
                                                            'url'=>CController::createUrl('ingresoProductividades/tipoProductividad'), //url to call.
                                                            //Style: CController::createUrl('currentController/methodToCall')
                                                            'update'=>'#ProductividadForm_tipo_prod_id', //selector to update
                                                            'data'=>array('cc_id'=>'js:this.value','persona_id'=>'js:ProductividadForm_persona_id.value','area_id'=>$area_seleccionada_id),
                                                            //leave out the data key to pass all form values through
                                                            'beforeSend' => 'function(){                                                        
                                                                $("#cargando_tipos_prod").addClass("ui-autocomplete-loading");
                                                            }',
                                                            'complete' => 'function(){       
                                                                $("#cargando_tipos_prod").attr("class","");
                                                            }',
                                                        )
                                        )
                                        );?>
                                    
                                        <?php if($form->error($model,'cc_id')){ ?>
                            
                                            <span class="red"><?php echo $form->error($model,'cc_id'); ?></span>
                                            <br>
                                        <?php }?>
                                </div>
                            </div>
                            <div class="col-md-1 text-right" >
                                <div id="cargando_cc">
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                        
                        
                        <?php 
                        $cc_editar_id=0;
                        if($editar){
                            $cc_editar_id = $model->cc_id;
                        }
                        echo $form->hiddenField($model,'cc_editar_id',array('type'=>"hidden",'value'=>$cc_editar_id)); ?>
                        
                    </div>
                          
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-group <?php if($form->error($model,'tipo_prod_id')){ ?> has-error has-feedback <?php }?>">
                                    
                                    <label class="control-label" for="inputError2"><?php echo $form->labelEx($model,'tipo_prod_id'); ?></label>
                                

                                    <?php echo $form->dropDownList($model,'tipo_prod_id', CHtml::listData($tipo_productividades,'tipo_prod_id','nombre_tipo_prod'),
                                                                    array(
                                                                    'disabled'=>$editar,    
                                                                    'empty' => 'Seleccionar Tipo..',

                                                                    'class'=>'form-control',
                                                                    
                                                                    ));?>
                                    
                                    
                                    <?php if($form->error($model,'tipo_prod_id')){ ?>
                            
                                        <span class="red"><?php echo $form->error($model,'tipo_prod_id'); ?></span>
                                        <br>
                                    <?php }?>
                                </div>
                            </div>
                            <div class="col-md-1 text-right">
                                <div id="cargando_tipos_prod">
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                        

                        <?php 
                        $tipo_prod_editar_id=0;
                        if($editar){
                            $tipo_prod_editar_id = $model->tipo_prod_id;
                        }
                        echo $form->hiddenField($model,'tipo_prod_editar_id',array('type'=>"hidden",'value'=>$tipo_prod_editar_id)); ?>
                        
                    </div>
                    
                    
                    
                </div>
                
                
                <div class="row">                    
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-10">
                            <?php echo $form->labelEx($model,'observaciones'); ?>
                                <?php echo $form->textArea($model,"observaciones",array('class'=>'form-control')); ?>      
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <b><?php echo $form->error($model,"observaciones"); ?></b>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <br>
                <div class="row">                    
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-saved"></span> Guardar</button>
                            
                    </div>
                    
                </div>
                
                
                
                
                
            </div>
        </div>
        
        <?php
        $this->endWidget();
        ?>
        
        <?php }?>
            


        <?php $this->endWidget(); ?>

        </div>
</div>
<br/>


<div class="row">
    <div class="col-md-12">
    

<?php $form_consulta=$this->beginWidget('CActiveForm', array(
	'id'=>'ConsultasForm'
	
));?>
    
<?php echo $form->hiddenField($model_consulta,'periodo_id',array('type'=>"hidden","value"=>$model->periodo_id)); ?>
<?php echo $form->hiddenField($model_consulta,'area_id',array('type'=>"hidden","value"=>$model->area_id)); ?>




<?php 

$es_academico=false;
if($model_consulta->tipo_personas==1){ 
    $es_academico=true;
}

?>

<div id="lista_productividades_academicos" style="overflow: auto">
<?php $this->widget('ext.productividades.LProductividadesWidget',
                    array(
                        'area_id'=>$area_seleccionada_id,
                        'es_academico'=>$es_academico,
                        'periodo_cerrado'=>!$puede_modificar,
                        'es_ingreso'=>true,
                        'periodo_id'=>$model->periodo_id,                      
                        'detalle_por_tipo_productividad'=>false,
                        'form'=>$form_consulta,
                        'model'=>$model_consulta
                        )
                    ); ?>
</div>    
<br>
 
<?php $this->endWidget(); ?>


<!-- form -->
    </div>
</div>