<?php
/* @var $this WebsiteControllerController */

$this->breadcrumbs=array(
	'Website Controller',
);
?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<p>
	You may change the content of this page by modifying
	the file <tt><?php echo __FILE__; ?></tt>.
</p>


<?php if(Yii::app()->user->hasFlash('success')):?>
<div class="flash-notice">
<?php echo Yii::app()->user->getFlash('success')?>
</div>
<?php endif?>
<?php if(Yii::app()->user->hasFlash('error')):?>
<div class="flash-error">
<?php echo Yii::app()->user->getFlash('error')?>
</div>
<?php endif?>