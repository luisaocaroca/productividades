<?php
/* @var $this ProdPeriodoController */
/* @var $model ProdPeriodo */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'periodo_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'nombre_periodo',array('maxlength'=>10)); ?>
    <?php echo $form->textFieldControlGroup($model,'mes'); ?>
    <?php echo $form->textFieldControlGroup($model,'anio'); ?>
    <?php echo $form->textFieldControlGroup($model,'estado_periodo_id'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Buscar',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
