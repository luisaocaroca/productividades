<?php
/* @var $this ProdPeriodoController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Prod Periodos',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-tasks','label'=>Yii::t('app', 'Manage') . ' ' . ProdPeriodo::label(2), 'url' => array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Prod Periodos') ?>
<?php $this->widget('bootstrap.widgets.BsListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>