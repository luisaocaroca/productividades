<?php
/* @var $this ProdPeriodoController */
/* @var $data ProdPeriodo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('periodo_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->periodo_id),array('view','id'=>$data->periodo_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_periodo')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_periodo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mes')); ?>:</b>
	<?php echo CHtml::encode($data->mes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anio')); ?>:</b>
	<?php echo CHtml::encode($data->anio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado_periodo_id')); ?>:</b>
	<?php echo CHtml::encode($data->estado_periodo_id); ?>
	<br />


</div>