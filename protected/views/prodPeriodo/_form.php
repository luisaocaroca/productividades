<?php
/* @var $this ProdPeriodoController */
/* @var $model ProdPeriodo */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'prod-periodo-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-xs-3">
            <?php echo $form->labelEx($model,'estado_periodo_id'); ?>
            <?php echo $form->dropDownList($model, 'estado_periodo_id', GxHtml::listDataEx(ProdEstadoPeriodo::model()->findAllAttributes(null, true))); ?>
            <?php echo $form->error($model,'estado_periodo_id'); ?> 
        </div>
    </div>
    
    <br>

    <?php echo BsHtml::submitButton('Guardar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>

<?php $this->endWidget(); ?>
