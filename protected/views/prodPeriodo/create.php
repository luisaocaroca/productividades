<?php
/* @var $this ProdPeriodoController */
/* @var $model ProdPeriodo */
?>

<?php
$this->breadcrumbs=array(
	'Prod Periodos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage ProdPeriodo', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Nuevo','ProdPeriodo') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>