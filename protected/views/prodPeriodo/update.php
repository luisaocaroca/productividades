<?php
/* @var $this ProdPeriodoController */
/* @var $model ProdPeriodo */
?>

<?php
$this->breadcrumbs=array(
	'Prod Periodos'=>array('index'),
	$model->periodo_id=>array('view','id'=>$model->periodo_id),
	'Update',
);

$this->menu=array(
    
    array('icon' => 'glyphicon glyphicon-list-alt','label' => Yii::t('app', 'View') . ' ' . $model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
    array('icon' => 'glyphicon glyphicon-edit','label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->periodo_id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Actualizar',GxHtml::encode($model->label()) . ' '. GxHtml::encode(GxHtml::valueEx($model)) ) ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>