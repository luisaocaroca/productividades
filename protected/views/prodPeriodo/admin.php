<?php
/* @var $this ProdPeriodoController */
/* @var $model ProdPeriodo */


$this->breadcrumbs=array(
	'Prod Periodos'=>array('index'),
	'Administrador',
);

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-list','label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#prod-periodo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php echo BsHtml::pageHeader(Yii::t('app', 'Manage'),GxHtml::encode($model->label(2))) ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo BsHtml::button('Busqueda Avanzada',array('class' =>'search-button', 'icon' => BsHtml::GLYPHICON_SEARCH,'color' => BsHtml::BUTTON_COLOR_PRIMARY), '#'); ?></h3>
    </div>
    <div class="panel-body">
        <p>
            Se puede adicionalmente ingresar un operador de comparación (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
                &lt;&gt;</b>
            o <b>=</b>) al inicio de cada valor  a buscar..
        </p>

        <div class="search-form" style="display:none">
            <?php $this->renderPartial('_search',array(
                'model'=>$model,
            )); ?>
        </div>
        <!-- search-form -->

        <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'prod-periodo-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
        		'periodo_id',
		'nombre_periodo',
		'mes',
		'anio',
                array(
                            'name'=>'estado_periodo_id',
                            'value'=>'GxHtml::valueEx($data->estadoPeriodo)',
                            'filter'=>GxHtml::listDataEx(ProdEstadoPeriodo::model()->findAllAttributes(null, true)),
                          ),

				array(
					'class'=>'bootstrap.widgets.BsButtonColumn',
                                        'template'=>'{view} {update}'
				),
			),
        )); ?>
    </div>
</div>




