<?php
/* @var $this ProdPeriodoController */
/* @var $model ProdPeriodo */
?>

<?php
$this->breadcrumbs=array(
	'Prod Periodos'=>array('index'),
	$model->periodo_id,
);

$this->menu=array(
        array('icon' => 'glyphicon glyphicon-edit','label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->periodo_id)),
        array('icon' => 'glyphicon glyphicon-tasks','label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Vista',GxHtml::encode($model->label()) . ' '. GxHtml::encode(GxHtml::valueEx($model)) ) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'periodo_id',
		'nombre_periodo',
		'mes',
		'anio',
		'estado_periodo_id',
	),
)); ?>