<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('rol_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->rol_id), array('view', 'id' => $data->rol_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nombre_rol')); ?>:
	<?php echo GxHtml::encode($data->nombre_rol); ?>
	<br />

</div>