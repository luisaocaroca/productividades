<?php
/* @var $this ProdRol2Controller */
/* @var $model ProdRol */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'rol_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'nombre_rol',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'rol_intranet',array('maxlength'=>100)); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Buscar',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
