
<div class="row">
    <div class="col-md-6">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'prod-rol-form',
	'enableAjaxValidation' => false,
));
?>

    <div class="row">
        <div class="col-md-12">
	<p class="note">
		<?php echo Yii::t('app', 'field_with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>
        </div>
    </div>
    <div class="row">
	<?php echo $form->errorSummary($model); ?>

    </div>
        
    
    <div class="row">
        <div class="col-md-4">

            
                <?php echo $form->labelEx($model,'nombre_rol'); ?>
                <?php echo $form->textField($model, 'nombre_rol', array('maxlength' => 100,"class"=>"form-control")); ?>
                <?php echo $form->error($model,'nombre_rol'); ?>
            

            
                <?php echo $form->labelEx($model,'rol_intranet'); ?>
                <?php echo $form->textField($model, 'rol_intranet', array("class"=>"form-control")); ?>
                <?php echo $form->error($model,'rol_intranet'); ?>
            
                <br>
                <?php echo $form->labelEx($model,'bo_activo'); ?>
                <br>
                <?php echo $form->radioButtonList($model, 'bo_activo',array(1=>'Activo', 0=>'Inactivo'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>
                <?php echo $form->error($model,'bo_activo'); ?>

        </div>
        <div class="col-md-2">
            
        </div>
        <div class="col-md-6">
            <B><?php echo GxHtml::encode($model->getRelationLabel('prodSubMenus')); ?></B>
            <BR>
            <?php echo $form->checkBoxList($model, 'prodSubMenus', GxHtml::encodeEx(GxHtml::listDataEx(ProdSubMenu::model()->findAllAttributes(null, true)), false, true),array('separator'=>'<br>','labelOptions'=>array('style'=>'display:inline'))); ?>
       </div>
    </div>        
        
    <br>
    
    <div class="row">
        <div class="col-md-4">
            <?php echo BsHtml::submitButton('Guardar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
        </div>   
    </div>
        
	

    </div>  
</div>

<?php

$this->endWidget();
?>
