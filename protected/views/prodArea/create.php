<?php
/* @var $this ProdAreaController */
/* @var $model ProdArea */
?>

<?php
$this->breadcrumbs = array(
        'Mantenedores'=>array('/administrador/mantenedores'),   
	$model->label(2) => array('index'),
	Yii::t('app', 'Create'),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Ingreso',$model->label(2)) ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>