<?php
/* @var $this ProdAreaController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Prod Areas',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-plus-sign','label'=>Yii::t('app', 'Create') . ' ' . ProdArea::label(), 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>Yii::t('app', 'Manage') . ' ' . ProdArea::label(2), 'url' => array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Prod Areas') ?>
<?php $this->widget('bootstrap.widgets.BsListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>