<?php
/* @var $this ProdAreaController */
/* @var $model ProdArea */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'area_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'nm_area',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'bo_activo'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Buscar',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
