<?php
/* @var $this ProdAreaController */
/* @var $model ProdArea */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'prod-area-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <div class="col-xs-3">
            
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->labelEx($model,'nm_area'); ?>
                    <?php echo $form->textField($model, 'nm_area',array("class"=>"form-control",'maxlength'=>100)); ?>
                    <?php echo $form->error($model,'nm_area'); ?>
                </div>
 
                <div class="col-xs-6">
                    <?php echo $form->labelEx($model,'bo_activo'); ?>
                        <?php echo $form->radioButtonList($model, 'bo_activo',array(1=>'Activo', 0=>'Inactivo'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>
                        <?php echo $form->error($model,'bo_activo'); ?>
                </div>
            </div><!-- row -->
            
            <br>
            <?php echo BsHtml::submitButton('Guardar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
        </div>
        
    </div>
    

    <div class='row'>
        <div class="col-xs-12">
            <h3><span class="glyphicon glyphicon-list"></span> Relación Centro Costo/Tipo Productividades</h3>
            <br><br>
            <table class="table-hover table-striped">

                <?php $listado_cc = ProdCentroCosto::getCentrosCosto();
                for($j=0; $j<  count($listado_cc) ; $j=$j+2){
                    
                    ?>
                    <thead>
                    <tr class="active">
                        <th colspan="2" class="success">
                            <b><?php echo $listado_cc[$j]?></b>                            
                        </th>
                        <th>
                            <br>
                        </th>
                        <th colspan="2" class="success">
                            <?php if($j < count($listado_cc)-1 ) {?>
                                <b><?php echo $listado_cc[$j+1]?></b>                            
                            <?php }?>
                        </th>
                    </tr>
                    </thead>
                    <tr>
                        <td width="30px"></td>
                        <td>

                            <?php foreach($listado_cc[$j]->prodTipoProductividads as $tipo_prod){

                                if($tipo_prod->bo_activo==1){
                                    $checked=False;

                                    if(isset($relacionesArea)){
                                        foreach($relacionesArea as $relacion){
                                            if($relacion->cc_id==$listado_cc[$j]->cc_id &&
                                               $relacion->tipo_prod_id == $tipo_prod->tipo_prod_id    
                                                    ){
                                                $checked=true;
                                            }

                                        }
                                    }

                                    ?>

                                    <?php
                                        if($checked){
                                            echo $form->checkBox($model,'prodTipoProductividadCentroCosto['.$listado_cc[$j]->cc_id.'][]',  array('value'=>$tipo_prod->tipo_prod_id,'checked'=>'checked'));
                                        }else{
                                            echo $form->checkBox($model,'prodTipoProductividadCentroCosto['.$listado_cc[$j]->cc_id.'][]',  array('value'=>$tipo_prod->tipo_prod_id));
                                        }
                                    ?>
                                    <?php echo $tipo_prod?>
                                    <br>


                                <?php 

                                }
                            }?>

                        </td>
                        
                        <td width="30px"></td>
                        
                        <td>
                            <?php if($j < count($listado_cc)-1 ) {?>
                            <?php foreach($listado_cc[$j+1]->prodTipoProductividads as $tipo_prod){

                                if($tipo_prod->bo_activo==1){
                                    $checked=False;

                                    if(isset($relacionesArea)){
                                        foreach($relacionesArea as $relacion){
                                            if($relacion->cc_id==$listado_cc[$j+1]->cc_id &&
                                               $relacion->tipo_prod_id == $tipo_prod->tipo_prod_id    
                                                    ){
                                                $checked=true;
                                            }

                                        }
                                    }

                                    ?>

                                    <?php
                                        if($checked){
                                            echo $form->checkBox($model,'prodTipoProductividadCentroCosto['.$listado_cc[$j+1]->cc_id.'][]',  array('value'=>$tipo_prod->tipo_prod_id,'checked'=>'checked'));
                                        }else{
                                            echo $form->checkBox($model,'prodTipoProductividadCentroCosto['.$listado_cc[$j+1]->cc_id.'][]',  array('value'=>$tipo_prod->tipo_prod_id));
                                        }
                                    ?>
                                    <?php echo $tipo_prod?>
                                    <br>


                                <?php 

                                }
                            }
                            
                            }?>
                                    
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <br>
                        </td>
                    </tr>

                    <?php 

                }?>
                </table>  


        </div>
        
    </div>
    
    

<?php $this->endWidget(); ?>
