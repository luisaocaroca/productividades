<?php
/* @var $this ProdAreaController */
/* @var $data ProdArea */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('area_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->area_id),array('view','id'=>$data->area_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nm_area')); ?>:</b>
	<?php echo CHtml::encode($data->nm_area); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bo_activo')); ?>:</b>
	<?php echo CHtml::encode($data->bo_activo); ?>
	<br />


</div>