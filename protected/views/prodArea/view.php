<?php
/* @var $this ProdAreaController */
/* @var $model ProdArea */
?>

<?php
$this->breadcrumbs=array(
	'Prod Areas'=>array('index'),
	$model->area_id,
);

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id'=>$model->area_id)),
        array('icon' => 'glyphicon glyphicon-tasks','label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('admin')),
);
?>

<?php echo BsHtml::pageHeader(Yii::t('app', 'View') ,GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model))) ?>
<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'area_id',
		'nm_area',
		'bo_activo',
	),
)); ?>