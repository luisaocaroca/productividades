<?php
/* @var $this ProdAreaController */
/* @var $model ProdArea */
?>

<?php
$this->breadcrumbs=array(
	'Prod Areas'=>array('index'),
	$model->area_id=>array('view','id'=>$model->area_id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-plus-sign','label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>Yii::t('app', 'View') . ' ' . $model->label(), 'url'=>array('view', 'id'=>$model->area_id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('admin')),
);
?>

<?php echo BsHtml::pageHeader(Yii::t('app', 'Update') ,GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model))) ?>
<?php $this->renderPartial('_form', array('model'=>$model,
                'relacionesArea' =>$relacionesArea)); ?>