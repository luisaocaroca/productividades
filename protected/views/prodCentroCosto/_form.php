

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'prod-centro-costo-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="help-block">Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>
        <div class="row">
            <div class="col-md-6">
		<div class="row">
                    <div class="col-md-12">
                        <?php echo $form->labelEx($model,'cc_id'); ?>
                        <?php echo $form->textField($model, 'cc_id',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'cc_id'); ?>
                    </div>
		</div><!-- row -->
		<div class="row">
                    <div class="col-md-12">
                        <?php echo $form->labelEx($model,'nombre_cc'); ?>
                        <?php echo $form->textField($model, 'nombre_cc', array("class"=>"form-control",'maxlength' => 100)); ?>
                        <?php echo $form->error($model,'nombre_cc'); ?>
                    </div>
		</div><!-- row -->
                
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $form->labelEx($model,'bo_activo'); ?>
                        <?php echo $form->radioButtonList($model, 'bo_activo',array(1=>'Activo', 0=>'Inactivo'),array('separator'=>' ','labelOptions'=>array('style'=>'display:inline'))); ?>
                        <?php echo $form->error($model,'bo_activo'); ?>
                    </div>
		</div><!-- row -->
                <br>
                 <?php
                    echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class' => "btn btn-primary"));
                    $this->endWidget();
                    ?>

            </div>  
            <div class="col-md-6">
                <table width="100%">
                    <tr>
                        <td valign="top">
                            <fieldset>
                                <legend><?php echo GxHtml::encode($model->getRelationLabel('prodTipoProductividads')); ?></legend>
                                <?php echo $form->checkBoxList($model, 'prodTipoProductividads', GxHtml::encodeEx(GxHtml::listDataEx(ProdTipoProductividad::model()->findAllAttributes(null, true)), false, true),array('separator'=>'<br>','labelOptions'=>array('style'=>'display:inline'))); ?>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

       