<?php
/* @var $this ProdCentroCostoBoostrapController */
/* @var $model ProdCentroCosto */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'cc_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'nombre_cc',array('maxlength'=>100)); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Buscar',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
