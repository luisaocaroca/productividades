<?php
/* @var $this ProdCentroCostoBoostrapController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
        'Mantenedores'=>array('/administrador/mantenedores'),   
	ProdCentroCosto::label(2),
	Yii::t('app', 'Index'),
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Nuevo ProdCentroCosto', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Listado ProdCentroCosto', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Prod Centro Costos') ?>
<?php $this->widget('bootstrap.widgets.BsListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>