<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo Yii::app()->charset?>" />
	<meta name="language" content="en" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        
    
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/productividades.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />

        

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
        
        <?php 

        $cs = Yii::app()->clientScript;
        $themePath = Yii::app()->theme->baseUrl;
        
        /**
         * StyleSHeets
         */
        $cs
            ->registerCssFile($themePath.'/assets/css/bootstrap.css')
            ->registerCssFile($themePath.'/assets/css/bootstrap-theme.css');

        /**
         * JavaScripts
         */
        $cs
            ->registerCoreScript('jquery',CClientScript::POS_END)
            ->registerCoreScript('jquery.ui',CClientScript::POS_END)
            ->registerScriptFile($themePath.'/assets/js/bootstrap.min.js',CClientScript::POS_END)

            ->registerScript('tooltip',
                "$('[data-toggle=\"tooltip\"]').tooltip();
                $('[data-toggle=\"popover\"]').tooltip()"
                ,CClientScript::POS_READY);

        ?>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/html5shiv.js"></script>
            <script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/respond.min.js"></script>
        <![endif]-->
        
       
        
        
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.number.min.js" />

        <style>
            body { padding-top: 60px; }
            .footer {
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 40px;
                background-color: #f5f5f5;
                  margin-left: -20px;
                margin-right: -20px;
                padding-left: 20px;
                padding-right: 20px;

              }
              
              .modal.modal-wide .modal-dialog {
                width: 99%;
                min-height: 500px;
              }
              .modal-wide .modal-body {
                overflow-y: auto;
              }

           
              #detalle_productividad .modal-body p { margin-bottom: 900px }
              
              #loading-indicator {
                    position: absolute;
                    left: 45%;
                    top: 45%;
                    z-index: 9999;
                }

        </style>
<script>

// when .modal-wide opened, set content-body height based on browser height; 200 is appx height of modal padding, modal title and button bar
$(function($) {
    $(".modal-wide").on("show.bs.modal", function() {
      var height = $(window).height() - 200;
      $(this).find(".modal-body").css("max-height", height);
    });

});
function number_format (number, decimals, dec_point, thousands_sep) {
  // http://kevin.vanzonneveld.net
  // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +     bugfix by: Michael White (http://getsprink.com)
  // +     bugfix by: Benjamin Lupton
  // +     bugfix by: Allan Jensen (http://www.winternet.no)
  // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // +     bugfix by: Howard Yeend
  // +    revised by: Luke Smith (http://lucassmith.name)
  // +     bugfix by: Diogo Resende
  // +     bugfix by: Rival
  // +      input by: Kheang Hok Chin (http://www.distantia.ca/)
  // +   improved by: davook
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // +      input by: Jay Klehr
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // +      input by: Amir Habibi (http://www.residence-mixte.com/)
  // +     bugfix by: Brett Zamir (http://brett-zamir.me)
  // +   improved by: Theriault
  // +      input by: Amirouche
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // *     example 1: number_format(1234.56);
  // *     returns 1: '1,235'
  // *     example 2: number_format(1234.56, 2, ',', ' ');
  // *     returns 2: '1 234,56'
  // *     example 3: number_format(1234.5678, 2, '.', '');
  // *     returns 3: '1234.57'
  // *     example 4: number_format(67, 2, ',', '.');
  // *     returns 4: '67,00'
  // *     example 5: number_format(1000);
  // *     returns 5: '1,000'
  // *     example 6: number_format(67.311, 2);
  // *     returns 6: '67.31'
  // *     example 7: number_format(1000.55, 1);
  // *     returns 7: '1,000.6'
  // *     example 8: number_format(67000, 5, ',', '.');
  // *     returns 8: '67.000,00000'
  // *     example 9: number_format(0.9, 0);
  // *     returns 9: '1'
  // *    example 10: number_format('1.20', 2);
  // *    returns 10: '1.20'
  // *    example 11: number_format('1.20', 4);
  // *    returns 11: '1.2000'
  // *    example 12: number_format('1.2000', 3);
  // *    returns 12: '1.200'
  // *    example 13: number_format('1 000,50', 2, '.', ' ');
  // *    returns 13: '100 050.00'
  // Strip all characters but numerical ones.
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function (n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}


</script>
<script>
            
            function mostrarLoader(){
                var pos = $(window).scrollTop() +200;


                //show the menu directly over the placeholder
                $('#loading-indicator').css({
                    position: "absolute",
                    top: pos + "px"
                }).show();

            }
                    
            </script>
</head>

<?php 
$ancho_pagina = Yii::app()->session['ancho_pagina']-100;
?>
<body>
    
    <img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif" id="loading-indicator" style="width:150px; height: 150px;display:none" />
     
    
    <div class="row navbar-fixed-top" >
        
        <div class="col-md-12">
            
            <div class="row" style=" background-color: white">
                <div class="col-md-9">
                    
                </div>
                <div class="col-md-3 text-right">
                    <?php echo CHtml::image(Yii::app()->baseUrl.'/images/logo-departamento.png','',array('height'=>'60','width'=>'320')); ?>
                </div>
            </div>    

            <div class="row" style=" background-color: #C9E0ED; height: 4px">
                <div class="col-md-12">
                    <br>
                </div>
            </div>    
        </div>
    </div>

    <?php
    
        $this->widget('bootstrap.widgets.BsNavbar', array(
            'collapse' => true,
            'brandLabel' => BsHtml::icon(BsHtml::GLYPHICON_HOME),
            'brandUrl' => Yii::app()->homeUrl,
            'color' => BsHtml::NAVBAR_COLOR_INVERSE,
            'position' => BsHtml::NAVBAR_POSITION_STATIC_TOP,
            'htmlOptions' => array(
                    'containerOptions' => array(
                        'fluid' => true
                    ),
                ),
            'items' => array(
                array(
                    'class' => 'bootstrap.widgets.BsNav',
                    'type' => 'navbar',                    
                    'activateParents' => true,
                    'items' => Yii::app()->session['items']
                ),
                array(
                    'class' => 'bootstrap.widgets.BsNav',
                    'type' => 'navbar',
                    'activateParents' => true,
                    'items' => array(
                         
                        array(
                            'label' => !Yii::app()->user->isGuest ?  Yii::app()->user->nombreCompleto . " / " . Yii::app()->user->perfil : '',
                            'url' => array(
                                '#'
                            ),
                            'icon' => BsHtml::GLYPHICON_USER,
                            'items' =>array(
                                            array(  
                                                'label' => "Datos Usuario", 
                                                'url' => array(
                                                                '/site/datosUsuario'
                                                            ),
                                                'visible'=> !Yii::app()->user->isGuest,
                                                'icon' => BsHtml::GLYPHICON_USER,
                                                ) ,
                                            array(  
                                                'label' => "Salir", 
                                                'url' => array(
                                                    '/site/logout'
                                                ),
                                                'visible'=> !Yii::app()->user->isGuest,
                                                'icon' => BsHtml::GLYPHICON_LOG_OUT,
                                                )      
                                            )
                        ),
                    ),
                    'htmlOptions' => array(
                        'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT
                    )
                )

            )
        ));
        ?>
    
        <div class="container-fluid" style=" background-color: window; min-height: 580px">            
            <div class="row">
                <div class="col-md-12">
                <?php if(isset($this->breadcrumbs)):?>
                <?php $this->widget('zii.widgets.CBreadcrumbs', array (
                'links' => $this->breadcrumbs,
                'homeLink' => CHtml::link ('<span class=" glyphicon glyphicon-home"></span> Inicio', Yii::app()->homeUrl),
                )); ?><!-- breadcrumbs --><!-- breadcrumbs -->
                <?php endif?>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-md-12">
                    <?php echo $content; ?>
                </div>
            </div>
            <!--muestra error que captura el handle-->

           
            
        </div>
         
        <br>
        <br>
        
	<div class="footrow footer">
            <div class="container text-center">
                Desarrollado por &copy; <?php echo date('Y'); ?> Luis Toledo Acuña.<br/>
                <?php echo Yii::powered(); ?>
            </div>
	</div><!-- footer -->

        <div id="crear_persona_modal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Crear Persona</h4>
                        </div>
                        <div class="modal-body">
                            <p></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                          
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div><!-- /.modal -->
            
            
            <div id="detalle_productividad" class="modal modal-wide fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Detalle Productividades</h4>
                        </div>
                        <div class="modal-body">
                            <p></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                          
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div><!-- /.modal -->
            
            <div  id="modalDetalle" class="modal modal-wide fade">
    
            </div>
            
            <div  id="modalDetalle2" class="modal modal-wide fade">
    
            </div>
            
            
            
</body>
</html>
