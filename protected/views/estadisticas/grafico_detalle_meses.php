


<div id="grafico_detalle_centro_costo">

    <?php


        $this->Widget('ext.highcharts.HighchartsWidget', array(
           'id'=>"chartDetalleCentroCosto",
            
           'options'=>array(
               'chart' => array(
                 'type' => 'bar',
                 'height'=>620,
                //  'width'=>700,
              ), 
              'title' => array('text' => $titulo,
                               'margin'=>50),
               
               
              
            
            
              'xAxis' => array(
                   'labels' => array(
                      'rotation' => 0,
                      'useHTML'=> true
                      //'align'=>'right',
                   ),
                 'categories' => $datos_grafico_detalle["categorias"]
              ),
              'yAxis' => array(
                 'title' => array('text' => '$ Productividades Pagadas'),
                  'stackLabels' => array( 'enabled'=> true , 
                                          'style'=>array('fontWeight'=>'normal','color'=>"(Highcharts.theme && Highcharts.theme.textColor) || 'gray'"),
                                          'formatter' => 'js:function(){
                                                              var s = "$"+  number_format(this.total/1000000,2) + "M" ;


                                                        return s; }'
                                        ),
              ),
              
              'series' => $datos_grafico_detalle["datos"],
              
              //'legend' => $legend, 
              'plotOptions' => array(
                 'bar' => array('stacking'=> 'normal',
                                   'cursor' => 'pointer',
                                    'dataLabels' =>array('enabled'=>false),
                                   'point'  => array('events'=>array('click'=>'js:function(){
                                                        var drilldown = this.drilldown;
                                                        if (drilldown) { // drill down                                                    
                                                            setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                                        } else { // restore
                                                            setChart(name, categories, data);
                                                        }

                                                    }') )              
                                  ),

              ),   
              'theme' => 'grid',
              
              'credits' => array('enabled' => false),
              'tooltip' => array(
                        'formatter' => 'js:function(){var point = this.point,
                                                              s = this.x + "<br>";
                                                              s += "<b><span style=\"color:"+this.series.color+"\">"+this.series.name +"</span></b>";
                                                              s += ":<b> $ "+ number_format(this.y) +"</b><br>";
                                                              s += "<b><span style=\"color:"+this.series.color+"\">Total</span></b>";
                                                              s += ":<b> $ "+ number_format(this.point.stackTotal) +"</b><br>";


                                                        return s; }'
                      ),
           )
        ));





        ?>

</div>