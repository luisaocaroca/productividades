<?php
/* @var $this IngresoProductividadesController */

$this->breadcrumbs=array(
	'Estadísticas y Proyecciones',
);
?>
<h1>Menú Estadísticas y Proyecciones</h1>

<p>
<ul>
    <li>
        <?php 
            echo CHtml::link(
                        "Ir Estadísticas",
                        Yii::app()->createUrl("estadisticas/estadisticas")
                );

        ?>
    </li> 
    <li>
        <?php 
            echo CHtml::link(
                        "Ir Proyecciones",
                        Yii::app()->createUrl("estadisticas/proyecciones")
                );

        ?>
    </li> 
</ul>
</p>
