<?php
/* @var $this EstadisticasController */

$this->breadcrumbs=array(
	'Estadísticas',
);
?>


<?php

//Yii::app()->clientScript->registerScript('script', " 
//        var categories = ['" . implode("','", $categorias)."'];
//        var data = " . CJavaScript::encode($datos[0]['data']) .";"
//    , CClientScript::POS_END);

$cs = Yii::app()->clientScript;
        $themePath = Yii::app()->theme->baseUrl;

        $cs
            ->registerCssFile($themePath.'/assets/css/bootstrap.css')
            ->registerCssFile($themePath.'/assets/css/bootstrap-theme.css');

        
        $cs
            ->registerCoreScript('jquery',CClientScript::POS_END)
            ->registerCoreScript('jquery.ui',CClientScript::POS_END)
            ->registerScriptFile($themePath.'/assets/js/bootstrap.min.js',CClientScript::POS_END)

            ->registerScript('tooltip',
                "$('[data-toggle=\"tooltip\"]').tooltip();
                $('[data-toggle=\"popover\"]').tooltip()"
                ,CClientScript::POS_READY);
        
?>


<script>
    
var name = 'Productividad Agrupada';



//function setChart(name, categories, data, color) {
//    
////    alert("setChart");
////    alert(name);
////    alert(categories);
////    alert(data);
////    alert(color);
//
//
//        chartLuisao.xAxis[0].setCategories(categories, false);
//        chartLuisao.series[0].remove(false);
//        chartLuisao.addSeries({
//                name: name,
//                data: data,
//                color: color || '#4572A7'
//        }, false);
//        chartLuisao.redraw();
//}
    
    
$(document).ready(function(){  
    

            
    $("#button").click(function() {
        
        //alert("entro1")
        
        if ($(this).hasClass("hideseries")) {
            hs = true;
        } else {
            hs = false;
        }
        //alert("entro2")
        $(chartLuisao.series).each(function(idx, item) {
            //alert(item);
                if (hs) {
                    item.show();
                } else {
                    item.hide();
                }

        });
        $(this).toggleClass("hideseries");
    });
    
    $("#actualizaGrafico").click(function() {
        mostrarLoader();
        $("#ConsultasForm_exportar_excel").val(0);
        $("#productividad-form-consulta-form").submit();
    });
});
    

function actualizaTipoGrafico(){  
    mostrarLoader();
    $("#ConsultasForm_exportar_excel").val(0);
    $("#productividad-form-consulta-form").submit();
}

function mostrar(id){
        
    if(id==1){
       $("#grafico_detalle").css("display", "block");
       $("#tabla").css("display", "none");
    }else{
       $("#grafico_detalle").css("display", "none"); 
       $("#tabla").css("display", "block");
    }

}

function exportarExcel(){    
    //    mostrarLoader();
    $("#ConsultasForm_exportar_excel").val(1);
    $("#productividad-form-consulta-form").submit();
}

function ejcutar_consulta(){
    mostrarLoader();
   $("#ConsultasForm_exportar_excel").val(0);
   $("#productividad-form-consulta-form").submit(); 
}



function verDetalleAnio(vl_anio){
    mostrarLoader();
   $("#anio_id").val(vl_anio);
   $("#productividad-form-consulta-form").submit(); 
    

}



function verDetalle(vl_anio){
   mostrarLoader();
   var this_url= "<?php echo Yii::app()->createUrl('estadisticas/getDetalleGrafico');?>";
   
   
    $.ajax({
            data:  {
                'vl_anio' : vl_anio
            },
            url:   this_url,
            type:  'post',
            beforeSend: function () {
                
            },
            success:  function (response) {
                //alert(response);
                $("#grafico_detalle").html(response);
                
                $("#loading-indicator").hide();
            },
            error:  function (error) {
                var output = '';
                for (property in error) {
                  output += property + ': ' + error[property]+'; ';
                }
                alert(output);
            }
    });
    

}

function verDetalleTiposProductividades(vl_anio,cc_id){
   mostrarLoader();
   var this_url= "<?php echo Yii::app()->createUrl('estadisticas/getDetalleGraficoTipoProductividad');?>";
   
   
    $.ajax({
            data:  {
                'cc_id':cc_id,
                'vl_anio' : vl_anio
            },
            url:   this_url,
            type:  'post',
            beforeSend: function () {
                
            },
            success:  function (response) {
                //alert(response);
                $("#grafico_detalle").html(response);
                
                $("#loading-indicator").hide();
            },
            error:  function (error) {
                var output = '';
                for (property in error) {
                  output += property + ': ' + error[property]+'; ';
                }
                alert(output);
            }
    });
    

}


function verDetalleCentroCostoMes(vl_periodo){

   mostrarLoader(); 
   var this_url= "<?php echo Yii::app()->createUrl('estadisticas/getDetalleGraficoCentroCostoMes');?>";
   //alert(this_url);
   
    $.ajax({
            data:  {
                'vl_periodo' : vl_periodo
            },
            url:   this_url,
            type:  'post',
            beforeSend: function () {
                
            },
            success:  function (response) {
                //alert(response);
                $("#grafico_detalle").html(response);
                
                $("#loading-indicator").hide();
            },
            error:  function (error) {
                var output = '';
                for (property in error) {
                  output += property + ': ' + error[property]+'; ';
                }
                alert(output);
            }
    });
    

}

function verDetalleTiposProductividadesMes(vl_periodo,cc_id){

    mostrarLoader();
    var this_url= "<?php echo Yii::app()->createUrl('estadisticas/getDetalleGraficoTipoProductividadMes');?>";
   //alert(this_url);
   
    $.ajax({
            data:  {
                'cc_id':cc_id,
                'vl_periodo' : vl_periodo
            },
            url:   this_url,
            type:  'post',
            beforeSend: function () {
                
            },
            success:  function (response) {
                //alert(response);
                $("#grafico_detalle").html(response);
                
                $("#loading-indicator").hide();
            },
            error:  function (error) {
                var output = '';
                for (property in error) {
                  output += property + ': ' + error[property]+'; ';
                }
                alert(output);
            }
    });
    

}




function volverGraficoCentroCosto(){
    
    var vl_anio = $("#anio_id").val();
    verDetalle(vl_anio);
}



function verDetalleTabla(vl_anio){
    verDetalleAnio(vl_anio);
}


function verDetallePersonas(cc_id,tipo_prod_id,vl_periodo,vl_anio){
    mostrarLoader();
    var this_url= "<?php echo Yii::app()->createUrl('estadisticas/verDetallePersonas');?>";
   // alert(this_url);
   
    $.ajax({
            data:  {
                'cc_id':cc_id,
                'tipo_prod_id':tipo_prod_id,
                'vl_periodo' : vl_periodo,
                'vl_anio' : vl_anio,
            },
            url:   this_url,
            type:  'post',
            beforeSend: function () {
                
            },
            success:  function (response) {
                
                $('#detellePersonas').html(response);
                $('#modalDetallePersonas').modal();
                
                $("#loading-indicator").hide();
            },
            error:  function (error) {
                var output = '';
                for (property in error) {
                  output += property + ': ' + error[property]+'; ';
                }
                alert(output);
            }
    });
}
    
    
</script>


<style>
    .nav-tabs > li.active > a,
.nav-tabs > li.active > a:hover,
.nav-tabs > li.active > a:focus{
    color: #555555;
    background-color: #E5F1F4;  
} 


</style>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'productividad-form-consulta-form',
	
));?>
    
<?php echo $form->hiddenField($model,'exportar_excel',array('type'=>"hidden","value"=>0)); ?>
<?php echo $form->hiddenField($model,'anio_id',array('id'=>'anio_id', 'type'=>"hidden","value"=>$anio_id)); ?>

    
    <div class="row">

        <div class="col-md-4">
            <span class="glyphicon glyphicon-filter"></span>
            <b>Centro Costo</b> <br>

            <?php echo $form->dropDownList($model,'cc_id', CHtml::listData($centros_costos_combo,'cc_id','nombreCompleto'),
            array(
                'empty' => 'Todos..',
                'class'=>'form-control'
                )
            );?>
        </div>
        <div class="col-md-1">
            <span class="glyphicon glyphicon-calendar"></span>
            <b>Año Desde</b> <br>

            <?php echo $form->dropDownList($model,'anio_desde_id', CHtml::listData($anios,'id','value'),
                                                                   array("class"=>"form-control","style"=>"min-width:120px;width:120px;"));?>
        </div>
        <div class="col-md-2">
            <br>
            <button class="btn btn-info" type="button" id="actualizaGrafico">
                <span class=" glyphicon glyphicon-search"></span>
            </button>
        </div>

    </div>
    <br>

    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_a" data-toggle="tab">Gráficos</a></li>
      <li><a href="#tab_b" data-toggle="tab">Datos Resumen</a></li>
      <li><a href="#tab_c" data-toggle="tab">Comparación</a></li>
    </ul>
    <div class="tab-content" style="background-color: #E5F1F4; padding-left: 20px  ">
            <div class="tab-pane active" id="tab_a">
                <br>
                <div class="row">
                    <div class="col-md-12">

                        
                        <div class="row">
                            <div id="grafico" class="col-md-12">

                                <div  class="row" id="grafico_anio">
                                    <div class="col-md-6">


                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php

                                            $this->Widget('ext.highcharts.HighchartsWidget', array(
                                               'id'=>"chartAnuales",
                                               'options'=>array(
                                                  'title' => array('text' => $tituloGrafico),
                                                  'xAxis' => array(
                                                       'labels' => array(
                                                          'rotation' => 0,
                                                          'useHTML'=> true
                                                          //'align'=>'right',
                                                       ),
                                                     'categories' => $datos_grafico_resumen["categorias"]
                                                  ),
                                                  'yAxis' => array(
                                                     'title' => array('text' => '$ Productividades Pagadas')
                                                  ),      
                                                  'series' => $datos_grafico_resumen["datos"],
                                                  'chart' => array(
                                                     'type' => 'column',
                                                      'height'=>300,
                                                      //'width'=>"100%",
                                                  ), 
                                                  //'legend' => $legend, 
                                                  'plotOptions' => array(
                                                     'column' => array('stacking'=> 'normal',
                                                                       'cursor' => 'pointer',
                                                                       'point'  => array('events'=>array('click'=>'js:function(){
                                                                                            var drilldown = this.drilldown;
                                                                                            if (drilldown) { // drill down                                                    
                                                                                                setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                                                                            } else { // restore
                                                                                                setChart(name, categories, data);
                                                                                            }

                                                                                        }') )              
                                                                      ),

                                                  ),   
                                                  'theme' => 'grid',                                            
                                                  'credits' => array('enabled' => false),
                                                  'tooltip' => array(
                                                            'formatter' => 'js:function(){var point = this.point,
                                                              s = this.x + "<br>";
                                                              s += "<b><span style=\"color:"+this.series.color+"\">"+this.series.name +"</span></b>";
                                                              s += ":<b> $ "+ number_format(this.y) +"</b><br>";
                                                              s += "<b><span style=\"color:"+this.series.color+"\">Total</span></b>";
                                                              s += ":<b> $ "+ number_format(this.point.stackTotal) +"</b><br>";


                                                        return s; }'
                                                          ),
                                               )
                                            ));





                                            ?>
                                            </div>
                                        </div>
                                        <br>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php

                                            $this->Widget('ext.highcharts.HighchartsWidget', array(
                                               'id'=>"chartDetalleMeses",
                                               'options'=>array(
                                                  'title' => array('text' => $tituloGraficoDetalle),
                                                  'xAxis' => array(
                                                       'labels' => array(
                                                          'rotation' => 0,
                                                          'useHTML'=> true
                                                          //'align'=>'right',
                                                       ),
                                                     'categories' => $datos_grafico_detalle_anio["categorias"]
                                                  ),
                                                  'yAxis' => array(
                                                     'title' => array('text' => '$ Productividades Pagadas')
                                                  ),      
                                                  'series' => $datos_grafico_detalle_anio["datos"],
                                                  'chart' => array(
                                                     'type' => 'column',
                                                     'height'=>300,
                                                     //'width'=>500,
                                                  ), 
                                                  //'legend' => $legend, 
                                                  'plotOptions' => array(
                                                     'column' => array('stacking'=> 'normal',
                                                                       'cursor' => 'pointer',
                                                                       'point'  => array('events'=>array('click'=>'js:function(){
                                                                                            var drilldown = this.drilldown;
                                                                                            if (drilldown) { // drill down                                                    
                                                                                                setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                                                                            } else { // restore
                                                                                                setChart(name, categories, data);
                                                                                            }

                                                                                        }') )              
                                                                      ),

                                                  ),   
                                                  'theme' => 'grid',              
                                                  'credits' => array('enabled' => false),
                                                  'tooltip' => array(
                                                            'formatter' => 'js:function(){var point = this.point,
                                                              s = this.x + "<br>";
                                                              s += "<b><span style=\"color:"+this.series.color+"\">"+this.series.name +"</span></b>";
                                                              s += ":<b> $ "+ number_format(this.y) +"</b><br>";
                                                              s += "<b><span style=\"color:"+this.series.color+"\">Total</span></b>";
                                                              s += ":<b> $ "+ number_format(this.point.stackTotal) +"</b><br>";


                                                        return s;  }'
                                                          ),
                                               )
                                            ));





                                            ?>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-11">
                                                <div id="grafico_detalle">
                                                    <?php echo $vista_grafico_detalle?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>



                            </div>
                        </div>




                    </div>
                </div>            
            </div>
            <div class="tab-pane" id="tab_b">
                <br>
                <div class="row">
                    <div class="col-md-10">
                        <h3>Totales Anuales</h3>    
                    </div>
                    <div class="col-md-1 text-right">
                        <table style="padding-bottom: 5px">
                            <tr>

                                <td align="rigth">
                                    <?php






                                    echo CHtml::image(Yii::app()->baseUrl."/images/Excel_48.png", "Excel", array('onclick'=>'exportarExcel()',
                                                                                                                         'title'=>'Exportar Datos Excel',
                                                                                                                         'alt'=>'Exportar Datos Excel',
                                                                                                                         'style'=>'cursor: pointer; text-decoration: underline;'
                                                                                                                       ));



                                    ?>
                                </td>


                            </tr>

                        </table>
                    </div>
                </div>
                <br>
                        
                <div class="row">
                    <div class="col-md-11">

                    
                    <?php 
                    
                    if($cc_id==0){
                        echo "<h3>Resumen Anual</h3>";
                    }else{
                        echo "<h3>Resumen Anual Centro Costo $centro_costo</h3>";
                    }

                    $datos_grilla = $datos_grafico_resumen["datos_grilla"] ;
                    $this->widget('zii.widgets.grid.CGridView',
                                  array('dataProvider' => $datos_grilla["dataProvider"],
                                        'columns'=>$datos_grilla["columnas"]


                                      )
                                 );

                    ?>


                    <?php

                    if($cc_id==0){
                        echo "<h3>Detalle  Año $anio_id</h3>";
                    }else{
                        echo "<h3>Detalle  Año $anio_id Centro Costo $centro_costo</h3>";
                    }
                    
                    $datos_grilla = $datos_grafico_detalle_anio["datos_grilla"] ;
                    $this->widget('zii.widgets.grid.CGridView',
                                  array('dataProvider' => $datos_grilla["dataProvider"],
                                        'columns'=>$datos_grilla["columnas"]


                                      )
                                 );

                    
                    if($cc_id==0){
                        echo "<h3>Detalle Centro Costo $anio_id</h3>";
                    }else{
                        echo "<h3>Detalle Tipo Productividad $anio_id Centro Costo $centro_costo</h3>";
                    }
                    ?>

                    
                    <?php

                    $datos_grilla = $datos_grafico_detalle["datos_grilla"] ;
                    $this->widget('zii.widgets.grid.CGridView',
                                  array('dataProvider' => $datos_grilla["dataProvider"],
                                        'columns'=>$datos_grilla["columnas"]


                                      )
                                 );

                    ?>

                    </div> 
                </div>     
            </div>
        
            <div class="tab-pane" id="tab_c">
                <br>
                <div class="row">
                    
                    <div class="col-md-12">
                        <table class=" table table-bordered table-responsive table-condensed table-hover"  style="background-color:  #ffffff">
                            <thead>
                            <tr>
                                <th style="background-color: #E5F1F4; border-color: #E5F1F4 !important;"></th>
                                <?php 
                                $k=0;
                                foreach($anios_reporte as $anio){
                                    $colspan=4;
                                    if($k==0){
                                        $colspan=3;
                                    }
                                    ?>
                                    <th colspan="<?php echo $colspan?>"  class="theader text-center"><?php echo $anio["id"]?></th>
                                    <?php 
                                    $k++;
                                }?>
                            </tr>
                            <tr>
                                <th class="tsubheader">Centro Costo</th>
                                <?php 
                                    $i=0;
                                    foreach($anios_reporte as $anio){?>
                                    <?php foreach($tipos_persona as $tipo){?>
                                        <th class="tsubheader"><?php echo $tipo["nombre"]?></th>
                                    <?php }?>
                                    <th class="tsubheader">Total</th>
                                    <?php if($i!=0){?>
                                        <th class="tsubheader">Diferencia</th>
                                    <?php }?>
                                    <?php 
                                    $i++;
                                }?>
                            </tr>

                            </thead>
                            <tbody>
                                <?php foreach($centros_costos as $centro_costo){?>
                                <tr>
                                    <td class=" tregistropersona">
                                        <?php echo $centro_costo ?>
                                    </td>
                                    
                                    <?php 
                                    $k=0;
                                    $total_anterior=0;
                                    foreach($anios_reporte as $anio){
                                        $i=1;
                                        
                                        foreach($resumenComparacion as $resumen){

                                            if($resumen["cc_id"]== $centro_costo->cc_id &&
                                               $resumen["anio"]["id"]== $anio["id"] ){    
                                                $class="tregistro";

                                                if($i==3){
                                                    $class="tregistrototal";

                                                }


                                            ?>
                                            <td class="<?php echo $class?>">
                                                $<?php echo number_format($resumen["valor"])?>
                                            </td>

                                            <?php if($i==3){
                                                
                                                if($k!=0){
                                                    $diferencia = $resumen["valor"] - $total_anterior;
                                                    ?>

                                                    <td class="tregistrototalred">
                                                        $<?php echo number_format($diferencia)?>
                                                    </td>
                                                    <?php 
                                                }

                                                $total_anterior = $resumen["valor"];            
                                                $i=0;
                                            }?>

                                            <?php
                                            $i++;
                                            }

                                        }?>


                                    
                                        <?php 
                                    
                                        $k++;
                                    } ?>
                                                
                                    </tr>
                                <?php 
                                
                                }
                                ?>
                                <tr>
                                    <td><b>TOTAL</b></td>
                                    
                                    <?php 
                                    $k=0;
                                    foreach($anios_reporte as $anio){
                                        
                                        

                                        $total_anio = 0;
                                        ?>
                                        <?php foreach($tipos_persona as $tipo){?>
                                            
                                            <?php 
                                                $total = 0;
                                                foreach($resumenComparacion as $resumen){
                                                    
                                                    

                                                    if($resumen["anio"]["id"]== $anio["id"] &&
                                                       $resumen["tipo"]== $tipo["id"]
                                                      ){

                                                        $total = $total + $resumen["valor"];
                                                    }

                                                }
                                                $total_anio = $total_anio + $total;
                                                ?>
                                                <th class="tregistro"><b>$<?php echo number_format($total)?></b></th>    
                                        <?php }?>
                                        <th class="tregistrototal"><b>$<?php echo number_format($total_anio)?></b></th>  
                                        
                                        <?php if($k!=0){
                                            $diferencia_total = $total_anio-$total_anio_anterior;
                                            ?>
                                            <th class="tregistrototalred"><b>$<?php echo number_format($diferencia_total)?></b></th>  
                                        <?php }?>
                                    <?php 
                                        $total_anio_anterior = $total_anio;
                                    $k++;
                                    }?>

                                    
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            
    </div><!-- tab content -->
<?php
$this->endWidget();
?>


    
    

<div class="modal modal-wide" id="modalDetallePersonas">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Detalle Personas</h4>
            </div>
            <div class="modal-body" id="detellePersonas">
                
            </div>

        </div>

    </div>
</div>





