

    
    
<?php 

        $cs = Yii::app()->clientScript;
        $themePath = Yii::app()->theme->baseUrl;

          
        $cs

            ->registerScriptFile($themePath.'/assets/js/bootstrap.min.js',CClientScript::POS_END)


        ?>



 <?php


        $this->Widget('ext.highcharts.HighchartsWidget', array(
           'id'=>'chartTipoProductividad',
           'options'=>array(
              'title' => array('text' => 'Detalle Tipo Productividades <br>Centro Costo  <b> ' . $centroCosto .  '</b> <br>  Año ' . $vl_anio,
                               'margin'=>50),
              'xAxis' => array(
                   'labels' => array(
                      'rotation' => 0,
                      'useHTML'=> true
                      //'align'=>'right',
                   ),
                 'categories' => $datos_grafico_detalle_tipo_prod["categorias"]
              ),
              'yAxis' => array(
                 'title' => array('text' => '$ Productividades Pagadas'), 
                  'stackLabels' => array( 'enabled'=> true , 
                                          'style'=>array('fontWeight'=>'normal','color'=>"(Highcharts.theme && Highcharts.theme.textColor) || 'gray'"),
                                          'formatter' => 'js:function(){
                                                              var s = "$"+  number_format(this.total/1000000,2) + "M" ;


                                                        return s; }'
                                        ),
              ),      
              'series' => $datos_grafico_detalle_tipo_prod["datos"],
              'chart' => array(
                 'type' => 'bar',
                 'height'=>620,
                 //'width'=>'100%'
              ), 
              //'legend' => $legend, 
              'plotOptions' => array(
                 'bar' => array('stacking'=> 'normal',
                                   'cursor' => 'pointer',
                                   'point'  => array('events'=>array('click'=>'js:function(){
                                                        var drilldown = this.drilldown;
                                                        if (drilldown) { // drill down                                                    
                                                            setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                                        } else { // restore
                                                            setChart(name, categories, data);
                                                        }

                                                    }') )              
                                  ),

              ),   
              'theme' => 'grid',
              
              'credits' => array('enabled' => false),
              'tooltip' => array(
                        'formatter' => 'js:function(){var point = this.point,
                                                              s = this.x + "<br>";
                                                              s += "<b><span style=\"color:"+this.series.color+"\">"+this.series.name +"</span></b>";
                                                              s += ":<b> $ "+ number_format(this.y) +"</b><br>";
                                                              s += "<b><span style=\"color:"+this.series.color+"\">Total</span></b>";
                                                              s += ":<b> $ "+ number_format(this.point.stackTotal) +"</b><br>";
                                                            


                                                        return s; }'
                      ),
               'exporting'=>array(
                            'buttons'=>array(
                                'customButton'=>array(
                                    'symbol'=>'url(' . Yii::app()->baseUrl."/images/Back-321.png)",
                                    'x'=>25,
                                    'width'=>35,
                                    'height'=>35,
                                    'symbolX'=>1,
                                    'symbolY'=>1,
                                    'align'=>'left',
                                    'onclick'=>'js:function(){ volverGraficoCentroCosto() }',
                                )
                            )
               )
           )
        ));


