<?php
/* @var $this EstadisticasController */

$this->breadcrumbs=array(
	'Estadísticas y Proyecciones'=>array('/estadisticas'),
	'Proyecciones',
);
?>

<script type="text/javascript" src="/productividades/assets/e833d733/highcharts.src.js"></script>
<script type="text/javascript" src="/productividades/assets/e833d733/modules/exporting.src.js"></script>
<script type="text/javascript" src="/productividades/assets/e833d733/themes/grid.js"></script>



<script>

$(document).ready(function(){  
 
});
    

function radioChange(value){
    var disabled_c1 = false;
    var disabled_c2 = false;
    
    if(value!=1){
        disabled_c1 = true;
    }    
    
    if(value!=2){
        disabled_c2 = true;
    }
    
    $("#ProyeccionesForm_base_calculo_mensual").attr("disabled", disabled_c1);
    $("#ProyeccionesForm_base_calculo_anual").attr("disabled", disabled_c2);
}   

function mostrar(id){
        
    if(id==1){
       $("#grafico").css("display", "block");
       $("#tabla").css("display", "none");
    }else{
       $("#grafico").css("display", "none"); 
       $("#tabla").css("display", "block");
    }

}

function exportarExcel(){    
    $("#ProyeccionesForm_exportar_excel").val(1);
    $("#proyecciones-form-consulta-form").submit();
}

function ejcutar_consulta(){
   $("#ProyeccionesForm_exportar_excel").val(0);
   $("#proyecciones-form-consulta-form").submit(); 
}


</script>



    
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'proyecciones-form-consulta-form',
	
));?>
<?php echo $form->hiddenField($model,'exportar_excel',array('type'=>"hidden","value"=>0)); ?>


<div class="form">
<div  class="search-form">
<table class="tfiltro">
    <tr>
        <td colspan="4" class="tfiltro">
            <b>Seleccionar Tipo Proyección</b>
        </td>
    </tr>  
    <tr>
        <td colspan="4"  class="tfiltro">
            <hr>
        </td>
    </tr>
    <tr>
        
        <td class="tfiltro" width="1%">
            <?php echo $form->radioButton($model,   'tipo_proyeccion', 
                                                    array('value'=>1,
                                                          'checked'=>true,
                                                          'onchange' => 'radioChange(this.value);')); ?>
        </td>
        <td class="tfiltro" width="24%">
            <b>Proyección Mensual:</b> 
            <br>Proyecta a partir de la historia los costos 
            mensuales de las productividades del mes siguiente. 
            <br>
            Seleccionar la base con que se desea calcular.
        </td>
        <td class="tfiltro" width="1%"> 
            <?php echo $form->radioButton($model,   'tipo_proyeccion', 
                                                    array('value'=>2,
                                                          'uncheckValue'=>null,
                                                          'onchange' => 'radioChange(this.value);')); ?>
        </td>
        <td class="tfiltro" width="24%">
            <b>Proyección Anual:</b> 
            <br>Proyecta a partir de la historia los costos anuales de las productividades
            <br>
            Seleccionar la base con que se desea calcular.
            
        </td>
        
    </tr>
    <tr>
        <td class="tfiltro"></td>
        <td class="tfiltro">
            <?php echo $form->dropDownList($model,'base_calculo_mensual', CHtml::listData($base_calculo_mensual,'id','value'),
                                                                              array('disabled'=>$disabled_c1,
                                                                                    'empty' => 'Base para cálculo Mensual',));?>        
        </td>
        <td class="tfiltro"></td>
        <td class="tfiltro">
            <?php echo $form->dropDownList($model,'base_calculo_anual', CHtml::listData($base_calculo_anual,'id','value'),
                                                                              array('disabled'=>$disabled_c2,
                                                                                    'empty' => 'Base para cálculo Anual',));?>    
        </td>
    </tr>
    <tr>
        <td colspan="4"  class="tfiltro">
            <hr>
        </td>
    </tr>
</table>
    
<?php echo CHtml::Button('Ejecutar',array("onclick"=>"ejcutar_consulta()")); ?>
   
   
</div>
<?php echo $form->errorSummary($model); ?>

    
<?php $this->endWidget(); ?> 

    



<?php
if($valido){
    
    ?>
    
    <table>
        <tr>
            <td width="90%"></td>
            <td>
                <?php 
                echo CHtml::image(Yii::app()->baseUrl."/images/Bar Graph _48x48.png","Grafico", array('onclick'=>'mostrar(1)',
                                                                                                  'title'=>'Ver Como Gráfico',
                                                                                                  'alt'=>'Ver Como Gráfico',
                                                                                                  'style'=>'cursor: pointer; text-decoration: underline;'
                                                                                                   ));
                ?>
            </td>
            <td>
                <?php 
                echo CHtml::image(Yii::app()->baseUrl."/images/Table-48.png","Tabla", array('onclick'=>'mostrar(2)',
                                                                                                     'title'=>'Ver Como Tabla',
                                                                                                     'alt'=>'Ver Como Tabla',
                                                                                                     'style'=>'cursor: pointer; text-decoration: underline;'
                                                                                                   ));
                ?>
            </td>
            <td align="rigth">
                <?php
                

                
                
                
                
                echo CHtml::image(Yii::app()->baseUrl."/images/Excel_48.png", "Excel", array('onclick'=>'exportarExcel()',
                                                                                                     'title'=>'Exportar Datos Excel',
                                                                                                     'alt'=>'Exportar Datos Excel',
                                                                                                     'style'=>'cursor: pointer; text-decoration: underline;'
                                                                                                   ));
                


                ?>
            </td>
            
               
        </tr>
        
    </table>
    
    <div id="grafico">
    <?php
    $this->Widget('ext.highcharts.HighchartsWidget', array(
       'options'=>array(
          'title' => array('text' => 'Grafico Proyección Productividades'),
          'xAxis' => array(
               'labels' => array(
                  'rotation' => -45,
                  'align'=>'right',
               ),
             'categories' => $categorias
          ),
          'yAxis' => array(
             'title' => array('text' => '$ Productividades Pagadas')
          ),      
          'series' => $datos,
          'chart' => array(
             'type' => $tipo
          ), 
          'legend' => $legend,  
          'theme' => 'grid',
          'width'=>500,
          'credits' => array('enabled' => false),
          
       )
    ));
    ?>
        
    </div>
    
    <div id="tabla" style="display: none">
        
        <?php 


        
        $dataProvider=new CArrayDataProvider($datos_tabla, array(
            'keys'=>array('tipo_productividad'),
            'sort'=>array(
                'attributes'=>array(
                     'tipo_productividad', 'monto', 
                ),
            ),
            'pagination'=>array(
                'pageSize'=>20,
            ),
        ));

        $total=  0;
        foreach ($datos_tabla as $dato){
            $total = $total + $dato["monto"];
        }
        
        $tota_str = "<b>$" . Yii::app()->format->formatNumber($total)."</b>";
        
        
        $this->widget('zii.widgets.grid.CGridView',
                      array('dataProvider' => $dataProvider,
                            'columns'=>array(                                
                                array(
                                    'name'=>'tipo_productividad',
                                    'header'=>'Tipo Productividad',
                                    'value'=>'$data["tipo_productividad"]',
                                    'footer'=>'<b>Total</b>'
                                ),
                                array(
                                    'name'=>'monto',
                                    'header'=>'Monto',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto"]) ',
                                    'footer'=>$tota_str,

                                ),
                                

                            ),
                          
                          
                          )
                     );


        ?>
        
    </div>

<?php        
}
?>

    
</div>




