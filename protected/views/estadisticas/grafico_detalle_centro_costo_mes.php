
<div id="grafico_detalle_centro_costo">

    <?php


        $this->Widget('ext.highcharts.HighchartsWidget', array(
           'id'=>"chartDetalleCentroCosto",
           'options'=>array(
              'title' => array('text' => 'Detalle Tipo Productividades<br><br>Centro Costo  <b>Mes ' . $periodo->nombre_periodo . "</b>",
                               'margin'=>50),
              'xAxis' => array(
                   'labels' => array(
                      'rotation' => 0,
                      'useHTML'=> true,
                      'style'=>array(   'width'=>'120px',
                                        'min-width'=> '120px'
                                     ),
                      //'align'=>'right',
                   ),
                 'categories' => $datos_grafico_detalle_centro_costo["categorias"]
              ),
              'yAxis' => array(
                 'title' => array('text' => '$ Productividades Pagadas'),
                  'stackLabels' => array( 'enabled'=> true , 
                                          //'x'=>-10,
                                          //  'y'=>-30,
                                          'style'=>array('fontWeight'=>'normal','color'=>"(Highcharts.theme && Highcharts.theme.textColor) || 'gray'"),
                                          'formatter' => 'js:function(){
                                                              var s = "$"+  number_format(this.total/1000000,2) + "M" ;


                                                        return s; }'
                                        ),
              ),      
              'series' => $datos_grafico_detalle_centro_costo["datos"],
              'chart' => array(
                 'type' => 'bar',
                 'height'=>620,
                //  'width'=>700,
              ), 
              //'legend' => $legend, 
              'plotOptions' => array(
                 'bar' => array('stacking'=> 'normal',
                                   'cursor' => 'pointer',
                                   'minPadding'=>100,
                                   'point'  => array('events'=>array('click'=>'js:function(){
                                                        var drilldown = this.drilldown;
                                                        if (drilldown) { // drill down                                                    
                                                            setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                                        } else { // restore
                                                            setChart(name, categories, data);
                                                        }

                                                    }') )              
                                  ),

              ),   
              'theme' => 'grid',
              
              'credits' => array('enabled' => false),
              'tooltip' => array(
                        'formatter' => 'js:function(){var point = this.point,
                                                              s = this.x + "<br>";
                                                              s += "<b><span style=\"color:"+this.series.color+"\">"+this.series.name +"</span></b>";
                                                              s += ":<b> $ "+ number_format(this.y) +"</b><br>";
                                                              s += "<b><span style=\"color:"+this.series.color+"\">Total</span></b>";
                                                              s += ":<b> $ "+ number_format(this.point.stackTotal) +"</b><br>";
                                                            


                                                        return s; }'
                      ),
           )
        ));





        ?>

</div>