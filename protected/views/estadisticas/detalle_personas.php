
<div class="row">
        <div class="col-md-12">
        <?php

        $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'detalle-prod',    
            'dataProvider'=>$detalle,
            'ajaxUpdate'=>true, 
            'enableSorting' => false,
            'rowCssClassExpression'=>'$data->color',
            'columns'=>array(    
                array(
                    'name'=>'periodo_id',
                    'header'=>'Período',
                    'value'=>'$data->periodo'
                ),     
                array(
                    'name'=>'persona_id',
                    'header'=>'Persona',
                    'value'=>'$data->persona'
                ),     

                array(
                    'name'=>'cc_id',
                    'header'=>'Centro Costo',
                    'value'=>'$data->cc'
                ), 
                array(
                    'name'=>'tipo_prod_id',
                    'header'=>'Tipo Productividad',
                    'value'=>'$data->tipoProd'
                ),             
                array(
                    'name'=>'estado_id',
                    'header'=>'Estado',
                    'value'=>'$data->estado'
                ),
                array(
                    'name'=>'numero_cuotas',
                    'header'=>'Cuotas',
                    'value'=>'$data->cuota_actual ." de ". $data->numero_cuotas'
                ),
                array(
                    'name'=>'monto',
                    'header'=>'Monto',
                    'value'=>'"$" . Yii::app()->format->formatNumber($data->monto)'
                ),

                array(
                    'name'=>'total_monto_asignado',
                    'header'=>'Total Pago',
                    'value'=>'"$" . Yii::app()->format->formatNumber($data->total_monto_asignado)'
                ),
                'observaciones',
            ),
        ));

    ?>
        </div>
    </div>