<?php
/**
 * Controlador del mantenedor de la tabla ProdPersona
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller
 */
class ProdPersonaController extends GxController {

        /**
        * Define la función de control de acceso que tiene el controlador
        * 
        * @return array funciones que realizan el control de acceso
        */      
        public function filters()
        {
            return array(
                'accessControl',
            );
        }
        
        /**
        * Control de acceso del módulo
        * En este caso tienen acceso a las funcionalidades solo el administrador del sistema
        * 
        * @return array permisos de usuario al módulo
        */
	public function accessRules()
        {
            //SOLO EL USUARIO ADMINISTRADOR PUEDE ACCEDER
            return array(
                array('allow',
                      'roles' => array('admin'),
                ),                
                //UN USUARIO AUTENTICADO PUEDE ENTRAR AL FORMULARIO
                //SIMPLIFICADO  DE CREAR PERSONAS
                array('allow',
                      'actions'=>array('crearPersona'),
                      'users'=>array('@'),
                ), 
                array('deny',
                    'users'=>array('*'),
                ),
            );
        }  
        

       /**
        * Despliega el detalle de un Persona
        * 
        * @param int $id Persona ID
        * @return string despliega la vista prodPersona/view
        */        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ProdPersona'),
		));
	}

        /**
        * Despliega el formulario para crear un registro
        * 
        * @return string despliega la vista prodPersona/create
        */            
	public function actionCreate() {
		$model = new ProdPersona;


		if (isset($_POST['ProdPersona'])) {
			$model->setAttributes($_POST['ProdPersona']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->persona_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

        /**
        * Despliega el formulario para actualizar un registro
        * 
        * @param int $id ID del registro a actualizar
        * @return string despliega la vista prodPersona/update
        */            
	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ProdPersona');


		if (isset($_POST['ProdPersona'])) {
			$model->setAttributes($_POST['ProdPersona']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

        /**
        * Elimina un registros un registro
        * 
        * @param int $id ID del registro a borrar
        * @return string redirect prodPersona/admin
        */         
	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
                    
                    try{
                       $this->loadModel($id, 'ProdPersona')->delete();
                       Yii::app()->user->setFlash('success','Borrado Correctamente');
                       echo "<div class='flash-success'>Borrado Correctamente</div>"; //for ajax
                    }catch(CDbException $e){
                        Yii::app()->user->setFlash('error','No se puede borrar el registro porque tiene datos relacionados.');

                        echo "<div class='flash-error'>No se puede borrar el registro porque tiene datos relacionados.</div>"; //for ajax
                    }
                    if (!Yii::app()->getRequest()->getIsAjaxRequest())
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin')); 


		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

        /**
        * Despliega el index del mantenedor
        * 
        * @return string despliega la vista prodPersona/admin
        */           
	public function actionIndex() {
//		$dataProvider = new CActiveDataProvider('ProdPersona');
//		$this->render('index', array(
//			'dataProvider' => $dataProvider,
//		));
                $this->actionAdmin();            
	}

        /**
        * Despliega el index del mantenedor
        * 
        * @return string despliega la vista prodPersona/admin
        */           
	public function actionAdmin() {
		$model = new ProdPersona('search');
		$model->unsetAttributes();

		if (isset($_GET['ProdPersona']))
			$model->setAttributes($_GET['ProdPersona']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

        /**
        * Crea una persona (Academico o No Academico).
         * Esta funcion se llama desde el ingreso de productividades o coordinaciones
        * 
        * @return string despliega la vista prodPersona/crearPersona
        */        
        public function actionCrearPersona()
	{
            //Yii::app()->clientScript->scriptMap['*.js'] = false;
            $model=new ProdPersona;

            // uncomment the following code to enable ajax-based validation
            /*
            if(isset($_POST['ajax']) && $_POST['ajax']==='prod-admin-persona-crearPersona-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
            */
            $criteria_rol = new CDbCriteria();
            $criteria_rol->compare('rol_id', array(ProdRol::ROL_CONSULTA));
            $roles = ProdRol::model()->findAll($criteria_rol);

            if(isset($_POST['ProdPersona']))
            {
                $model->attributes=$_POST['ProdPersona'];
                if($model->validate())
                {
                    $model->rol_id = ProdRol::ROL_CONSULTA;
                    if($model->apellido_materno == null || empty($modal->apellido_materno) ){
                        $model->apellido_materno = "";
                    }
                    
                    // form inputs are valid, do something here
                    if ($model->save()) {
                        Yii::app()->user->setFlash('success', "Persona Creada correctamente");
                    }else{
                        Yii::app()->user->setFlash('error', "Error al crear la persona");
                    }
                    
                    $flashMessages = Yii::app()->user->getFlashes();
                    if ($flashMessages) {
                        foreach($flashMessages as $key => $message) {
                            echo '<div style="width:300px" class="flash-' . $key . '">' . $message . "</div>";
                        }
                    }

                    Yii::app()->end();

                }
            }
            
            $this->renderPartial('crearPersona',array('model'=>$model,'roles'=>$roles));
	}

}