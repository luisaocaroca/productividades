<?php
/**
 * Controlador Login Usuario.
 * 
 * Controla que el ingreso del usuario se realice a través de los parámetros de session
 * que se reciben desde la intranet del DCC.
 * Genera los datos del menú de la aplicación a partir del rol del usuario.
 * Controla la navegación luego de que la session del usuario expire.
 * Controla la navegación general del sistema a partir de los links de los menús y submenus
 * almacenados en la base de datos.
 * 
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller
 */

include("EstadisticasController.php");
include("ValidacionProductividadesController.php");
include("ProductividadesController.php");


class SiteController extends Controller
{
        private $_identity;
        public $rememberMe;
        
        
        public $url = "http://localhost/productividades";


        /**
        * punto de entrada al sistema, se realiza el login
        * 
        * Este es el punto de entrada al sistema y la página de inicio.
        * Si el usuario ingresa por primera vez se ejecuta el proceso de login.
        * En caso contrario se va solamente a la página de inicio
        * 
        * @return string the rendering result 
        */
	public function actionIndex()
	{
//                echo "<pre>";
//                print_r(Yii::app()->session['ancho_pagina']);
//                echo "</pre>";

                //login al sitio
                //SI NO ESTA LOGIADO HACER EL PROCESO DE LOGIN
            
                
            
                if(isset($_GET)){
                    if(isset($_GET["wp"])){
                        $wp = $_GET["wp"];
                        Yii::app()->session['ancho_pagina'] = $wp;
                    }
                }
                
                            
                if(Yii::app()->user->id==null)
		{
                    $this->cargaUsuario();
                }
                
                $form = new ConsultasForm;   
                
                $rol_id = Yii::app()->user->rolId;
                
                $anios = ProdPeriodo::getAnios();

                if(isset($_POST['ConsultasForm']))
                {
                    //para mantener el periodo seleccionado cuando se ejecuta el formulario

                    $form->attributes=$_POST['ConsultasForm'];                  
                    if(!$form->validate()){
                        $valido=false;
                    }

                    $anio_id = $form->anio_id;
                

                }else{
                    $anio_id = date("Y");  
                    $form->anio_id = $anio_id;

                }
                
                $periodos = ProdPeriodo::getPeriodosCargados();
                
                foreach($periodos as $periodo){
                    if($periodo->anio==$anio_id){
                        $vl_periodo=$periodo->periodo_id;
                        break;
                    }
                }
                
                //SI EL PERIODO NO EXISTE PQ CAMBIO DE AÑO
                if(!isset($vl_periodo)){
                    $periodo_actual_id = date("Ym");                
                    ProdPeriodo::verifica_crear_periodo($periodo_actual_id);
                    
                    $anios = ProdPeriodo::getAnios();
                    $periodos = ProdPeriodo::getPeriodosCargados();
                
                    foreach($periodos as $periodo){
                        if($periodo->anio==$anio_id){
                            $vl_periodo=$periodo->periodo_id;
                            break;
                        }
                    }
                }
               

                if($rol_id == ProdRol::ROL_CONSULTA){
                //if(true){
                    
                    //$vl_periodo =  date("Ym");  
                    $periodo = ProdPeriodo::getPeriodo($vl_periodo);
                    
                    $controller = new ProductividadesController('test');
                    $datos_grafico_detalle_anio = $controller->getDatosGraficoDetalleAnualPersona(Yii::app()->user->personaId,$anio_id); 
                    
                    $datos_grafico_detalle = $controller->getDatosCentroCostoMesPersona(Yii::app()->user->personaId,$vl_periodo);
                    
                    $resumenEstado = $controller->getResumenEstadoPersona(Yii::app()->user->personaId,$vl_periodo);
                    
                    $resumen = $this->renderPartial("resumenIndividuales", 
                                                    array( "datos_grafico_detalle_anio"=>$datos_grafico_detalle_anio,
                                                           "datos_grafico_centro_costo"=>$datos_grafico_detalle,
                                                           "resumenEstado"=>$resumenEstado,
                                                           "periodo"=>$periodo,
                                                           "anio_id"=>$anio_id,
                                                            "anios"=>$anios,
                                                            'model'=>$form,
                                                          ),
                                                    true);
                    
                }else{
                    
                    //FILTRAR POR AREA
                    $area_id = 0;
                    $area = ProdArea::model()->findByPk($area_id);
                    
                    if($rol_id == ProdRol::ROL_ENCARGADO || $rol_id == ProdRol::ROL_INGRESO_DATOS){
                        
                        
                        //FILTRAR POR AREA
                        $area_id = Yii::app()->user->areaId;
                        
                        $area = ProdArea::model()->findByPk($area_id);
                        
                        $controller = new EstadisticasController('test');
                        $datos_grafico_detalle_anio = $controller->getDatosGraficoDetalleAnual($anio_id,0,$area_id); 


                       // $vl_periodo =  date("Ym");  
                        $periodo = ProdPeriodo::getPeriodo($vl_periodo);



                        $datos_grafico_detalle = $controller->getDatosCentroCostoMes($vl_periodo,$area_id);



                        $controller2 = new ValidacionProductividadesController('test');
                        $resumenEstado = $controller2->getResumenEstado($vl_periodo,$area_id);
                        
                    }else{
                        $controller = new EstadisticasController('test');
                        $datos_grafico_detalle_anio = $controller->getDatosGraficoDetalleAnual($anio_id); 


                        //$vl_periodo =  date("Ym");  
                        $periodo = ProdPeriodo::getPeriodo($vl_periodo);



                        $datos_grafico_detalle = $controller->getDatosCentroCostoMes($vl_periodo);



                        $controller2 = new ValidacionProductividadesController('test');
                        $resumenEstado = $controller2->getResumenEstado($vl_periodo);
                    }
                    
                    $resumen = $this->renderPartial("resumenGlobales", array( "datos_grafico_detalle_anio"=>$datos_grafico_detalle_anio,
                                                                               "datos_grafico_detalle_centro_costo"=>$datos_grafico_detalle,
                                                                               "datos_grafico_centro_costo"=>$datos_grafico_detalle,
                                                                               "resumenEstado"=>$resumenEstado,
                                                                               "periodo"=>$periodo,
                                                                               "anio_id"=>$anio_id,
                                                                               "area"=>$area,
                                                                               "anios"=>$anios,
                                                                               'model'=>$form,
                                                                              ),
                                            true);
                    
                }
                
                    
                $tipo_ingreso = Yii::app()->request->cookies['tipo_ingreso'];
                if($tipo_ingreso=="1"){
                    unset(Yii::app()->request->cookies['tipo_ingreso']);
                    unset(Yii::app()->request->cookies['persona']);
                    $this->redirect('/productividades/index.php/validacionProductividades/index');
                }
                if($tipo_ingreso=="2"){
                    unset(Yii::app()->request->cookies['tipo_ingreso']);
                    unset(Yii::app()->request->cookies['persona']);
                    $this->redirect('/productividades/index.php/validacionProductividades/validaEncargado');
                }
                
                
                $this->render('index',array("resumen"=>$resumen));

	}
        
        public function actionDetalleMes(){
            $vl_periodo=$_POST['vl_periodo'];
            $periodo = ProdPeriodo::getPeriodo($vl_periodo);
            
            $rol_id = Yii::app()->user->rolId;
            
            $controller = new EstadisticasController('test');
            $controller2 = new ValidacionProductividadesController('test');
            
            if($rol_id == ProdRol::ROL_ENCARGADO || $rol_id == ProdRol::ROL_INGRESO_DATOS){
                        //FILTRAR POR AREA
                $area_id = Yii::app()->user->areaId;
                
                $datos_grafico_detalle = $controller->getDatosCentroCostoMes($vl_periodo,$area_id);
                $resumenEstado = $controller2->getResumenEstado($vl_periodo,$area_id);
                        
            }else{
                $datos_grafico_detalle = $controller->getDatosCentroCostoMes($vl_periodo);
                $resumenEstado = $controller2->getResumenEstado($vl_periodo);
            }
            
            
            $datos_grilla = $datos_grafico_detalle["datos_grilla"] ;
            
            
            
            
            $this->beginWidget('bootstrap.widgets.BsPanel', array(
                'title' => '<span class="glyphicon glyphicon-dashboard"></span>  Detalle Mes '  . $periodo->nombre_periodo,
                'type' => BsHtml::PANEL_TYPE_PRIMARY
            ));

            $this->widget('zii.widgets.grid.CGridView',
                                          array('dataProvider' => $resumenEstado["dataProvider"],
                                                'rowCssClassExpression'=>'$data["color"]',
                                                'columns'=>$resumenEstado["columnas"],
                                                'tagName'=>'Resumen Período' )
                                         );
            
            

            $this->widget('zii.widgets.grid.CGridView',
                          array('dataProvider' => $datos_grilla["dataProvider"],
                                'columns'=>$datos_grilla["columnas"]                          

                              )
                         );
            
            $this->endWidget();
        }
        
        
        public function actionDetalleMesIndividual(){
            
            $vl_periodo=$_POST['vl_periodo'];
            $periodo = ProdPeriodo::getPeriodo($vl_periodo);
            
            $controller = new ProductividadesController('test');

            $datos_grafico_detalle = $controller->getDatosCentroCostoMesPersona(Yii::app()->user->personaId,$vl_periodo);
            $resumenEstado = $controller->getResumenEstadoPersona(Yii::app()->user->personaId,$vl_periodo);
            
            $datos_grilla = $datos_grafico_detalle["datos_grilla"] ;            
           
            $this->beginWidget('bootstrap.widgets.BsPanel', array(
                'title' => '<span class="glyphicon glyphicon-dashboard"></span>  Detalle Mes '  . $periodo->nombre_periodo,
                'type' => BsHtml::PANEL_TYPE_PRIMARY
            ));

            $this->widget('zii.widgets.grid.CGridView',
                                          array('dataProvider' => $resumenEstado["dataProvider"],
                                                'rowCssClassExpression'=>'$data["color"]',
                                                'columns'=>$resumenEstado["columnas"],
                                                'tagName'=>'Resumen Período' )
                                         );
            
            

            $this->widget('zii.widgets.grid.CGridView',
                          array('dataProvider' => $datos_grilla["dataProvider"],
                                'columns'=>$datos_grilla["columnas"]                          

                              )
                         );
            
            $this->endWidget();
        }
        
        
        
        /**
        * Este método es llamado por el framework cuando se acaba la session del usuarios. 
        * 
        * Se realiza una carga del menú con datos básicos para que la aplicación no se caiga.
        *         
        * @return {@link redirect}  
        */
	public function actionDatosUsuario()
	{
            $model = new InformacionPersonaForm;   

            if(isset($_POST['InformacionPersonaForm']))
            {
                //para mantener el periodo seleccionado cuando se ejecuta el formulario

                $model->attributes=$_POST['InformacionPersonaForm'];   
                if($model->validate()){
                    $persona = ProdPersona::model()->findByAttributes(array('persona_id'=>Yii::app()->user->personaId));
                    $persona->email = $model->mail;
                    $persona->save();
                    Yii::app()->user->setFlash('success', "El mail fue modificado correctamente");

                    Yii::app()->getSession()->add('model', $persona);

                }


            }

            $this->render('datosUsuario',array('model'=>$model));
	}

        /**
        * Action para manejar excepciones externas
        * 
        *         
        * @return string 
        */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	
	/**
        * Este método es llamado por el framework cuando se acaba la session del usuarios
        * y se requiere hacer nuevamente el login. 
        * Cuando esto sucede se llama a {@link actionSinPermiso} para que muestre 
        * mensaje de error de session
        *         
        * @return string 
        */
	public function actionLogin()
	{
            $this->actionSinPermiso();
	}

	/**
        * Este método es llamado por el framework cuando se acaba la session del usuarios. 
        * 
        * Se realiza una carga del menú con datos básicos para que la aplicación no se caiga.
        *         
        * @return {@link redirect}  
        */
	public function actionLogout()
	{
            $items = array();

            $returnarray =array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest);
            array_push($items, $returnarray);

            //setea el menu en una variable session
            Yii::app()->session['items'] = $items;
            
            $this->_identity=null;
            
            Yii::app()->user->logout();
            
            $this->redirect("sinSession");
	}
        
        /**
         * Muestra mensaje de error cuando no se detecta las sessiones de usuario
         * correctas o cuando no se tiene permiso para ingresar a un controller-action
         * en particular
         *         
         * @param int $error_code ='' Codigo del Error
         * @param string $message ='' Mensaje del Error
         * @return string 
        */
        public function actionSinPermiso($error_code=0,$message="")
	{
            $this->render('error',array("code"=>$error_code,"message"=>$message));
        }
        
        /**
        * Muestra mensaje de error cuando no se detecta las sessiones de usuario
        * correctas. Estas sessiones corresponden a los datos entregados 
        * por la intranet de la escuela
        *         
        * @return string 
        */
        public function actionSinSession()
	{
            $items = array();

            //$returnarray =array('label'=>'Inicio', 'url'=>array('/site/sinSession'), 'visible'=>Yii::app()->user->isGuest);
            //array_push($items, $returnarray);

            //setea el menu en una variable session
            Yii::app()->session['items'] = $items;
                      
            $this->redirect('https://intranet.dcc.uchile.cl/');
            
            //$this->render('error',array("code"=>0,"message"=>"Debe ingresar al sistema desde la Intranet"));
        }
        
        /**
        * Redirecciona desde el menú horizontal del sistema al index del administrador
        *         
        * @return CController::redirect   
        */
        public function actionAdministrador()
	{
            $this->redirect(array('administrador/index', 'title'=>""));
	}
        
        /**
        * Redirecciona desde el menú horizontal del sistema al index de los mantenedores
        *         
        * @return CController::redirect   
        */
        public function actionMantenedores()
	{
            $this->redirect(array('administrador/Mantenedores', 'title'=>""));
	}
        
        /**
        * Redirecciona desde el menú horizontal del sistema al index de la consulta
        * de productividades
        *         
        * @return CController::redirect   
        */
        public function actionProductividades()
	{
            $this->redirect(array('productividades/index', 'title'=>""));
	}
        
        /**
        * Redirecciona desde el menú horizontal del sistema a la consulta
        * de productividades individuales
        *         
        * @return CController::redirect   
        */
        public function actionProductividadesIndv()
	{
            $this->redirect(array('productividades/individuales', 'title'=>""));
	}
        
        /**
        * Redirecciona desde el menú horizontal del sistema a la consulta
        * de productividades globales
        *         
        * @return CController::redirect   
        */
        public function actionProductividadesGlob()
	{
            $this->redirect(array('productividades/globales', 'title'=>""));
	}
        
        /**
        * Redirecciona desde el menú horizontal del sistema al index del 
        * ingreso de productividades
        *         
        * @return CController::redirect   
        */
        public function actionIngresoProductividades(){
            $this->redirect(array('ingresoProductividades/index', 'title'=>""));
        }
        
        /**
        * Redirecciona desde el menú horizontal del sistema al 
        * ingreso de productividades
        *         
        * @return CController::redirect   
        */
        public function actionCargaProductividades(){
            $this->redirect(array('ingresoProductividades/carga', 'title'=>""));
        }
        
        /**
        * Redirecciona desde el menú horizontal del sistema al 
        * ingreso de coordinaciones
        *         
        * @return CController::redirect   
        */
        public function actionCoordinaciones(){
            $this->redirect(array('coordinaciones/coordinaciones', 'title'=>""));
        }
        
        /**
        * Redirecciona desde el menú horizontal del sistema al index de 
        * las validaciones de las productividades
        *         
        * @return CController::redirect   
        */
        public function actionValidacionProductividades(){
            $this->redirect(array('validacionProductividades/index', 'title'=>""));
        }
        
        /**
        * Redirecciona desde el menú horizontal del sistema al index de 
        * las validaciones de las productividades
        *         
        * @return CController::redirect   
        */
        public function actionValidaEncargado(){
            $this->redirect(array('validacionProductividades/validaEncargado', 'title'=>""));
        }
        
        /**
        * Redirecciona desde el menú horizontal del sistema al index de 
        * las estadisticas y proyecciones
        *         
        * @return CController::redirect   
        */
        public function actionEstadisticasProyecciones(){
            $this->redirect(array('estadisticas/index', 'title'=>""));
        }
        
        /**
        * Redirecciona desde el menú horizontal al las estadisticas
        *         
        * @return CController::redirect   
        */
        public function actionEstadisticas(){
            $this->redirect(array('estadisticas/estadisticas', 'title'=>""));
        }
        
        /**
        * Redirecciona desde el menú horizontal al las estadisticas
        *         
        * @return CController::redirect   
        */
        public function actionProyecciones(){
            $this->redirect(array('estadisticas/proyecciones','title'=>""));
        }
        
        /**
        * Redirecciona desde el menú horizontal a los informes de productividades
        *         
        * @return CController::redirect   
        */
        public function actionInformes(){
            $this->redirect(array('informes/index', 'title'=>""));

        }        
        
        /**
        * Redirecciona desde el menú horizontal a la página de administración
        * de personas
        *         
        * @return CController::redirect    
        */
        public function actionPersonas()
	{
            $this->redirect(array('administrador/personas', 'title'=>""));
	}
        
        /**
        * Redirecciona desde el menú horizontal a la página de log productividades
        *         
        * @return CController::redirect    
        */
        public function actionLogProductividades()
	{
            $this->redirect(array('/prodLogProductividad/admin', 'title'=>""));
	}
        
        /**
        * Redirecciona desde el menú horizontal a la página de control periodos
        *         
        * @return CController::redirect    
        */
        public function actionPeriodos()
	{
            $this->redirect(array('/prodPeriodo/admin', 'title'=>""));
	}
        
        /**
        * verifica si la session de usuario que debe venir con el formato
        * de la intranet de la escuela es correcta.
        * Si es correcta devuelve un TRUE y guarda los datos del usuaario
        * en las variables de session del framework
        *         
        * @return bool
        */
        protected function get_datos_usuario(){
            //VARIABLES SE SESSION DEL USUARIO
            //TODO: BORRAR CUANDO ENTRA POR EL PROTAL DE LA U
            
            Yii::app()->session;
            
            
            
            if(isset($_SESSION["USERNAME"])){
                
                $intranet = $_SESSION["intranet"];
                
                //si no entro por intranet no lo deja entrar al sistemas
                if(!$intranet){
                    return false;
                }
                                
                $user_name = $_SESSION["USERNAME"];
                
                //Si no tiene el rut no lo deja entrar
                if(!isset($_SESSION["RUT"])){
                    return false;
                }
                $rut = $_SESSION["RUT"];
                $drut = $_SESSION["DRUT"];
                $nombre  = $_SESSION["NOMBRE"];
                $apellido_paterno = $_SESSION["APELLIDOPAT"];
                
                if(isset($_SESSION["APELLIDOMAT"])){
                    $apellido_materno = $_SESSION["APELLIDOMAT"];
                }else{
                    $apellido_materno = "";
                }
                
                if(isset($_SESSION["TELEFONO"])){
                    $telefono = $_SESSION["TELEFONO"];
                }else{
                    $telefono = 0;
                }
                
                if(isset($_SESSION["EMAIL"])){
                    $email = $_SESSION["EMAIL"];
                }else{
                    $email = "";
                }
                
                                
                
                $numero_sistemas = $_SESSION["numero_sistemas"];
                $IDSIS = $_SESSION["sistema_actual"];
                
                //RECORRO LOS ACCESOS DEL USUARIO
                //BUSCANDO EL SISTEMA DE PROD
                for($i=0;$i<$numero_sistemas;$i++){
                    
                    if(isset($_SESSION["rol_$i"])){
                        $id_sistema = $_SESSION["rol_$i"]["IDSIS"];

                        //ES EL SISTEMA DE PROD
                        if($id_sistema==$IDSIS){
                            $nombre_rol = $_SESSION["rol_$i"]["nombre"];
                        }
                    }
                }
                
                Yii::app()->session['prod_username'] = $user_name;
                Yii::app()->session['prod_rut'] = $rut;
                Yii::app()->session['prod_drut'] = $drut;
                Yii::app()->session['prod_nombre'] = $nombre;
                Yii::app()->session['prod_apellido_paterno'] = $apellido_paterno;
                Yii::app()->session['prod_apellido_materno'] = $apellido_materno;
                Yii::app()->session['prod_email'] = $email;
                Yii::app()->session['prod_telefono'] = $telefono;
                //VARIABLES SE SESSION DEL SISTEMA
                Yii::app()->session['nombre_rol'] = $nombre_rol;
                Yii::app()->session['id_sistema'] = $IDSIS;
                
                return true;
            
            }else{
                return false;
            }

             
        }
        
    public function ActionSendMail()
    {   
        date_default_timezone_set("America/Santiago");
        
        $personas = ProdPersona::model()->findAll();
        
        foreach($personas as $persona){
        
            if($persona->email!=""){
                
                echo "enviando email:" . $persona->email .PHP_EOL;
            
                    
                $persona_id = $persona->persona_id ;
                $periodo_fin_id = date("Ym") ;            
                $periodo_id  = date("Ym",strtotime('-12 month'));

                $periodo = ProdPeriodo::getPeriodo($periodo_id);
                $periodo_fin = ProdPeriodo::getPeriodo($periodo_fin_id);     


                $model = new ProdProductividad();
                $model->persona_id = $persona_id;
                $model->estado_id =  ProdEstado::TODOS;

                if($periodo_fin_id!=0){
                    $route = "productividades/detalleProductividadPersona/persona_id/".$persona_id."/periodo_id/".$periodo_id."/periodo_fin_id/".$periodo_fin_id. "/";
                }else{
                    $route = "productividades/detalleProductividadPersona/persona_id/".$persona_id."/periodo_id/".$periodo_id . "/";
                }
                
                $detalle = ProdProductividad::getProductividadesIndividualesDataProvider($model, $periodo_id,$periodo_fin_id,$route,true,100);

                try{

                    $message            = new YiiMailMessage;
                       //this points to the file test.php inside the view path
                    $message->view = "resumenProductividades";

                    $params              = array('persona'=>$persona,
                                                 'periodo'=>$periodo,
                                                 'periodo_fin'=>$periodo_fin,
                                                 'model'=>$model,
                                                 'detalle'=>$detalle,
                                                );
                    $message->subject    = 'Resumen Productividades ' . $persona->nombre_completo();
                    $message->setBody($params, 'text/html');                
                    //$message->addTo('ltoledo@impsys-am.com');
                    $message->setTo(array($persona->email, $persona->email =>  $persona->nombre_completo()));
                    $message->setFrom(array('luistoledo@ltamaster.com' => 'Sistema Productividades'));
                    //$message->from = 'ltoledo.acuna@gmail.com';   
                    Yii::app()->mail->send($message);   

                    echo "mail enviado email:" . $persona->email.PHP_EOL;
                }catch(Exception $e){
                    echo "error:" . $e->getMessage().PHP_EOL;
                }
                     
            }
        }
    }
    
    public function ActionEnviaMailProductividadesPendientes()
    {   
        date_default_timezone_set("America/Santiago");
        
        $areas = ProdArea::getAreasFiltrada();
        
        $url =$this->url;
        
        foreach($areas as $area){
        
            $personas = ProdPersona::getEncargadosArea($area->area_id);
            
            $model = new ProdProductividad();
            $model->area_id = $area->area_id;
            $model->estado_id =  ProdEstado::PENDIENTE;
            $route = "productividades/productividadesPendientes/area_id/".$area->area_id;
            
            $listadoProductividadesArea = ProdProductividad::getProductividadesPendientesDataProvider($model,$route);
            
            if(count($listadoProductividadesArea->data)>0){

                foreach($personas as $persona){

                    if($persona->email!=""){

                        try{
                            echo "enviando email:" . $persona->email.PHP_EOL;
                            $url =$this->url . "/index.php/site/ingresoMail/tipo_ingreso/2";

                            $message            = new YiiMailMessage;
                               //this points to the file test.php inside the view path
                            $message->view = "pageListadoProductividadesPendientesEncargado";

                                $params              = array('area'=>$area,
                                                             'model'=>$model,
                                                             'detalle'=>$listadoProductividadesArea,
                                                            "url"=>$url
                                                        );
                            $message->subject    = 'Productividades Pendientes ' . $area->nm_area;
                            $message->setBody($params, 'text/html');                
                            //$message->addTo('ltoledo@impsys-am.com');
                            $message->setTo(array($persona->email, $persona->email =>  $persona->nombre_completo()));
                            $message->setFrom(array('luistoledo@ltamaster.com' => 'Sistema Productividades'));
                            //$message->from = 'ltoledo.acuna@gmail.com';   
                            Yii::app()->mail->send($message);   
                        }catch(Exception $e){
                            echo "error:" . $e->getMessage().PHP_EOL;
                        }
                    }
                }
            }else{
                echo "no hay productividades pendientes para el area: " . ( $area->nm_area ).PHP_EOL;
            }
        }
    }
    
    
    public function ActionEnviaMailProductividadesPendientesJefeAdministrativo()
    {   
        date_default_timezone_set("America/Santiago");
        

        $personas = ProdPersona::getJefesAdministrativos();

        $model = new ProdProductividad();
        $model->estado_id =  ProdEstado::APROBADO_ENCARGADO;
        $route = "productividades/productividadesPendientesJefeAdministrativo";

        
        
        $listadoProductividades = ProdProductividad::getProductividadesPendientesDataProvider($model,$route);
                

        if(count($listadoProductividades->data)>0){
        
            foreach($personas as $persona){

                if($persona->email!=""){

                    try{
                        echo "enviando email:" . $persona->email.PHP_EOL;

                        $url =$this->url . "/index.php/site/ingresoMail/tipo_ingreso/1";
                        
                        $message            = new YiiMailMessage;
                           //this points to the file test.php inside the view path
                        $message->view = "pageListadoProductividadesPendientesJefeAdministrativo";

                        $params              = array('model'=>$model,
                                                     'detalle'=>$listadoProductividades,
                                                     "url"=>$url
                                                    );
                        $message->subject    = 'Productividades Pendientes Validación Jefe Administrativo';
                        $message->setBody($params, 'text/html');                
                        //$message->addTo('ltoledo@impsys-am.com');
                        $message->setTo(array($persona->email, $persona->email =>  $persona->nombre_completo()));
                        $message->setFrom(array('luistoledo@ltamaster.com' => 'Sistema Productividades'));
                        //$message->from = 'ltoledo.acuna@gmail.com';   
                        Yii::app()->mail->send($message);   
                    }catch(Exception $e){
                        echo "error:" . $e->getMessage().PHP_EOL;
                    }
                }
            }
        }else{
            echo "no hay productividades pendientes".PHP_EOL;
        }
    }
    
    
    
    
    public function cargaUsuario(){
        $estado = $this->get_datos_usuario();    
                    
        //si no tiene seteada la session con el formato de la intranet
        //del DCC se sale
        if($estado){

            //Yii::log($message, $level, $category);
            $username = Yii::app()->session['prod_username'];

            //SE REALIZA LA AUTENTICACION DEL USUARIO 
            //VERIFICA SI PUEDE INGRESAR AL SISTEMA
            $this->_identity=new UserIdentity($username);

            $this->_identity->rut = Yii::app()->session['prod_rut'];
            $this->_identity->drut = Yii::app()->session['prod_drut'];
            $this->_identity->nombre = Yii::app()->session['prod_nombre'];
            $this->_identity->apellido_paterno = Yii::app()->session['prod_apellido_paterno'];
            $this->_identity->apellido_materno = Yii::app()->session['prod_apellido_materno'] ;
            $this->_identity->email = Yii::app()->session['prod_email'];
            $this->_identity->telefono = Yii::app()->session['prod_telefono'];
            $this->_identity->nombre_rol = Yii::app()->session['nombre_rol'];        

            $this->_identity->authenticate();

            //SI DA UN ERROR DE INGRESO VA AL INICIO
            if($this->_identity->errorCode!=UserIdentity::ERROR_NONE){
                $items = array();

                //$returnarray =array('label'=>'Inicio', 'url'=>array('/site/index'), 'visible'=>Yii::app()->user->isGuest);
               // array_push($items, $returnarray);


                //setea el menu en una variable session
                Yii::app()->session['items'] = $items;

                $this->actionSinPermiso($this->_identity->errorCode,$this->_identity->errorMessage);

            }else{
                //SI TIENE PERMISO PARA INGRESA SE REALIZA EL LOGIN
                $duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
                Yii::app()->user->login($this->_identity,$duration);

                Yii::log("Usuario loggiado: " . Yii::app()->user->id, "info", "application.controllers.SiteController");

                $this->render('index_get_size');
            }

        }else{
            //MUESTA MENSAJE ERROR DE QUE NO TIENE SESSION PARA INGRESAR
            $this->actionSinSession();
        }
    }
        
    public function ActionIngresoMail($tipo_ingreso){
                        
        Yii::app()->request->cookies['tipo_ingreso'] = new CHttpCookie('tipo_ingreso', $tipo_ingreso);
        
        if(Yii::app()->user->id==null)
        {
            $this->actionSinSession();
        }else{
            $this->actionIndex();
        }
        
        
    }    
}