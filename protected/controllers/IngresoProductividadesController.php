<?php
/**
 * Controlador del módulo de ingreso de productividades
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller
 */
class IngresoProductividadesController extends SecureController
{
        /**
        * Control de acceso del módulo
        * En este caso tienen acceso a las funcionalidades 
        * los usuarios Ingreso (encargados) y  Validadores (jefe academico)
        * El administrador por defecto puede ver todos los módulos
        * 
        * @return array permisos de usuario al módulo
        */
	public function accessRules()
        {
            //SOLOS LOS USUARIOS CON PERMISO INGRESO O VALIDACION PUEDEN ENTRAR
            return array(
                array('allow',
                      'roles' => array('ingreso','validador'),
                ),
                array('deny',
                    'users'=>array('*'),
                ),
            );
        }
        
        /**
        * Funcionalidad para mostrar el listado de academicos en formato autocomplete
        * 
        * @return array listado academicos
        */        
        public function actions()
        {
          return array(
            'personaList'=>array(
              'class'=>'application.controllers.ingresoProductividades.PersonaAutoCompleteAction',
            ),
            'academicos'=>array(
              'class'=>'application.controllers.ingresoProductividades.AcademicosAutoCompleteAction',
            ),
          );
        }
  
        /**
        * Despliega el  menu principal del modulo
        * 
        * @return string despliega la vista ingresoProductividades/index
        */         
	public function actionIndex()
	{
		$this->actionCarga();
	}
        
        /**
        * Despliega la pagina de carga de productividades
        * Primero verifica si se debe o no crear el período actual en la base de datos
        * 
        * @return string despliega la vista ingresoProductividades/ingreso
        */         
        public function actionCarga()
	{
            
            //PRIMER PASO ES BUSCAR LOS PERIODOS ABIERTOS
            //SE TOMA EL MES ACTUAL Y SE VERIFICA SI EXISTE EN LA TABLA PERIODOS
            //SI NO EXISTE SE CARGA EL REGISTRO
            $periodo_actual_id = date("Ym");                
            ProdPeriodo::verifica_crear_periodo($periodo_actual_id);
                        
            //va al formulario de ingreso
            $this->actionIngreso();
	}
        
        /**
        * Despliega la pagina de carga de productividades        * 
        * 
        * @return string despliega la vista ingresoProductividades/ingreso
        */           
        public function actionIngreso()
        {
            
            
            $periodos = ProdPeriodo::getPeriodosCargados();
            
            foreach($periodos as $periodo){
                $periodo_id = $periodo->periodo_id;
                break;
            }
            
            $centros_costos = array();
            $tipo_productividades= array();
            $tipo_persona_carga = ProdPersona::TIPO_PERSONA_ACADEMICO;
            
            
            $rol_id = Yii::app()->user->rolId;
            
            $listado_areas = ProdArea::getAreasFiltrada();
            
            $es_administrador = false;
            if($rol_id == ProdRol::ROL_ADMINISTRDOR || $rol_id == ProdRol::ROL_JEFE_ADMINISTRATIVO){
                $es_administrador = true;
                
                foreach($listado_areas as $area){
                    $area_seleccionada_id = $area->area_id;
                    break;
                }
                
            }else{
                $area_seleccionada_id = Yii::app()->user->areaId;
            }
            
            

            //DATOS FORMULARIO CONSULTA
            //SE USA PARA CONTROLAR LA GRILLA
            $model_consulta = new ConsultasForm;
            
            if(isset($_POST['ConsultasForm']))
            {
                //para mantener el periodo seleccionado cuando se ejecuta el formulario
                $model_consulta->attributes=$_POST['ConsultasForm']; 
                
                if(isset($model_consulta->periodo_id)){
                    $periodo_id = $model_consulta->periodo_id;
                } 
                
                if(isset($model_consulta->area_id)){
                    $area_seleccionada_id = $model_consulta->area_id;
                } 
                
                
                
            }else{
                $tipo_personas = 1;
                $model_consulta->tipo_personas = $tipo_personas;
            }
            
            
            //FIN FORMULARIO CONSULTA
            
            $model=new ProductividadForm("nuevo");
            
                        
            
            if(isset($_POST['ProductividadForm']))
            {
                $model->attributes=$_POST['ProductividadForm'];
                
                //echo "tipo_prod: " . $model->tipo_prod_id;
                
                $actualiza_periodo = $model->actualiza_periodo;
                //para mantener el periodo seleccionado cuando se ejecuta el formulario
                $periodo_id = $model->periodo_id;
                $area_seleccionada_id = $model->area_id;
                
                $editar_productividad = $model->editar_productividad;
                
                if(!$actualiza_periodo){
                    
                    

                    //cuando se edita el combo de tipo productividad se desabilita, por lo tanto hay que tomar el valor de otro lado
                    if($editar_productividad){
                        $cc_id = $model->cc_editar_id;
                        $tipo_prod_id = $model->tipo_prod_editar_id;      
                    }else{
                        $cc_id = $model->cc_id;
                        $tipo_prod_id = $model->tipo_prod_id;
                    }                    

                    $productividad_cargada = ProdProductividad::buscarProductividadUsuario($model->persona_id, $model->periodo_id, $cc_id, $tipo_prod_id,Yii::app()->user->personaId);

                    $model->setScenario("nuevo");
                    if($editar_productividad){
                        $model->setScenario("actualizar");                            
                    }
                    

                    if($model->validate())
                    {
                        //SE USA POR SI LA CARGA FALLA (POR EJEMPLO QUE EXISTA) 
                        //PARA QUE MUESTRE LOS COMBOS CON LOS VALORES QUE TENIA
                        $centros_costos = $this->getCentrosCostoAreaCarga($area_seleccionada_id);

                        $cc_id = $model->cc_id;
                        $persona_id = $model->persona_id;

                        if($cc_id > 0){
                            $tipo_productividades = $this->getTiposProductividadesCentroCosto($persona_id, $cc_id,$area_seleccionada_id);
                        }

                        ///////////////////////////////////////////////////////////

                        $persona = ProdPersona::getPersona($model->persona_id);
                        $tipo_persona_carga = ProdPersona::getRolIngresoProductividades($persona);
                            
                        if(!is_null($productividad_cargada)){
                            
                            if(!$editar_productividad){
                                Yii::app()->user->setFlash('error', "Productividad ya existe, puede modificarla a través del boton actualizar" );
                            }else{
                            
                                //VUELVO A BUSCAR LA PRODUCTIVIDAD ORIGINAL
                                //PARA COMPARARLA CON LOS CAMBIOS
                                //NO ME FUNCIONO CLONARLA
                                $productividad_original = ProdProductividad::buscarProductividadUsuario($model->persona_id, $model->periodo_id, $cc_id, $tipo_prod_id,Yii::app()->user->personaId);
                                
                                $productividad_cargada->monto= round($model->monto/$model->numero_pagos,0);
                                $productividad_cargada->total_monto_asignado= $model->monto;
                                $productividad_cargada->estado_id=0;
                                $productividad_cargada->observaciones = $model->observaciones;
                                $productividad_cargada->area_id = $area_seleccionada_id;
                                
                             

                                if ($productividad_cargada->save()) {
                                    Yii::app()->user->setFlash('success', "Productividad Actualizada Correctamente");
                                    $model=new ProductividadForm;
                                    $centros_costos = array();
                                    
                                    $differences = $productividad_original->compare($productividad_cargada);
                                
                                    if(!empty($differences)){
                                        $log_modificaciones = new ProdLogProductividad();
                                        $log_modificaciones->productividad_id = $productividad_cargada->productividad_id;
                                        $log_modificaciones->periodo_id = $productividad_cargada->periodo_id;
                                        $log_modificaciones->persona_id = $productividad_cargada->persona_id;
                                        $log_modificaciones->persona_modifica_id = Yii::app()->user->personaId;
                                        $log_modificaciones->fecha_modificacion = new CDbExpression('NOW()');
                                        

                                        $string_diferencias="";
                                        foreach($differences as $key=>$value){        
                                            
                                            $nuevo="";
                                            if($key =="monto" || $key=="total_monto_asignado" ){
                                                $nuevo = Globals::aesDecrypt( $value["new"]);
                                            }else{
                                                $nuevo = $value["new"];
                                            }
                                                
                                            $string_diferencias .= "Modificación campo " . $key . "<br>valor antiguo: " . $value["old"] . "<br>valor nuevo " . $nuevo . "<br><br>";
                                        
                                        }
                                        
                                        $log_modificaciones->observaciones = $string_diferencias;
                                        $log_modificaciones->save();
                                        
                                        //ENVIAR MAIL A LA PERSONA QUE CARGO ORIGINALMENTE
                                        if(Yii::app()->user->personaId ==$productividad_original->persona_carga_id){
                                            
                                             $personaCarga = $productividad_original->personaCarga;
                                            
                                             $message            = new YiiMailMessage;
                                             $message->view      = "detalleMailModificacionProductividad";

                                             $params              = array('persona'=>$productividad_original->persona,
                                                                          'personaCarga'=>$personaCarga,
                                                                          'log_modificaciones'=>$log_modificaciones
                                                                         );
                                             $message->subject    = 'Modificación Productividades ' . $productividad_original->persona;
                                             $message->setBody($params, 'text/html');                
                                             //$message->addTo('ltoledo@impsys-am.com');
                                             $message->setTo(array($personaCarga->email, $personaCarga->email =>  $personaCarga->nombre_completo()));
                                             $message->setFrom(array('luistoledo@ltamaster.com' => 'Sistema Productividades'));
                                             //$message->from = 'ltoledo.acuna@gmail.com'; 
                                             
                                             try{
                                                Yii::app()->mail->send($message);   
                                             }catch(Exception $e){
                                                 Yii::app()->user->setFlash('error', "No se pudo enviar mail de aviso por modificacion: " . $e->getMessage() );
                                             }
                                            
                                        }
                                        

                                    }
                                    
                                    

                                }else{
                                    Yii::app()->user->setFlash('error', "Productividad no se actualizó" );
                                }
                            }
                        }else{

                            for($i=0;$i<$model->numero_pagos; $i++){
                                
                                
                                $productividad = new ProdProductividad();
                                
                                if($i==0){
                                   $productividad->periodo_id = $model->periodo_id;
                                }else{
                                   $productividad->periodo_id = $this->obtiene_periodo_siguiente($model->periodo_id, $i);                                  
                                    ProdPeriodo::verifica_crear_periodo($productividad->periodo_id);
                                   
                                }
                                
                                $productividad->persona_id = $model->persona_id;
                                $productividad->tipo_prod_id= $model->tipo_prod_id;
                                $productividad->cc_id = $model->cc_id;
                                $productividad->monto= round($model->monto/$model->numero_pagos,0);
                                $productividad->numero_cuotas=$model->numero_pagos;
                                $productividad->cuota_actual=($i+1);
                                $productividad->total_monto_asignado=$model->monto;
                                $productividad->persona_carga_id = Yii::app()->user->personaId;
                                $productividad->estado_id=0;
                                $productividad->observaciones = $model->observaciones;
                                $productividad->area_id = $area_seleccionada_id;
                                
                                if ($productividad->save()) {
                                    Yii::app()->user->setFlash('success', "Productividad Almacenada Correctamente");
                                    

                                }else{
                                    Yii::app()->user->setFlash('error', "Productividad no se cargó (cuota " . ($i+1) . ")" );
                                }
                            }
                            
                           
                        
                            //LIMPIA EL FORMULARIO AL FINAL DEL PROCESO
                            $model=new ProductividadForm;
                            $centros_costos = array();
                        }
                        
                        if($tipo_persona_carga==  ProdPersona::TIPO_PERSONA_ACADEMICO){
                            $model_consulta->tipo_personas = 1;
                        }else{
                            $model_consulta->tipo_personas = 2;
                        }
                        

                    }else{
                        $errores = $model->getErrors();
                        
                        $persona_id = $model->persona_id;
                        Yii::app()->user->setFlash('error', "Error formulario, debe ingresar todos los campos de forma correcta" );
                        
                        if($persona_id>0){
                            $centros_costos = $this->getCentrosCostoAreaCarga($area_seleccionada_id);
                            
                            $cc_id = $model->cc_id;
                            
                            if($cc_id > 0){
                                $tipo_productividades = $this->getTiposProductividadesCentroCosto($persona_id, $cc_id,$area_seleccionada_id);
                            }
                        }
                        
                        
                    }
                }
            }else{
                $model->area_id = $area_seleccionada_id;
            }
                        
            
            
            foreach($periodos as $periodo){
                if($periodo->periodo_id==$periodo_id){
                    $periodo_seleccionado = $periodo;
                }
            }
            
            $puede_modificar=false;
            $mensaje="";
            if($periodo_seleccionado->estado_periodo_id==ProdPeriodo::PERIODO_ABIERTO){
                $puede_modificar=true;
                $dia_actual = date("d"); 
                $month_end = strtotime('last day of this month', time());
                $dia_fin_mes =  date('d', $month_end);
                $restantes = $dia_fin_mes-$dia_actual;
                
                if($restantes<7){
                    $mensaje = "Quedan $restantes días para el cierre del mes, luego no podrá seguir cargando datos a este período";
                    Yii::app()->user->setFlash('error', $mensaje);
                    
                }
            }else{
                Yii::app()->user->setFlash('error', "Período cerrado, no se puede ingresar Productividades" );
            }
            
            //si el jefe administrativo la lista será grande por lo que el ancho debe ser mayor
            
            $model->periodo_id = $periodo_id;
            $model_consulta->periodo_id = $periodo_id;
            

            
            $data = array(  'model'=>$model,
                            'periodos' => $periodos,
                            'tipo_productividades'=>$tipo_productividades,
                            'centros_costos'=>$centros_costos,
                            'periodo_id'=>$periodo_id,
                            'puede_modificar'=> $puede_modificar,
                            'editar'  => false,
                            'mensaje'=>$mensaje,
                            'model_consulta'=>$model_consulta,
                            'rol_carga'=>$tipo_persona_carga,
                            'listado_areas'=>$listado_areas,
                            'es_administrador'=>$es_administrador,
                            'area_seleccionada_id'=>$area_seleccionada_id
                         );

            
             
            $this->render('ingreso',$data);
        }
        
        /**
         * Obtiene el listados de centros de costo asociados a un tipo de productividad
         * Se usa para actualizar a través de AJAX la etiqueta SELECT de centros de costo
         * del formulario de ingreso de las productividades.
         * 
         * @return string Listado de centros de costo en formato de etiqueta HTML SELECT
         */           
        public function actionCentroCostos()
        {
            
            $tipo_prod_id = $_POST['ProductividadForm']['tipo_prod_id'];            
                        
            $listado_cc_tipos_prod_rol = ProdRolTipoProdCC::getCentrosCostosTiposProductividadesRol(Yii::app()->user->rolId,true);
               
            $centros_costo = array();

            foreach($listado_cc_tipos_prod_rol as $cc_tipos_prod_rol){
                if($cc_tipos_prod_rol->tipoProd->tipo_prod_id == $tipo_prod_id){
                    if(!in_array($cc_tipos_prod_rol->cc,$centros_costo)){
                        array_push($centros_costo, $cc_tipos_prod_rol->cc);
                    }
                }
            }
            
            
            $data=CHtml::listData($centros_costo,'cc_id','nombre_completo_cc');
            foreach($data as $value=>$name)
            {
                echo CHtml::tag('option',
                           array('value'=>$value),CHtml::encode($name),true);
            }
        }
        
        /**
         * Obtiene el listados de tipos de productividades asociados a los CC
         * Se usa para actualizar a través de AJAX la etiqueta SELECT de centros de costo
         * del formulario de ingreso de las productividades.
         * 
         * @return string Listado de centros de costo en formato de etiqueta HTML SELECT
         */           
        public function actionTipoProductividad()
        {
            
            $cc_id = $_POST['cc_id'];      
            $persona_id = $_POST['persona_id'];    
            $area_id = $_POST['area_id'];   
            
            $tipos_productividad = $this->getTiposProductividadesCentroCosto($persona_id, $cc_id,$area_id);
            
            
            $data=CHtml::listData($tipos_productividad,'tipo_prod_id','nombre_tipo_prod');
            foreach($data as $value=>$name)
            {
                echo CHtml::tag('option',
                           array('value'=>$value),CHtml::encode($name),true);
            }
        }
        /**
         * Muestra el detalle de las productividades de una persona para un periodo en particular
         * Los datos se despliegan a través de AJAX-JQUERY
         * 
        * @return string despliega la vista ingresoProductividades/detalleProductividadPersona
         */          
        public function actionDetalleProductividadPersona(){
            $persona_id = $_POST['persona_id'] ;
            $periodo_id = $_POST['periodo_id'] ;
            $area_id    = $_POST['area_id'] ;
            
            $persona = ProdPersona::getPersona($persona_id);
            $periodo = ProdPeriodo::getPeriodo($periodo_id);
            
            $criteria_prod = new CDbCriteria();
            $criteria_prod->compare('persona_id', $persona_id);
            $criteria_prod->compare('periodo_id', $periodo_id);            
            $criteria_prod->compare("area_id", $area_id);
            
            $detalle = ProdProductividad::model()->findAll($criteria_prod);
            
            $this->renderPartial('detalleProductividadPersona',array("detalle"=>$detalle,"persona"=>$persona,'periodo'=>$periodo));
        }
        
        /**
         * Elimina una productividad de una persona y actualiza la lista de productividades 
         * en pantalla a través de AJAX-JQUERYs
         * 
         * @return string redirect a actionDetalleProductividadPersona
         */          
        public function actionBorrarProductividad(){
            $persona_id = $_GET['persona_id'] ;
            $periodo_id = $_GET['periodo_id'] ;
            $productividad_id = $_GET['productividad_id'] ;            
            
            try{
                $productividad = ProdProductividad::model()->findByPk($productividad_id);
                
                $monto_prod = $productividad->monto;
                $cc_prod = $productividad->cc;
                $tipo_prod = $productividad->tipoProd;
                $nro_cuotas = $productividad->numero_cuotas;
                
                if($productividad->estado_id == ProdEstado::APROBADO){
                    Yii::app()->user->setFlash('error',"No se puede borrar la productividad ya que está aprobada");
                }else{                
                    $productividad->delete();
                    
                    $log_modificaciones = new ProdLogProductividad();
                    $log_modificaciones->productividad_id = $productividad_id;
                    $log_modificaciones->periodo_id = $periodo_id;
                    $log_modificaciones->persona_id = $persona_id;
                    $log_modificaciones->persona_modifica_id = Yii::app()->user->personaId;
                    $log_modificaciones->fecha_modificacion = new CDbExpression('NOW()');

                    $string_diferencias="Eliminación de productividad<br>Monto: " . $monto_prod . "<br>Centro Costo: " . $cc_prod ."<br>" .
                                        "Tipo Productividad: " .$tipo_prod . "<br>Número Cuotas: " . $nro_cuotas;
                    
                    $log_modificaciones->observaciones = $string_diferencias;
                    $log_modificaciones->save();

                    Yii::app()->user->setFlash('success', "Productividad borrada correctamente");
                }
            }catch (Exception $e){
                Yii::app()->user->setFlash('error',$e->getMessage());
                Yii::log("Error Insert: " . $e->getMessage(), "error", "application.controllers.ProdMenuController");
                

            }
            
            $flashMessages = Yii::app()->user->getFlashes();
            if ($flashMessages) {
                foreach($flashMessages as $key => $message) {
                    echo '<div style="width:810px" class="flash-' . $key . '">' . $message . "</div>";
                }
            }
                
            
            $_POST['persona_id'] = $persona_id;
            $_POST['periodo_id'] = $periodo_id;            
            
            
            $this->actionDetalleProductividadPersona();
        }
        
        
        /**
         * Muestra el listado de productividades 
         * en pantalla a través de AJAX-JQUERYs
         * 
         * @return string despliega la extension ext.productividades.LProductividadesWidget
         */         
        public function actionActualizaListaProductividades(){
            
            $es_academico = $_REQUEST["es_academico"];
            $periodo_id = $_REQUEST["periodo_id"];  
            
            $datos = array(
                        'es_academico'=>$es_academico,
                        'periodo_id'=>$periodo_id,
                        );
            
            $this->widget('ext.productividades.LProductividadesWidget',$datos);

            
        }
    
        

        /**
         * Obtiene el periodo siguiente (definido por la variable $cantidad_meses) a partir del periodo actual 
         * @param int $periodo_actual_id Periodo Actual ID
         * @param int $cantidad_meses Cantidad de meses que faltan para el periodo buscado
         * @return int Periodo siguente.
         */         
        public function obtiene_periodo_siguiente($periodo_actual_id,$cantidad_meses){
            
//            echo "en obtiene_periodo_siguiente:" . $periodo_actual ."-".$cantidad_meses."<br>";
            
            $periodo_siguiente=0;
            $periodo_actual = ProdPeriodo::getPeriodo($periodo_actual_id);
            
            $mes_actual = $periodo_actual->mes;
            $ano_actual = $periodo_actual->anio;   
            
            for($i=0;$i<$cantidad_meses;$i++){
                
                if($mes_actual==12){         
                    $mes_siguiente = 1;
                    $ano_siguiente = $ano_actual + 1;                    
                }else{
                    $mes_siguiente = $mes_actual+1;
                    $ano_siguiente= $ano_actual;
                }
                
                if(strlen($mes_siguiente)==1){
                    $periodo_siguiente = $ano_siguiente . "0" . $mes_siguiente;
                }else{
                    $periodo_siguiente = $ano_siguiente . "" . $mes_siguiente;
                }
                
                $mes_actual = $mes_siguiente;
                $ano_actual = $ano_siguiente;   

            }
            
            
            return $periodo_siguiente;
            
            
        }
        
        /**
        * Toma una productividad en particular de un usuario y la carga en el formulario
        * de ingreso para que esta pueda ser modificada por el usuario
        * 
        * @return string despliega la vista ingresoProductividades/ingreso
        */
        public function actionEditarProductividad(){
            
            $productividad_id = $_REQUEST['productividad_id'] ;
            $periodo_id = $_REQUEST['periodo_id'] ;
            $area_id = $_REQUEST['area_id'] ;
                        
            $periodos = ProdPeriodo::getPeriodosCargados();
            $productividad_editar = ProdProductividad::model()->findByPk($productividad_id);
                        
            $centros_costos = ProdCentroCosto::getCentrosCostosTipoProductividad($productividad_editar->tipo_prod_id);
            $tipo_productividades = $productividad_editar->cc->prodTipoProductividads;
            
            $model=new ProductividadForm;
            $model->periodo_id = $periodo_id;
            $model->tipo_prod_id = $productividad_editar->tipo_prod_id;
            $model->cc_id =    $productividad_editar->cc_id;
            $model->monto = $productividad_editar->total_monto_asignado;
            $model->numero_pagos = $productividad_editar->numero_cuotas;
            $model->persona_id = $productividad_editar->persona_id;
            $model->nombre_persona = $productividad_editar->persona;
            $model->observaciones =    $productividad_editar->observaciones;    
            $model->area_id =    $productividad_editar->area_id;    
            
            foreach($periodos as $periodo){
                if($periodo->periodo_id==$periodo_id){
                    $periodo_seleccionado = $periodo;
                }
            }
            
            $puede_modificar=false;
            $mensaje="";
            if($periodo_seleccionado->estado_periodo_id==ProdPeriodo::PERIODO_ABIERTO){
                $puede_modificar=true;
                $dia_actual = date("d"); 
                $month_end = strtotime('last day of this month', time());
                $dia_fin_mes =  date('d', $month_end);
                $restantes = $dia_fin_mes-$dia_actual;
                
                if($restantes<7){
                    $mensaje = "<legend class='alertaCierre'> Quedan $restantes días para el cierre del mes, luego no podrá seguir cargando datos a este período.</legend>";
                }
            }else{
                Yii::app()->user->setFlash('error', " Período cerrado, no se puede ingresar productividades" );
            }
            
         
            //DATOS FORMULARIO CONSULTA
            //SE USA PARA CONTROLAR LA GRILLA
            $model_consulta = new ConsultasForm;
            
            if(isset($_POST['ConsultasForm']))
            {
                //para mantener el periodo seleccionado cuando se ejecuta el formulario
                $model_consulta->attributes=$_POST['ConsultasForm']; 
                
            }else{
                $tipo_personas = 1;
                $model_consulta->tipo_personas = $tipo_personas;
            }
            //FIN FORMULARIO CONSULTA
            
            $rol_id = Yii::app()->user->rolId;
            
            $listado_areas = ProdArea::getAreas();
            
            $es_administrador = false;
            if($rol_id == ProdRol::ROL_ADMINISTRDOR){
                $es_administrador = true;
                
            }
            
            $data = array(  'model'=>$model,
                            'periodos' => $periodos,
                            'tipo_productividades'=>$tipo_productividades,
                            'centros_costos'=>$centros_costos,
                            'periodo_id'=>$periodo_id,
                            'puede_modificar'=> $puede_modificar,
                            'mensaje'=>$mensaje,
                            'editar'  => true,
                            'model_consulta'  => $model_consulta,
                            'area_seleccionada_id'  => $area_id,
                            "es_administrador"=>$es_administrador,
                            "listado_areas"=>$listado_areas,
                           );

            
             
            $this->render('ingreso',$data);
            
            
        }
        
        
        
        /**
         * Obtiene los centros de costos a los que puede acceder una persona
         * dependiendo si es o no academico
         * 
         * @return array Listado de Tipos de productividades en formato HTML etiqueta SELECT
         */          
        public function actionCentroCostosPersonas(){
            
            try{
                $persona_id = $_REQUEST["persona_id"];
                $area_id = $_REQUEST["area_id"];
                
                $centros_costo = $this->getCentrosCostoAreaCarga($area_id);
              
                $data=CHtml::listData($centros_costo,'cc_id','nombreCompleto');

                echo CHtml::tag('option',
                               array('value'=>""),CHtml::encode("Seleccionar Tipo .."),true);
                foreach($data as $value=>$name)
                {
                    echo CHtml::tag('option',
                               array('value'=>$value),CHtml::encode($name),true);
                }
            }catch(Exception $e){
                echo $e->getMessage();
            }
        }
        
                
        /**
         * Obtiene los centros de costos asociados al area
         * 
         * @param int $area_id Area que carga         
         * @return array Listado de Tipos de productividades en formato HTML etiqueta SELECT
         */           
        public function getCentrosCostoAreaCarga($area_id){

            $listado_cc_tipos_prod_rol = ProdAreaTipoPodCc::getCentrosCostosTiposProductividadesArea($area_id,true);

            $centros_costo = array();

            foreach($listado_cc_tipos_prod_rol as $cc_tipos_prod_rol){
                if(!in_array($cc_tipos_prod_rol->cc,$centros_costo)){
                    array_push($centros_costo, $cc_tipos_prod_rol->cc);
                }
            }

            return $centros_costo;
        }
        
        public function getTiposProductividadesCentroCosto($persona_id,$cc_id,$area_id){
            $persona = ProdPersona::getPersona($persona_id);
            
            $es_academico = false;
            if($persona->bo_academico==1){
                $es_academico = true;
            }
                        
            $listado_cc_tipos_prod_rol = ProdAreaTipoPodCc::getCentrosCostosTiposProductividadesArea($area_id,true);
               
            $tipos_productividad = array();

            if($es_academico){
                foreach($listado_cc_tipos_prod_rol as $cc_tipos_prod_rol){
                    if($cc_tipos_prod_rol->cc->cc_id == $cc_id){
                        if($cc_tipos_prod_rol->tipoProd->acadamico == true &&  !in_array($cc_tipos_prod_rol->tipoProd,$tipos_productividad)){
                            array_push($tipos_productividad, $cc_tipos_prod_rol->tipoProd);
                        }
                    }
                }
            }else{
                foreach($listado_cc_tipos_prod_rol as $cc_tipos_prod_rol){
                    if($cc_tipos_prod_rol->cc->cc_id == $cc_id){
                        if($cc_tipos_prod_rol->tipoProd->no_academico == true && !in_array($cc_tipos_prod_rol->tipoProd,$tipos_productividad)){
                            array_push($tipos_productividad, $cc_tipos_prod_rol->tipoProd);
                        }
                    }
                }
            }
            
            return $tipos_productividad;
        }
        
        /**
         * Muestra el detalle de las productividades de una persona para un periodo en particular
         * Los datos se despliegan a través de AJAX-JQUERY
         * 
        * @return string despliega la vista ingresoProductividades/detalleProductividadPersona
         */          
        public function actionGetLogModificaciones(){
            $persona_id = $_POST['persona_id'] ;
            $periodo_id = $_POST['periodo_id'] ;
            
            $persona = ProdPersona::getPersona($persona_id);
            $periodo = ProdPeriodo::getPeriodo($periodo_id);
            
            $listado_logs_productividades = ProdLogProductividad::getLogsModificaciones($persona_id, $periodo_id);            
            
            $this->renderPartial('detalleLogProductividadPersona',array("listadoLogProductividades"=>$listado_logs_productividades,
                                                                        "persona"=>$persona,
                                                                        'periodo'=>$periodo));
        }
        
   
        
        
        
        
}