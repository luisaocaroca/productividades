<?php

class ProdAreaController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column2';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('allow',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new ProdArea;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProdArea']))
		{
			$model->attributes=$_POST['ProdArea'];
                        
                        $array_tipo_prod_cc = array();
                        $listado_cc_tipo_prod = $_POST['ProdArea']['prodTipoProductividadCentroCosto'];
                        
                        foreach($listado_cc_tipo_prod as $cc_id=>$listado_tipos_prod){
                            
                            if(is_array($listado_tipos_prod)){
                                foreach($listado_tipos_prod as $tipo_prod_id){
                                    if($tipo_prod_id!="0"){
                                        array_push($array_tipo_prod_cc, array($cc_id,$tipo_prod_id));
                                    }
                                    
                                }
                            }
                            
                        }
                        
                        
			if($model->save()){
                            foreach($array_tipo_prod_cc as $relacion){
                                $tipo_prod_cc = new ProdAreaTipoPodCc();
                                $tipo_prod_cc->area_id =  $model->area_id;
                                $tipo_prod_cc->cc_id = $relacion[0];
                                $tipo_prod_cc->tipo_prod_id = $relacion[1];
                                $tipo_prod_cc->save();                                
                            }
                            
                            $this->redirect(array('admin'));
                                
                        }
		}

		$this->render('create',array(
		'model'=>$model,'relacionesArea'=>array()
                    
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

                $relacion_cc_tipo_prod = ProdAreaTipoPodCc::getRelacionesArea($id);
                
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
		if(isset($_POST['ProdArea']))
		{
			$model->attributes=$_POST['ProdArea'];
                        
                        $array_tipo_prod_cc = array();
                        $listado_cc_tipo_prod = $_POST['ProdArea']['prodTipoProductividadCentroCosto'];
                        
                        foreach($listado_cc_tipo_prod as $cc_id=>$listado_tipos_prod){
                            
                            if(is_array($listado_tipos_prod)){
                                foreach($listado_tipos_prod as $tipo_prod_id){
                                    if($tipo_prod_id!="0"){
                                        array_push($array_tipo_prod_cc, array($cc_id,$tipo_prod_id));
                                    }
                                    
                                }
                            }
                            
                        }
                        
                        
			if($model->save()){
                            foreach($relacion_cc_tipo_prod as $relacioCCTipoProd){
                                $relacioCCTipoProd->delete();
                            }
                            
                            foreach($array_tipo_prod_cc as $relacion){
                                $tipo_prod_cc = new ProdAreaTipoPodCc();
                                $tipo_prod_cc->area_id =  $model->area_id;
                                $tipo_prod_cc->cc_id = $relacion[0];
                                $tipo_prod_cc->tipo_prod_id = $relacion[1];
                                $tipo_prod_cc->save();                                
                            }
                            
                            $this->redirect(array('admin'));
                                
                        }
		}

		$this->render('update',array(
			'model'=>$model,
                        'relacionesArea' =>$relacion_cc_tipo_prod
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ProdArea');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new ProdArea('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ProdArea']))
			$model->attributes=$_GET['ProdArea'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return ProdArea the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model=ProdArea::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param ProdArea $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='prod-area-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}