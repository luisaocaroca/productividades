<?php
/**
 * Controlador del mantenedor de la tabla ProdTipoProductividad
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller
 */
class ProdTipoProductividadController extends GxController {

        /**
        * Define la función de control de acceso que tiene el controlador
        * 
        * @return array funciones que realizan el control de acceso
        */         
        public function filters()
        {
            return array(
                'accessControl',
            );
        }
        
        /**
        * Control de acceso del módulo
        * En este caso tienen acceso a las funcionalidades solo el administrador del sistema
        * 
        * @return array permisos de usuario al módulo
        */
	public function accessRules()
        {
            //SOLO EL USUARIO ADMINISTRADOR PUEDE ACCEDER
            return array(
                array('allow',
                      'roles' => array('admin'),
                ),                
                array('deny',
                    'users'=>array('*'),
                ),
            );
        }     
        
       /**
        * Despliega el detalle de un Tipo Productividad
        * 
        * @param int $id Tipo Productividad ID
        * @return string despliega la vista prodTipoProductividad/view
        */              
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ProdTipoProductividad'),
		));
	}

        /**
        * Despliega el formulario para crear un registro
        * 
        * @return string despliega la vista prodTipoProductividad/create
        */            
	public function actionCreate() {
		$model = new ProdTipoProductividad;


		if (isset($_POST['ProdTipoProductividad'])) {
			$model->setAttributes($_POST['ProdTipoProductividad']);
			$relatedData = array(
				);

			if ($model->saveWithRelated($relatedData)) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->tipo_prod_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

        /**
        * Despliega el formulario para actualizar un registro
        * 
        * @param int $id ID del registro a actualizar
        * @return string despliega la vista prodTipoProductividad/update
        */            
	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ProdTipoProductividad');


		if (isset($_POST['ProdTipoProductividad'])) {
			$model->setAttributes($_POST['ProdTipoProductividad']);
			$relatedData = array(
				);

			if ($model->saveWithRelated($relatedData)) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

        /**
        * Elimina un registros un registro
        * 
        * @param int $id ID del registro a borrar
        * @return string redirect prodTipoProductividad/admin
        */         
	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
                    
                    try{
                       $this->loadModel($id, 'ProdTipoProductividad')->delete();
                       Yii::app()->user->setFlash('success','Borrado Correctamente');
                       echo "<div class='flash-success'>Borrado Correctamente</div>"; //for ajax
                    }catch(CDbException $e){
                        Yii::app()->user->setFlash('error','No se puede borrar el registro porque tiene datos relacionados.');

                        echo "<div class='flash-error'>No se puede borrar el registro porque tiene datos relacionados.</div>"; //for ajax
                    }
                    if (!Yii::app()->getRequest()->getIsAjaxRequest())
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin')); 


			
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

        /**
        * Despliega el index del mantenedor
        * 
        * @return string despliega la vista prodTipoProductividad/admin
        */           
	public function actionIndex() {
//		$dataProvider = new CActiveDataProvider('ProdTipoProductividad');
//		$this->render('index', array(
//			'dataProvider' => $dataProvider,
//		));
            $this->actionAdmin();
	}
        
        
        /**
        * Despliega el index del mantenedor
        * 
        * @return string despliega la vista prodTipoProductividad/admin
        */   
	public function actionAdmin() {
		$model = new ProdTipoProductividad('search');
		$model->unsetAttributes();

		if (isset($_GET['ProdTipoProductividad']))
			$model->setAttributes($_GET['ProdTipoProductividad']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}