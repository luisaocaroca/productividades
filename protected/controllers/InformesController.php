<?php
/**
 * Controlador del módulo de informes
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller
 */
class InformesController extends SecureController
{
        /**
        * Control de acceso del módulo
        * En este caso tienen acceso a las funcionalidades los usuarios Validadores y Directores
        * El administrador por defecto puede ver todos los módulos
        * 
        * @return array permisos de usuario al módulo
        */
	public function accessRules()
        {
            return array(
                //SOLO EL USUARIO CON PERMISO VALIDACION Y DIRECTOR PUEDE ACCEDER AL CONTROLLER
                array('allow',
                      'roles' => array('validador','director'),
                ),
                array('deny',
                    'users'=>array('*'),
                ),
            );
        }
        
        /**
        * Despliega el  menu principal del modulo
        * 
        * @return string despliega la vista informes/index o un archivo excel con el detalle del informe.        
        */           
	public function actionIndex()
	{
            $form = new ConsultasForm;   
            
            
                         
            $anios = ProdPeriodo::getAnios();
            $meses = ProdPeriodo::$MESES;
                       
            //por defecto el tipo de consulta se deja como el año
            $tipo_consulta_id = 2;
            $mes_id = date("m");
            $valido=true;
            $anio_actual_id = date("Y");  
            $exportar_excel = false;
            $tipo_personas = 1;
            $mostrar_positivos=0;
            if(isset($_POST['ConsultasForm']))
            {
                //para mantener el periodo seleccionado cuando se ejecuta el formulario
                
                $form->attributes=$_POST['ConsultasForm']; 
                
                if(!$form->validate()){
                    $valido=false;
                }
                
                $tipo_consulta_id = $form->tipo_consulta;
                $anio_id = $form->anio_id;
                $exportar_excel = $form->exportar_excel;
                $mostrar_positivos = $form->mostrar_positivos;
                                
            }else{
                               
                $anio_id= $anio_actual_id;
                $form->anio_id = $anio_actual_id;
                $form->tipo_consulta = $tipo_consulta_id;
                $form->anio_mes_id= $anio_actual_id;
                $form->mes_id = $mes_id;
                $form->tipo_personas = $tipo_personas;
                $form->mostrar_positivos=0;
                

            }
            
            
            //para hablitar los controles en la página
            //dependiendo de la consulta seleccionada
            $disabled_c1=false;
            $disabled_c2=false;
            $disabled_c3=false;
            
            if($tipo_consulta_id!=1){
                $disabled_c1=true;
            }
            
            if($tipo_consulta_id!=2){
                $disabled_c2=true;
            }
            
            if($tipo_consulta_id!=3){
                $disabled_c3=true;
            }
            
            if($tipo_consulta_id==1){
                $periodo_inicio_id = $form->anio_id*100 + 1;
                $periodo_fin_id = $form->anio_id*100 + 12;                
                $anio_id = $form->anio_id;

                
            }
            
            if($tipo_consulta_id==2){
                $periodo_inicio_id = $form->anio_mes_id*100 + $form->mes_id;                
                $periodo_fin_id = 0;
            }
            
            if($tipo_consulta_id==3){
                $periodo_inicio_id = $form->anio_desde_id*100 + $form->mes_desde_id;
                $periodo_fin_id = $form->anio_hasta_id*100 + $form->mes_hasta_id;
                
            }
            

            
            $periodo = ProdPeriodo::getPeriodo($periodo_inicio_id);
            $periodo_fin = null;
            if($periodo_fin_id!=0){
                $periodo_fin = ProdPeriodo::getPeriodo($periodo_fin_id);
            }
            
            $resumenCentroCosto = $this->getResumenCentroCosto($periodo_inicio_id,$periodo_fin_id);  
            
            
            if(!$exportar_excel){ 
            
                $this->render('index',array('model'=>$form,
                                            "anios"=>$anios,
                                            "meses"=>$meses,            
                                            "anio_id"=>$anio_id,
                                            "disabled_c1"=>$disabled_c1,
                                            "disabled_c2"=>$disabled_c2,
                                            "disabled_c3"=>$disabled_c3,
                                            "valido"=>$valido,
                                            "periodo_id"=>$periodo_inicio_id,
                                            "periodo_fin_id"=>$periodo_fin_id,
                                            "periodo"=>$periodo,
                                            "periodo_fin"=>$periodo_fin,
                                            "mostrar_detalle_tipo_productividad"=>false,
                                            "mostrar_positivos"=>$mostrar_positivos,
                                            "resumenCentroCosto"=>$resumenCentroCosto,
                                             ));  
            }else{
                $centros_costo_academicos = $this->getCentrosCostos(true);
                $salida_academicos = $this->getProductividades($periodo_inicio_id,$periodo_fin_id,$centros_costo_academicos, true);

                $centros_costo_no_academicos = $this->getCentrosCostos(false);
                $salida_no_academicos = $this->getProductividades($periodo_inicio_id,$periodo_fin_id,$centros_costo_no_academicos, false);

                if(is_null($periodo_fin)){
                    $periodo_fin= new ProdPeriodo;
                }
                $this->getExcel($periodo,
                                $periodo_fin,
                                $centros_costo_academicos,
                                $salida_academicos,
                                $centros_costo_no_academicos,
                                $salida_no_academicos
                                );
        
            }
            
            //echo $periodo_inicio_id . "-" . $periodo_fin_id;
            
//            if(!$exportar_excel){                        
//                if($valido){
//                    $html = $this->renderPartial('informeContabilidad',
//                                        array(  
//                                            'periodo'=>$periodo_inicio,
//                                            'periodo_fin'=>$periodo_fin,
//                                            //"centros_costo_academicos"=>$centros_costo_academicos,
//                                            //"salida_academicos"=>$salida_academicos,
//                                            //"centros_costo_no_academicos"=>$centros_costo_no_academicos,
//                                            //"salida_no_academicos"=>$salida_no_academicos),
//                                        TRUE);
//                }else{
//                    $html = "";
//                }
//
//                $this->render('index',array('model'=>$form,
//                                                   "anios"=>$anios,
//                                                   "meses"=>$meses,            
//                                                   "anio_id"=>$anio_id,
//                                                   "disabled_c1"=>$disabled_c1,
//                                                   "disabled_c2"=>$disabled_c2,
//                                                   "disabled_c3"=>$disabled_c3,
//                                                   "html_consulta"=>$html
//                                                    )); 
//            }else{
//                $this->getExcel($periodo_inicio,
//                                $periodo_fin,
//                                $centros_costo_academicos,
//                                $salida_academicos,
//                                $centros_costo_no_academicos,
//                                $salida_no_academicos
//                                );
//            }
            
 
	}
                
        /**
        * Obtiene los centros de costos asociados al perfil del usuario
        * @param bool $es_academico Define si va a mostrar solamente las productividades de los usuarios academicos
        * @return array Listado centro de costos
        */          
        public function getCentrosCostos($es_academico){
            $centros_costos=array();
            $tipos_productividades = array();
            
            $rol_id = Yii::app()->user->rolId;            
            
            
            $criteria_rol = new CDbCriteria();
            $criteria_rol->compare('rol_id', $rol_id);
            $tipos_productividad_rol = ProdRolTipoProd::model()->findAll($criteria_rol);           

            foreach($tipos_productividad_rol as $tipo_productividad){
                //echo $tipo_productividad->tipoProductividad;
                array_push($tipos_productividades, $tipo_productividad->tipoProductividad);
            }
            
            foreach($tipos_productividades as $tipo_productividad){
            
                if(($tipo_productividad->acadamico && $es_academico) ||
                   ($tipo_productividad->no_academico && !$es_academico)){
                    foreach($tipo_productividad->prodCentroCostos as $centro_costo){
                        $existe = false;
                        foreach($centros_costos as $centro_costo_arreglo){
                            if($centro_costo_arreglo->cc_id==$centro_costo->cc_id){
                                $existe = true;
                            }
                        }

                        if(!$existe){
                            array_push($centros_costos, $centro_costo);
                        }

                    }
                }
            }
            
            sort($centros_costos );
            
            return $centros_costos;
        }
        
        /**
        * Obtiene las productividades de una ventana de tiempo
        * @param int $periodo_id Periodo inicio de la ventana de las productividades a mostrar
        * @param int $periodo_fin Periodo fin de la ventana de las productividades a mostrar
        * @param array $centros_costos Listado de centros de costos a mostrar
        * @param bool $es_academico Define si va a mostrar solamente las productividades de los usuarios academicos
        * @return array Listado Productividades con formato de informe de contabilidad
        */        
        public function getProductividades($periodo_id,$periodo_fin,$centros_costos,$es_academico){
                                    
            //OBTIENE PRODUCTIVIDADES CARGADAS        
            $estado_id = ProdEstado::APROBADO;
            $order_nombre_persona = true;
            $productividades = ProdProductividad::getProductividadesGlobales($estado_id, $periodo_id, $periodo_fin, $order_nombre_persona);
            
            $personas = array();
            
            foreach($productividades  as $productividad){
                $existe = false;
                foreach($personas as $persona){
                    if($persona->persona_id == $productividad->persona->persona_id ){
                        $existe = true;
                    }
                }

                if(!$existe && 
                   (($es_academico && $productividad->persona->bo_academico == 1) || 
                    (!$es_academico && $productividad->persona->bo_academico == 0) 
                   )
                  ){

                    array_push($personas, $productividad->persona);
                }
            }

            $salida_personas = array();
            
            //recorro el listado de personas        
            foreach($personas as $persona){            
                $prod_persona = array();

                //filtro del total de productividades las productividades de la persona
                foreach($productividades  as $productividad){
                    if($productividad->persona_id==$persona->persona_id){
                        array_push($prod_persona, $productividad);
                    }
                }

                //se carga la productividad de las personas
                //en el formato de salida
                $detalle_productividad = array();
                
                foreach($centros_costos as $centro_costo){
                    //echo $centro_costo . ":" . count($centro_costo->prodTipoProductividads)."<br>";
                    $monto=0; 
                    foreach($centro_costo->prodTipoProductividads as $tipo_productividad){

                        if(($tipo_productividad->acadamico && $es_academico) ||
                           ($tipo_productividad->no_academico && !$es_academico)){  
                                                   
                            foreach($prod_persona as $productividad){                

                                //si existe la prodcutvidad para el tipo se carga el monto
                                if(($productividad->tipo_prod_id==$tipo_productividad->tipo_prod_id) &&
                                   ($productividad->cc_id == $centro_costo->cc_id )     
                                  ){
                                    $monto = $monto + $productividad->monto;                                    
                                }
                            }                            
                        }
                    }
                    
                    //se genera el registro salida
                    $salida = array("cc_id"=>$centro_costo->cc_id,
                                    "monto"=>$monto
                                  );
                    //se carga al listado
                    array_push($detalle_productividad, $salida);   
                            
                }
                //FIN RECORRE PROD

                $array_persona_formateado = array("persona"=>$persona, 
                                                  "listado_productividades"=>$detalle_productividad);
                array_push($salida_personas, $array_persona_formateado);

            }
            
            //OBTIENE TOTALES
            $totales_columna = array();          

            foreach($centros_costos as $centro_costo){
                $monto_total_columna=0;
                
                foreach($centro_costo->prodTipoProductividads as $tipo_productividad){
                    if(($tipo_productividad->acadamico && $es_academico) ||
                       ($tipo_productividad->no_academico && !$es_academico)){  

                        
                        foreach($personas as $persona){  
                            $prod_persona = array();

                            foreach($productividades  as $productividad){
                                if($productividad->persona_id==$persona->persona_id){
                                    array_push($prod_persona, $productividad);
                                }
                            }

                            foreach($prod_persona as $productividad){ 
                                //si existe la prodcutvidad para el tipo se carga el monto
                                if(($productividad->tipo_prod_id==$tipo_productividad->tipo_prod_id) &&
                                   ($productividad->cc_id == $centro_costo->cc_id )){
                                    $monto_total_columna = $monto_total_columna + $productividad->monto;                                
                                }
                           }
                       }
                    }
                }
                
                array_push($totales_columna, $monto_total_columna);

            }
            
            $salida = array("salida_personas"=>$salida_personas,
                            "totales_columna"=>$totales_columna);
            
            return $salida;
            
        }
        
        /**
        * Exportar a excel el listado las productividades de una ventana de tiempo
        * @param ProdPeriodo $periodo_inicio Periodo inicio de la ventana de las productividades a mostrar
        * @param ProdPeriodo $periodo_fin Periodo fin de la ventana de las productividades a mostrar
        * @param array $centros_costo_academicos Listado de centros de costos a mostrar asociados a rol academico
        * @param array $salida_academicos Listado de productividades de usuarios academicos
        * @param array $centros_costo_no_academicos Listado de centros de costos a mostrar asociados a rol no academico
        * @param array $salida_no_academicos Listado de productividades de usuarios no academicos
        * @return PHPExcel_IOFactory se retorna un archivo excel 
        */            
        public function getExcel(ProdPeriodo $periodo_inicio,
                                 ProdPeriodo $periodo_fin,
                                 $centros_costo_academicos,
                                 $salida_academicos,
                                 $centros_costo_no_academicos,
                                 $salida_no_academicos){
            
            
            $objPHPExcel = new PHPExcel();
            
            $styleHeaderArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'E1E0F7'),
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )   ,
                'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )          
            );
            
            $styleHeaderCCArray = array(
                'borders' => array(
                                'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                                ) 
                            ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'E1E0F7'),
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )            ,
                'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            )
            );
            
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'font' => array(
                    'size' => 8
                )  
            );
            
            $styleTotalArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )  
            );

            $currencyFormat =  iconv("ISO-8859-1", "UTF-8", '$ #,##0');
            

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle('Academicos');

            $row = 1; 
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "Período Consulta:" );
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $periodo_inicio->nombrePeriodo);
            if(!is_null($periodo_fin->periodo_id)){
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, "al " . $periodo_fin->nombrePeriodo);
            }
            
            $row = 4;            
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "ACADEMICOS");
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$row, "CENTROS COSTO");  
            $merge = $this->getRangoAscii(4,  count($centros_costo_academicos) , $row);                        
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells($merge);
            $objPHPExcel->getActiveSheet()->getStyle($merge)->applyFromArray($styleHeaderCCArray);
                    
//            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E'.$row.':H'. $row);            
//            $objPHPExcel->getActiveSheet()->getStyle('E'.$row.':H'. $row)->applyFromArray($styleHeaderCCArray);

            $row++;
            
            $col=0;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Apellido Paterno");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Apellido Materno");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Nombres");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Total");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            foreach($centros_costo_academicos as $centro_costo){            
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $centro_costo->nombre_completo_cc);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;
            }
            

            
            $row++;
            
            foreach($salida_academicos["salida_personas"] as $persona){
                $col=0;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $persona["persona"]->apellido_paterno);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $col++;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $persona["persona"]->apellido_materno);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $col++;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $persona["persona"]->nombres);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $col++;
                
                $total=0;
                foreach($persona["listado_productividades"] as $detalle_productividad){
                    
                    $total=$total+$detalle_productividad["monto"];                    
                   
                }
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $total);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $col++;
                
                
                foreach($persona["listado_productividades"] as $detalle_productividad){
                    
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $detalle_productividad["monto"]);  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);


                    $col++;
                }
                

                
                
                $row++;
                
            }
            
            
            $col=3;
            $total_academicos = 0;
            foreach($salida_academicos["totales_columna"] as $total){      
                $total_academicos = $total_academicos + $total;
            }
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $total_academicos);  
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            
            $col++;
            
            foreach($salida_academicos["totales_columna"] as $total){      
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $total);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $col++;
            }
            
            
            $row++;
            
            
            //NO ACADEMICO
            $objPHPExcel->createSheet();
            $objPHPExcel->setActiveSheetIndex(1);
            $objPHPExcel->getActiveSheet()->setTitle('No Academicos');
            
            $row = 1; 
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "Período Consulta:" );
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $periodo_inicio->nombrePeriodo);
            if(!is_null($periodo_fin->periodo_id)){
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, "al " . $periodo_fin->nombrePeriodo);
            }
            
            $row=4;
            
            //NO ACADEMICOS
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "NO ACADEMICOS");
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$row, "CENTROS COSTO");     
            $merge = $this->getRangoAscii(4,  count($centros_costo_no_academicos) , $row);                        
            $objPHPExcel->getActiveSheet()->mergeCells($merge);
            $objPHPExcel->getActiveSheet()->getStyle($merge)->applyFromArray($styleHeaderCCArray);
            
            //$objPHPExcel->getActiveSheet()->mergeCells('D'.$row.':H'. $row);
            //$objPHPExcel->getActiveSheet()->getStyle('D'.$row.':H'. $row)->applyFromArray($styleHeaderCCArray);
            $row++;
                        
            $col=0;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Apellido Paterno");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Apellido Materno");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Nombres");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Total");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
                        
            
            foreach($centros_costo_no_academicos as $centro_costo){            
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $centro_costo->nombre_completo_cc);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;
            }

            $row++;
            
            foreach($salida_no_academicos["salida_personas"] as $persona){
                $col=0;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $persona["persona"]->apellido_paterno);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $col++;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $persona["persona"]->apellido_materno);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $col++;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $persona["persona"]->nombres);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $col++;
                
                $total=0;
                foreach($persona["listado_productividades"] as $detalle_productividad){
                    
                    $total=$total+$detalle_productividad["monto"];
                  
                }
                
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $total);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $col++;
                
                foreach($persona["listado_productividades"] as $detalle_productividad){
                    
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $detalle_productividad["monto"]);  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                    $col++;
                }
                

                
                
                $row++;
                
            }
            
            $col=3;
            $total_no_academicos = 0;
            foreach($salida_no_academicos["totales_columna"] as $total){      
                $total_no_academicos = $total_no_academicos + $total;               
            }
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $total_no_academicos);  
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            
            $col++;
            
            foreach($salida_no_academicos["totales_columna"] as $total){      
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $total);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $col++;
            }
 
            $row++;
            

            //ob_end_clean();
            //ob_start();

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="resultado_consulta.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }

        
        public function getRangoAscii($col_desde,$cuenta,$row){
                    
            //LETRA ASSCCI
            $col_desde = $col_desde + 65;
            $col_desde_ascii = chr($col_desde);

            $col_hasta = $col_desde + ($cuenta-1);
            $col_hasta_ascii = chr($col_hasta);

            if($col_desde>90){
                $col_desde = ($col_desde - 91) + 65;            
                $col_desde_ascii = chr(65) . chr($col_desde);
            }

            if($col_hasta>90){
                $col_hasta = ($col_hasta - 91) + 65;            
                $col_hasta_ascii = chr(65) . chr($col_hasta);
            }


            return $col_desde_ascii .''.$row.':'. $col_hasta_ascii .''. $row;

        }

        
        public function getResumenCentroCosto($periodo_id,$periodo_fin_id){
            $resumenProceso = ProdProductividad::getResumenCentroCostoRango($periodo_id,$periodo_fin_id, 0 ,ProdEstado::APROBADO);
        
            $columnas = array(
                 'nombre_cc','monto_academico','monto_no_academico','monto_total', 
            );

            $total = 0;
            $totalAcademico = 0;
            $totalNoAcademico = 0;
            
            
            foreach($resumenProceso as $resumen){
                $total = $total  + $resumen["monto_total"];
                $totalAcademico = $totalAcademico + $resumen["monto_academico"];
                $totalNoAcademico = $totalNoAcademico + $resumen["monto_no_academico"];
            }
            
            $detalle_columnas_resumen = array(       
                                array(
                                    'name'=>'nombre_cc',
                                    'header'=>'Centro Costo',
                                    'value'=>'$data["nombre_cc"] ',
                                    'footer'=>''
                                ),
                                array(
                                    'name'=>'monto_academico',
                                    'header'=>'Monto Académico',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_academico"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($totalAcademico) . '</b>'

                                ),
                                array(
                                    'name'=>'monto_no_academico',
                                    'header'=>'Monto No Académico',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_no_academico"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($totalNoAcademico) . '</b>'

                                ),
                                array(
                                    'name'=>'monto_total',
                                    'header'=>'Monto Total',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_total"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($total) . '</b>'

                                ),


                            );
        
                $dataProviderResumen=new CArrayDataProvider($resumenProceso, array(
                    'keys'=>array('concepto'),
                    'sort'=>array(
                        'attributes'=>$columnas,
                    ),
                    'pagination'=>array(
                        'pageSize'=>10000,
                    ),
                ));
                
                return array("dataProvider"=>$dataProviderResumen,"columnas"=>$detalle_columnas_resumen);
                
        }
}