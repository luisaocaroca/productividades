<?php
/**
 * Controlador del mantenedor de la tabla ProdMenu
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller
 */
class ProdMenuController extends GxController {

        /**
        * Define la función de control de acceso que tiene el controlador
        * 
        * @return array funciones que realizan el control de acceso
        */         
        public function filters()
        {
            return array(
                'accessControl',
            );
        }
        
        /**
        * Control de acceso del módulo
        * En este caso tienen acceso a las funcionalidades solo el administrador del sistema
        * 
        * @return array permisos de usuario al módulo
        */
	public function accessRules()
        {
            //SOLO EL USUARIO ADMINISTRADOR PUEDE ACCEDER
            return array(
                array('allow',
                      'roles' => array('admin'),
                ),                
                array('deny',
                    'users'=>array('*'),
                ),
            );
        }      

       /**
        * Despliega el detalle de un Menu
        * 
        * @param int $id Menu ID
        * @return string despliega la vista prodMenu/view
        */         
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ProdMenu'),
		));
	}

        /**
        * Despliega el formulario para crear un registro
        * 
        * @return string despliega la vista prodMenu/create
        */            
	public function actionCreate() {
		$model = new ProdMenu;


		if (isset($_POST['ProdMenu'])) {
			$model->setAttributes($_POST['ProdMenu']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->menu_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

        /**
        * Despliega el formulario para actualizar un registro
        * 
        * @param int $id ID del registro a actualizar
        * @return string despliega la vista prodMenu/update
        */            
	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ProdMenu');


		if (isset($_POST['ProdMenu'])) {
			$model->setAttributes($_POST['ProdMenu']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

        /**
        * Elimina un registros un registro
        * 
        * @param int $id ID del registro a borrar
        * @return string redirect prodMenu/admin
        */         
	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
                    
                    try{
                       $this->loadModel($id, 'ProdMenu')->delete();
                       Yii::app()->user->setFlash('success','Borrado Correctamente');
                       echo "<div class='flash-success'>Borrado Correctamente</div>"; //for ajax
                    }catch(CDbException $e){
                        Yii::app()->user->setFlash('error','No se puede borrar el registro porque tiene datos relacionados.');

                        echo "<div class='flash-error'>No se puede borrar el registro porque tiene datos relacionados.</div>"; //for ajax
                    }
                    if (!Yii::app()->getRequest()->getIsAjaxRequest())
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin')); 

		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

        /**
        * Despliega el index del mantenedor
        * 
        * @return string despliega la vista prodMenu/admin
        */           
	public function actionIndex() {
//		$dataProvider = new CActiveDataProvider('ProdMenu');
//		$this->render('index', array(
//			'dataProvider' => $dataProvider,
//		));
                $this->actionAdmin();            
	}

        /**
        * Despliega el index del mantenedor
        * 
        * @return string despliega la vista prodMenu/admin
        */           
	public function actionAdmin() {
		$model = new ProdMenu('search');
		$model->unsetAttributes();

		if (isset($_GET['ProdMenu']))
			$model->setAttributes($_GET['ProdMenu']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}