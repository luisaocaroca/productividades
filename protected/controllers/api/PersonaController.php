<?php
/**
 * Controlador del mantenedor de personas a través de metodología REST
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller.api
 */
class PersonaController extends WRestController {
    
    /**
     *
     * @var string nombre del model 
     */
    protected $_modelName = "ProdPersona"; //model to be used as resource
        
    /**
    * Funcionalidades por defecto de REST
    * 
    * @return array opciones de REST
    */      
    public function actions() //determine which of the standard actions will support the controller
    {
        return array(
            'list' => array( //use for get list of objects
                'class' => 'WRestListAction',
                'filterBy' => array( //this param user in `where` expression when forming an db query
                    'persona_id' => 'id', // 'name_in_table' => 'request_param_name'
                ),
                'limit' => 'limit', //request parameter name, which will contain limit of object
                'page' => 'page', //request parameter name, which will contain requested page num
                'order' => 'order', //request parameter name, which will contain ordering for sort
            ),
            'delete' => 'WRestDeleteAction',
            'get' => 'WRestGetAction',
            'create' => 'WRestCreateAction', //provide 'scenario' param
            'update' => array(
                'class' => 'WRestUpdateAction',
                'scenario' => 'update', //as well as in WRestCreateAction optional param
                )
        );
    }
    
}

?>
