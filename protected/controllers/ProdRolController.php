<?php
/**
 * Controlador del mantenedor de la tabla ProdRol
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller
 */
class ProdRolController extends GxController {

        /**
        * Define la función de control de acceso que tiene el controlador
        * 
        * @return array funciones que realizan el control de acceso
        */         
        public function filters()
        {
            return array(
                'accessControl',
            );
        }
        
        /**
        * Control de acceso del módulo
        * En este caso tienen acceso a las funcionalidades solo el administrador del sistema
        * 
        * @return array permisos de usuario al módulo
        */
	public function accessRules()
        {
            //SOLO EL USUARIO ADMINISTRADOR PUEDE ACCEDER
            return array(
                array('allow',
                      'roles' => array('admin'),
                ),                
                array('deny',
                    'users'=>array('*'),
                ),
            );
        }  
        

       /**
        * Despliega el detalle de un Rol
        * 
        * @param int $id Rol ID
        * @return string despliega la vista prodRol/view
        */            
	public function actionView($id) {
            
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ProdRol'),
		));
	}
        
        /**
        * Despliega el formulario para crear un registro
        * 
        * @return string despliega la vista prodRol/create
        */    
	public function actionCreate() {
		$model = new ProdRol;


		if (isset($_POST['ProdRol'])) {
			$model->setAttributes($_POST['ProdRol']);
			$relatedData = array(
				'prodSubMenus' => $_POST['ProdRol']['prodSubMenus'] === '' ? null : $_POST['ProdRol']['prodSubMenus'],
				);

			if ($model->saveWithRelated($relatedData)) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->rol_id));
			}
		}

		$this->render('create', array( 'model' => $model,'relacionesRol'=>array()));
	}

        /**
        * Despliega el formulario para actualizar un registro
        * 
        * @param int $id ID del registro a actualizar
        * @return string despliega la vista prodRol/update
        */            
	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ProdRol');
                
		if (isset($_POST['ProdRol'])) {
			$model->setAttributes($_POST['ProdRol']);
                        
                             
			$relatedData = array(
				'prodSubMenus' => $_POST['ProdRol']['prodSubMenus'] === '' ? null : $_POST['ProdRol']['prodSubMenus'],
				);
                        
                        

			if ($model->saveWithRelated($relatedData)) {                           
                            
                            $this->redirect(array('admin'));
			}
		}

		$this->render('update', array( 'model' => $model));
	}

        /**
        * Elimina un registros un registro
        * 
        * @param int $id ID del registro a borrar
        * @return string redirect prodRol/admin
        */         
	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
                    
                    try{
                       $this->loadModel($id, 'ProdRol')->delete();
                       Yii::app()->user->setFlash('success','Borrado Correctamente');
                       echo "<div class='flash-success'>Borrado Correctamente</div>"; //for ajax
                    }catch(CDbException $e){
                        Yii::app()->user->setFlash('error','No se puede borrar el registro porque tiene datos relacionados.');

                        echo "<div class='flash-error'>No se puede borrar el registro porque tiene datos relacionados.</div>"; //for ajax
                    }
                    if (!Yii::app()->getRequest()->getIsAjaxRequest())
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin')); 


		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

        /**
        * Despliega el index del mantenedor
        * 
        * @return string despliega la vista prodRol/admin
        */           
	public function actionIndex() {
                $this->actionAdmin();            
	}

        /**
        * Despliega el index del mantenedor
        * 
        * @return string despliega la vista prodRol/admin
        */           
	public function actionAdmin() {
		$model = new ProdRol('search');
		$model->unsetAttributes();

		if (isset($_GET['ProdRol']))
			$model->setAttributes($_GET['ProdRol']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}