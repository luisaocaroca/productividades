<?php
/**
 * Controlador del módulo de validación de productividades
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller
 */
class ValidacionProductividadesController extends SecureController
{
    
        /**
         * Control de acceso del módulo
         * En este caso solo el usuario validador puede entrar al módulo 
         * El administrador por defecto puede ver todos los módulos
         * 
         * @return array permisos de usuario al módulo
        */
	public function accessRules()
        {
            return array(
                //SOLO EL USUARIO CON PERMISO VALIDACION PUEDE ACCEDER AL CONTROLLER
                array('allow',
                      'roles' => array('validador','encargado'),
                ),
                array('deny',
                    'users'=>array('*'),
                ),
            );
        }
        
        /**
        * Funcionalidad para mostrar el listado de academicos en formato autocomplete
        * 
        * @return array listado academicos
        */         
        public function actions()
        {
          return array(
            'personaList'=>array(
              'class'=>'application.controllers.ingresoProductividades.PersonaAutoCompleteAction',
            ),
            'academicos'=>array(
              'class'=>'application.controllers.ingresoProductividades.AcademicosAutoCompleteAction',
            ),
          );
        }
        
        
        /**
         * Action principal del módulo de validación
         * 
         * @return string despliega la vista validacionProductividades/index
        */           
	public function actionIndex()
	{
                        
            $this->actionValidaciones();
	}
        
        
        /**
         * Action principal del módulo de validación
         * @param int $tipos_personas ='0' Tipo de persona: Academico/ No Academico
         * @return string despliega la vista validacionProductividades/index
        */          
        public function actionValidaciones(){
            
            $model=new ValidacionForm;
            
            $periodos = ProdPeriodo::getPeriodosCargados();            
            
            foreach($periodos as $periodo){
                $periodo_id = $periodo->periodo_id;
                break;
            }
            
            
            $rol_id = Yii::app()->user->rolId;
            if($rol_id == ProdRol::ROL_ADMINISTRDOR){
                $rol_id = ProdRol::ROL_JEFE_ADMINISTRATIVO;
            }
            
            $mostrar_detalle_tipo_productividad = 0;
            $tipo_personas = 1;
            if(isset($_POST['ValidacionForm']))
            {                
                $model->attributes=$_POST['ValidacionForm'];
                if(!$model->validate()){
                    $valido=false;
                }
                
                $periodo_id = $model->periodo_id;
                $mostrar_detalle_tipo_productividad = $model->mostrar_detalle_tipo_productividad;
                
                if($model->tipo_personas == null){
                    $model->tipo_personas = $tipo_personas;
                }
                
            }else{
                $model->periodo_id = $periodo_id;            
                $model->mostrar_detalle_tipo_productividad=0;
                $model->tipo_personas = $tipo_personas;
                
            }
            
            Yii::app()->getSession()->add('form_exportar',$model);
            
                       
            $periodo_seleccionado = ProdPeriodo::getPeriodo($periodo_id);
            
            $puede_validar=true;
            if($periodo_seleccionado->estado_periodo_id==ProdPeriodo::PERIODO_CERRADO){
                $puede_validar=false;                
            }
            
            $estado_periodo = "";
            if($periodo_seleccionado->estado_periodo_id == ProdPeriodo::PERIODO_ABIERTO){
                Yii::app()->user->setFlash('success', " Período abieto para el ingreso y la validación de productividades" );
            }
            
            if($periodo_seleccionado->estado_periodo_id == ProdPeriodo::PERIODO_VALIDACION){
                $dia_actual = date("d"); 
                
                $restantes = ProdPeriodo::DIAS_PARA_VALIDACION - $dia_actual;
                
                $registros_pendientes = ProdProductividad::getEstadoValidacionPeriodo($periodo_id);
                
                $submensaje = "";
                if($registros_pendientes>0){
                    $submensaje="<b>Aún existen registros sin validar</b>";
                }
                
                Yii::app()->user->setFlash('success', "<legend class='alertaCierre'> Período cerrado para el ingreso y abierto para la validación de productividades</legend>
                                 El período se cerrará en $restantes días. $submensaje" );

            }
            
            if($periodo_seleccionado->estado_periodo_id == ProdPeriodo::PERIODO_CERRADO){
                Yii::app()->user->setFlash('error', " Período cerrado, no se puede ingresar o validar productividades" );
            }
            

            
            $personas_cargan = ProdProductividad::getPersonasCargan($periodo_id);
            
            
        
            $resumenEstado = $this->getResumenEstado($periodo_id);
            $resumenCentroCosto = $this->getResumenCentroCosto($periodo_id);    
            
            $this->render('index',array('model'=>$model,
                                        "periodos"=>$periodos,
                                        "periodo_id"=>$periodo_id,
                                        "puede_validar"=>$puede_validar,          
                                        "personas_cargan"=>$personas_cargan,    
                                        "estado_periodo"=>$estado_periodo, 
                                        "resumenEstado"=>$resumenEstado, 
                                        "resumenCentroCosto"=>$resumenCentroCosto, 
                                        "rol_id"=>$rol_id,
                                        "mostrar_detalle_tipo_productividad"=>$mostrar_detalle_tipo_productividad
                                        ));
        }
        
        /**
         * Cambia el estado de las productividades de un listado de personas.
         * 
         * @return string despliega la vista validacionProductividades/index
        */            
        public function actionCambiarEstadoTodos()
        {
            
            if(Yii::app()->request->isPostRequest)
            {
                $model_validacion = new ValidacionForm;
                $model_grilla = new JqgridParameterForm;
                
                if(isset($_POST['ValidacionForm']))
                {                
                    $model_validacion->attributes=$_POST['ValidacionForm'];
                    
                    $periodo_id = $model_validacion->periodo_id;
                    $tipo_personas = $model_validacion->tipo_personas;

                }
                
                $personas = array();
                
                if(isset($_POST['JqgridParameterForm']))
                 { 
                      $model_grilla->attributes=$_POST['JqgridParameterForm'];

                 }
                    
                
                if($tipo_personas==1){
                    if(isset($model_grilla["academicos"])){
                        $personas = $model_grilla["academicos"];
                    }
                    
                }else{
                    if(isset($model_grilla["no_academicos"])){
                        $personas = $model_grilla["no_academicos"];
                    }
                }
                
                foreach($personas as $persona_id=>$values){
//                    echo "persona_id:" . $persona_id;
//                    echo "aprueba:" . $values["aprueba"] . "<br>";
//                    echo "rechaza:" . $values["rechaza"] . "<br>";
                    
                    $estado_id = ProdEstado::APROBADO_ENCARGADO;
                    $productividades = ProdProductividad::getProductividadesIndividuales($persona_id, ProdArea::TODOS,$estado_id,$periodo_id);
                    
                    foreach($productividades as $productividad){
                        //echo $productividad->estado_id;
                        if($values["aprueba"]==1){
                            
                            $productividad->estado_id=  ProdEstado::APROBADO;
                            $productividad->persona_valida_id = Yii::app()->user->personaId;
                            $productividad->fecha_valida =  new CDbExpression('NOW()');
                            
                            if(empty($productividad->observaciones)){
                                $validador = ( Yii::app()->user->nombreCompleto);
                                $productividad->observaciones = "Productividad Aprobada por Jefe Administrativo " . $validador;
                            }
                            
                        }

                        $productividad->save();
                    }
                    
                    
                    
                }
             
                //llena los datos para que se recarge la pagina
                
                $model = new ValidacionForm;
                $model->periodo_id = $periodo_id;
                $_POST['ValidacionForm']=array();
                $_POST["ValidacionForm"]["periodo_id"] = $periodo_id;
                
                $this->actionValidaciones();
            } 
            else
            throw new CHttpException(400,"Invalid request. Please do not repeat this request again.");
        }
        
        
        /**
         * Cambia el estado de las productividades de un listado de personas.
         * 
         * @return string despliega la vista validacionProductividades/index
        */            
        public function actionCambiarEstadoEncargadoTodos()
        {
            
            if(Yii::app()->request->isPostRequest)
            {
                $model_validacion = new ValidacionForm;
                $model_grilla = new JqgridParameterForm;
                
                if(isset($_POST['ValidacionForm']))
                {                
                    $model_validacion->attributes=$_POST['ValidacionForm'];
                    
                    $periodo_id = $model_validacion->periodo_id;
                    $tipo_personas = $model_validacion->tipo_personas;
                    $area_id = $model_validacion->area_id;

                }
                
                $personas = array();
                
                if(isset($_POST['JqgridParameterForm']))
                 { 
                      $model_grilla->attributes=$_POST['JqgridParameterForm'];

                 }
                    
                
                if($tipo_personas==1){
                    if(isset($model_grilla["academicos"])){
                        $personas = $model_grilla["academicos"];
                    }
                    
                }else{
                    if(isset($model_grilla["no_academicos"])){
                        $personas = $model_grilla["no_academicos"];
                    }
                }
                
                foreach($personas as $persona_id=>$values){
//                    echo "persona_id:" . $persona_id;
//                    echo "aprueba:" . $values["aprueba"] . "<br>";
//                    echo "rechaza:" . $values["rechaza"] . "<br>";
                    
                    $estado_id = ProdEstado::PENDIENTE;
                    $productividades = ProdProductividad::getProductividadesIndividuales($persona_id, $area_id,$estado_id,$periodo_id);
                    
                    foreach($productividades as $productividad){
                        //echo $productividad->estado_id;
                        if($values["aprueba"]==1){
                            
                            $productividad->estado_id=  ProdEstado::APROBADO_ENCARGADO;
                            $productividad->persona_encargado_id = Yii::app()->user->personaId;
                            $productividad->fecha_encargado =  new CDbExpression('NOW()');
                            
                            if(empty($productividad->observaciones)){
                                $validador = ( Yii::app()->user->nombreCompleto);
                                $productividad->observaciones = "Productividad Aprobada por Encargado " . $validador;
                            }
                        }

                        $productividad->save();
                    }
                    
                    
                    
                }
             
                //llena los datos para que se recarge la pagina
                
                $model = new ValidacionForm;
                $model->periodo_id = $periodo_id;
                $model->area_id = $area_id;
                $_POST['ValidacionForm']=array();
                $_POST["ValidacionForm"]["periodo_id"] = $periodo_id;
                $_POST["ValidacionForm"]["area_id"] = $area_id;
                
                $this->actionValidaEncargado();
            } 
            else
            throw new CHttpException(400,"Invalid request. Please do not repeat this request again.");
        }
        
        
        /**
         * Carga el detalle del rechazo de una productividad.
         * Actualemente el rechazo se deshabilito.
         * Se llama desde AJAX/JQUERY
         * 
         * @return string despliega la vista validacionProductividades/detalleRechazoProductividad
        */ 
        public function actionDetalleRechazo(){
            
            $model=new CambiarEstadoForm;
            
            if(isset($_POST["CambiarEstadoForm"])){
                //SE USA CUANDO SE RETORNA DEL HISTORIAL,
                //PORQUE SE USA EL FORMULARIO CambiarEstadoForm PARA RETORNAR
                $model->attributes = $_POST["CambiarEstadoForm"];
                $persona_id = $model->persona_id;
                $periodo_id = $model->periodo_id;
                $es_academico = $model->es_academico; 
                $rol_id = $model->rol_id; 
                $area_id = $model->area_id; 
            }else{
                //SE USA CUANDO SE VE EL DETALLE DE LA LISTA
                //PORQUE SE USA UN LINK CON DIRECTO PARA ENTRAR A ESTA FUNCIONALIDAD
                //print_r($_REQUEST);
                $persona_id = $_REQUEST["persona_id"];
                $periodo_id = $_REQUEST["periodo_id"];
                $es_academico = $_REQUEST["es_academico"];                
                $rol_id = $_REQUEST["rol_id"];    
                $area_id = $_REQUEST["area_id"];    
                
            }
                
            $persona = ProdPersona::getPersona($persona_id);            
            $periodo = ProdPeriodo::getPeriodo($periodo_id);

            
            $estado_id = ProdEstado::TODOS;
            
            if($rol_id==ProdRol::ROL_JEFE_ADMINISTRATIVO){
                $detalle = ProdProductividad::getProductividadesIndividuales($persona_id,  ProdArea::TODOS,$estado_id, $periodo_id);
            }else{
                $detalle = ProdProductividad::getProductividadesIndividuales($persona_id,  $area_id,$estado_id, $periodo_id);
            }
            
            $this->renderPartial('detalleRechazoProductividad',array("model"=>$model,
                                                                     "detalle"=>$detalle,
                                                                     "persona"=>$persona,
                                                                     'periodo'=>$periodo,
                                                                     'rol_id'=>$rol_id,
                                                                     'area_id'=>$area_id,
                                                                     'es_academico'=>$es_academico));
        }
        
        /**
         * Cambia el estado de las productividades desde el detalle de la información 
         * de una persona en particular
         * Se llama desde AJAX/JQUERY
         * 
         * @return string despliega la vista validacionProductividades/detalleRechazoProductividad
        */         
        public function actionCambiarEstadoDetalle(){
            $persona_id = $_POST['CambiarEstadoForm']['persona_id'];
            $periodo_id = $_POST['CambiarEstadoForm']['periodo_id'];
            $es_academico = $_POST['CambiarEstadoForm']['es_academico'];
            $rol_id = $_POST['CambiarEstadoForm']['rol_id'];
            $area_id = $_POST['CambiarEstadoForm']['area_id'];
            
            $estado_id = ProdEstado::TODOS;
            $detalle = ProdProductividad::getProductividadesIndividuales($persona_id,$area_id, $estado_id, $periodo_id);
            
            if(isset($_POST['CambiarEstadoForm']))
            {
                
                foreach($detalle as $i=>$prod) 
                {
//                    echo "<pre>";
//                    echo print_r($prod);
//                    echo "</pre>";
                    $prod->attributes=$_POST['ProdProductividad'][$i];
                    
                    if($prod->validate())
                    {
//                        echo "observacones:" . $prod->observaciones ."<br>";
//                        echo "aprueba:" . $_POST['ProdProductividad'][$i]["aprueba"] ."<br>";
//                        echo "rechaza:" . $_POST['ProdProductividad'][$i]["rechaza"] ."<br>";
                        
                        if($_POST['ProdProductividad'][$i]["aprueba"]==1){
                            
                            if($rol_id  == ProdRol::ROL_JEFE_ADMINISTRATIVO){
                                $prod->estado_id = ProdEstado::APROBADO;
                                $prod->persona_valida_id = Yii::app()->user->personaId;
                                $prod->fecha_valida =  new CDbExpression('NOW()');
                            
                            }
                            
                            if($rol_id  == ProdRol::ROL_ENCARGADO){
                                $prod->estado_id = ProdEstado::APROBADO_ENCARGADO;
                                $prod->persona_encargado_id = Yii::app()->user->personaId;
                                $prod->fecha_encargado =  new CDbExpression('NOW()');
                            }
                            
                            if(empty($prod->observaciones)){
                                $validador = utf8_decode( Yii::app()->user->nombreCompleto);
                                $prod->observaciones = "Productividad Aprobada por " . $validador;
                            }
                        }
                        
//                        if($_POST['ProdProductividad'][$i]["rechaza"]==1){
//                            $prod->estado_id = 2;
//                        }
                        
                        $prod->save();
                        Yii::app()->user->setFlash('success', "Cambio Estado Realizado");
                            
                    }
                }                
            }
            
            //vuelvo a llamar los datos ya que se hizo un update de la prod. y se pierde la decodificacion del monto
            $detalle = ProdProductividad::getProductividadesIndividuales($persona_id, $area_id, $estado_id, $periodo_id);
            
            $persona = ProdPersona::getPersona($persona_id);
            $periodo = ProdPeriodo::getPeriodo($periodo_id);

            $model=new CambiarEstadoForm;
            
            $this->renderPartial('detalleRechazoProductividad',array("model"=>$model,
                                                                     "detalle"=>$detalle,
                                                                     "persona"=>$persona,
                                                                     'periodo'=>$periodo,
                                                                     'rol_id'=>$rol_id,
                                                                     'area_id'=>$area_id,
                                                                     'es_academico'=>$es_academico));

        }
        
        /**
         * Actualiza el listado de productividades de la pantalla principal 
         * luego de que se actualizó el estado de las productividades de una persona.
         * Se llama desde AJAX/JQUERY
         * 
         * @return string despliega la extensión ext.productividades.LProductividadesWidget
        */         
        public function actionActualizaListaProductividades(){
            
            //print_r($_REQUEST);
            
            $es_academico = $_REQUEST["es_academico"];
            $periodo_id = $_REQUEST["periodo_id"];
            
            if($es_academico=="false"){
                $es_academico=false;
            }else{
                $es_academico=true;
            }
            
            
            $model_validacion=new ValidacionForm;
            
            $width= 1000;
            if($es_academico){
                $width = 1300;
            }
            $this->widget('ext.productividades.LProductividadesWidget', array(
                'es_academico' => $es_academico,
                'periodo_id' => $periodo_id,
                'es_validacion' => true,
                'model' => $model_validacion,
                'width' => $width
                )
            );
            
        }
        
       /**
         * Cierra un período en particular cuando ya no se desea  hacer más cambios en las productividades.
         * La condición para que se ejecute el cierre del período es que todas las productividades
         * de las personas esten aprobadas y que el período anterior también este cerrado.  
         * OBSOLETO, FINALMENTE EL CIERRE SE HARA DE FORMA AUTOMATICA POR CRON        
         * 
         * @return string despliega la vista validacionProductividades/index
         */               
        public function actionCerrarPeriodo(){
            
            $model=new ValidacionForm("nuevo");
            
            if(isset($_POST['ValidacionForm']))
            {                
                $model->attributes=$_POST['ValidacionForm'];
                $periodo_id = $model->periodo_id;
                
                $registros_pendientes = ProdProductividad::getEstadoValidacionPeriodo($periodo_id);
                                               
                //echo "registros:" . $registros_pendientes;
                
                if($registros_pendientes>0){
                    Yii::app()->user->setFlash('error', "No se puede cerrar el período porque existen validaciones pendientes" );
                }else{
                    //CAMBIA ESTADO PERIODO
                    $periodosAnterioresAbiertos = ProdPeriodo::buscaPeriodoAnteriorAbierto($periodo_id);
                    
                    if($periodosAnterioresAbiertos>0){
                        Yii::app()->user->setFlash('error', "Un período anterior no ha sido cerrado, debe cerrar ese período antes" );
                    }else{                    
                        $periodo = ProdPeriodo::getPeriodo($periodo_id);                
                        $periodo->estado_periodo_id=  ProdPeriodo::PERIODO_CERRADO;
                        $periodo->save();
                    }
                }
            }
                      
            
            $this->actionValidaciones();
        }
        
        public function getResumenEstado($periodo_id,$area_id=0){
            
            
            $resumenProceso = ProdProductividad::getEstadoValidacionProductividades($periodo_id,$area_id);
            
            $tipos_persona = array();
        
            array_push($tipos_persona, array("id"=>"academico","nombre"=>"Academico"));
            array_push($tipos_persona, array("id"=>"no_academico","nombre"=>"No Academico"));
            
            $estados = ProdEstado::getEstados();
            $datos_tabla = array();
            
            foreach($estados as $estado){
                $total_academico=0;
                $total_no_academico=0;
                
                $color="normal";
                if($estado->estado_id == ProdEstado::APROBADO){
                    $color="green";
                }

                if($estado->estado_id == ProdEstado::APROBADO_ENCARGADO){
                    $color="yellow";
                }
                
                if($estado->estado_id == ProdEstado::PENDIENTE){
                    $color="red";
                }
                
                
                foreach($tipos_persona as $tipo_persona){
                    
                    foreach($resumenProceso as $registro){
                        if($registro["estado_id"] == $estado->estado_id && $registro["tipo"]==$tipo_persona["id"]) {
                            if($tipo_persona["id"]=="academico"){
                                $total_academico = $total_academico+ intval($registro["monto"]); 
                            }else{
                                $total_no_academico = $total_no_academico+intval($registro["monto"]); 
                            }
                            
                           
                        }                        
                        
                    } 
                }
                
                $total = $total_academico+$total_no_academico; 
                
                //if($estado->estado_id<=ProdEstado::APROBADO){
                    array_push($datos_tabla, array("nombre_estado"=>$estado->nombre_estado,"color"=>$color,"monto_academico"=>$total_academico,"monto_no_academico"=>$total_no_academico,"monto_total"=>$total));
               // }
                
            }
        
            $columnas = array(
                 'nombre_estado','monto_academico','monto_no_academico','monto_total', 
            );

            $total = 0;
            $total_academico =0;
            $total_no_academico =0;
            
            foreach($datos_tabla as $dato){
                $total = $total  + $dato["monto_total"];
                $total_academico = $total_academico  + $dato["monto_academico"];
                $total_no_academico = $total_no_academico  + $dato["monto_no_academico"];
            }
            
            $detalle_columnas_resumen = array(       
                                array(
                                    'name'=>'nombre_estado',
                                    'header'=>'Estado',
                                    'value'=>'$data["nombre_estado"]',
                                    'footer'=>'Total'
                                ),
                                array(
                                    'name'=>'monto_academico',
                                    'header'=>'Monto Academico',
                                    'value'=>'"$ ".Yii::app()->format->formatNumber($data["monto_academico"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($total_academico) . '</b>'
                                ),
                                array(
                                    'name'=>'monto_no_academico',
                                    'header'=>'Monto No Academico',
                                    'value'=>'"$ ".Yii::app()->format->formatNumber($data["monto_no_academico"]) ', 
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($total_no_academico) . '</b>'
                                ),
                                array(
                                    'name'=>'monto_total',
                                    'header'=>'Total',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_total"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($total) . '</b>'

                                ),


                            );
        
                $dataProviderResumen=new CArrayDataProvider($datos_tabla, array(
                    'keys'=>array('concepto'),
                    'sort'=>array(
                        'attributes'=>$columnas,
                    ),
                    'pagination'=>array(
                        'pageSize'=>10000,
                    ),
                ));
                
                return array("dataProvider"=>$dataProviderResumen,"columnas"=>$detalle_columnas_resumen);
        }
        
        public function getResumenCentroCosto($periodo_id,$area_id=0){
            $resumenProceso = ProdProductividad::getResumenCentroCostoRango($periodo_id,0,$area_id);
        
            $columnas = array(
                 'nombre_cc','monto_academico','monto_no_academico','monto_total', 
            );

            $total = 0;
            $totalAcademico = 0;
            $totalNoAcademico = 0;
            
            
            foreach($resumenProceso as $resumen){
                $total = $total  + $resumen["monto_total"];
                $totalAcademico = $totalAcademico + $resumen["monto_academico"];
                $totalNoAcademico = $totalNoAcademico + $resumen["monto_no_academico"];
            }
            
            $detalle_columnas_resumen = array(       
                                array(
                                    'name'=>'nombre_cc',
                                    'header'=>'Centro Costo',
                                    'value'=>'$data["nombre_cc"] ',
                                    'footer'=>''
                                ),
                                array(
                                    'name'=>'monto_academico',
                                    'header'=>'Monto Académico',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_academico"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($totalAcademico) . '</b>'

                                ),
                                array(
                                    'name'=>'monto_no_academico',
                                    'header'=>'Monto No Académico',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_no_academico"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($totalNoAcademico) . '</b>'

                                ),
                                array(
                                    'name'=>'monto_total',
                                    'header'=>'Monto Total',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_total"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($total) . '</b>'

                                ),


                            );
        
                $dataProviderResumen=new CArrayDataProvider($resumenProceso, array(
                    'keys'=>array('concepto'),
                    'sort'=>array(
                        'attributes'=>$columnas,
                    ),
                    'pagination'=>array(
                        'pageSize'=>10000,
                    ),
                ));
                
                return array("dataProvider"=>$dataProviderResumen,"columnas"=>$detalle_columnas_resumen);
                
        }
        
        
        
        
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
    public function actionValidaEncargado($model = null){
        
        if($model==null){
            $model=new ValidacionForm;
        }
            
        $periodos = ProdPeriodo::getPeriodosCargados();            

        foreach($periodos as $periodo){
            $periodo_id = $periodo->periodo_id;
            break;
        }
        
        $rol_id = Yii::app()->user->rolId;
            
        $listado_areas = ProdArea::getAreasFiltrada();

        $es_administrador = false;
        if($rol_id == ProdRol::ROL_ADMINISTRDOR){
            $rol_id = ProdRol::ROL_ENCARGADO;
            $es_administrador = true;

            foreach($listado_areas as $area){
                $area_seleccionada_id = $area->area_id;
                break;
            }
        }else{
            $area_seleccionada_id = Yii::app()->user->areaId;
        }

        $mostrar_detalle_tipo_productividad = 0;
        $tipo_personas = 1;
        if(isset($_POST['ValidacionForm']))
        {                
            $model->attributes=$_POST['ValidacionForm'];
            if(!$model->validate()){
                $valido=false;
            }

            $periodo_id = $model->periodo_id;
            $area_seleccionada_id = $model->area_id;
            $mostrar_detalle_tipo_productividad = $model->mostrar_detalle_tipo_productividad;
            
            if($model->tipo_personas == null){
                $model->tipo_personas = $tipo_personas;
            }
            
            if($model->area_id == null){
                $model->area_id = $area_seleccionada_id;
            }


        }else{
            $model->periodo_id = $periodo_id;            
            $model->mostrar_detalle_tipo_productividad=0;
            $model->tipo_personas = $tipo_personas;
            $model->area_id = $area_seleccionada_id;

        }

        Yii::app()->getSession()->add('form_exportar',$model);


        $periodo_seleccionado = ProdPeriodo::getPeriodo($periodo_id);

        $puede_validar=true;
        if($periodo_seleccionado->estado_periodo_id==ProdPeriodo::PERIODO_CERRADO){
            $puede_validar=false;                
        }

        $estado_periodo = "";
        if($periodo_seleccionado->estado_periodo_id == ProdPeriodo::PERIODO_ABIERTO){
            Yii::app()->user->setFlash('success', " Período abieto para el ingreso y la validación de productividades" );
        }

        if($periodo_seleccionado->estado_periodo_id == ProdPeriodo::PERIODO_VALIDACION){
            $dia_actual = date("d"); 

            $restantes = ProdPeriodo::DIAS_PARA_VALIDACION - $dia_actual;

            $registros_pendientes = ProdProductividad::getEstadoValidacionPeriodoArea($periodo_id,$area_seleccionada_id);

            $submensaje = "";
            if($registros_pendientes>0){
                $submensaje="<b>Aún existen registros sin validar</b>";
            }

            
            Yii::app()->user->setFlash('success', "<legend class='alertaCierre'> Período cerrado para el ingreso y abierto para la validación de productividades</legend>
                             El período se cerrará en $restantes días. $submensaje" );

        }

        if($periodo_seleccionado->estado_periodo_id == ProdPeriodo::PERIODO_CERRADO){
            Yii::app()->user->setFlash('error', " Período cerrado, no se puede ingresar o validar productividades" );
        }

        

        $personas_cargan = ProdProductividad::getPersonasCarganArea($periodo_id,$area_seleccionada_id);



        $resumenEstado = $this->getResumenEstado($periodo_id,$area_seleccionada_id);
        $resumenCentroCosto = $this->getResumenCentroCosto($periodo_id,$area_seleccionada_id);    


        

        $this->render('pageValidaEncargado',array(  'model'=>$model,
                                                    'periodos'=>$periodos,
                                                    "periodo_id"=>$periodo_id,
                                                    'listado_areas'=>$listado_areas,
                                                    'area_seleccionada_id'=>$area_seleccionada_id,
                                                    'es_administrador'=>$es_administrador,
                                                    "puede_validar"=>$puede_validar,          
                                                    "personas_cargan"=>$personas_cargan,    
                                                    "estado_periodo"=>$estado_periodo, 
                                                    "resumenEstado"=>$resumenEstado, 
                                                    "resumenCentroCosto"=>$resumenCentroCosto, 
                                                    "rol_id"=>$rol_id,
                                                    "mostrar_detalle_tipo_productividad"=>$mostrar_detalle_tipo_productividad
                                                  ));
    }
	
}