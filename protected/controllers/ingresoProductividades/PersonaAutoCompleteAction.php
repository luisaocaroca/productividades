<?php
/**
 * Action que se usa para buscar personas a través de autocompletes
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller.ingresoProductividades
 */
class PersonaAutoCompleteAction extends CAction
{

    /**
     *
     * @var array Arreglo con los resultados
     */
    private $results = array();
 
    /**
     * Función principal del action
     * 
     * @return array JSON arreglo con las personas encontradas
     */    
    public function run()
    {
        
        //if(isset($this->model) && isset($this->attribute)) {
            $criteria = new CDbCriteria();
            $busqueda = $_GET['term'];
            
            $criteria->addSearchCondition("nombres",$busqueda, true, 'OR');
            $criteria->addSearchCondition("apellido_paterno",$busqueda, true, 'OR');
            

            $model = new ProdPersona();
            foreach($model->findAll($criteria) as $m)
            {
                if($m->{"bo_activo"} == 1){
                    $this->results[] = array(    
                        'label'=>$m->{"nombres"} . " " . $m->{"apellido_paterno"} . " " . $m->{"apellido_materno"},
                        'value'=>$m->{"nombres"} . " " . $m->{"apellido_paterno"} . " " . $m->{"apellido_materno"},
                        'persona_id' =>$m->{"persona_id"}       
                     );
                }
                
            }
 
        //}
        echo CJSON::encode($this->results);
    }
}
?>