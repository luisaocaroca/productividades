<?php
/**
 * Action que se usa para buscar personas (académicos) a través de autocompletes
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller.ingresoProductividades
 */
class AcademicosAutoCompleteAction extends CAction
{

    /**
     *
     * @var array Arreglo con los resultados
     */
    private $results = array();
 
    /**
     * Función principal del action
     * 
     * @return array JSON arreglo con las personas encontradas
     */
    public function run()
    {
        
        //if(isset($this->model) && isset($this->attribute)) {
            $criteria = new CDbCriteria();
            $busqueda = $_GET['term'];
            $criteria->compare("nombres",$busqueda , true);
            $criteria->compare('bo_academico', 1);
            $criteria->compare("bo_activo",1);
            $model = new ProdPersona();
            foreach($model->findAll($criteria) as $m)
            {
                $this->results[] = array(    
                    'label'=>$m->{"nombres"} . " " . $m->{"apellido_paterno"} . " " . $m->{"apellido_materno"},
                    'value'=>$m->{"nombres"} . " " . $m->{"apellido_paterno"} . " " . $m->{"apellido_materno"},
                    'persona_id' =>$m->{"persona_id"}       
                 );
                
            }
 
        //}
        echo CJSON::encode($this->results);
    }
}
?>