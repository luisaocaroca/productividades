<?php

class ProdLogProductividadController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ProdLogProductividad'),
		));
	}

	public function actionCreate() {
		$model = new ProdLogProductividad;


		if (isset($_POST['ProdLogProductividad'])) {
			$model->setAttributes($_POST['ProdLogProductividad']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->historial_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ProdLogProductividad');


		if (isset($_POST['ProdLogProductividad'])) {
			$model->setAttributes($_POST['ProdLogProductividad']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->historial_id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'ProdLogProductividad')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
            
                $columnas = array();
                
		$dataProvider = new CActiveDataProvider('ProdLogProductividad',$columnas);
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new ProdLogProductividad('search');
		$model->unsetAttributes();

		if (isset($_GET['ProdLogProductividad']))
			$model->setAttributes($_GET['ProdLogProductividad']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}