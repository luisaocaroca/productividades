<?php
/**
 * Controlador del módulo de carga y mantención de las coordinaciones
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller
 */
class CoordinacionesController extends SecureController
{    
	
        /**
        * Control de acceso del módulo
        * En este caso solo el usuario validador puede entrar al módulo 
        * El administrador por defecto puede ver todos los módulos
        * 
        * @return array permisos de usuario al módulo
        */
	public function accessRules()
        {
            return array(
                //SOLO EL USUARIO CON PERMISO VALIDACION PUEDE ACCEDER AL CONTROLLER
                array('allow',
                      'roles' => array('validador'),
                ),
                array('deny',
                'users' => array('*'),
                ),
            );
        }
        
        /**
        * Funcionalidad para mostrar el listado de academicos en formato autocomplete
        * 
        * @return array listado academicos
        */
        public function actions()
        {
          return array(
            'academicos'=>array(
              'class'=>'application.controllers.ingresoProductividades.AcademicosAutoCompleteAction',
            ),
          );
        }
        
        
        /**
        * Action principal de la carga de coordinaciones
        * 
        * @return string despliega la vista coordinaciones/coordinaciones
        */        
        public function actionCoordinaciones()
	{
            
            $model = new CoordinacionesForm;
            $centros_costos = array();
            $centros_costos_coordinaciones = array();
            if(isset($_POST['CoordinacionesForm']))
            {
                $model->attributes=$_POST['CoordinacionesForm'];
                
                
                if($model->validate())
                {

                    $coordinacion_cargada = ProdCoordinacion::getCoordinacionesPersonaTipo($model->persona_id, $model->cc_id, $model->tipo_prod_id);
                    
                    //SI LA COORDINACION EXISTE LA ACTUALIZA, SINO LA CREA
                    if(!is_null($coordinacion_cargada)){
                        $coordinacion = ProdCoordinacion::model()->findByPk($coordinacion_cargada->coordinacion_id);
                        
                        $coordinacion->monto = $model->monto;                          
                        $coordinacion->cargada_productividad = 0;  
                        $coordinacion->descripcion = $model->descripcion;
                        
//                        echo "<pre>";
//                        print_r($coordinacion);
//                        echo "</pre>";
                        
                        if ($coordinacion->save()) {                           
                            Yii::app()->user->setFlash('success', "Coordinacion Actualizada Correctamente");
                            $model = new CoordinacionesForm;
                            $centros_costos = array();
                        }else{
                            
//                            print_r($coordinacion->getErrors());
                            $error_salida="";
                            foreach($coordinacion->getErrors() as $error){
                                $error_salida = $error[0];
                            }
                            
                            Yii::app()->user->setFlash('error', "La Coordinacion no se actualizó " . $error_salida);
                        }
                        
                        
                        
                    }else{                        
                        $coordinacion = new ProdCoordinacion();
                        $coordinacion->persona_id = $model->persona_id;
                        $coordinacion->tipo_prod_id = $model->tipo_prod_id;
                        $coordinacion->cc_id = $model->cc_id;
                        $coordinacion->monto = $model->monto;
                        $coordinacion->descripcion = $model->descripcion;
                        
                        if ($coordinacion->save()) { 
                            Yii::app()->user->setFlash('success', "Coordinacion Actualizada Correctamente");
                            $model = new CoordinacionesForm;
                        }else{
                            Yii::app()->user->setFlash('error', "La Coordinacion no se cargó");
                        }
                    }
                    
                }else{
                    if(!empty($model->tipo_prod_id)){
                        $centros_costos=  ProdCentroCosto::getCentrosCostosTipoProductividad($model->tipo_prod_id);
                    }
                }
                
            }
            
            $tipos_productividades = ProdTipoProductividad::getTiposCoordinaciones();            
            $coordinaciones = ProdCoordinacion::getCoordinaciones(true);
                        
            $traspasada_productividad = true;
            foreach($coordinaciones  as $coordinacion){
                
                $existe_cc = false;
                
                foreach($centros_costos_coordinaciones as $cc){
                    if($cc->cc_id == $coordinacion->cc_id){
                        $existe_cc=true;
                    }
                }
                
                if(!$existe_cc){
                    array_push($centros_costos_coordinaciones, $coordinacion->cc);
                }
                
                if(!$coordinacion->cargada_productividad){
                    $traspasada_productividad=false;
                }
            }
            
            foreach($centros_costos_coordinaciones as $cc){
                $tipos_prod = array();
                
                foreach($coordinaciones  as $coordinacion){
                    if($cc->cc_id == $coordinacion->cc_id){
                        if(!in_array($coordinacion->tipoProd, $tipos_prod)){
                            array_push($tipos_prod, $coordinacion->tipoProd);
                        }
                    }
                }
                $cc->prodTipoProductividads = $tipos_prod;
                
            }
            
            $salida_personas = $this->generaListadoCoordinaciones($centros_costos_coordinaciones,$coordinaciones);
            
            
            $totales_coordinacion = $this->getTotalesCoordinaciones($coordinaciones, $centros_costos_coordinaciones);
            
            $data = array("tipos_productividades"=>$tipos_productividades,
                          "salida_personas"=>$salida_personas,
                          "model"=>$model,
                          "traspasada_productividad"=>$traspasada_productividad,
                          "totales_coordinacion"=>$totales_coordinacion,
                          "centros_costos"=>$centros_costos,
                          "centros_costos_coordinaciones"=>$centros_costos_coordinaciones
                          );
            
            $this->render('coordinaciones',$data);
	}        
        
        /**
         * Despliega en pantalla el listado de coordinaciones
         * 
         * @param array $tipos_productividades Listado de tipos de productividades (solo coordinaciones)
         * @param array $coordinaciones listado de coordinaciones cargadas en la BD
         * @return array Listado de coordinaciones formateada para ver en pantalla
         */           
        public function generaListadoCoordinaciones($centros_costos,$coordinaciones){
            
            $personas = array();
        
            foreach($coordinaciones  as $coordinacion){

                $existe = false;
                foreach($personas as $persona){
                    if($persona->persona_id == $coordinacion->persona->persona_id ){
                        $existe = true;
                    }
                }

                if(!$existe){
                    array_push($personas, $coordinacion->persona);
                }
            }
            
            $salida_personas = array();
        
            //recorro el listado de personas        
            foreach($personas as $persona){            
                $coord_persona = array();

                //filtro del total de productividades las productividades de la persona
                foreach($coordinaciones  as $coordinacion){
                    if($coordinacion->persona_id==$persona->persona_id){
                        array_push($coord_persona, $coordinacion);
                    }
                }
                
                $detalle_coordinacion = array();
                
                foreach($centros_costos as $centro_costo){
                    foreach($centro_costo->prodTipoProductividads as $tipo_productividad){
                        $monto=0;                        

                        foreach($coord_persona as $coordinacion){  
                            //si existe la coordinacion para el tipo se carga el monto
                            if($coordinacion->tipo_prod_id==$tipo_productividad->tipo_prod_id &&
                               $coordinacion->cc_id == $centro_costo->cc_id
                               )     
                            {
                                $monto = $coordinacion->monto;                            
                            }

                        }

                        $salida = array("tipo_prod_id"=>$tipo_productividad->tipo_prod_id,
                                        "monto"=>$monto
                                      );
                        //se carga al listado
                        array_push($detalle_coordinacion, $salida);  
                    }
                }
                
                
                $array_persona_formateado = array("persona"=>$persona, 
                                                  "listado_coorinaciones"=>$detalle_coordinacion);
                array_push($salida_personas, $array_persona_formateado);
            }
            
            return $salida_personas;
        }
        
        /**
         * Ejecuta la actualización de las productividades a partir de las coordinaciones
         * Se usa para actualizarlo por ambiente WEB
         * 
         * @return string redirect a actionCoordinaciones
         */          
        function actionAplicarPeriodoAbiertos(){
            
            //SE APLICAN LOS CAMBIOS DE COORDINACIONES A LAS PRODUCTIVIDADES DEL PERIODO ACTUAL SIEMPRE Y CUANDO
            //ESTE ABIERTO          
            $this->aplicarCoordinacionesPeriodosAbiertos(Yii::app()->user->personaId);

            $this->actionCoordinaciones();
        }
        
        /**
         * Muestra el detalle de las coordinaciones de una persona
         * Los datos se despliegan a través de AJAX-JQUERY
         * 
        * @return string despliega la vista coordinaciones/detalleCoordinaciones
         */         
        public function actiondetalleCoordinaciones(){
            $persona_id = $_POST['persona_id'] ;
            
            $persona = ProdPersona::getPersona($persona_id);                       
            $detalle = ProdCoordinacion::getCoordinacionesPersona($persona_id);
            
            $this->renderPartial('detalleCoordinaciones',array("detalle"=>$detalle,"persona"=>$persona));
        }
        
        /**
         * Elimina una cooridinacion de una persona y actualiza la lista de coordinaciones
         * en pantalla a través de AJAX-JQUERYs
         * 
         * @return string redirect a actiondetalleCoordinaciones
         */             
        public function actionBorrarCoordinacion(){
            
            $persona_id = $_REQUEST['persona_id'] ;
            $coordinacion_id = $_REQUEST['coordinacion_id'] ;            
            
            $coordinacion = ProdCoordinacion::model()->findByPk($coordinacion_id);
                        
            $coordinacion->monto=0;
            $coordinacion->save();            
            
            $_POST['persona_id'] = $persona_id;
            
            $this->actiondetalleCoordinaciones();
        }
        
        /**
         * Muestra el listado de coordinaciones 
         * en pantalla a través de AJAX-JQUERYs
         * 
        * @return string despliega la vista coordinaciones/listadoCoordinaciones
         */        
        public function actionListadoCoordinaciones(){
            
            $tipos_productividades = ProdTipoProductividad::getTiposCoordinaciones();             
            $coordinaciones = ProdCoordinacion::getCoordinaciones();
            
            $traspasada_productividad = true;
            foreach($coordinaciones  as $coordinacion){
                if(!$coordinacion->cargada_productividad){
                    $traspasada_productividad=false;
                }
            }
            
            $salida_personas = $this->generaListadoCoordinaciones($tipos_productividades,$coordinaciones);
            
            $data = array("tipos_productividades"=>$tipos_productividades,
                          "salida_personas"=>$salida_personas,
                          "traspasada_productividad"=>$traspasada_productividad
                          );
            
            $this->renderPartial('listadoCoordinaciones',$data);
            
        }
        
        /**
        * Toma una coordinación en particular de un usuario y la carga en el formulario
        * de ingreso para que esta pueda ser modificada por el usuario
        * 
        * @return string despliega la vista coordinaciones/coordinaciones
        */             
        public function actionEditarCoordinacion(){
            
            $coordinacion_id = $_REQUEST['coordinacion_id'] ;

            $coordinacion = ProdCoordinacion::model()->findByPk($coordinacion_id);
            
            $model = new CoordinacionesForm;
            $model->monto = $coordinacion->monto;
            $model->tipo_prod_id = $coordinacion->tipo_prod_id;
            $model->persona_id = $coordinacion->persona_id;
            $model->nombre_persona = $coordinacion->persona;
            $model->cc_id = $coordinacion->cc_id;
            $model->descripcion = $coordinacion->descripcion;
            
            $centros_costos_coordinaciones = array();
            $tipos_productividades = ProdTipoProductividad::getTiposCoordinaciones();            
            $centros_costos=  ProdCentroCosto::getCentrosCostosTipoProductividad($coordinacion->tipo_prod_id);
            
            $coordinaciones = ProdCoordinacion::getCoordinaciones(true);
                        
            $traspasada_productividad = true;
            foreach($coordinaciones  as $coordinacion){
                
                $existe_cc = false;
                
                foreach($centros_costos_coordinaciones as $cc){
                    if($cc->cc_id == $coordinacion->cc_id){
                        $existe_cc=true;
                    }
                }
                
                if(!$existe_cc){
                    array_push($centros_costos_coordinaciones, $coordinacion->cc);
                }
                
                if(!$coordinacion->cargada_productividad){
                    $traspasada_productividad=false;
                }
            }
            
            foreach($centros_costos_coordinaciones as $cc){
                $tipos_prod = array();
                
                foreach($coordinaciones  as $coordinacion){
                    if($cc->cc_id == $coordinacion->cc_id){
                        if(!in_array($coordinacion->tipoProd, $tipos_prod)){
                            array_push($tipos_prod, $coordinacion->tipoProd);
                        }
                    }
                }
                $cc->prodTipoProductividads = $tipos_prod;
                
            }
            
            $salida_personas = $this->generaListadoCoordinaciones($centros_costos_coordinaciones,$coordinaciones);
            
            
            $totales_coordinacion = $this->getTotalesCoordinaciones($coordinaciones, $centros_costos_coordinaciones);
            
            $data = array("tipos_productividades"=>$tipos_productividades,
                          "salida_personas"=>$salida_personas,
                          "model"=>$model,
                          "traspasada_productividad"=>$traspasada_productividad,
                          "totales_coordinacion"=>$totales_coordinacion,
                          "centros_costos"=>$centros_costos,
                          "centros_costos_coordinaciones"=>$centros_costos_coordinaciones
                          );
            
            $this->render('coordinaciones',$data);
//            
//   
//            $tipos_productividades = ProdTipoProductividad::getTiposCoordinaciones();            
//            $centros_costos=  ProdCentroCosto::getCentrosCostosTipoProductividad($coordinacion->tipo_prod_id);
//            
//            $coordinaciones = ProdCoordinacion::getCoordinaciones(true);
//            
//            $centros_costos_coordinaciones=array();
//            
//            $traspasada_productividad = true;
//            foreach($coordinaciones  as $coordinacion){
//                
//                $existe_cc = false;
//                
//                foreach($centros_costos_coordinaciones as $cc){
//                    if($cc->cc_id == $coordinacion->cc_id){
//                        $existe_cc=true;
//                    }
//                }
//                
//                if(!$existe_cc){
//                    array_push($centros_costos_coordinaciones, $coordinacion->cc);
//                }
//                
//                if(!$coordinacion->cargada_productividad){
//                    $traspasada_productividad=false;
//                }
//            }
//            
//            foreach($centros_costos_coordinaciones as $cc){
//                $tipos_prod = array();
//                
//                foreach($coordinaciones  as $coordinacion){
//                    if($cc->cc_id == $coordinacion->cc_id){
//                        if(!in_array($coordinacion->tipoProd, $tipos_prod)){
//                            array_push($tipos_prod, $coordinacion->tipoProd);
//                        }
//                    }
//                }
//                $cc->prodTipoProductividads = $tipos_prod;
//                
//            }
//            
//            
//            $salida_personas = $this->generaListadoCoordinaciones($centros_costos_coordinaciones,$coordinaciones);
//            
//            $totales_coordinacion = $this->getTotalesCoordinaciones($coordinaciones, $tipos_productividades);
//            
//            $data = array("tipos_productividades"=>$tipos_productividades,
//                          "salida_personas"=>$salida_personas,
//                          "model"=>$model,
//                          "traspasada_productividad"=>$traspasada_productividad,
//                          "totales_coordinacion"=>$totales_coordinacion,
//                          "centros_costos"=>$centros_costos,
//                          "centros_costos_coordinaciones"=>$centros_costos
//                          );
//            
//            $this->render('coordinaciones',$data);
            
        }

        /**
         * Calcula el total de las coordinaciones
         * 
         * @param array $coordinaciones listado de coordinaciones cargadas en la BD
         * @param array $tipos_productividades Listado de tipos de productividades (solo coordinaciones)
         * @return int Valor total de las coordinaciones
         */              
        private function getTotalesCoordinaciones($coordinaciones,$centros_costos){
            $totales_coordinacion = array();
            
            foreach($centros_costos as $centro_costo){
            
                foreach($centro_costo->prodTipoProductividads as $tipo_productividad){
                
                    $monto_total=0;                        

                    foreach($coordinaciones  as $coordinacion){
                        //si existe la coordinacion para el tipo se carga el monto
                        if($coordinacion->cc_id==$centro_costo->cc_id &&
                           $coordinacion->tipo_prod_id==$tipo_productividad->tipo_prod_id
                           )     
                        {
                            $monto_total = $monto_total + $coordinacion->monto;                            
                        }

                    }

                    //se carga al listado
                    array_push($totales_coordinacion, $monto_total);  
                    
                }
            }
            
            return $totales_coordinacion;
        }
        
        /**
         * Se aplican los cambios en las coordinacones a las productividades de los períodos abiertos.
         * 
         * @param int $persona_id Persona que aplica el cambio.
         */            
        public function aplicarCoordinacionesPeriodosAbiertos($persona_id){
            $coordinaciones = ProdCoordinacion::getCoordinaciones();
            $periodo_actual_id = date("Ym");   
            
            $periodo = ProdPeriodo::getPeriodo($periodo_actual_id);
            
            //SE APLICA EL CAMBIO SI EL PERIODO ESTA ABIERTO
            if($periodo->estado_periodo_id == ProdPeriodo::PERIODO_ABIERTO){
                //BORRAR PRODUCTIVIDADES
                $productividades = ProdProductividad::getProductividadesObtenidasDeCoordinaciones($periodo_actual_id);

                foreach($productividades as $productividad){
                    $productividad->delete();      
                }

                //CARGAR PRODUCTIVIDADES
                foreach($coordinaciones  as $coordinacion){

                    //las coordinaciones no se borran directamente de la pagina, sino se dejan en cero
                    //para saber que es necesario actualizar los cambios hechos hacia las prodictividades
                    if($coordinacion->monto>0){
                        $productividad = new ProdProductividad();
                        $productividad->periodo_id = $periodo_actual_id;
                        $productividad->tipo_prod_id = $coordinacion->tipo_prod_id;
                        $productividad->persona_id = $coordinacion->persona_id;
                        $productividad->cc_id = $coordinacion->cc_id;
                        $productividad->cuota_actual = 1;
                        $productividad->numero_cuotas = 1;
                        $productividad->monto = $coordinacion->monto;
                        $productividad->total_monto_asignado = $coordinacion->monto;
                        $productividad->estado_id = ProdEstado::APROBADO;
                        $productividad->persona_carga_id = $persona_id;
                        $productividad->observaciones = $coordinacion->descripcion;
                        $productividad->area_id =0;

                        $productividad->save();

                        //actualiza coordinacion para marcar que está realizado el cambio
                        //LO HAGO CON OTRO OBJETO PORQUE SI MODIFICO EL DE LA LISTA PIERDO LA DESENCRIPTACOON DEL MONTO
                        $modificar_coordinacion = ProdCoordinacion::model()->findByPk($coordinacion->coordinacion_id);                        
                        $modificar_coordinacion->cargada_productividad=1;
                        $modificar_coordinacion->save();

                    }else{
                        $coordinacion->delete();
                    }

                }
            }
            
        }


        /**
         * Obtiene el listados de centros de costo asociados a un tipo de productividad
         * (que es una coordinacion)
         * Se usa para actualizar a través de AJAX la etiqueta SELECT de centros de costo
         * del formulario de ingreso de las coordinaciones.
         * 
         * @return string Listado de centros de costo en formato de etiqueta HTML SELECT
         */   
        public function actionCentroCostos()
        {
            
            $tipo_prod_id = $_POST['CoordinacionesForm']['tipo_prod_id'];            
            $centros_costo = ProdCentroCosto::getCentrosCostosTipoProductividad($tipo_prod_id);
                        
            $data=CHtml::listData($centros_costo,'cc_id','nombreCompleto');
            foreach($data as $value=>$name)
            {
                echo CHtml::tag('option',
                           array('value'=>$value),CHtml::encode($name),true);
            }
        }
}