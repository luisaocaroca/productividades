<?php
/**
 * Controlador del módulo de consultas de las productividades
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller
 */
class ProductividadesController extends SecureController
{
    
        /**
        * Control de acceso del módulo
        * Todos los usuarios tienen accceso a las productividades individuales
        * Para las productividades globales, pueden acceder los usuarios ingreso (encargados),
        * validadores (jefe administrativo) y el director.
        * El administrador por defecto puede ver todos los módulos
        * 
        * @return array permisos de usuario al módulo
        */
	public function accessRules()
        {
            return array(
                //SOLOS LOS USUARIOS CON PERMISO INGRESO O VALIDACION PUEDE ENTRAR A LAS 
                //PRODUCTIVIDADES GLOBALES
                array('allow',
                      'actions'=>array('globales','historiaProductividadesPersona'),
                      'roles' => array('ingreso','encargado','validador','director')
                ),
                //TODO USUARIO AUTENTICADO PUEDE INGRESAR A ESTAS ACCIONES
                array('allow',
                      'actions'=>array( 'index',
                                        'individuales',
                                        'detalleProductividadPersona',
                                        'exportarExcel',
                                        'filtroGlobales',
                                        'exportarGolbalesExcel',
                                        'exportarExcelDetallePersona',
                                    ),
                      'users' =>array('@')
                ),
                array('deny',
                    'users'=>array('*'),
                ),
            );
        }
        
        /**
        * Index del modulo. 
         * Si el usuario que ingresa es académico o no académico, se mostraran las productividades individuales
         * sino se mostrara el index del modulo
        * 
        * @return string despliega la vista productividades/inicio
        */           
	public function actionIndex()
	{
            if(Yii::app()->user->rolId==ProdRol::ROL_CONSULTA){
                $this->actionIndividuales();
            }else{
                $this->actionGlobales();
            }
	}

        /**
        * Muestra las productividades individuales del usuario que se conecta
        * 
        * @return string despliega la vista productividades/individuales
        */           
        public function actionIndividuales()
	{
            //echo "<pre>";
            //print_r($_POST);
            //print_r($_GET);
            //echo "</pre>";
            
            $form = new ConsultasForm;   
            $anio_actual_id = date("Y");  
             
            $anios = ProdPeriodo::getAnios();
            $meses = ProdPeriodo::getMeses();
                       
            //por defecto el tipo de consulta se deja como el año
            $tipo_consulta_id = 0;
            $form->tipo_consulta = 0;
            $valido = true;
            if(isset($_POST['ConsultasForm']))
            {
                //para mantener el periodo seleccionado cuando se ejecuta el formulario
                
                $form->attributes=$_POST['ConsultasForm'];    
                
                if($form->limpiar==1){
                    $form = new ConsultasForm; 
                    $form->tipo_consulta=0;
                    $anio_id = $anio_actual_id;
                }else{  
                    if(!$form->validate()){
                        $valido=false;
                    }

                    $tipo_consulta_id = $form->tipo_consulta;
                    $anio_id = $form->anio_id;
                }
            }else{
                //SI LA CONSULTA SE LLAMO POR GET (DETALLE DE LA CONSULTA POR AÑO)
                if(isset($_GET["tipo_consulta"]) && !isset($_GET["ProdProductividad_sort"]) ){
                    
                    $tipo_consulta_id = $_GET["tipo_consulta"];
                    $form->tipo_consulta = $tipo_consulta_id;
                    
                    if($tipo_consulta_id==1){
                         $form->anio_id = $_GET["anio_id"];
                         $anio_id= $_GET["anio_id"];
                    }
                    
                    if($tipo_consulta_id==2){
                        $form->mes_id = $_GET["mes_id"];
                        $form->anio_mes_id = $_GET["anio_mes_id"];

                        $anio_id= $_GET["anio_mes_id"];
                        $form->anio_id = $anio_actual_id;
                    }
                    
                    if($tipo_consulta_id==3){
                        $form->mes_desde_id = $_GET["mes_desde_id"];
                        $form->anio_desde_id = $_GET["anio_desde_id"];
                        $form->mes_hasta_id = $_GET["mes_hasta_id"];
                        $form->anio_hasta_id = $_GET["anio_hasta_id"];
                        
                        $anio_id= $_GET["anio_desde_id"];
                        $form->anio_id = $anio_id;
                    }
                    
                    
                    
                }else{          
                    $anio_id= $anio_actual_id;
                    //$form->anio_id = $anio_actual_id;
                    //
                    //cuando ordena recupera el form desde session
                    if(isset($_GET["ProdProductividad_sort"]) || isset($_GET["ProdProductividad_page"])|| isset($_GET["ajax"])){
                        $form = Yii::app()->getSession()->get('form_individual');
                        $tipo_consulta_id = $form->tipo_consulta;
                        $anio_id = $form->anio_id;
                    }else{
                        Yii::app()->getSession()->add('form_individual',null);  
                    }
                    
                    
                }
            }
                     
            Yii::app()->getSession()->add('form_individual',$form); 
            Yii::app()->getSession()->add('form_exportar',$form); 
            $form->persona_id = Yii::app()->user->personaId;
            
            //para hablitar los controles en la página
            //dependiendo de la consulta seleccionada          
            $disabled_c1=false;
            $disabled_c2=false;
            $disabled_c3=false;
            
            if($tipo_consulta_id!=1){
                $disabled_c1=true;
            }
            
            if($tipo_consulta_id!=2){
                $disabled_c2=true;
            }
            
            if($tipo_consulta_id!=3){
                $disabled_c3=true;
            }
            
            
            if($valido){
                Yii::app()->getSession()->add('form_individual',$form); 
                if($tipo_consulta_id==0){
                    $html = $this->getVistaIndividualTotal($form);
                }
                
                if($tipo_consulta_id==1){
                    $html = $this->getVistaIndividualAnio($form);
                }

                if($tipo_consulta_id==2){
                    $html = $this->getVistaConsultaIndividual($form);
                }

                if($tipo_consulta_id==3){
                    $html = $this->getVistaConsultaIndividualPeriodo($form);
                }
            
            }else{
                $html = "";
            }
                        
            $this->render('individuales',array('model'=>$form,
                                               "anios"=>$anios,
                                               "meses"=>$meses,            
                                               "anio_id"=>$anio_id,
                                               "disabled_c1"=>$disabled_c1,
                                               "disabled_c2"=>$disabled_c2,
                                               "disabled_c3"=>$disabled_c3,
                                               "html_consulta"=>$html
                                                ));           
	}
        
        /**
         * Muestra las productividades globales, es decir el listado de productividades
         * de todos los usuarios en un mes en particular.
         * Si el usuario es un encargado, solo verá las productividades que el cargo.
         * Si el usuario es validador o director, verán todas las productividades
         * 
         * @return string despliega la vista productividades/globales
        */            
        public function actionGlobales()
	{

            $form = new ConsultasForm;   
            
            $anios = ProdPeriodo::getAnios();
            $meses = ProdPeriodo::getMeses();
                       
            //por defecto el tipo de consulta se deja como el año y mes actual
            //VALORES POR DEFECTO PARA LA PRIMERA VEZ QUE ENTRA
            $tipo_consulta_id = 2;
            $mes_id = date("m");
            $anio_actual_id = date("Y"); 
            $tipo_personas = 1;
            $valido = true;
            $mostrar_detalle_tipo_productividad=0;
            $mostrar_positivos=0;
            
            if(isset($_POST['ConsultasForm']))
            {
                //para mantener el periodo seleccionado cuando se ejecuta el formulario
                
                $form->attributes=$_POST['ConsultasForm'];    
                //limpiar filtros

                
                if(!$form->validate()){
                    $valido=false;
                }
                
                $tipo_consulta_id = $form->tipo_consulta;
                $anio_id = $form->anio_id;
                $mostrar_detalle_tipo_productividad = $form->mostrar_detalle_tipo_productividad;
                $mostrar_positivos = $form->mostrar_positivos;

                
            }else{
                $anio_id= $anio_actual_id;
                $form->anio_id = $anio_actual_id;
                $form->tipo_personas = $tipo_personas;
                $form->tipo_consulta = $tipo_consulta_id;
                $form->anio_mes_id= $anio_actual_id;
                $form->mes_id = $mes_id;
                $form->mostrar_detalle_tipo_productividad=0;
                $form->mostrar_positivos=0;

            }
                                
            Yii::app()->getSession()->add('form_exportar',$form);
            
            //para hablitar los controles en la página
            //dependiendo de la consulta seleccionada
            $disabled_c1=false;
            $disabled_c2=false;
            $disabled_c3=false;
            
            if($tipo_consulta_id!=1){
                $disabled_c1=true;
            }
            
            if($tipo_consulta_id!=2){
                $disabled_c2=true;
            }
            
            if($tipo_consulta_id!=3){
                $disabled_c3=true;
            }
            
            if($tipo_consulta_id==1){
                $periodo_inicio_id = $form->anio_id*100 + 1;
                $periodo_fin_id = $form->anio_id*100 + 12;
            }
            
            if($tipo_consulta_id==2){
                $periodo_inicio_id = $form->anio_mes_id*100 + $form->mes_id;
                $periodo_fin_id = 0;
            }
            
            if($tipo_consulta_id==3){
                $periodo_inicio_id = $form->anio_desde_id*100 + $form->mes_desde_id;
                $periodo_fin_id = $form->anio_hasta_id*100 + $form->mes_hasta_id;
            }
            
                        
            
            
            $periodo = ProdPeriodo::getPeriodo($periodo_inicio_id);
            if($periodo_fin_id!=0){
                $periodo_fin = ProdPeriodo::getPeriodo($periodo_fin_id);
            }else{
                $periodo_fin=null;
            }
            
            
            $area_id =0;
            $rol_id = Yii::app()->user->rolId;
            if($rol_id == ProdRol::ROL_ENCARGADO || $rol_id == ProdRol::ROL_INGRESO_DATOS){
                $area_id = Yii::app()->user->areaId;
                        
            }
            
            $this->render('globales', array('model'=>$form,
                                            "anios"=>$anios,
                                            "meses"=>$meses,            
                                            "anio_id"=>$anio_id,
                                            "disabled_c1"=>$disabled_c1,
                                            "disabled_c2"=>$disabled_c2,
                                            "disabled_c3"=>$disabled_c3,
                                            "periodo_id"=>$periodo_inicio_id,
                                            "periodo_fin_id"=>$periodo_fin_id,
                                            "periodo"=>$periodo,
                                            "periodo_fin"=>$periodo_fin,
                                            "valido"=>$valido,
                                            "mostrar_detalle_tipo_productividad"=>$mostrar_detalle_tipo_productividad,
                                            "mostrar_positivos"=>$mostrar_positivos,
                                            'mostrar_filtro'=>false,
                                            'area_id'=>$area_id
                                             ));        



	}
        
        /**
         * Muestra el detalle de las productividades de una persona para un periodo en particular
         * o un rango de periodos.
         * Los datos son desplegados a través de AJAX/JQUERY
         * 
         * @return string despliega la vista productividades/detalleProductividadPersona
        */          
        public function actionDetalleProductividadPersona(){
            $persona_id = $_REQUEST['persona_id'] ;
            $periodo_id = $_REQUEST['periodo_id'] ;
            
            if(isset($_REQUEST['periodo_fin_id'] )){
               $periodo_fin_id = $_REQUEST['periodo_fin_id'] ; 
            }else{
                $periodo_fin_id=0;
            }
            
            
            if($periodo_fin_id==0){
                $periodo_fin_id = null;
                $periodo_fin = null;            
            }else{
                $periodo_fin = ProdPeriodo::getPeriodo($periodo_fin_id);     
            }
            
//            echo $periodo_fin_id;            
//            echo $periodo_fin;
            
            
            $persona = ProdPersona::getPersona($persona_id);
            $periodo = ProdPeriodo::getPeriodo($periodo_id);      
                        
//            $estado_id = 1;
//            $detalle = ProdProductividad::getProductividadesIndividuales($persona_id, ProdArea::TODOS, $estado_id, $periodo_id,$periodo_fin_id);
//            
//            $periodos = array();
//            foreach($detalle as $prod){
//                if(!in_array($prod->periodo, $periodos)){
//                    array_push($periodos, $prod->periodo);
//                }
//            }
            
            $model = new ProdProductividad();
            $model->persona_id = $persona_id;
            $model->estado_id =  ProdEstado::TODOS;
            
            if($periodo_fin_id!=0){
                $route = "productividades/detalleProductividadPersona/persona_id/".$persona_id."/periodo_id/".$periodo_id."/periodo_fin_id/".$periodo_fin_id. "/";
            }else{
                $route = "productividades/detalleProductividadPersona/persona_id/".$persona_id."/periodo_id/".$periodo_id . "/";
            }
            
            $area_id =0;
            $rol_id = Yii::app()->user->rolId;
            if($rol_id == ProdRol::ROL_ENCARGADO || $rol_id == ProdRol::ROL_INGRESO_DATOS){
                $area_id = Yii::app()->user->areaId;
                        
            }
            
            $detalle = ProdProductividad::getProductividadesIndividualesDataProvider($model, $periodo_id,$periodo_fin_id,$route,true,100,$area_id);

            //Yii::app()->clientScript->scriptMap['*.js'] = false;

            $this->renderPartial('detalleProductividadPersona',array("detalle"=>$detalle,
                                                                     "persona"=>$persona,
                                                                     'periodo'=>$periodo,
                                                                     'periodo_fin'=>$periodo_fin,
                                                                     'model'=>$model
                                                                    ), false, true);
        }
        
        /**
         * Muestra el listado de productividades agrupadas por año de una persona en particular
         * Los datos son desplegados a través de AJAX/JQUERY
         * 
         * @param ConsultasForm $form Formulario de consultas
         * @return string despliega la vista productividades/resumenAnual
        */          
        public function getVistaIndividualAnio(ConsultasForm $form,$excel=false){
            $anio_id = $form->anio_id;
            $persona_id =  $form->persona_id;
            $persona = ProdPersona::getPersona($persona_id);
            $periodo_actual_id = date("Ym"); 
            
            $periodo_inicio = $anio_id . "01";
            $periodo_fin = $anio_id . "12";
            
            $estado_id = ProdEstado::TODOS; 
            $detalle = ProdProductividad::getProductividadesIndividuales($persona_id,  ProdArea::TODOS, $estado_id, $periodo_inicio, $periodo_fin);
            
            $periodos = array();
            
            //obtiene periodos del año
            foreach($detalle as $prod){
//                echo $prod->periodo_id . "->";
//                echo $prod->monto;
//                echo "<br>";
//                
                if($prod->periodo_id<=$periodo_actual_id){
                    $detalle_periodo = array("periodo"=>$prod->periodo);

                    if(!in_array($detalle_periodo, $periodos)){
                        array_push($periodos, $detalle_periodo);
                    }    
                }
                
                
            }
            
            $periodos_salida = array();
            //resumen los montos y genera array para pantalla
            foreach($periodos as $periodo){
                $monto_periodo = 0;
                foreach($detalle as $prod){
                    if($prod->periodo_id==$periodo["periodo"]->periodo_id){
                        $monto_periodo = $monto_periodo + $prod->monto;
                    }
                }
                
                $periodo_array=array("periodo"=>$periodo["periodo"],"monto"=>$monto_periodo);
                array_push($periodos_salida, $periodo_array);
            }
            
            
            if($excel){
                return $periodos_salida;
            }else{
            
                $dataProvider=new CArrayDataProvider($periodos_salida, array(
                    'keys'=>array('periodo'),
                    'sort'=>array(
                        'attributes'=>array('periodo','monto'),
                    ),
                    'pagination'=>array(
                        'pageSize'=>10000,
                    ),
                ));

                        
                        
                return $this->renderPartial('resumenAnual',
                                            array("anio_id"=>$anio_id,
                                                  "persona"=>$persona,
                                                  "periodos"=>$periodos_salida,
                                                  "dataProvider"=>$dataProvider
                                                  ),
                                            true);           
            }
            
            
            
        }
        
        
        /**
         * Muestra el listado de productividades de toda la historia agrupada por año
         * Los datos son desplegados a través de AJAX/JQUERY
         * 
         * @param ConsultasForm $form Formulario de consultas
         * @return string despliega la vista productividades/resumenTotal
        */          
        public function getVistaIndividualTotal(ConsultasForm $form,$excel=false){
            $persona_id =  $form->persona_id;
            $persona = ProdPersona::getPersona($persona_id);
          
            
            $estado_id = ProdEstado::TODOS; 
            $detalle = ProdProductividad::getProductividadesIndividuales($persona_id,  ProdArea::TODOS , $estado_id);
            
            $anios = array();
            
            //obtiene periodos del año
            foreach($detalle as $prod){
//                echo $prod->periodo_id . "->";
//                echo $prod->monto;
//                echo "<br>";
//                
                $detalle_anio = array("anio"=>$prod->periodo->anio);

                if(!in_array($detalle_anio, $anios)){
                    array_push($anios, $detalle_anio);
                }    
                
                
            }
            
            $periodos_salida = array();
            //resumen los montos y genera array para pantalla
            foreach($anios as $anio){
                $monto_anual = 0;
                foreach($detalle as $prod){
                    if($prod->periodo->anio==$anio["anio"]){
                        $monto_anual = $monto_anual + $prod->monto;
                    }
                }
                
                $periodo_array=array("anio"=>$anio["anio"],"monto"=>$monto_anual);
                array_push($periodos_salida, $periodo_array);
            }
            
            if($excel){
                return $periodos_salida;
            }else{
                
                 $dataProvider=new CArrayDataProvider($periodos_salida, array(
                    'keys'=>array('anio'),
                    'sort'=>array(
                        'attributes'=>array('anio','monto'),
                    ),
                    'pagination'=>array(
                        'pageSize'=>10000,
                    ),
                ));

            
                return $this->renderPartial('resumenTotal',
                                            array(  "persona"=>$persona,
                                                    "periodos"=>$periodos_salida,
                                                    "dataProvider"=>$dataProvider
                                                  ),
                                            true);      
            }
	
            
            
            
        }
        
        /**
         * Muestra el resumen de las productividades de un periodo para una persona en particular
         * Los datos son desplegados a través de AJAX/JQUERY
         * 
         * @param ConsultasForm $form Formulario de consultas
         * @return string despliega la vista productividades/reporteIndividualesPeriodo
        */           
        public function getVistaConsultaIndividual(ConsultasForm $form,$excel=false){
            
            $periodo_id = $form->anio_mes_id*100 + $form->mes_id; 
            
            $persona_id =  $form->persona_id;
            $persona = ProdPersona::getPersona($persona_id);            
            $periodo = ProdPeriodo::getPeriodo($periodo_id);    
            
            if($excel){
                return  ProdProductividad::getProductividadesIndividuales($persona_id, ProdArea::TODOS, ProdEstado::TODOS, $periodo_id);
            }else{
           
                $model = new ProdProductividad();
                $model->persona_id = $persona_id;
                $model->estado_id =  ProdEstado::TODOS;

                $detalle = ProdProductividad::getProductividadesIndividualesDataProvider($model,$periodo_id);


                return $this->renderPartial('reporteIndividualesPeriodo',
                                            array(  "periodo_id"=>$periodo_id,
                                                    "detalle"=>$detalle,
                                                    "persona"=>$persona,
                                                    'periodo'=>$periodo,
                                                    'tipo_consulta'=>1,
                                                    'model'=>$model
                                                 ),
                                            true);
            }
        }
        
        /**
         * Muestra el resumen de las productividades de un periodo para una rango de tiempo
         * Los datos son desplegados a través de AJAX/JQUERY
         * 
         * @param ConsultasForm $form Formulario de consultas
         * @return string despliega la vista productividades/reporteIndividualesPeriodo
        */            
        public function getVistaConsultaIndividualPeriodo(ConsultasForm $form,$excel=false){
            
            $periodo_desde_id = $form->anio_desde_id*100 + $form->mes_desde_id; 
            $periodo_hasta_id = $form->anio_hasta_id*100 + $form->mes_hasta_id; 
            
            $periodo = ProdPeriodo::getPeriodo($periodo_desde_id);   
            $periodo_fin = ProdPeriodo::getPeriodo($periodo_hasta_id);   
            
            $persona_id =  $form->persona_id;
            $persona = ProdPersona::getPersona($persona_id);     
            
            if($excel){
                return  ProdProductividad::getProductividadesIndividuales($persona_id, ProdArea::TODOS, ProdEstado::TODOS, $periodo_desde_id,$periodo_hasta_id);
            }else{
                $model = new ProdProductividad();
//            if (isset($_GET['ProdProductividad']))
//                $model->setAttributes($_GET['ProdProductividad']);
            
                $model->persona_id = $persona_id;
                $model->estado_id =  ProdEstado::TODOS;
                $detalle = ProdProductividad::getProductividadesIndividualesDataProvider($model, $periodo_desde_id,$periodo_hasta_id);


                return $this->renderPartial('reporteIndividualesPeriodo',
                                            array(  "periodo_id"=>$periodo_hasta_id,
                                                    "detalle"=>$detalle,
                                                    "persona"=>$persona,
                                                    'periodo'=>$periodo,
                                                    'periodo_fin'=>$periodo_fin,
                                                    'tipo_consulta'=>3,
                                                    'model'=>$model),
                                            true);
            }
            
            
        }
        
        /**
         * Muestra el historial de productividades de una persona.
         * Se usa para ver los datos de productividades de periodos anteriores (ultimos 6 meses)
         * de una persona desde el modulo de validacion
         * Los datos son desplegados a través de AJAX/JQUERY
         * 
         * @return string despliega la vista productividades/historialProductividadesPersona
        */                  
        public function actionHistoriaProductividadesPersona(){
            //print_r($_POST);
            
            $form = new CambiarEstadoForm();
            $form->attributes = $_POST["CambiarEstadoForm"];
                        
            $persona_id = $form->persona_id;
            $periodo_id = $form->periodo_id;
            $es_academico = $form->es_academico;
 
            
            $persona = ProdPersona::getPersona($persona_id);            
            $periodo_fin = ProdPeriodo::getPeriodo($periodo_id);
            
            $anio = $periodo_fin->anio;
            $mes = $periodo_fin->mes;
            
            
            for($i=0;$i<=11;$i++){
                
                if($mes==1){
                    $mes=12;
                    $anio=$anio-1;
                }else{
                    $mes--;
                }
                //echo $mes."-";
            }
            
            $periodo_inicio_id=$anio*100+$mes;
            $periodo = ProdPeriodo::getPeriodo($periodo_inicio_id);            
            
            $model = new ProdProductividad();
            $model->persona_id = $persona_id;
            $model->estado_id =  ProdEstado::TODOS;
            
            
            
            $route = "productividades/historiaProductividadesPersona/persona_id/".$persona_id."/periodo_id/".$periodo_id. "/";

            
            $detalle = ProdProductividad::getProductividadesIndividualesDataProvider($model, $periodo_inicio_id,$periodo_id,$route,true,100);

            
                
            
            return $this->renderPartial('historialProductividadesPersona',
                                        array(  "periodo_id"=>$periodo_id,
                                                "es_academico"=>$es_academico,
                                                "resultado_html"=>"",
                                                "persona"=>$persona,
                                                "detalle"=>$detalle,
                                                "periodo"=>$periodo,
                                                "periodo_fin"=>$periodo_fin,
                                                "model"=>$model)
                                        );
            
        }
        
        
        public function actionExportarExcel(){
            $form = Yii::app()->getSession()->get('form_individual');
                        
            $persona_id =  $form->persona_id;
            $persona = ProdPersona::getPersona($persona_id);  
            
            $periodo_inicio = null;
            $periodo_termino = null;
            
            
            
            if($form->tipo_consulta==0){
                $detalle = $this->getVistaIndividualTotal($form, true);
            }
            
            if($form->tipo_consulta==1){
                $detalle = $this->getVistaIndividualAnio($form, true);
            }
            
            if($form->tipo_consulta==2){
                
                $periodo_id = $form->anio_mes_id*100 + $form->mes_id;             
                $periodo_inicio = ProdPeriodo::getPeriodo($periodo_id);  

                $detalle = $this->getVistaConsultaIndividual($form, true);
            }
            
            if($form->tipo_consulta==3){
                $periodo_desde_id = $form->anio_desde_id*100 + $form->mes_desde_id; 
                $periodo_hasta_id = $form->anio_hasta_id*100 + $form->mes_hasta_id; 

                $periodo_inicio = ProdPeriodo::getPeriodo($periodo_desde_id);   
                $periodo_termino = ProdPeriodo::getPeriodo($periodo_hasta_id);  
            
                $detalle = $this->getVistaConsultaIndividualPeriodo($form, true);
            }
            
           
            $objPHPExcel = new PHPExcel();
            
            $styleHeaderArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'E1E0F7'),
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )   ,
                'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )          
            );
            
            $styleHeaderCCArray = array(
                'borders' => array(
                                'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                                ) 
                            ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'E1E0F7'),
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )            ,
                'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            )
            );
            
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'font' => array(
                    'size' => 8
                )  
            );
            
            $styleTotalArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )  
            );

            $currencyFormat =  iconv("ISO-8859-1", "UTF-8", '$ #,##0');
            

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle('Resultados Consulta');

            $row = 2; 
            
            
            
            if($form->tipo_consulta==0){
                
                $titulo = "Detalle Productividades " . $persona . ", resumen Toda la Historia";
                
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$titulo );
                
                $row++;
                $row++;

                $col=0;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Año");  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Total");  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;
                
                $row++;
                
                foreach($detalle as $productividad){
                    $col=0;

                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad["anio"]);  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $col++;
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$productividad["monto"]);  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                    $col++;
                    $row++;
                    
                    
                }
                
                
            
            }
            
            if($form->tipo_consulta==1){
                $titulo = "Detalle Productividades " . $persona . ", Consulta Resumen Año  " . $form->anio_id;                
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$titulo );
                           
                $row++;
                $row++;

                $col=0;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Período");  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Total");  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;
                $row++;
                
                foreach($detalle as $productividad){
                    $col=0;

                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad["periodo"]->nombrePeriodo);  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $col++;
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$productividad["monto"]);  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                    $col++;
                    $row++;
                    
                    
                }
            }
            
            if($form->tipo_consulta>1){
                $titulo = "Detalle Productividad " . $persona;
                
                if(isset($periodo_termino)){
                    $titulo = $titulo . " desde " . $periodo_inicio->nombrePeriodo . " a " . $periodo_termino->nombrePeriodo;
                }else{
                     $titulo = $titulo . " periodo " . $periodo_inicio->nombrePeriodo;
                }
                
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$titulo );
                
                $row++;
                $row++;

                $col=0;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Período");  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Centro Costo");  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Tipo Productividad");  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Cargado Por");  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Estado");  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Cuotas");  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Monto");  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Observaciones");  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
                $col++;
                
                $row++;
                
                foreach($detalle as $productividad){
                    $col=0;

                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->periodo->nombrePeriodo);  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $col++;

                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->cc->nombre_cc);  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $col++;
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->tipoProd->nombre_tipo_prod);  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $col++;
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->personaCarga->nombre_completo());  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $col++;

                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->estado->nombre_estado);  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $col++;
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->cuota_actual ." de ". $productividad->numero_cuotas);  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $col++;

                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->monto);  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                    $col++;
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->observaciones);  
                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                    $col++;
                    
                    $row++;
                }
                
                
            }
            
           
            
            
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="resultado_consulta_'.$persona.'.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }
        
        
        public function actionExportarGolbalesExcel(){
            $form = Yii::app()->getSession()->get('form_global');
            //$form = new ConsultasForm;     
            
            $valido=false;
            if(!is_null($form))
            {
                $valido=true;
                
                $tipo_consulta_id = $form->tipo_consulta;
                
            }
            
            
            if($valido){
                if($tipo_consulta_id==1){
                    $periodo_inicio_id = $form->anio_id*100 + 1;
                    $periodo_fin_id = $form->anio_id*100 + 12;
                }

                if($tipo_consulta_id==2){
                    $periodo_inicio_id = $form->anio_mes_id*100 + $form->mes_id;
                    $periodo_fin_id = 0;
                }

                if($tipo_consulta_id==3){
                    $periodo_inicio_id = $form->anio_desde_id*100 + $form->mes_desde_id;
                    $periodo_fin_id = $form->anio_hasta_id*100 + $form->mes_hasta_id;
                }


                $periodo = ProdPeriodo::getPeriodo($periodo_inicio_id);
                if($periodo_fin_id!=0){
                    $periodo_fin = ProdPeriodo::getPeriodo($periodo_fin_id);
                }else{
                    $periodo_fin=null;
                }
                
                $es_academico=false;
                if($form->tipo_personas==1){
                    $es_academico=true;
                }
                
                $this->widget('ext.productividades.LProductividadesWidget',
                                array(
                                    'es_academico'=>$es_academico,
                                    'periodo_id'=>$periodo_inicio_id,
                                    'periodo_fin_id'=>$periodo_fin_id,
                                    'es_consulta'=>true,
                                    'exportar_excel'=> true
                                    )
                                ); 
            
            
            }
            
        }
        
        
        public function actionExportarExcelDetallePersona(){
            $persona_id = $_REQUEST['persona_id'] ;
            $periodo_id = $_REQUEST['periodo_id'] ;
            
            if(isset($_REQUEST['periodo_fin_id'] )){
               $periodo_fin_id = $_REQUEST['periodo_fin_id'] ; 
            }else{
                $periodo_fin_id=0;
            }
            
            
            if($periodo_fin_id==0){
                $periodo_fin_id = null;
                $periodo_fin = null;            
            }else{
                $periodo_fin = ProdPeriodo::getPeriodo($periodo_fin_id);     
            }
 
            
            $persona = ProdPersona::getPersona($persona_id);
            $periodo = ProdPeriodo::getPeriodo($periodo_id);      
                        
            
            $detalle = ProdProductividad::getProductividadesIndividuales($persona_id, ProdArea::TODOS, ProdEstado::TODOS, $periodo_id,$periodo_fin_id);
            
           
            $objPHPExcel = new PHPExcel();
            
            $styleHeaderArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'E1E0F7'),
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )   ,
                'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )          
            );
            
            $styleHeaderCCArray = array(
                'borders' => array(
                                'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                                ) 
                            ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'E1E0F7'),
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )            ,
                'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            )
            );
            
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'font' => array(
                    'size' => 8
                )  
            );
            
            $styleTotalArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )  
            );

            $currencyFormat =  iconv("ISO-8859-1", "UTF-8", '$ #,##0');
            

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle('Resultados Consulta');

            $row = 2; 
            

            $titulo = "Detalle Productividad " . $persona;

            if(isset($periodo_fin)){
                $titulo = $titulo . " desde " . $periodo->nombrePeriodo . " a " . $periodo_fin->nombrePeriodo;
            }else{
                 $titulo = $titulo . " periodo " . $periodo->nombrePeriodo;
            }

            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$titulo );

            $row++;
            $row++;

            $col=0;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Período");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Tipo Productividad");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Estado");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Cuotas");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Monto");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Observaciones");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;

            $row++;

            foreach($detalle as $productividad){
                $col=0;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->periodo->nombrePeriodo);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $col++;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->tipoProd->nombre_tipo_prod);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $col++;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->estado->nombre_estado);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $col++;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->cuota_actual ." de ". $productividad->numero_cuotas);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $col++;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->monto);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $col++;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $productividad->observaciones);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $col++;

                $row++;
            }
                
            
            
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="resultado_consulta_'.$persona.'.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }
        
        
    
        
        
        public function getDatosGraficoDetalleAnualPersona($persona_id,$anio){
            
            
            $meses = ProdPeriodo::$MESES;
                        
            $meses_graficar = array();
            foreach($meses as $mes){
                $periodo = $anio . $mes["id"];
                $string_anio = "<a href='javascript:verDetalleCentroCostoMes(".$periodo.")'>".$mes["label"]."</a>";
                array_push($meses_graficar,$string_anio);
                
            }

            $periodo_desde_id = $anio."01";
            $periodo_hasta_id = $anio."12";
                    

            $productividades = ProdProductividad::getProductividadesDetalleAnioPersona($persona_id,$periodo_desde_id, $periodo_hasta_id);
                  
            $arreglo_meses = array();
            $datos_tabla= array();
            
            $data_mes = array();
            foreach($meses as $mes){

                $vl_periodo = intval($anio . $mes["id"]);
                $nm_periodo = ($mes["id"] . "/" .  $anio);

                $total_periodo=0;

                foreach($productividades as $productividad){

                    if($productividad["periodo_id"] == $vl_periodo){
                        $total_periodo = $total_periodo + $productividad["monto"];
                    }
                }

                array_push($data_mes, $total_periodo);  



            }

            array_push($arreglo_meses, array("tipo"=>"Productividad","valor"=>$data_mes));

            
            foreach($meses as $mes){
                $total=0;

                $nm_periodo = ($mes["id"] . "/" .  $anio);
                $vl_periodo = intval($anio . $mes["id"]);
                
                
                    
                foreach($productividades as $registro){
                    if($registro["periodo_id"] == $vl_periodo) {
                        $total = $registro["monto"]; 
                    }                        

                } 
                
                array_push($datos_tabla, array("nm_periodo"=>$nm_periodo,"monto_total"=>$total));
                
            }
            
            
            //obtiene los datos para las tablas+
            
            $datos_graficos = array();
            foreach($arreglo_meses as $datos_grafico){

                $dato_grafico = array("name"=>"". utf8_encode($datos_grafico["tipo"])."",
                                      "data"=>$datos_grafico["valor"]
                                      );

                array_push($datos_graficos, $dato_grafico);
            }

            

            $datos_grilla = $this->getDatosResumenPeriodo($datos_tabla);

            return array("categorias"=>$meses_graficar,"datos"=>$datos_graficos,"datos_grilla"=>$datos_grilla);
            
            
        }
        
        public function getDatosResumenPeriodo($datos_tabla){
        
            $columnas = array(
                 'nm_periodo','monto_total', 
            );

            $total = 0;
            $totalAcademico = 0;
            $totalNoAcademico = 0;
            
            
            foreach($datos_tabla as $resumen){
                $total = $total  + $resumen["monto_total"];
            }
            
            $detalle_columnas_resumen = array(       
                                array(
                                    'name'=>'nm_periodo',
                                    'header'=>'Período',
                                    'value'=>'$data["nm_periodo"] ',
                                    'footer'=>''
                                ),
                                array(
                                    'name'=>'monto_total',
                                    'header'=>'Monto Total',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_total"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($total) . '</b>'

                                ),


                            );
        
                $dataProviderResumen=new CArrayDataProvider($datos_tabla, array(
                    'keys'=>array('concepto'),
                    'sort'=>array(
                        'attributes'=>$columnas,
                    ),
                    'pagination'=>array(
                        'pageSize'=>10000,
                    ),
                ));
                
                return array("dataProvider"=>$dataProviderResumen,"columnas"=>$detalle_columnas_resumen);
                
        }
        
        public function getDatosCentroCostoMesPersona($persona_id,$vl_periodo){
            
            $centros_costo = ProdCentroCosto::getCentrosCosto();
            
            $periodo_desde_id = $vl_periodo;
            $periodo_hasta_id = $vl_periodo;
      
            $productividades = ProdProductividad::getProductividadesDetalleCentroCostoPersona($persona_id,$periodo_desde_id, $periodo_hasta_id);
            
            ////CARGA CATEGORIAS CENTRO COSTO
            $categorias=array();
            $datos_tabla= array();
             
            foreach($centros_costo as $centro_costo){
                
                $string_cc = "<a style='font-size:7px' href='javascript:verDetalleTiposProductividadesMes(".$vl_periodo . ",".$centro_costo->cc_id.")' alt='".$centro_costo->nombre_cc."' title='".$centro_costo->nombre_cc."'>".$centro_costo->cc_id. " - " . $centro_costo->nombre_cc."</a>";
                
                $total_periodo=0;

                foreach($productividades as $productividad){

                    if($productividad["cc_id"] == $centro_costo->cc_id){
                       $total_periodo = $total_periodo + $productividad["monto"];
                    }
                }
                
                if($total_periodo>0){

                    $dato_tabla = array("concepto"=>$string_cc,
                                        "centro_costo"=>$centro_costo,
                                        "monto"=>$total_periodo
                                        ); 

                    array_push($datos_tabla, $dato_tabla);
                }
                
                
                
                
                
                
            }

            //ORDENA CATEGORIA POR MONTOS
            
            $orden = array();
            $cantidad = array();
            
            foreach ($datos_tabla as $key => $row) {
                $orden[$key]  = $row['concepto'];
                $cantidad[$key] = $row['monto'];
            }

            array_multisort($cantidad , SORT_DESC,$orden , SORT_DESC, $datos_tabla);

            $array_centros_costo = array();

            foreach ($datos_tabla as $key => $row) {
                array_push($array_centros_costo, $row['centro_costo']);            
                array_push($categorias, ($row['concepto']));    
                
            }
            //FIN CARGA CATEGORIAS CENTRO COSTO
            
            
            
            $arreglo_centros_costos = array();

            
            $array_datos = array();
            foreach($array_centros_costo as $centro_costo){

                $total_periodo=0;

                foreach($productividades as $productividad){

                    if($productividad["cc_id"] == $centro_costo->cc_id){
                       $total_periodo = $total_periodo + $productividad["monto"];
                    }
                }

                array_push($array_datos, $total_periodo);


            }

            array_push($arreglo_centros_costos, array("tipo"=>"Productividad","color"=>"","valor"=>$array_datos));
            
            $datos_graficos = array();
            foreach($arreglo_centros_costos as $datos_grafico){

                $dato_grafico = array("name"=>"". utf8_encode($datos_grafico["tipo"])."",
                                      "data"=>$datos_grafico["valor"],
                                      "color"=>$datos_grafico["color"]
                                      );

                array_push($datos_graficos, $dato_grafico);
            }
            
            //PREPARA DATOS TABLA
            $datos_tabla= array();
            foreach($array_centros_costo as $centro_costo){
                $total=0;

                $nm_cc = $centro_costo->cc_id . "-" . $centro_costo->nombre_cc;
                
                    
                foreach($productividades as $registro){
                    if($registro["cc_id"] == $centro_costo->cc_id) {
                        $total = $total+$registro["monto"];
                    } 
                } 
                
                array_push($datos_tabla, array("nombre_completo_cc"=>$nm_cc,"monto_total"=>$total));
                
            }
            $datos_grilla = $this->getDatosResumenCentroCosto($datos_tabla);
            //FIN DATOS TABLA


            return array("categorias"=>$categorias,"datos"=>$datos_graficos,"datos_grilla"=>$datos_grilla);
            
            
        }
        
        
        public function getDatosResumenCentroCosto($datos_tabla){
        
            $columnas = array(
                 'nombre_completo_cc','monto_total', 
            );

            $total = 0;
            $totalAcademico = 0;
            $totalNoAcademico = 0;
            
            
            foreach($datos_tabla as $resumen){
                $total = $total  + $resumen["monto_total"];
            }
            
            $detalle_columnas_resumen = array(       
                                array(
                                    'name'=>'nombre_completo_cc',
                                    'header'=>'Centro Costo',
                                    'value'=>'$data["nombre_completo_cc"] ',
                                    'footer'=>''
                                ),
                                
                                array(
                                    'name'=>'monto_total',
                                    'header'=>'Monto Total',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_total"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($total) . '</b>'

                                ),


                            );
        
                $dataProviderResumen=new CArrayDataProvider($datos_tabla, array(
                    'keys'=>array('concepto'),
                    'sort'=>array(
                        'attributes'=>$columnas,
                    ),
                    'pagination'=>array(
                        'pageSize'=>10000,
                    ),
                ));
                
                return array("dataProvider"=>$dataProviderResumen,"columnas"=>$detalle_columnas_resumen);
                
        }
        
        public function getResumenEstadoPersona($persona_id,$periodo_id){
            
            
            $resumenProceso = ProdProductividad::getEstadoValidacionProductividadesPersona($persona_id,$periodo_id);
            
            $estados = ProdEstado::getEstados();
            $datos_tabla = array();
            
            foreach($estados as $estado){
                $total=0;
                
                $color="normal";
                if($estado->estado_id != ProdEstado::APROBADO){
                    $color="red";
                }
                    
                foreach($resumenProceso as $registro){
                    if($registro["estado_id"] == $estado->estado_id ) {
                        $total = $total+$registro["monto"]; 
                    }                        

                } 
                
                if($estado->estado_id<=ProdEstado::APROBADO){
                    array_push($datos_tabla, array("nombre_estado"=>$estado->nombre_estado,"color"=>$color,"monto_total"=>$total));                
                }
                
            }
        
            $columnas = array(
                 'nombre_estado','monto_total', 
            );

            $total = 0;
            $total_academico =0;
            $total_no_academico =0;
            
            foreach($datos_tabla as $dato){
                $total = $total  + $dato["monto_total"];
            }
            
            $detalle_columnas_resumen = array(       
                                array(
                                    'name'=>'nombre_estado',
                                    'header'=>'Estado',
                                    'value'=>'$data["nombre_estado"]',
                                    'footer'=>'Total'
                                ),
                                array(
                                    'name'=>'monto_total',
                                    'header'=>'Total',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_total"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($total) . '</b>'

                                ),


                            );
        
                $dataProviderResumen=new CArrayDataProvider($datos_tabla, array(
                    'keys'=>array('concepto'),
                    'sort'=>array(
                        'attributes'=>$columnas,
                    ),
                    'pagination'=>array(
                        'pageSize'=>10000,
                    ),
                ));
                
                return array("dataProvider"=>$dataProviderResumen,"columnas"=>$detalle_columnas_resumen);
        }
        
}