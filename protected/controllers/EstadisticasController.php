<?php
/**
 * Controlador del módulo de estadisticas y proyecciones
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller
 */
class EstadisticasController extends SecureController
{
    
        /**
          * @var array Listado de opciones para cálculo de proyecciones mensuales
          * Define las opciones de cálculo de proyecciones mensuales
          */    
        private $base_calculo_mensual = array(
                                            array("id"=>1,"value"=>"Toda la historia"),
                                            array("id"=>2,"value"=>"Últimos dos años"),
                                            array("id"=>3,"value"=>"Último año"),
                                            array("id"=>4,"value"=>"Últimos 6 meses"),
                                            array("id"=>5,"value"=>"Últimos 2 meses"),
                                        );
                
        /**
          * @var array Listado de opciones para cálculo de proyecciones anuales
          * Define las opciones de cálculo de proyecciones anuales
          */           
        private $base_calculo_anual = array(
                                        array("id"=>1,"value"=>"Toda la historia"),
                                        array("id"=>3,"value"=>"Últimos 2 años"),

                                    );
            
    
        /**
        * Control de acceso del módulo
        * En este caso tienen acceso a las funcionalidades los usuarios Validadores y Directores
        * El administrador por defecto puede ver todos los módulos
        * 
        * @return array permisos de usuario al módulo
        */
	public function accessRules()
        {
            return array(
                //SOLO EL USUARIO CON PERMISO VALIDACION PUEDE ACCEDER AL CONTROLLER
                array('allow',
                      'roles' => array('validador','director'),
                ),
                array('deny',
                    'users'=>array('*'),
                ),
            );
        }
        
        /**
        * Despliega el  menu principal del modulo
        * 
        * @return string despliega la vista estadisticas/index
        */           
	public function actionIndex()
	{
            $this->actionEstadisticas();
        }
        
        /**
        * Despliega el módulo de estadisticas
        * 
        * @return string despliega la vista estadisticas/estadisticas
        */           
	public function actionEstadisticas()
	{
            
            $form = new ConsultasForm;   
            $anio_actual_id = date("Y");  
             
            $anios = ProdPeriodo::getAnios();
            asort($anios);
            
            $meses = ProdPeriodo::getMeses();
            $datos_tabla = array();
                       
            //por defecto el tipo de consulta se deja como el año
            $valido = true;
            $exportar_excel=false;
            
            if(isset($_POST['ConsultasForm']))
            {
                //para mantener el periodo seleccionado cuando se ejecuta el formulario
                
                $form->attributes=$_POST['ConsultasForm'];                  
                if(!$form->validate()){
                    $valido=false;
                }
                
                $anio_id = $form->anio_id;
                $anio_desde_id = $form->anio_desde_id;
                $periodo_inicio_id= intval( $anio_desde_id . "01");
                $cc_id = $form->cc_id;
                
                if($form->exportar_excel==1){
                    $exportar_excel=true;
                }
                    
            }else{
                $cc_id = 0;        
                $anio_id= $anio_actual_id;
                $form->anio_id = $anio_actual_id;
                foreach($anios as $anio){
                    $periodo_inicio_id= intval( $anio["id"] . "01");
                    break;
                }
                
                $form->anio_desde_id = $anio["id"];
            }
                                
            
            $anios_reporte = array();
            foreach($anios as $anio){
                if($anio["id"]>=$form->anio_desde_id){
                    array_push($anios_reporte, $anio);
                }
            }
            
            $datos_grafico_resumen = $this->getDatosGraficoResumenAnual($periodo_inicio_id,$cc_id,$anios_reporte);            
            $datos_grafico_detalle_anio = $this->getDatosGraficoDetalleAnual($anio_id,$cc_id); 
            
            $centro_costo=null;
            if($cc_id==0){
                $tituloGrafico="Resumen Anual";
                $tituloGraficoDetalle='Detalle Año ' . $anio_id;
                $datos_grafico_detalle = $this->getDatosCentroCosto($anio_id);
            }else{
                $centro_costo = ProdCentroCosto::getCentroCosto($cc_id);
                $datos_grafico_detalle = $this->getDatosCentroCostoTipoProductividad($anio_id,$cc_id);
                $tituloGrafico="Resumen Anual " . $centro_costo;  
                $tituloGraficoDetalle='Detalle Año ' . $centro_costo . ' ' . $anio_id;
            }
                        
            $vista_grafico_detalle= $this->mostrarGraficoDetalle($anio_id,$cc_id);
            
            
            $centros_costos = ProdCentroCosto::getCentrosCosto();
            
            $tipos_persona = array();
            
            array_push($tipos_persona, array("id"=>"academico","nombre"=>"Academicos","color"=>"#ffad67"));
            array_push($tipos_persona, array("id"=>"no_academico","nombre"=>"No Academicos","color"=>"#FFF263"));
            
            $centros_costos_filtrado=array();
            
            if($cc_id!=0){
                foreach($centros_costos as $centro_costo){
                    if($centro_costo->cc_id == $cc_id){
                        array_push($centros_costos_filtrado, $centro_costo);
                    }
                }
            }else{
                $centros_costos_filtrado = $centros_costos;
            }
            
            
            $resumenComparacion = $this->generaTablaComparacion($periodo_inicio_id, $centros_costos_filtrado, $anios_reporte,$tipos_persona);
    
            
            if($exportar_excel){
                $this->getExcelEstadisticas($anio_id,
                                            $cc_id,
                                            $datos_grafico_resumen,
                                            $datos_grafico_detalle_anio,
                                            $datos_grafico_detalle);
                $form->exportar_excel = 0;
            }else{
                $this->render('estadisticas',array( 'model'=>$form,
                                                    "anios"=>$anios,
                                                    "anios_reporte"=>$anios_reporte,
                                                    "meses"=>$meses,            
                                                    "anio_id"=>$anio_id,
                                                    "valido"=>$valido,
                                                    "cc_id"=>$cc_id,
                                                    "centros_costos"=>$centros_costos_filtrado,
                                                    "centros_costos_combo"=>$centros_costos,
                                                    "centro_costo"=>$centro_costo,
                                                    "datos_grafico_resumen" => $datos_grafico_resumen,
                                                    "datos_grafico_detalle_anio" => $datos_grafico_detalle_anio,
                                                    "datos_grafico_detalle"=>$datos_grafico_detalle,
                                                    "vista_grafico_detalle" => $vista_grafico_detalle,                    
                                                    "datos_tabla"=>$datos_tabla,
                                                    "tituloGrafico"=>$tituloGrafico,
                                                    "tituloGraficoDetalle"=>$tituloGraficoDetalle,
                                                    "titulo_header"=>"",
                                                    "resumenComparacion"=>$resumenComparacion,
                                                    "tipos_persona"=>$tipos_persona
                    ));
            }
	}
        
        /**
        * Despliega el módulo de proyecciones
        * 
        * @return string despliega la vista estadisticas/proyecciones
        */      
        public function actionProyecciones()
	{
                $form = new ProyeccionesForm; 
                $categorias = array();
                $datos = array();
                $datos_tabla = array();
                
                $disabled_c1=false;
                $disabled_c2=false;
                $valido=false;
                
                $exportar_excel=false;
                
                if(isset($_POST['ProyeccionesForm']))
                {   
                    //echo "<pre>" . print_r($_POST['ProyeccionesForm'])."</pre>";
                    $form->attributes=$_POST['ProyeccionesForm'];   
                    
                    if($form->validate()){
                        $valido=true;
                    }
                    
                    $tipo_proyeccion = $form->tipo_proyeccion;
                    
                    if($tipo_proyeccion!=1){
                        $disabled_c1=true;
                    }

                    if($tipo_proyeccion!=2){
                        $disabled_c2=true;
                    }
                    
                    if($form->exportar_excel==1){
                        $exportar_excel=true;
                    }
                }else{
                    $disabled_c2=true;
                }
                
                
                if($valido){
                    //calcula proyecciones
                    $salida_grafico = $this->calculaProyecciones($form);
                    $categorias = $salida_grafico["categorias"];
                    $datos = $salida_grafico["datos"];
                    $datos_tabla = $salida_grafico["datos_tabla"];
                    
                }
                
                $tipo="column";
                
                $legend = array(
                        'layout' => 'vertical',
                        'align' => 'right',
                        'verticalAlign' => 'middle',
                         'borderWidth' => '1',
                        'backgroundColor' => '#FFFFFF',
                        'shadow' => 'true',
                    );
                
                
                if($exportar_excel){
                    $this->getExcelProyecciones($datos_tabla,$form);
                    $form->exportar_excel = 0;
                }else{
                
                    $this->render('proyecciones',array( 'model'=>$form,
                                                        'disabled_c1'=>$disabled_c1,
                                                        'disabled_c2'=>$disabled_c2,
                                                        'base_calculo_mensual'=>$this->base_calculo_mensual,
                                                        'base_calculo_anual'=>$this->base_calculo_anual,
                                                        'valido'=>$valido,
                                                        'categorias'=>$categorias,
                                                        'datos'=>$datos,
                                                        'datos_tabla'=>$datos_tabla,
                                                        'tipo'=>$tipo,
                                                        'legend'=>$legend,

                                ));
                }
	}
        
        
        
        /**
        *Grafica las estadisticas de un mes en particular
        * @param ConsultasForm $form formulario
        * @param int $periodo Periodo a mostrar
        * @param array $centros_costo Centros Costos a mostrar en el gráfico
        * @param array $tipos_productividades Tipos de productividades a mostrar en el gráfico
        * @return array Datos de productividades formateados para ser graficados
        */  
        public function graficarDatosMensuales(ConsultasForm $form, $periodo,$centros_costo,$tipos_productividades){
            $categorias = array();
            $centros_costo_datos = array();

            $productividades = ProdProductividad::getProductividadesGlobales(ProdEstado::APROBADO, $periodo->periodo_id);
            
            // SI ES SOLO UN MES    
            $datos = array();
            $datos_tabla= array();
            $datos_salida = array();
            
            
            //grafica por ambos conceptos CC/TIPO_PROD
            if($form->tipo_salida==1){
            
                foreach($centros_costo as $centro_costo){       
                    $total_cc=0;
                    foreach($productividades as $productividad){
                        if($centro_costo->cc_id == $productividad->cc_id){
                            $total_cc = $total_cc + $productividad->monto;                              
                        }
                    }

                    if($total_cc>0){
                        array_push($centros_costo_datos, $centro_costo);
                        array_push($categorias, $centro_costo->nombre_completo_cc);
                    }
                }


                foreach($tipos_productividades as $tipo_productividad){
                    $datos_cc = array();

                    foreach($centros_costo_datos as $centro_costo){   

                        $total_prod_cc=0;
                        foreach($productividades as $productividad){
                            if($productividad->periodo  == $periodo && 
                                $tipo_productividad->tipo_prod_id==$productividad->tipo_prod_id && 
                                $centro_costo->cc_id == $productividad->cc_id){
                                $total_prod_cc = $total_prod_cc + $productividad->monto;  
                            }
                        }
                        
                        //if($total_prod_cc>0){
                        array_push($datos_cc, $total_prod_cc);
                        //}
                    }

                    array_push($datos_salida, array("name"=>$tipo_productividad->descripcion,
                                                    "data"=>$datos_cc));


                }
                
                //llenar tabla salida
                foreach($centros_costo_datos as $centro_costo){   

                    foreach($tipos_productividades as $tipo_productividad){   

                        $total_prod_cc=0;
                        foreach($productividades as $productividad){
                            if($productividad->periodo  == $periodo && 
                                $tipo_productividad->tipo_prod_id==$productividad->tipo_prod_id && 
                                $centro_costo->cc_id == $productividad->cc_id){
                                $total_prod_cc = $total_prod_cc + $productividad->monto;  
                            }
                        }
                        
                        if($total_prod_cc>0){
  

                            $dato_tabla = array("periodo"=>$periodo->nombrePeriodo,
                                                "concepto"=>$centro_costo->nombre_completo_cc,
                                                "concepto1"=>$tipo_productividad->descripcion,
                                                "monto"=>$total_prod_cc
                                                ); 

                            array_push($datos_tabla, $dato_tabla);
                        }
                    }



                }
            }
            
            
            //grafica agrupado por centro costo
            if($form->tipo_salida==2){
                foreach($tipos_productividades as $tipo_productividad){     
                    $total_tp=0;
                    foreach($productividades as $productividad){
                        if($tipo_productividad->tipo_prod_id==$productividad->tipo_prod_id){
                            $total_tp = $total_tp + $productividad->monto;                              
                        }
                    }

                    if($total_tp>0){
//                        array_push($centros_costo_datos, $centro_costo);
                        array_push($datos, $total_tp);
                        array_push($categorias, $tipo_productividad->descripcion);
                            
                        
                    }
                    
                    $dato_tabla = array("periodo"=>$periodo->nombrePeriodo,
                                        "concepto"=>$tipo_productividad->nombre_tipo_prod,
                                        "monto"=>$total_tp
                                        ); 

                    array_push($datos_tabla, $dato_tabla);
                    
                }
                
                array_push($datos_salida, array("name"=>"Tipos Productividades","data"=>$datos));
            }
            
            //grafica agrupado por centro costo
            if($form->tipo_salida==3){
                foreach($centros_costo as $centro_costo){       
                    $total_cc=0;
                    foreach($productividades as $productividad){
                        if($centro_costo->cc_id == $productividad->cc_id){
                            $total_cc = $total_cc + $productividad->monto;                              
                        }
                    }

                    if($total_cc>0){
//                        array_push($centros_costo_datos, $centro_costo);
                        array_push($datos, $total_cc);
                        array_push($categorias, $centro_costo->nombre_completo_cc);
                            
                        
                    }
                    
                    $dato_tabla = array("periodo"=>$periodo->nombrePeriodo,
                                        "concepto"=>$centro_costo->nombre_completo_cc,
                                        "monto"=>$total_cc
                                        ); 

                    array_push($datos_tabla, $dato_tabla);
                    
                }
                
                array_push($datos_salida, array("name"=>"Centros Costo","data"=>$datos));
            }
            

            
            //echo "<pre>";
            //print_r($datos_salida);
            //echo "</pre>";
            return array("categorias"=>$categorias,"datos"=>$datos_salida,"datos_tabla"=>$datos_tabla);
        }
        
        /**
        *Grafica las estadisticas de un periodo (mes inicio a mes termino)
        * @param ConsultasForm $form formulario
        * @param int $periodo Periodo inicio de la ventana de analisis
        * @param int $periodo_fin Periodo fin de la ventana de analisis
        * @param array $tipos_productividades Tipos de productividades a mostrar en el gráfico
        * @param array $centros_costo Se usa cuando se desglosa por centro de costo
        * @param int $tipo_salida_id Define se se va a mostra los datos arupados o desagregados por tipo productividad
        * @param int $tipo_prod_id ='0' Si es distinto de cero dice que se va a graficar un tipo de productividad en particular
        * @param int $cc_id ='0' Si es distinto de cero dice que se va a graficar un centro de costo en particular
        * @return array Datos de productividades formateados para ser graficados
        */          
        public function graficarDatosPeriodo(ConsultasForm $form,$periodo,$periodo_fin,$tipos_productividades,$centros_costo,$tipo_prod_id=0,$cc_id=0){
            $categorias = array();
            $periodos = array();
            $datos_tabla= array();
            
            $periodo_tmp_inicio_id = $periodo->periodo_id;            

            while ($periodo_tmp_inicio_id <= $periodo_fin->periodo_id){

                $periodo_tmp = ProdPeriodo::getPeriodo($periodo_tmp_inicio_id);
                $anio = $periodo_tmp->anio;
                $mes = $periodo_tmp->mes;

                if($mes==12){
                    $mes=1;
                    $anio=$anio+1;
                }else{
                    $mes++;
                }

                $periodo_tmp_inicio_id=$anio*100+$mes;

                array_push($categorias, $periodo_tmp->nombre_periodo);
                array_push($periodos, $periodo_tmp);

            }
            
            $productividades = ProdProductividad::getProductividadesGlobales(ProdEstado::APROBADO, $periodo->periodo_id, $periodo_fin->periodo_id, true);
            
            $datos = array();
            
            if($form->tipo_salida==2){
                //CUANDO SE CONSULTA EL DETALLE DE PRODUCTIVIDADES 
                foreach($tipos_productividades as $tipo_productividad){
                    $visible = true;
                    if($tipo_prod_id!=0 && $tipo_productividad->tipo_prod_id !=$tipo_prod_id){
                        $visible = false;
                    }else {
                        $visible = true;
                    }
                    
                    $data = array();

                    $total_periodo = 0;
                    foreach($periodos as $periodo){

                        $total = 0;
                        foreach($productividades as $productividad){
                            if($productividad->tipoProd ==  $tipo_productividad && 
                               $productividad->periodo  == $periodo){
                                $total = $total + $productividad->monto;
                            }
                        }

                        array_push($data, $total);
                        $total_periodo = $total_periodo + $total;
                        
                        if($visible && $total>0){
                            $dato_tabla = array("periodo"=>$periodo->nombrePeriodo,
                                                "concepto"=>$tipo_productividad->nombre_tipo_prod,
                                                "monto"=>$total
                                                ); 
                            
                            array_push($datos_tabla, $dato_tabla);
                        }
                        
                        
                    }

                    
                    $subtitulo = "";
                    if($total_periodo==0){
                        $visible=false;
                        $subtitulo = "(0)";
                    }

                    

                    $dato_prod = array('name'=>$tipo_productividad->descripcion . $subtitulo, 'data'=>$data , 'visible'=>$visible);

                    array_push($datos,$dato_prod);

                }
                
                sort($datos_tabla);
            
            }else{
                if($form->tipo_salida==3){
                    //desglosado por centro de costo
                    foreach($centros_costo as $centro_costo){
                        $visible = true;
                        if($cc_id!=0 && $centro_costo->cc_id !=$cc_id){
                            $visible = false;
                        }else {
                            $visible = true;
                        }

                        $data = array();

                        $total_periodo = 0;
                        foreach($periodos as $periodo){

                            $total = 0;
                            foreach($productividades as $productividad){
                                if($productividad->cc ==  $centro_costo && 
                                   $productividad->periodo  == $periodo){
                                    $total = $total + $productividad->monto;
                                }
                            }

                            array_push($data, $total);
                            $total_periodo = $total_periodo + $total;

                            if($visible && $total>0){
                                $dato_tabla = array("periodo"=>$periodo->nombrePeriodo,
                                                    "concepto"=>$centro_costo->nombre_completo_cc,
                                                    "monto"=>$total
                                                    ); 

                                array_push($datos_tabla, $dato_tabla);
                            }


                        }


                        $subtitulo = "";
                        if($total_periodo==0){
                            $visible=false;
                            $subtitulo = "(0)";
                        }



                        $dato_prod = array('name'=>$centro_costo->nombre_completo_cc . $subtitulo, 'data'=>$data , 'visible'=>$visible);

                        array_push($datos,$dato_prod);

                    }
                    
                    sort($datos_tabla);
                    
                }else{
                
                    //CASO DE CONSULTAS DE PRODUCTIVIDADES AGRUPADAS
                    $data = array();


                    foreach($periodos as $periodo){
                        $total_periodo=0;
                        foreach($productividades as $productividad){
                            if($productividad->periodo  == $periodo){
                                $total_periodo = $total_periodo + $productividad->monto;
                            }
                        }

                        $form_detalle = new ConsultasForm();
                        $form_detalle->tipo_consulta = 1;
                        $form_detalle->tipo_salida = 3;
                        
                        
                        $datos_detalle = $this->graficarDatosMensuales($form_detalle,$periodo, $centros_costo,$tipos_productividades);
                        
                        $data_detalle_grafico = array();
                        if(isset($datos_detalle["datos"][0]["data"])){
                            $data_detalle_grafico=$datos_detalle["datos"][0]["data"];
                        }

                        $datos_periodo = array("y"=>$total_periodo,
                                               "color"=>"#4572A7",                        
                                               "drilldown"=> array("name"=>$periodo->nombrePeriodo,
                                                                   "categories"=>$datos_detalle["categorias"],
                                                                   "data"=>$data_detalle_grafico,
                                                                   "color"=>"#4572A7",
                                                                   "dataLabels"=>array("enabled"=>true,
                                                                                       "rotation"=>-45,
                                                                                       "color"=>"#FFFFFF",
                                                                                       "align"=>"right",
                                                                                       "x"=>4,
                                                                                       "y"=>10
                                                                                        )
                                                                   )
                                              );

                        $dato_tabla = array("concepto"=>$periodo->nombrePeriodo,
                                            "monto"=>$total_periodo
                                            );     

                        array_push($data,$datos_periodo);
                        array_push($datos_tabla, $dato_tabla);


                    }           

    //                ,"dataLabels"=>array("enabled"=>true,
    //                                    "rotation"=>-90,
    //                                    "color"=>"#FFFFFF",
    //                                    "align"=>"right",
    //                                    "x"=>4,
    //                                    "y"=>10
    //                                     )

                    $dato_prod = array('name'=>'Productividad Agrupada', 'data'=>$data,"color"=>"#4572A7");

                    array_push($datos,$dato_prod);
                }
                
            }
            
//            echo "<pre>";
//            print_r($datos);
//            echo "</pre>";
            
            return array("categorias"=>$categorias,"datos"=>$datos,"datos_tabla"=>$datos_tabla);
        }
        
        /**
        * Calculo de proyecciones
        * 
        * Función que realiza el cálculo de las proyecciones en base a lo seleccionado por el usuario en el formulario
        * 
        * @param ProyeccionesForm $form formulario con los datos seleccionados {@link ProyeccionesForm}
        * @return array 
        */
        private function calculaProyecciones(ProyeccionesForm $form){
            $tipo_proyeccion = $form->tipo_proyeccion;
            $salida = array();
            
            if($tipo_proyeccion==1){
                //CALCULO PROYECCION MENSUAL    
                $base_calculo_mensual = $form->base_calculo_mensual;
                
                $productividades = array();
                
                if($base_calculo_mensual==1){
                    //toda la historia
                    $productividades = ProdProductividad::getProductividadesGlobales(ProdEstado::APROBADO);
                }else{
                    
                    $numero_meses_resta=0;
                    
                    if($base_calculo_mensual==2){
                        //ultimos dos años
                        $numero_meses_resta = 24;
                    }
                    
                    if($base_calculo_mensual==3){
                        //ultimos dos años
                        $numero_meses_resta = 12;
                    }
                    
                    if($base_calculo_mensual==4){
                        //ultimos dos años
                        $numero_meses_resta = 6;
                    }
                    
                    if($base_calculo_mensual==5){
                        //ultimos dos años
                        $numero_meses_resta = 2;
                    }
                    
                    
                    
                    $periodo_fin_id = date("Ym");
                    $periodo_tmp_id = $periodo_fin_id;

                    for($i=0;$i<$numero_meses_resta-1;$i++){
                        $periodo_tmp = ProdPeriodo::getPeriodo($periodo_tmp_id);  
                        $anio = $periodo_tmp->anio;
                        $mes = $periodo_tmp->mes;

                        if($mes==1){
                            $mes=12;
                            $anio=$anio-1;
                        }else{
                            $mes--;
                        }

                        $periodo_tmp_id=$anio*100+$mes;                            

                    }
                    $periodo_inicio_id = $periodo_tmp_id;

//                    echo $periodo_inicio_id . "-";
//                    echo $periodo_fin_id;
                    
                    
                
                    
                    $productividades = ProdProductividad::getProductividadesGlobales(ProdEstado::APROBADO, 
                                                                                     $periodo_inicio_id, 
                                                                                     $periodo_fin_id);
                }
                
                $periodos = array();
                foreach($productividades as $productividad){

                    if(!in_array($productividad->periodo_id, $periodos)){
                        array_push($periodos, $productividad->periodo_id);
                    }

                }
                
                $categorias = array();
                $datos = array();
                $datos_tabla = array();
                
                //OBSOLETO 
                //AHORA SE OBTENE EL CENTRO DE COSTO DESDE EL AREA
                /*
                $tipos_productividades =ProdRolTipoProdCC::getTiposProductividadesRol(Yii::app()->user->rolId,false);
                
                
                foreach($tipos_productividades as $tipo_productividad){
                                       
                    $total_productividades = 0;
                    $promedio_productividades = 0;
                    foreach($productividades as $productividad){
                        if($productividad->tipo_prod_id == $tipo_productividad->tipo_prod_id){                                                        
                            $total_productividades = $total_productividades + $productividad->monto;
                        }                        
                    }   
                    
                    $promedio_productividades = round($total_productividades / count($periodos));
                    
                    array_push($categorias, $tipo_productividad->descripcion);
                    array_push($datos, $promedio_productividades);
                    
                    $dato_tabla = array("tipo_productividad"=>$tipo_productividad->nombre_tipo_prod,
                                        "monto"=>$promedio_productividades
                                        );
                    
                    array_push($datos_tabla, $dato_tabla);
                    
                    //echo $tipo_productividad . ":" . $promedio_productividades . "<br>";
                }
                 * 
                 * 
                 */

//                echo "<pre>";
//                print_r($periodos);
//                echo "</pre>";   
                $datos_proyecciones = array(array("name"=>"Proyecciones",
                                                  "data"=>$datos));
                
                $salida = array("categorias"=>$categorias,"datos"=>$datos_proyecciones,"datos_tabla"=>$datos_tabla);               
                
                
            }else{
                //CALCULO PROYECCION ANUAL
                $base_calculo_anual = $form->base_calculo_anual;
            }
            
            
            
            
            return $salida;
            
        }

        /**
        * Exporta a excel el listado de datos de proyecciones calculadas
        * 
        * Función que realiza la exportación a excel de las proyecciones
        * @param array $datos Datos a exportar
        * @param ProyeccionesForm $form formulario con los datos seleccionados {@link ProyeccionesForm}
        * @return PHPExcel_IOFactory se retorna un archivo excel 
        */	
        public function getExcelProyecciones($datos, ProyeccionesForm $form){
            
            
            $objPHPExcel = new PHPExcel();
            
            $styleHeaderArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'E1E0F7'),
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )   ,
                'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )          
            );
            
            $styleHeaderCCArray = array(
                'borders' => array(
                                'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                                ) 
                            ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'E1E0F7'),
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )            ,
                'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            )
            );
            
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'font' => array(
                    'size' => 8
                )  
            );
            
            $styleTotalArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )  
            );

            $currencyFormat =  iconv("ISO-8859-1", "UTF-8", '$ #,##0');
            

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle('Proyecciones');
            
            $titulo = "";
            $subtitulo = "";
            if($form->tipo_proyeccion==1){
                $titulo = "Mensuales";   
                
                foreach($this->base_calculo_mensual as $base){
                    if($base["id"]==$form->base_calculo_mensual){
                        $subtitulo = $base["value"];
                    }
                }                
                
            }else{
                $titulo = "Anuales";    
            }
            
            

            $row = 1; 
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "Detalle Proyecciones " . $titulo );
                       
            $row++;       
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "Base Cálculo:  " . $subtitulo );
            
            $row++;
            
            $col=0;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Tipo Productividad");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Monto");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            $row++;
            $total=0;
            foreach ($datos as $dato){
                $total=$total+$dato["monto"];
                
                $col=0;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["tipo_productividad"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);                
                $col++;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["monto"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);

                
                $row++;

            }
            
            $col = 1;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $total);  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);            
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
 
            
        
            

            //ob_end_clean();
            //ob_start();

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="resultado_proyecciones.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }
        
        /**
        * Exporta a excel el listado de datos de estadisticas calculadas
        * 
        * Función que realiza la exportación a excel de las estadisticas
        * @param array $datos Datos a exportar
        * @param ConsultasForm $form formulario con los datos seleccionados {@link ConsultasForm}
        * @return PHPExcel_IOFactory se retorna un archivo excel 
        */        
        public function getExcelEstadisticas($anio_id,
                                             $cc_id,
                                             $datos_grafico_resumen,
                                             $datos_grafico_detalle_anio,
                                             $datos_grafico_centro_costo){
            
            
            $objPHPExcel = new PHPExcel();
            
            $styleHeaderArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'E1E0F7'),
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )   ,
                'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )          
            );
            
            $styleHeaderCCArray = array(
                'borders' => array(
                                'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                                ) 
                            ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'E1E0F7'),
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )            ,
                'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            )
            );
            
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'font' => array(
                    'size' => 8
                )  
            );
            
            $styleTotalArray = array(
                'borders' => array(
                    'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'font' => array(
                    'bold' => true,
                    'size' => 8
                )  
            );

            $currencyFormat =  iconv("ISO-8859-1", "UTF-8", '$ #,##0');
            

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle('Estadísticas');
            
            if($cc_id==0){
                $titulo = "Año " . $anio_id ;
            }else{
                $centroCosto = ProdCentroCosto::getCentroCosto($cc_id);
                $titulo = $centroCosto . " Año "  . $anio_id ;
            }
            
            $subtitulo = "Totales Anuales";
          

            $row = 1; 
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "Consulta Productividades " . $titulo );
                       
            $row++;       
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subtitulo );
            
            $row++;
            
            //IMPRIME TOTALES ANUALES
            $col=0;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Año");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,"Monto Academicos");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,"Monto No Academicos");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Monto");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            $row++;
            $total=0;
            $totalAcademico=0;
            $totalNoAcademico=0;
            
            $datos_grilla = $datos_grafico_resumen["datos_grilla"]; 
            $datos=$datos_grilla["dataProvider"]->rawData ;
                    
            foreach ($datos as $dato){
                $total=$total+$dato["monto_total"];
                $totalAcademico=$totalAcademico+$dato["monto_academico"];
                $totalNoAcademico=$totalNoAcademico+$dato["monto_no_academico"];
                
                $col=0;
                
                //en estos casos no se imprime el periodo (consultas agrupadas por mes),el periodo va como concepto
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["nm_periodo"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);                
                $col++;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["monto_academico"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);                
                $col++; 
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["monto_no_academico"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);                
                $col++;                      
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["monto_total"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);

                
                $row++;

            }
            $col=0;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Total");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);            
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $col++;
 
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $totalAcademico);  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);            
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $col++;
 
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $totalNoAcademico);  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);            
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $col++;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $total);  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);            
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
 
            //FIN TOTALES ANUALES
            
            $row++;
            $row++;
            
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "Detalle Por Mes Año " . $anio_id );
                       
            $row++;                
            //IMPRIME DETALLE POR MES
            $col=0;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Período");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,"Monto Academicos");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,"Monto No Academicos");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Monto");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $row++;
            
            
            $total=0;
            $totalAcademico=0;
            $totalNoAcademico=0;
            
            $datos_grilla = $datos_grafico_detalle_anio["datos_grilla"]; 
            $datos=$datos_grilla["dataProvider"]->rawData ;
            
                    
            foreach ($datos as $dato){
                $total=$total+$dato["monto_total"];
                $totalAcademico=$totalAcademico+$dato["monto_academico"];
                $totalNoAcademico=$totalNoAcademico+$dato["monto_no_academico"];
                
                $col=0;
                
                //en estos casos no se imprime el periodo (consultas agrupadas por mes),el periodo va como concepto
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["nm_periodo"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);                
                $col++;
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["monto_academico"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);                
                $col++; 
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["monto_no_academico"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);                
                $col++;                      
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["monto_total"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);

                
                $row++;

            }
            
           
            $col=0;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Total");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);            
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $col++;
 
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $totalAcademico);  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);            
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $col++;
 
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $totalNoAcademico);  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);            
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $col++;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $total);  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);            
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            
            //FIN DETALLE POR MES
            
            $row++;
            $row++;
            //IMPRIME DETALLE CENTROS COSTO
            $col=0;
            
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "Detalle Por Centro Costo Año " . $anio_id );
                       
            $row++;   
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Centro Costo");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,"Monto Academicos");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,"Monto No Academicos");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;

            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Monto");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
            $col++;
            
            $row++;
            
            
            
            $total=0;
            $totalAcademico=0;
            $totalNoAcademico=0;
            
            $datos_grilla = $datos_grafico_centro_costo["datos_grilla"]; 
            $datos=$datos_grilla["dataProvider"]->rawData ;
                    
            foreach ($datos as $dato){
                $total=$total+$dato["monto_total"];
                $totalAcademico=$totalAcademico+$dato["monto_academico"];
                $totalNoAcademico=$totalNoAcademico+$dato["monto_no_academico"];
                
                $col=0;
                
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["nm_periodo"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);                
                $col++;                
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["monto_academico"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);                
                $col++; 
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["monto_no_academico"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);                
                $col++;                      
                
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $dato["monto_total"]);  
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);

                
                $row++;

            }
            
            $col=0;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Total");  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);            
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $col++;
 
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $totalAcademico);  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);            
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $col++;
 
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $totalNoAcademico);  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);            
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $col++;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $total);  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);            
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            

            //ob_end_clean();
            //ob_start();

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="resultado_estadisticas.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }
        
        
        
        public function getDatosGraficoResumenAnual($periodo_desde,$cc_id,$anios_reporte){
            
            
            $anios = $anios_reporte;
            
            $anios_graficar = array();
            foreach($anios as $anio){
                $string_anio = "<a href='javascript:verDetalleAnio(".$anio["id"].")'>".$anio["id"]."</a>";
                array_push($anios_graficar,$string_anio );
            }

            $tipos_persona = array();
        
            array_push($tipos_persona, array("id"=>"academico","nombre"=>"Academicos"));
            array_push($tipos_persona, array("id"=>"no_academico","nombre"=>"No Academicos"));

            
            $productividades = ProdProductividad::getProductividadesAnioTipo($periodo_desde,$cc_id);
                  
            $arreglo_anio = array();
            $datos_tabla= array();
            
            foreach($tipos_persona as $tipo_persona){
            
                $data_anio = array();
                foreach($anios as $anio){
                    $total_periodo=0;

                    foreach($productividades as $productividad){

                        
                        if($productividad["anio"] == $anio["id"] &&
                           $productividad["tipo"] == $tipo_persona["id"]){
                            $total_periodo = $total_periodo + $productividad["monto"];
                        }
                    }

                    array_push($data_anio, $total_periodo);
                    
                    
                }
                
                
                
                array_push($arreglo_anio, array("tipo"=>$tipo_persona["nombre"],"valor"=>$data_anio));
            }
            
            foreach($anios as $anio){
                $total_academico=0;
                $total_no_academico=0;

                
                foreach($tipos_persona as $tipo_persona){
                    
                    foreach($productividades as $registro){
                        if($registro["anio"] == $anio["id"] && $registro["tipo"]==$tipo_persona["id"]) {
                            if($tipo_persona["id"]=="academico"){
                                $total_academico = $total_academico+$registro["monto"]; 
                            }else{
                                $total_no_academico = $total_no_academico+$registro["monto"]; 
                            }
                            
                           
                        }                        
                        
                    } 
                }
                
                $total = $total_academico+$total_no_academico; 
                
                array_push($datos_tabla, array("nm_periodo"=>$anio["id"],"monto_academico"=>$total_academico,"monto_no_academico"=>$total_no_academico,"monto_total"=>$total));
                
            }
            
            
            $datos_graficos = array();
            foreach($arreglo_anio as $datos_grafico){

                $dato_grafico = array("name"=>"'". utf8_encode($datos_grafico["tipo"])."'",
                                      "data"=>$datos_grafico["valor"]
                                      );

                array_push($datos_graficos, $dato_grafico);
            }

            $str_datos_grafico = json_encode($datos_graficos);
            $str_categorias = json_encode($anios_graficar);
            
            $datos_grilla = $this->getDatosResumenAnio($datos_tabla);

            return array("categorias"=>$anios_graficar,"datos"=>$datos_graficos,"datos_grilla"=>$datos_grilla);
            
            
        }
        
        
        public function getDatosGraficoDetalleAnual($anio,$cc_id=0,$area_id=0){
            
            
            $meses = ProdPeriodo::$MESES;
                        
            $meses_graficar = array();
            foreach($meses as $mes){
                $periodo = $anio . $mes["id"];
                if($cc_id==0){
                    $string_anio = "<a href='javascript:verDetalleCentroCostoMes(".$periodo.")'>".$mes["label"]."</a>";
                }else{
                    $string_anio = "<a href='javascript:verDetalleTiposProductividadesMes(".$periodo.",".$cc_id.")'>".$mes["label"]."</a>";
                }
                array_push($meses_graficar,$string_anio);
                
            }

            $periodo_desde_id = $anio."01";
            $periodo_hasta_id = $anio."12";
                    
            $tipos_persona = array();
        
            array_push($tipos_persona, array("id"=>"academico","nombre"=>"Academicos"));
            array_push($tipos_persona, array("id"=>"no_academico","nombre"=>"No Academicos"));

            $productividades = ProdProductividad::getProductividadesDetalleAnio($periodo_desde_id, $periodo_hasta_id,$cc_id,$area_id);
                  
            $arreglo_meses = array();
            $datos_tabla= array();
            
            foreach($tipos_persona as $tipo_persona){
            
                $data_mes = array();
                foreach($meses as $mes){
                    
                    $vl_periodo = intval($anio . $mes["id"]);
                    $nm_periodo = ($mes["id"] . "/" .  $anio);
                    
                    $total_periodo=0;
                    
                    foreach($productividades as $productividad){
                        
                        if($productividad["periodo_id"] == $vl_periodo &&
                           $productividad["tipo"] == $tipo_persona["id"]){
                            $total_periodo = $total_periodo + $productividad["monto"];
                        }
                    }

                    array_push($data_mes, $total_periodo);  
                    
                    
                    
                }
                
                array_push($arreglo_meses, array("tipo"=>$tipo_persona["nombre"],"valor"=>$data_mes));
            }
            
            $productividadesPendientes = ProdProductividad::getProductividadesDetalleAnioPendientes($periodo_desde_id, $periodo_hasta_id,$cc_id,$area_id);
            
            $data_mes = array();
            foreach($meses as $mes){

                $vl_periodo = intval($anio . $mes["id"]);
                $nm_periodo = ($mes["id"] . "/" .  $anio);

                $total_periodo=0;

                foreach($productividadesPendientes as $productividad){

                    if($productividad["periodo_id"] == $vl_periodo){
                        $total_periodo = $total_periodo + $productividad["monto"];
                    }
                }

                array_push($data_mes, $total_periodo);  



            }

            array_push($arreglo_meses, array("tipo"=>"Pendientes","valor"=>$data_mes));
            
            
            foreach($meses as $mes){
                $total_academico=0;
                $total_no_academico=0;

                $nm_periodo = ($mes["id"] . "/" .  $anio);
                $vl_periodo = intval($anio . $mes["id"]);
                
                foreach($tipos_persona as $tipo_persona){
                    
                    foreach($productividades as $registro){
                        if($registro["periodo_id"] == $vl_periodo && $registro["tipo"]==$tipo_persona["id"]) {
                            if($tipo_persona["id"]=="academico"){
                                $total_academico = $total_academico+$registro["monto"]; 
                            }else{
                                $total_no_academico = $total_no_academico+$registro["monto"]; 
                            }
                            
                           
                        }                        
                        
                    } 
                }
                
                $total = $total_academico+$total_no_academico; 
                
                array_push($datos_tabla, array("nm_periodo"=>$nm_periodo,"monto_academico"=>$total_academico,"monto_no_academico"=>$total_no_academico,"monto_total"=>$total));
                
            }
            
            
            //obtiene los datos para las tablas+
            
            $datos_graficos = array();
            foreach($arreglo_meses as $datos_grafico){

                $dato_grafico = array("name"=>"". utf8_encode($datos_grafico["tipo"])."",
                                      "data"=>$datos_grafico["valor"]
                                      );

                array_push($datos_graficos, $dato_grafico);
            }

            
//            foreach ($datos_tabla as $key => $row) {
//                $orden[$key]  = $row['periodo'];
//                $cantidad[$key] = $row['monto'];
//            }
//
//            array_multisort($orden , SORT_ASC,$cantidad  , SORT_DESC, $datos_tabla);

            $datos_grilla = $this->getDatosResumenPeriodo($datos_tabla);

            return array("categorias"=>$meses_graficar,"datos"=>$datos_graficos,"datos_grilla"=>$datos_grilla);
            
            
        }
        
        
        public function getDatosResumenAnio($datos_tabla){
        
            $columnas = array(
                 'nm_periodo','monto_academico','monto_no_academico','monto_total', 
            );

            $total = 0;
            $totalAcademico = 0;
            $totalNoAcademico = 0;
            
            
            foreach($datos_tabla as $resumen){
                $total = $total  + $resumen["monto_total"];
                $totalAcademico = $totalAcademico + $resumen["monto_academico"];
                $totalNoAcademico = $totalNoAcademico + $resumen["monto_no_academico"];
            }
            
            $detalle_columnas_resumen = array(       
                                array(
                                    'class'=>'CLinkColumn',
                                    'labelExpression'=>'"Año " .$data["nm_periodo"]',
                                    'header'=>'Año',
                                    'urlExpression'=>'"javascript:verDetalleTabla(".$data["nm_periodo"] .")"',
                                    'footer'=>''
                                ),
                                array(
                                    'name'=>'monto_academico',
                                    'header'=>'Monto Académico',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_academico"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($totalAcademico) . '</b>'

                                ),
                                array(
                                    'name'=>'monto_no_academico',
                                    'header'=>'Monto No Académico',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_no_academico"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($totalNoAcademico) . '</b>'

                                ),
                                array(
                                    'name'=>'monto_total',
                                    'header'=>'Monto Total',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_total"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($total) . '</b>'

                                ),


                            );
        
                $dataProviderResumen=new CArrayDataProvider($datos_tabla, array(
                    'keys'=>array('concepto'),
                    'sort'=>array(
                        'attributes'=>$columnas,
                    ),
                    'pagination'=>array(
                        'pageSize'=>10000,
                    ),
                ));
                
                return array("dataProvider"=>$dataProviderResumen,"columnas"=>$detalle_columnas_resumen);
                
        }
        
        
        public function getDatosResumenPeriodo($datos_tabla){
        
            $columnas = array(
                 'nm_periodo','monto_academico','monto_no_academico','monto_total', 
            );

            $total = 0;
            $totalAcademico = 0;
            $totalNoAcademico = 0;
            
            
            foreach($datos_tabla as $resumen){
                $total = $total  + $resumen["monto_total"];
                $totalAcademico = $totalAcademico + $resumen["monto_academico"];
                $totalNoAcademico = $totalNoAcademico + $resumen["monto_no_academico"];
            }
            
            $detalle_columnas_resumen = array(       
                                array(
                                    'name'=>'nm_periodo',
                                    'header'=>'Período',
                                    'value'=> '$data["nm_periodo"]',
                                    'footer'=>'<b>Total</b>'

                                ),
                                array(
                                    'name'=>'monto_academico',
                                    'header'=>'Monto Académico',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_academico"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($totalAcademico) . '</b>'

                                ),
                                array(
                                    'name'=>'monto_no_academico',
                                    'header'=>'Monto No Académico',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_no_academico"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($totalNoAcademico) . '</b>'

                                ),
                                array(
                                    'name'=>'monto_total',
                                    'header'=>'Monto Total',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_total"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($total) . '</b>'

                                ),


                            );
        
                $dataProviderResumen=new CArrayDataProvider($datos_tabla, array(
                    'keys'=>array('concepto'),
                    'sort'=>array(
                        'attributes'=>$columnas,
                    ),
                    'pagination'=>array(
                        'pageSize'=>10000,
                    ),
                ));
                
                return array("dataProvider"=>$dataProviderResumen,"columnas"=>$detalle_columnas_resumen);
                
        }
        
        
        public function getDatosResumenCentroCosto($datos_tabla){
        
            $columnas = array(
                 'nombre_completo_cc','monto_academico','monto_no_academico','monto_total', 
            );

            $total = 0;
            $totalAcademico = 0;
            $totalNoAcademico = 0;
            
            
            foreach($datos_tabla as $resumen){
                $total = $total  + $resumen["monto_total"];
                $totalAcademico = $totalAcademico + $resumen["monto_academico"];
                $totalNoAcademico = $totalNoAcademico + $resumen["monto_no_academico"];
            }
            
            $detalle_columnas_resumen = array(       
                                array(
                                    'name'=>'nombre_completo_cc',
                                    'header'=>'Centro Costo',
                                    'value'=>'$data["nombre_completo_cc"] ',
                                    'footer'=>''
                                ),
                                array(
                                    'name'=>'monto_academico',
                                    'header'=>'Monto Académico',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_academico"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($totalAcademico) . '</b>'

                                ),
                                array(
                                    'name'=>'monto_no_academico',
                                    'header'=>'Monto No Académico',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_no_academico"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($totalNoAcademico) . '</b>'

                                ),
                                array(
                                    'name'=>'monto_total',
                                    'header'=>'Monto Total',
                                    'value'=> '"$ ".Yii::app()->format->formatNumber($data["monto_total"]) ',
                                    'footer'=>'<b>$ ' .Yii::app()->format->formatNumber($total) . '</b>'

                                ),


                            );
        
                $dataProviderResumen=new CArrayDataProvider($datos_tabla, array(
                    'keys'=>array('concepto'),
                    'sort'=>array(
                        'attributes'=>$columnas,
                    ),
                    'pagination'=>array(
                        'pageSize'=>10000,
                    ),
                ));
                
                return array("dataProvider"=>$dataProviderResumen,"columnas"=>$detalle_columnas_resumen);
                
        }
        
        
        
        public function getDatosCentroCosto($anio){
            
            $centros_costo = ProdCentroCosto::getCentrosCosto();
            
            $periodo_desde_id = $anio."01";
            $periodo_hasta_id = $anio."12";
      

            $productividades = ProdProductividad::getProductividadesDetalleCentroCosto($periodo_desde_id, $periodo_hasta_id);
            
            
            ////CARGA CATEGORIAS CENTRO COSTO
            $categorias=array();
            $datos_tabla= array();
             
            foreach($centros_costo as $centro_costo){
                
                $string_cc = "<a style='font-size:7px' href='javascript:verDetalleTiposProductividades(".$anio . ",".$centro_costo->cc_id.")' alt='".$centro_costo->nombre_cc."' title='".$centro_costo->nombre_cc."'>".$centro_costo->cc_id . " - " .$centro_costo->nombre_cc."</a>";
                
                $total_periodo=0;

                foreach($productividades as $productividad){

                    if($productividad["cc_id"] == $centro_costo->cc_id){
                       $total_periodo = $total_periodo + $productividad["monto"];
                    }
                }
                
                if($total_periodo>0){

                    $dato_tabla = array("concepto"=>$string_cc,
                                        "centro_costo"=>$centro_costo,
                                        "monto"=>$total_periodo
                                        ); 

                    array_push($datos_tabla, $dato_tabla);
                }
            }

            //ORDENA CATEGORIA POR MONTOS
            
            $orden = array();
            $cantidad = array();
            
            foreach ($datos_tabla as $key => $row) {
                $orden[$key]  = $row['concepto'];
                $cantidad[$key] = $row['monto'];
            }

            array_multisort($cantidad , SORT_DESC,$orden , SORT_DESC, $datos_tabla);

            $array_centros_costo = array();

            foreach ($datos_tabla as $key => $row) {
                array_push($array_centros_costo, $row['centro_costo']);            
                array_push($categorias, ($row['concepto']));    
                
            }
            
            
            
            //FIN CARGA CATEGORIAS CENTRO COSTO
                  
            //PREPARA DATOS GRAFICOS
            $tipos_persona = array();
        
            array_push($tipos_persona, array("id"=>"academico","nombre"=>"Academicos","color"=>"#FF9655"));
            array_push($tipos_persona, array("id"=>"no_academico","nombre"=>"No Academicos","color"=>"#FFF263"));
            
            $arreglo_centros_costos = array();

            
            foreach($tipos_persona as $tipo_persona){
                
                $array_datos = array();
                foreach($array_centros_costo as $centro_costo){

                    $total_periodo=0;

                    foreach($productividades as $productividad){

                        if($productividad["cc_id"] == $centro_costo->cc_id&&
                           $productividad["tipo"] == $tipo_persona["id"]   ){
                           $total_periodo = $total_periodo + $productividad["monto"];
                        }
                    }


                    array_push($array_datos, $total_periodo);


                }

                array_push($arreglo_centros_costos, array("tipo"=>$tipo_persona["nombre"],"color"=>$tipo_persona["color"],"valor"=>$array_datos));

            
            }
            
            $datos_graficos = array();
            foreach($arreglo_centros_costos as $datos_grafico){

                $dato_grafico = array("name"=>"". utf8_encode($datos_grafico["tipo"])."",
                                      "data"=>$datos_grafico["valor"],
                                      //"color"=>$datos_grafico["color"]
                                      );

                array_push($datos_graficos, $dato_grafico);
            }

            //FIN DATOS GRAFICOS
            
            //PREPARA DATOS TABLA
            $datos_tabla= array();
            foreach($array_centros_costo as $centro_costo){
                $total_academico=0;
                $total_no_academico=0;

                $nm_cc = $centro_costo->cc_id . "-" . $centro_costo->nombre_cc;
                
                foreach($tipos_persona as $tipo_persona){
                    
                    foreach($productividades as $registro){
                        if($registro["cc_id"] == $centro_costo->cc_id && $registro["tipo"]==$tipo_persona["id"]) {
                            if($tipo_persona["id"]=="academico"){
                                $total_academico = $total_academico+$registro["monto"]; 
                            }else{
                                $total_no_academico = $total_no_academico+$registro["monto"]; 
                            }
                            
                           
                        }                        
                        
                    } 
                }
                
                $total = $total_academico+$total_no_academico; 
                
                array_push($datos_tabla, array("nm_periodo"=>$nm_cc,"monto_academico"=>$total_academico,"monto_no_academico"=>$total_no_academico,"monto_total"=>$total));
                
            }
            $datos_grilla = $this->getDatosResumenPeriodo($datos_tabla);
            //FIN DATOS TABLA

            return array("categorias"=>$categorias,"datos"=>$datos_graficos,"datos_grilla"=>$datos_grilla);
            
            
        }
        
        
        
        public function mostrarGraficoDetalle($anio,$cc_id){
            
            
            if($cc_id==0){
                $datos_grafico_detalle = $this->getDatosCentroCosto($anio);
                $titulo = "Detalle Centro Costo Año $anio  ";
            }else{
                $centroCosto = ProdCentroCosto::getCentroCosto($cc_id);
                $datos_grafico_detalle = $this->getDatosCentroCostoTipoProductividad($anio,$cc_id);
                $titulo = "Detalle Tipo Productividad<br>Centro Costo $centroCosto<br> Año $anio";
            }
            
            
            
            return $this->renderPartial("grafico_detalle_meses", array("datos_grafico_detalle"=>$datos_grafico_detalle,
                                                                       "vl_anio"=>$anio,
                                                                       "titulo"=>$titulo
                                                                        ),true);
            
            
        }
               
        
        public function actionGetDetalleGrafico(){
            
            $anio = $_POST['vl_anio']; 
            $datos_grafico_detalle_centro_costo = $this->getDatosCentroCosto($anio);
            $titulo = "Detalle Centro Costo Año " . $anio;
            
            $this->renderPartial("grafico_detalle_meses", array("datos_grafico_detalle"=>$datos_grafico_detalle_centro_costo,
                                                                "titulo"=>$titulo,
                                                                "vl_anio"=>$anio), false, true);
            

            
        }
                
                
                
        public function getDatosCentroCostoTipoProductividad($anio,$cc_id){
            
            $centroCosto = ProdCentroCosto::getCentroCosto($cc_id);
            
            $periodo_id     = intval($anio . "01");
            $periodo_fin_id = intval($anio . "12");
                    
            $tipos_productividades = ProdCentroCosto::getTiposProducividadesTodos($cc_id,$periodo_id, $periodo_fin_id);
            
            
            $periodo_desde_id = $anio."01";
            $periodo_hasta_id = $anio."12";
      

            $productividades = ProdProductividad::getProductividadesDetalleCentroCostoTipoProductividad($periodo_desde_id, $periodo_hasta_id,$cc_id);
            
            
            ///CARGA CATEGORIAS TIPOS PRODUCTIVIDAD
            $categorias=array();
            $datos_tabla= array();
             
            foreach($tipos_productividades as $tipos_productividad){
                
                $string_cc = "<a style='font-size:7px' href='javascript:verDetallePersonas(".$cc_id.",".$tipos_productividad->tipo_prod_id.",0,$anio)' alt='".$tipos_productividad->descripcion."' title='".$tipos_productividad->descripcion."'>".$tipos_productividad->descripcion."</a>";
                
                
                $total_periodo=0;

                foreach($productividades as $productividad){

                    if($productividad["tipo_prod_id"] == $tipos_productividad->tipo_prod_id){
                       $total_periodo = $total_periodo + $productividad["monto"];
                    }
                }
                
                if($total_periodo>0){

                    $dato_tabla = array("concepto"=>$string_cc,
                                        "tipos_productividad"=>$tipos_productividad,
                                        "monto"=>$total_periodo
                                        ); 

                    array_push($datos_tabla, $dato_tabla);
                }
                
            }

            //ORDENA CATEGORIA POR MONTOS
            
            $orden = array();
            $cantidad = array();
            
            foreach ($datos_tabla as $key => $row) {
                $orden[$key]  = $row['concepto'];
                $cantidad[$key] = $row['monto'];
            }

            array_multisort($cantidad , SORT_DESC,$orden , SORT_DESC, $datos_tabla);

            $array_tipos_productividad = array();

            foreach ($datos_tabla as $key => $row) {
                array_push($array_tipos_productividad, $row['tipos_productividad']);            
                array_push($categorias, ($row['concepto']));    
                
            }
            
            //FIN CARGA CATEGORIAS TIPOS PRODUCTIVIDAD
            
            
            $tipos_persona = array();
        
            array_push($tipos_persona, array("id"=>"academico","nombre"=>"Academicos","color"=>"#ffad67"));
            array_push($tipos_persona, array("id"=>"no_academico","nombre"=>"No Academicos","color"=>"#FFF263"));
            

            
            $arreglo_datos_tipos_productividad = array();
            
            foreach($tipos_persona as $tipo_persona){
            
                $array_datos = array();
                foreach($array_tipos_productividad as $tipos_productividad){

                    $total_periodo=0;

                    foreach($productividades as $productividad){

                        if($productividad["tipo_prod_id"] == $tipos_productividad->tipo_prod_id &&
                           $productividad["tipo"] == $tipo_persona["id"]){
                           $total_periodo = $total_periodo + $productividad["monto"];
                        }
                    }

                    array_push($array_datos, $total_periodo);


                }

                array_push($arreglo_datos_tipos_productividad, array("tipo"=>$tipo_persona["nombre"],"color"=>$tipo_persona["color"],"valor"=>$array_datos));
            }
            
            $datos_graficos = array();
            foreach($arreglo_datos_tipos_productividad as $datos_grafico){

                $dato_grafico = array("name"=>"". utf8_encode($datos_grafico["tipo"])."",
                                      "data"=>$datos_grafico["valor"],
                                    //  "color"=>$datos_grafico["color"]
                                      );

                array_push($datos_graficos, $dato_grafico);
            }

            //PREPARA DATOS TABLA
            $datos_tabla= array();
            foreach($array_tipos_productividad as $tipos_productividad){
                
                $total_academico=0;
                $total_no_academico=0;

                $nm_tipo_prod = $tipos_productividad->nombre_tipo_prod;
                
                foreach($tipos_persona as $tipo_persona){
                    
                    foreach($productividades as $registro){
                        if($registro["tipo_prod_id"] == $tipos_productividad->tipo_prod_id && $registro["tipo"]==$tipo_persona["id"]) {
                            if($tipo_persona["id"]=="academico"){
                                $total_academico = $total_academico+$registro["monto"]; 
                            }else{
                                $total_no_academico = $total_no_academico+$registro["monto"]; 
                            }
                            
                           
                        }                        
                        
                    } 
                }
                
                $total = $total_academico+$total_no_academico; 
                
                array_push($datos_tabla, array("nm_periodo"=>$nm_tipo_prod,"monto_academico"=>$total_academico,"monto_no_academico"=>$total_no_academico,"monto_total"=>$total));
                
            }
            $datos_grilla = $this->getDatosResumenPeriodo($datos_tabla);
            //FIN DATOS TABLA
            

            return array("categorias"=>$categorias,"datos"=>$datos_graficos,"datos_grilla"=>$datos_grilla);
            
            
        }
                
 
               
        
        public function actionGetDetalleGraficoTipoProductividad(){
            
            $anio = $_POST['vl_anio']; 
            $cc_id = $_POST['cc_id']; 
            
            $centroCosto = ProdCentroCosto::getCentroCosto($cc_id);
            
            $datos_grafico_detalle = $this->getDatosCentroCostoTipoProductividad($anio,$cc_id);

            
            $this->renderPartial("grafico_detalle_tipos_prod", array("datos_grafico_detalle_tipo_prod"=>$datos_grafico_detalle,
                                                                     "centroCosto"=>$centroCosto,
                                                                     "vl_anio"=>$anio), false, true);
            

            
        }
                
        public function actionGetDetalleGraficoCentroCostoMes(){
            
            $vl_periodo = $_POST['vl_periodo']; 
            
            $periodo = ProdPeriodo::getPeriodo($vl_periodo);
            
            $datos_grafico_detalle = $this->getDatosCentroCostoMes($vl_periodo);

            
            $this->renderPartial("grafico_detalle_centro_costo_mes", array("datos_grafico_detalle_centro_costo"=>$datos_grafico_detalle,
                                                                     "periodo"=>$periodo), false, true);
            

            
        }
        
        public function getDatosCentroCostoMes($vl_periodo,$area_id=0){
            
            $centros_costo = ProdCentroCosto::getCentrosCosto();
            
            $periodo_desde_id = $vl_periodo;
            $periodo_hasta_id = $vl_periodo;
      
            $productividades = ProdProductividad::getProductividadesDetalleCentroCosto($periodo_desde_id, $periodo_hasta_id,$area_id);
            
            ////CARGA CATEGORIAS CENTRO COSTO
            $categorias=array();
            $datos_tabla= array();
             
            foreach($centros_costo as $centro_costo){
                
                $string_cc = "<a style='font-size:7px' href='javascript:verDetalleTiposProductividadesMes(".$vl_periodo . ",".$centro_costo->cc_id.")' alt='".$centro_costo->nombre_cc."' title='".$centro_costo->nombre_cc."'>".$centro_costo->cc_id. " - " . $centro_costo->nombre_cc."</a>";
                
                $total_periodo=0;

                foreach($productividades as $productividad){

                    if($productividad["cc_id"] == $centro_costo->cc_id){
                       $total_periodo = $total_periodo + $productividad["monto"];
                    }
                }
                
                if($total_periodo>0){

                    $dato_tabla = array("concepto"=>$string_cc,
                                        "centro_costo"=>$centro_costo,
                                        "monto"=>$total_periodo
                                        ); 

                    array_push($datos_tabla, $dato_tabla);
                }
                
                
                
                
                
                
            }

            //ORDENA CATEGORIA POR MONTOS
            
            $orden = array();
            $cantidad = array();
            
            foreach ($datos_tabla as $key => $row) {
                $orden[$key]  = $row['concepto'];
                $cantidad[$key] = $row['monto'];
            }

            array_multisort($cantidad , SORT_DESC,$orden , SORT_DESC, $datos_tabla);

            $array_centros_costo = array();

            foreach ($datos_tabla as $key => $row) {
                array_push($array_centros_costo, $row['centro_costo']);            
                array_push($categorias, ($row['concepto']));    
                
            }
            //FIN CARGA CATEGORIAS CENTRO COSTO
            
            $tipos_persona = array();
        
            array_push($tipos_persona, array("id"=>"academico","nombre"=>"Academicos","color"=>"#FF9655"));
            array_push($tipos_persona, array("id"=>"no_academico","nombre"=>"No Academicos","color"=>"#FFF263"));
            
            $arreglo_centros_costos = array();

            
            foreach($tipos_persona as $tipo_persona){
                
            
                $array_datos = array();
                foreach($array_centros_costo as $centro_costo){

                    $total_periodo=0;

                    foreach($productividades as $productividad){

                        if($productividad["cc_id"] == $centro_costo->cc_id&&
                           $productividad["tipo"] == $tipo_persona["id"]  ){
                           $total_periodo = $total_periodo + $productividad["monto"];
                        }
                    }

                    array_push($array_datos, $total_periodo);


                }

                array_push($arreglo_centros_costos, array("tipo"=>$tipo_persona["nombre"],"color"=>$tipo_persona["color"],"valor"=>$array_datos));
            }
            
            $datos_graficos = array();
            foreach($arreglo_centros_costos as $datos_grafico){

                $dato_grafico = array("name"=>"". utf8_encode($datos_grafico["tipo"])."",
                                      "data"=>$datos_grafico["valor"],
                                   //   "color"=>$datos_grafico["color"]
                                      );

                array_push($datos_graficos, $dato_grafico);
            }
            
            //PREPARA DATOS TABLA
            $datos_tabla= array();
            foreach($array_centros_costo as $centro_costo){
                $total_academico=0;
                $total_no_academico=0;

                $nm_cc = $centro_costo->cc_id . "-" . $centro_costo->nombre_cc;
                
                foreach($tipos_persona as $tipo_persona){
                    
                    foreach($productividades as $registro){
                        if($registro["cc_id"] == $centro_costo->cc_id && $registro["tipo"]==$tipo_persona["id"]) {
                            if($tipo_persona["id"]=="academico"){
                                $total_academico = $total_academico+$registro["monto"]; 
                            }else{
                                $total_no_academico = $total_no_academico+$registro["monto"]; 
                            }
                            
                           
                        }                        
                        
                    } 
                }
                
                $total = $total_academico+$total_no_academico; 
                
                array_push($datos_tabla, array("nombre_completo_cc"=>$nm_cc,"monto_academico"=>$total_academico,"monto_no_academico"=>$total_no_academico,"monto_total"=>$total));
                
            }
            $datos_grilla = $this->getDatosResumenCentroCosto($datos_tabla);
            //FIN DATOS TABLA


            return array("categorias"=>$categorias,"datos"=>$datos_graficos,"datos_grilla"=>$datos_grilla);
            
            
        }
        
        public function actionGetDetalleGraficoTipoProductividadMes(){
            
            $vl_periodo = $_POST['vl_periodo']; 
            $cc_id = $_POST['cc_id']; 
            $periodo = ProdPeriodo::getPeriodo($vl_periodo);
            
            $centroCosto = ProdCentroCosto::getCentroCosto($cc_id);
            
            $datos_grafico_detalle = $this->getDatosCentroCostoTipoProductividadMes($vl_periodo,$cc_id);

            
            $this->renderPartial("grafico_detalle_tipo_prod_mes", array("datos_grafico_detalle_tipo_prod"=>$datos_grafico_detalle,
                                                                     "centroCosto"=>$centroCosto,
                                                                     "periodo"=>$periodo), false, true);
            

            
        }
        
        public function getDatosCentroCostoTipoProductividadMes($vl_periodo,$cc_id){
            
            $centroCosto = ProdCentroCosto::getCentroCosto($cc_id);
            
            $tipos_productividades = ProdCentroCosto::getTiposProducividadesTodos($cc_id,$vl_periodo, 0);
                        
            $periodo_desde_id = $vl_periodo;
            $periodo_hasta_id = $vl_periodo;
      

            $productividades = ProdProductividad::getProductividadesDetalleCentroCostoTipoProductividad($periodo_desde_id, $periodo_hasta_id,$cc_id);
                  
            
            ///CARGA CATEGORIAS TIPOS PRODUCTIVIDAD
            $categorias=array();
            $datos_tabla= array();
             
            foreach($tipos_productividades as $tipos_productividad){
                
                $string_cc = "<a style='font-size:7px' href='javascript:verDetallePersonas(".$cc_id.",".$tipos_productividad->tipo_prod_id.",$vl_periodo,0)' alt='".$tipos_productividad->descripcion."' title='".$tipos_productividad->descripcion."'>".$tipos_productividad->descripcion."</a>";

                
                $total_periodo=0;

                foreach($productividades as $productividad){

                    if($productividad["tipo_prod_id"] == $tipos_productividad->tipo_prod_id){
                       $total_periodo = $total_periodo + $productividad["monto"];
                    }
                }
                
                if($total_periodo>0){

                    $dato_tabla = array("concepto"=>$string_cc,
                                        "tipos_productividad"=>$tipos_productividad,
                                        "monto"=>$total_periodo
                                        ); 

                    array_push($datos_tabla, $dato_tabla);
                }
                
            }

            //ORDENA CATEGORIA POR MONTOS
            
            $orden = array();
            $cantidad = array();
            
            foreach ($datos_tabla as $key => $row) {
                $orden[$key]  = $row['concepto'];
                $cantidad[$key] = $row['monto'];
            }

            array_multisort($cantidad , SORT_DESC,$orden , SORT_DESC, $datos_tabla);

            $array_tipos_productividad = array();

            foreach ($datos_tabla as $key => $row) {
                array_push($array_tipos_productividad, $row['tipos_productividad']);            
                array_push($categorias, ($row['concepto']));    
                
            }
            
            //FIN CARGA CATEGORIAS TIPOS PRODUCTIVIDAD
            
            $tipos_persona = array();
        
            array_push($tipos_persona, array("id"=>"academico","nombre"=>"Academicos","color"=>"#ffad67"));
            array_push($tipos_persona, array("id"=>"no_academico","nombre"=>"No Academicos","color"=>"#FFF263"));

            
            $arreglo_centros_costos = array();
            
            foreach($tipos_persona as $tipo_persona){
                
                $array_datos = array();
                foreach($array_tipos_productividad as $tipos_productividad){

                    $total_periodo=0;

                    foreach($productividades as $productividad){

                        if($productividad["tipo_prod_id"] == $tipos_productividad->tipo_prod_id &&
                           $productividad["tipo"] == $tipo_persona["id"]   ){
                           $total_periodo = $total_periodo + $productividad["monto"];
                        }
                    }

                    array_push($array_datos, $total_periodo);


                }

                array_push($arreglo_centros_costos, array("tipo"=>$tipo_persona["nombre"],"color"=>$tipo_persona["color"],"valor"=>$array_datos));
            }
            
            $datos_graficos = array();
            foreach($arreglo_centros_costos as $datos_grafico){

                $dato_grafico = array("name"=>"". utf8_encode($datos_grafico["tipo"])."",
                                      "data"=>$datos_grafico["valor"],
                                      //"color"=>$datos_grafico["color"]
                                      );

                array_push($datos_graficos, $dato_grafico);
            }


            return array("categorias"=>$categorias,"datos"=>$datos_graficos);
            
            
        }
        
        
        function actionVerDetallePersonas(){
            
            $vl_periodo = $_POST['vl_periodo']; 
            $tipo_prod_id = $_POST['tipo_prod_id']; 
            $cc_id = $_POST['cc_id']; 
            $vl_anio = $_POST['vl_anio']; 
            
            $route = "productividades/estadisticas/verDetallePersonas";
            
            $model = new ProdProductividad();
            $detalle = ProdProductividad::getDetalleTipoProductividadDataProvider($model,$cc_id, $tipo_prod_id,$vl_periodo,$vl_anio,$route);

            echo $this->renderPartial("detalle_personas", array("detalle"=>$detalle),true);
        }
        
        
        function generaTablaComparacion ($periodo_inicio_id,$centros_costos,$anios,$tipos_persona){
            
            $listadoProductividades = ProdProductividad::getProductividadesComparaCCAnioTipo($periodo_inicio_id);
            

            $salida_datos = array();
            /* @var $centro_costo ProdCentroCosto */
            foreach ($centros_costos as $centro_costo){
                
                foreach($anios as $anio){
                    
                    $valor_total =0;
                    foreach($tipos_persona as $tipo){
                        
                        $valor = 0;
                        foreach($listadoProductividades as $resumen){
                            
                            if($centro_costo->cc_id == $resumen["cc_id"] &&
                               $anio["id"] == $resumen["anio"] &&
                               $tipo["id"] == $resumen["tipo"]){

                                
                                $valor = $valor + $resumen["monto"];

                            }

                        }
                        
                        $valor_total = $valor_total + $valor;
                        
                        array_push($salida_datos, array("cc_id"=>$centro_costo->cc_id,"anio"=>$anio,"valor"=>$valor,"tipo"=>$tipo["id"]));
                    }
                    
                    array_push($salida_datos, array("cc_id"=>$centro_costo->cc_id,"anio"=>$anio,"valor"=>$valor_total,"tipo"=>"total"));
                }
                
                
            }
            
            
            return $salida_datos;
            
            
        }
}
