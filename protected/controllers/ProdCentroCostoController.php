<?php
/**
 * Controlador del mantenedor de la tabla ProdCentroCosto
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller
 */
class ProdCentroCostoController extends GxController {

        /**
        * Define la función de control de acceso que tiene el controlador
        * 
        * @return array funciones que realizan el control de acceso
        */         
        public function filters()
        {
            return array(
                'accessControl',
            );
        }
        
        /**
        * Control de acceso del módulo
        * En este caso tienen acceso a las funcionalidades solo el administrador del sistema
        * 
        * @return array permisos de usuario al módulo
        */
	public function accessRules()
        {
            //SOLO EL USUARIO ADMINISTRADOR PUEDE ACCEDER
            return array(
                array('allow',
                      'roles' => array('admin'),
                ),                
                array('deny',
                    'users'=>array('*'),
                ),
            );
        }   

        /**
        * Despliega el detalle de un Centro de Costo
        * 
        * @param int $id Centro de Costo ID
        * @return string despliega la vista prodCentroCosto/view
        */          
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ProdCentroCosto'),
		));
	}

        /**
        * Despliega el formulario para crear un registro
        * 
        * @return string despliega la vista prodCentroCosto/create
        */         
	public function actionCreate() {
            
		$model = new ProdCentroCosto;
                
                
		$this->performAjaxValidation($model, 'prod-centro-costo-form');

                try
                {
                    if (isset($_POST['ProdCentroCosto'])) {
                            $model->setAttributes($_POST['ProdCentroCosto']);
                            $relatedData = array(
                                    'prodTipoProductividads' => $_POST['ProdCentroCosto']['prodTipoProductividads'] === '' ? null : $_POST['ProdCentroCosto']['prodTipoProductividads'],
                                    );

                            if ($model->saveWithRelated($relatedData)) {
                                    if (Yii::app()->getRequest()->getIsAjaxRequest())
                                            Yii::app()->end();
                                    else
                                            $this->redirect(array('view', 'id' => $model->cc_id));
                            }
                    }
                }catch (Exception $e){
                    Yii::app()->user->setFlash('error',$e->getMessage());
                    Yii::log("Error Insert: " . $e->getMessage(), "error", "application.controllers.ProdCentroCostoController");


                }

                
		$this->render('create', array( 'model' => $model));
	}

        /**
        * Despliega el formulario para actualizar un registro
        * 
        * @param int $id ID del registro a actualizar
        * @return string despliega la vista prodCentroCosto/update
        */         
	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ProdCentroCosto');

		$this->performAjaxValidation($model, 'prod-centro-costo-form');
                try
                {

                    if (isset($_POST['ProdCentroCosto'])) {
                            $model->setAttributes($_POST['ProdCentroCosto']);
                            $relatedData = array(
                                    'prodTipoProductividads' => $_POST['ProdCentroCosto']['prodTipoProductividads'] === '' ? null : $_POST['ProdCentroCosto']['prodTipoProductividads'],
                                    );

                            if ($model->saveWithRelated($relatedData)) {
                                
                                $relacionesActuales = ProdTipoProdCc::getTiposProductividadesCentroCosto($id);
                                
                                $areas = ProdAreaTipoPodCc::getAreasCentroCosto($id);
                                
                                foreach ($areas as $area){
                                
                                    $relacion_cc_tipo_prod = ProdAreaTipoPodCc::getRelacionesArea($area->area_id);
                                    
                                    foreach($relacion_cc_tipo_prod as $cc_tipo_prod){
                                        
                                        $existeRelacion = false;
                                        foreach($relacionesActuales as $relacionActual){
                                            if($cc_tipo_prod->cc_id == $relacionActual->cc_id &&
                                               $cc_tipo_prod->tipo_prod_id == $relacionActual->tipo_prod_id){
                                                $existeRelacion = true;
                                                break;
                                            }
                                        }
                                        
                                        if(!$existeRelacion){
                                            $cc_tipo_prod->delete();
                                        }
                                        
                                    }
                                }
                                
                                $this->redirect(array('admin'));
                            }
                    }

                    $this->render('update', array(
                                    'model' => $model,
                                    ));
                }catch (Exception $e){
                    Yii::app()->user->setFlash('error',$e->errorInfo[0]." - ". $e->errorInfo[1]." - ". $e->errorInfo[2]);
                    Yii::log("Error Insert: " . $e->errorInfo[0]." - ". $e->errorInfo[1]." - ". $e->errorInfo[2], "error", "application.controllers.ProdCentroCostoController");

                    //echo $e;
                }
	}

        /**
        * Elimina un registros un registro
        * 
        * @param int $id ID del registro a borrar
        * @return string redirect prodCentroCosto/admin
        */           
	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
                    
                    try{
                       $this->loadModel($id, 'ProdCentroCosto')->delete();
                       Yii::app()->user->setFlash('success','Borrado Correctamente');
                       echo "<div class='flash-success'>Borrado Correctamente</div>"; //for ajax
                    }catch(CDbException $e){
                        Yii::app()->user->setFlash('error','No se puede borrar el registro porque tiene datos relacionados.');

                        echo "<div class='flash-error'>No se puede borrar el registro porque tiene datos relacionados.</div>"; //for ajax
                    }
                    if (!Yii::app()->getRequest()->getIsAjaxRequest())
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin')); 

			
		} else
                    throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

        
        /**
        * Despliega el index del mantenedor
        * 
        * @return string despliega la vista prodCentroCosto/admin
        */           
	public function actionIndex() {
//		$dataProvider = new CActiveDataProvider('ProdCentroCosto');
//		$this->render('index', array(
//			'dataProvider' => $dataProvider,
//		));
            $this->actionAdmin();
	}

        /**
        * Despliega el index del mantenedor
        * 
        * @return string despliega la vista prodCentroCosto/admin
        */         
	public function actionAdmin() {
		$model = new ProdCentroCosto('search');
		$model->unsetAttributes();

		if (isset($_GET['ProdCentroCosto']))
			$model->setAttributes($_GET['ProdCentroCosto']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}