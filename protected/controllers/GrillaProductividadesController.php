<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GrillaProductividadesController
 *
 * @author Luisao
 */
class GrillaProductividadesController extends SecureController{
    //put your code here
    public $tipos_productividades=array();
    public $centros_costos=array();
    public $es_academico=true;
    public $productividades=array();
    public $filtro = array();
    public $periodo_id = 0;
    public $periodo_fin_id = 0;
    public $es_consulta=false;
    public $es_validacion = false;
    public $es_validacion_encargado = false;
    public $es_ingreso = false;
    public $model;
    public $width=1000;
    public $exportar_excel = false;
    public $periodo_cerrado = false;
    public $area_id = 0;
    public $detalle_por_tipo_productividad = false;
    public $mostrar_positivos = false;
    
        
    
    /**
        * Control de acceso del módulo
        * Todos los usuarios tienen accceso a las productividades individuales
        * Para las productividades globales, pueden acceder los usuarios ingreso (encargados),
        * validadores (jefe administrativo) y el director.
        * El administrador por defecto puede ver todos los módulos
        * 
        * @return array permisos de usuario al módulo
        */
	public function accessRules()
        {
            return array(
                //SOLOS LOS USUARIOS CON PERMISO INGRESO O VALIDACION PUEDE ENTRAR A LAS 
                //PRODUCTIVIDADES GLOBALES
                array('allow',
                      'actions'=>array('getDatosGrilla','ExportarExcel'),
                      'roles' => array('consulta','ingreso','encargado','validador','director')
                ),
                //TODO USUARIO AUTENTICADO PUEDE INGRESAR A ESTAS ACCIONES
                array('allow',
                      'actions'=>array( 'getDatosGrilla'
                                    ),
                      'users' =>array('@')
                ),
                array('deny',
                    'users'=>array('*'),
                ),
            );
        }
        
    function actionGetDatosGrilla(){
        
        $periodo_id = $_POST["periodo_id"];
        $periodo_fin_id = $_POST["periodo_fin_id"];
        $es_academico = $_POST["es_academico"];
        $es_consulta = $_POST["es_consulta"];
        $es_validacion = $_POST["es_validacion"];
        $es_validacion_encargado = $_POST["es_validacion_encargado"];
        $es_ingreso = $_POST["es_ingreso"];
        $periodo_cerrado = $_POST["periodo_cerrado"];
        $area_id = $_POST["area_id"];
        $detalle_por_tipo_productividad = $_POST["detalle_por_tipo_productividad"];
        $mostrar_positivos = $_POST["mostrar_positivos"];
        $filtro = $_POST["filtro"];
        
        $this->filtro = json_decode($filtro);
        
        $this->periodo_id = $periodo_id;
        $this->periodo_fin_id = $periodo_fin_id;
        $this->area_id = $area_id;
        
        
        if($es_academico=="true"){
            $this->es_academico = true;
        }else{
            $this->es_academico = false;
        }
        
        if($es_consulta=="true"){
            $this->es_consulta = true;
        }else{
            $this->es_consulta = false;
        }
        
        if($es_validacion=="true"){
            $this->es_validacion = true;
        }else{
            $this->es_validacion = false;
        }
        
        if($es_validacion_encargado=="true"){
            $this->es_validacion_encargado = true;
        }else{
            $this->es_validacion_encargado = false;
        }
        
        if($es_ingreso=="true"){
            $this->es_ingreso = true;
        }else{
            $this->es_ingreso = false;
        }        
        
        if($periodo_cerrado=="true"){
            $this->periodo_cerrado = true;
        }else{
            $this->periodo_cerrado = false;
        }
        
        if($detalle_por_tipo_productividad=="1"){
            $this->detalle_por_tipo_productividad = true;
        }else{
            $this->detalle_por_tipo_productividad = false;
        }
        
        if($mostrar_positivos=="1"){
            $this->mostrar_positivos = true;
        }else{
            $this->mostrar_positivos = false;
        }
        
        
        $jqgrid = new JqgridParameterForm;
        $jqgrid->attributes = $_POST;
        
        $salida_personas = $this->preparaDatos($jqgrid);
        $count = count($salida_personas);
        $jqgrid->calculaPaginas($count);
       
        $jqgrid->rows = array();
        
        
        
        $i=0;     

        foreach($salida_personas as $persona){

            $link="";
            $imghtml=CHtml::image(Yii::app()->baseUrl."/images/Search_32.png","Ver Detalle",array("width"=>20));
            $url="";
            
            

            if($this->es_validacion || $this->es_validacion_encargado){
                
                $rol_id = Yii::app()->user->rolId;
                if($rol_id == ProdRol::ROL_ADMINISTRDOR && $this->es_validacion ){
                    $rol_id = ProdRol::ROL_JEFE_ADMINISTRATIVO;
                }
                
                if($rol_id == ProdRol::ROL_ADMINISTRDOR && $this->es_validacion_encargado ){
                    $rol_id = ProdRol::ROL_ENCARGADO;
                }


                if(!$this->periodo_cerrado){
                    
                    
                    $url=Yii::app()->createUrl('validacionProductividades/detalleRechazo');
                    
                    $data = array('persona_id'=>$persona["persona"]->persona_id,'periodo_id'=>$this->periodo_id,'es_academico'=>$this->es_academico,'rol_id'=>$rol_id,'area_id'=>$this->area_id);
                    $link = CHtml::ajaxLink($imghtml,Yii::app()->createUrl('validacionProductividades/detalleRechazo'),
                                        array('type'=>'POST',
                                              'data'=>array('persona_id'=>$persona["persona"]->persona_id,'periodo_id'=>$this->periodo_id,'es_academico'=>$this->es_academico,'rol_id'=>$rol_id,'area_id'=>$this->area_id),
                                              'update'=>'#preview',
                                              'complete'=>'afterAjax'
                                        ),
                                        array('id' =>'persona_' . $persona["persona"]->persona_id));
                }else{
                    $url=Yii::app()->createUrl('productividades/detalleProductividadPersona');
                    $data = array('persona_id'=>$persona["persona"]->persona_id,
                                                            'periodo_id'=>$this->periodo_id,
                                                            'periodo_fin_id'=>$this->periodo_fin_id,
                                                            'ajax'=>'ajax');
                    
                    $link = CHtml::ajaxLink($imghtml,Yii::app()->createUrl('productividades/detalleProductividadPersona'),
                                        array('type'=>'POST',
                                              'data'=>array('persona_id'=>$persona["persona"]->persona_id,
                                                            'periodo_id'=>$this->periodo_id,
                                                            'periodo_fin_id'=>$this->periodo_fin_id,
                                                            'ajax'=>'ajax'),
                                              'update'=>'#preview',
                                              'complete'=>'afterAjax'
                                        ),
                                        array('id' =>'persona_' . $persona["persona"]->persona_id));
                }
            }else{
                if($this->es_consulta || ($this->es_ingreso && $this->periodo_cerrado)){
                    $url=Yii::app()->createUrl('productividades/detalleProductividadPersona');
                    
                    $data = array('persona_id'=>$persona["persona"]->persona_id,
                                                            'periodo_id'=>$this->periodo_id,
                                                            'periodo_fin_id'=>$this->periodo_fin_id,
                                                            'ajax'=>'ajax');
                            
                            
                    $link = CHtml::ajaxLink($imghtml,Yii::app()->createUrl('productividades/detalleProductividadPersona'),
                                        array('type'=>'POST',
                                              'data'=>array('persona_id'=>$persona["persona"]->persona_id,
                                                            'periodo_id'=>$this->periodo_id,
                                                            'periodo_fin_id'=>$this->periodo_fin_id,
                                                            'ajax'=>'ajax'),
                                              'update'=>'#preview',
                                              'complete'=>'afterAjax'
                                        ),
                                        array('id' =>'persona_' . $persona["persona"]->persona_id));
                }else{
                    $url=Yii::app()->createUrl('ingresoProductividades/detalleProductividadPersona');
                    
                    $data = array('persona_id'=>$persona["persona"]->persona_id,'periodo_id'=>$this->periodo_id,'area_id'=>$this->area_id,'ajax'=>'ajax');
                    
                    $link = CHtml::ajaxLink($imghtml,Yii::app()->createUrl('ingresoProductividades/detalleProductividadPersona'),
                                        array('type'=>'POST',
                                              'data'=>array('persona_id'=>$persona["persona"]->persona_id,'periodo_id'=>$this->periodo_id,'ajax'=>'ajax'),
                                              'update'=>'#preview',
                                              'complete'=>'afterAjax'
                                        ),
                                        array('id' =>'persona_' . $persona["persona"]->persona_id));
                }
            }
            
            
            
            //$link = "<a href=javascript:verDetallePersona('$url','".  json_encode($data)."')>" . $imghtml . "</a>";
            //$link = "<a name='detalle_persona' id='".  json_encode($data)."' href='$url')>" . $imghtml . "</a>";
            $link = "<a name='detalle_persona' id='".  json_encode($data)."' href='$url')><span class='glyphicon glyphicon-search'></span></a>";
            
            $jqgrid->rows[$i]['id']=$persona["persona"]->persona_id;
            
            $total=0;
            $observacionesTotales="";
            foreach($persona["listado_productividades"] as $detalle_productividad){
                
                if($detalle_productividad["observaciones"]!=""){
                    $observacionesTotales = $observacionesTotales . $detalle_productividad["observaciones"] . "\n";
                }
                $total=$total+$detalle_productividad["monto"];
            }
            
            //$link = str_replace('"', "'", $link);
            
            $estado_global_id = $persona["estado_global_id"];
            $class="tregistro";
            $checked_aprobado=false;
            $checked_disabled = false;

            
            if($this->es_validacion || $this->es_validacion_encargado){
            
                if($rol_id==ProdRol::ROL_JEFE_ADMINISTRATIVO){
                    if($estado_global_id==ProdEstado::PENDIENTE){
                        $class="tregistroapendiente";
                        $class_total ="tregistroatotalpendiente";
                        $class_persona ="tregistroapersonapendiente";
                        $checked_disabled = true;
                    }

                    if($estado_global_id==ProdEstado::APROBADO_ENCARGADO){
                        $class="tregistroencargado";
                        $class_total ="tregistrototalencargado";
                        $class_persona ="tregistropersonaencargado";


                    }

                    if($estado_global_id==ProdEstado::APROBADO){
                        $class="tregistroaprobado";
                        $class_total ="tregistrototalaprobado";
                        $class_persona ="tregistropersonaaprobado";
                        $checked_aprobado=true;
                        $checked_disabled = true;
                    }

                }

                if($rol_id==ProdRol::ROL_ENCARGADO){
                    if($estado_global_id==ProdEstado::PENDIENTE){
                        $class="tregistroapendiente";
                        $class_total ="tregistroatotalpendiente";
                        $class_persona ="tregistroapersonapendiente";

                    }

                    if($estado_global_id==ProdEstado::APROBADO_ENCARGADO){
                        $class="tregistroencargado";
                        $class_total ="tregistrototalencargado";
                        $class_persona ="tregistropersonaencargado";
                        $checked_aprobado=true;
                        $checked_disabled = true;
                    }

                    if($estado_global_id==ProdEstado::APROBADO){
                        $class="tregistroaprobado";
                        $class_total ="tregistrototalaprobado";
                        $class_persona ="tregistropersonaaprobado";
                        $checked_aprobado=true;
                        $checked_disabled = true;
                    }

                }
            
            }

            if($this->es_ingreso){
                $class="tregistro";
                $class_persona = "tregistropersona";
                $class_total = "tregistrototal";
                
                if($estado_global_id==ProdEstado::PENDIENTE){
                    $class="tregistroapendiente";
                    $class_total ="tregistroatotalpendiente";
                    $class_persona ="tregistroapersonapendiente";
                }

                if($estado_global_id==ProdEstado::APROBADO_ENCARGADO){
                    $class="tregistroencargado";
                    $class_total ="tregistrototalencargado";
                    $class_persona ="tregistropersonaencargado";


                }

                if($estado_global_id==ProdEstado::APROBADO){
                    $class="tregistroaprobado";
                    $class_total ="tregistrototalaprobado";
                    $class_persona ="tregistropersonaaprobado";
                }

            }
            
            if($this->es_consulta){
                $class="tregistro";
                $class_persona = "tregistropersona";
                $class_total = "tregistrototal";
            }

            if($this->es_consulta){
                $class="tregistro";
            }

            $validar = "";
            if( ($this->es_validacion || $this->es_validacion_encargado) && !$this->periodo_cerrado){
                if($this->es_academico){
                    $name_check = "academicos[".$persona['persona']->persona_id."][aprueba]";

                }else{
                    $name_check = "no_academicos[".$persona['persona']->persona_id."][aprueba]";
                }
                
                $validar = CHtml::activeCheckBox($jqgrid,$name_check,array("checked"=>$checked_aprobado,"onclick"=>"clickedVal(this)","disabled"=>$checked_disabled) );
                $validar = str_replace('"', "'", $validar);
            }

            
            $cell = array(  $persona["persona"]->persona_id,
                            $link,
                            $validar,
                            "<span class='$class_persona'>" .$persona["persona"]->apellido_paterno . "</span>",
                            "<span class='$class_persona'>" .$persona["persona"]->apellido_materno. "</span>",
                            "<span class='$class_persona'>" .$persona["persona"]->nombres. "</span>", 
                            "<a href='#' data-toggle='tooltip' title='".$observacionesTotales."'><span class='$class_total'>" ."$ " . Yii::app()->format->formatNumber($total). "</span></a>",
                            $total
                            );  
            
            
            foreach($persona["listado_productividades"] as $detalle_productividad){
                
                array_push($cell,"<a href='#' title='".$detalle_productividad["observaciones"]."'><span class='$class' >" ."$ " . Yii::app()->format->formatNumber($detalle_productividad["monto"]). "</span></a>"); 
                array_push($cell,$detalle_productividad["monto"]);
                
            }
            
            $jqgrid->rows[$i]['cell']=$cell;          
           
            $i++;
        }            

        //echo count($respuesta);

        // La respuesta se regresa como json
        echo json_encode($jqgrid); 
        
        
    }
    
        
    function preparaDatos(JqgridParameterForm $jqgrid){
        //print_r($this->filtro);
        //echo "periodo:" . $this->periodo_id . " : <br>";
        $this->centros_costos=array();
        
        $rol_id = Yii::app()->user->rolId;
               
        
        $centros_costos = array();
        
        if($this->es_consulta || $this->es_validacion ){
            
            
            if($this->es_consulta && ($rol_id == ProdRol::ROL_ENCARGADO || $rol_id == ProdRol::ROL_INGRESO_DATOS) ){
                
                $listado_cc_tipos_prod_rol = ProdAreaTipoPodCc::getCentrosCostosTiposProductividadesArea($this->area_id, true);

                foreach($listado_cc_tipos_prod_rol as $cc_tipos_prod_rol){
                    if(!in_array($cc_tipos_prod_rol->cc,$centros_costos)){
                        array_push($centros_costos, $cc_tipos_prod_rol->cc);
                    }
                }
            }else{
                $this->tipos_productividades = ProdTipoProductividad::model()->findAll();
                $centros_costos = ProdCentroCosto::model()->findAll();
            }
            
        }else{
            //SI ES INGRESO
            $listado_cc_tipos_prod_rol = ProdAreaTipoPodCc::getCentrosCostosTiposProductividadesArea($this->area_id, true);

            foreach($listado_cc_tipos_prod_rol as $cc_tipos_prod_rol){
                if(!in_array($cc_tipos_prod_rol->cc,$centros_costos)){
                    array_push($centros_costos, $cc_tipos_prod_rol->cc);
                }
            }
            
             
        }
                
        foreach($centros_costos as $centro_costo){
            $tipo_prod_salida = array();
            
            $relacionesCentroCostoTipoProductividad = ProdCentroCosto::getTiposProducividades($centro_costo->cc_id,$this->periodo_id, $this->periodo_fin_id,$this->es_academico);
            
            foreach($relacionesCentroCostoTipoProductividad as $tipo_productividad){
                array_push($tipo_prod_salida, $tipo_productividad);
            }
            sort($tipo_prod_salida );
            $centro_costo->prodTipoProductividads = $tipo_prod_salida;
            
            if(count($centro_costo->prodTipoProductividads)>0){
                array_push($this->centros_costos, $centro_costo);
            }
            
        }
        
               
        //OBTIENE PRODUCTIVIDADES CARGADAS
        
        $criteria_prod = new CDbCriteria();
        
        if($this->periodo_fin_id!=0){
            $criteria_prod->addBetweenCondition('periodo_id',$this->periodo_id,$this->periodo_fin_id);
            $periodo_actual_id = date("Ym");  
            $criteria_prod->compare('periodo_id <',$periodo_actual_id);
        }else{
            $criteria_prod->compare('periodo_id', $this->periodo_id);
        }
        
        if($this->area_id !=0){
            $criteria_prod->compare('area_id', $this->area_id);
        }
        
                
        //PARE EL RESTO DE PERFILES  muetra todo
        //1	Jefe Administrativo
	//6	Director
	//7	Administrador Sistema
        
        //LOS ACADEMICOS Y NO ACADEMICOS NO TIENE PERMISOS A ESTE MODULO
        
        //SI ES CONSULTA SOLO MUESTRA LAS PRODUCTIVIDADES APROBADAS
        if($this->es_consulta){
            $criteria_prod->compare('estado_id', ProdEstado::APROBADO);
        }
        
        //SI ES INGRESO
        if($this->es_ingreso){
            $criteria_prod->condition .= ' and exists (select 1 
                                                         from prod_tipo_productividad
                                                        where t.tipo_prod_id = prod_tipo_productividad.tipo_prod_id 
                                                          and prod_tipo_productividad.es_coordinacion = 0)';
            
          
        }
        
        //SE APLICA EL FILTRO A LA LISTA
        //if(count($this->filtro)>0){
        foreach($this->filtro as $filtro){
                       
            $key = $filtro->key;
            $value = $filtro->value;
            
            //FILTRO POR UNA PERSONA EN PARTICULAR
            if($key=="persona_id"){
                //echo "aplicar filtro:" . $value;
                $criteria_prod->compare('persona_id', $value);
            }
            
            //FILTRO PARA LAS PRODUCTIVIDADES QUE CARGO UNA PERSONA
            if($key=="persona_carga_id"){
                //echo "aplicar filtro:" . $value;
                $criteria_prod->compare('persona_carga_id', $value);
            }
            
            //FILTRO POR MONTO
            if($key=="monto"){
                //echo "aplicar filtro:" . $value;
                //$criteria_prod->compare("AES_DECRYPT(monto, '".Globals::KEY."') >", "convert(".$value.",SIGNED INTEGER)" );
                // $criteria_prod->compare("t.monto >", $value);
                $criteria_prod->condition .= " and AES_DECRYPT(monto, '".Globals::KEY."') > " . $value;
            }
            
        }


//        $criteria_prod->join = "JOIN prod_persona persona on persona.persona_id = t.persona_id";
//        $criteria_prod->order= 'persona.nombres ASC';
        
        
        $this->productividades = ProdProductividad::model()->findAll($criteria_prod);
   
        
        
        $criteria_personas = new CDbCriteria();

        if($this->es_academico){            
            $criteria_personas->compare('bo_academico', 1);
        }else{
            $criteria_personas->compare('bo_academico', 0);
        }
        
                
        $criteria_personas->order=$jqgrid->sidx . " " . $jqgrid->sord;// 'apellido_paterno ASC';
        
        
        if(count($this->filtro)){
            
            $personas = array();
        
            foreach($this->productividades  as $productividad){

                $existe = false;
                foreach($personas as $persona){
                    if($persona->persona_id == $productividad->persona->persona_id ){
                        $existe = true;
                    }
                }


                if(!$existe && 
                   (($this->es_academico && $productividad->persona->bo_academico == ProdPersona::TIPO_PERSONA_ACADEMICO) || 
                    (!$this->es_academico && $productividad->persona->bo_academico == ProdPersona::TIPO_PERSONA_NO_ACADEMICO) 
                   )
                  ){

                    array_push($personas, $productividad->persona);
                }
            }
        }else{
            $personas = ProdPersona::model()->findAll($criteria_personas);
        }
        
        
   
        
        //OBTIENE LOS LISTADOS DE SALIDA PARA PANTALLA
        if($this->detalle_por_tipo_productividad){
            $salida = $this->generaArregloSalidaDetalleTipoProductividad($personas);
        }else{
            $this->width=2000;
            $salida = $this->generaArregloSalidaDetalleCentroCosto($personas);
        }
        
        $salida_personas=$salida["salida_personas"];
        $totales_columna=$salida["totales_columna"];
        
//        echo "<pre>";
//        print_r($salida_personas);
//        echo "</pre>";
//        Yii::app()->end();
        
        
        return $salida_personas;
    }        
    
    public function generaArregloSalidaDetalleTipoProductividad($personas){
        $salida_personas = array();
        
        //recorro el listado de personas        
        foreach($personas as $persona){            
            $prod_persona = array();
            $estado_global_id=0;
            $estados = array();
            
            //filtro del total de productividades las productividades de la persona
            foreach($this->productividades  as $productividad){
                if($productividad->persona_id==$persona->persona_id){
                    array_push($prod_persona, $productividad);
                }
            }
            
            
            
//            if($persona->persona_id==10){
//                echo "--------------------------------<br>";
//                foreach($prod_persona as $productividad){   
//                    echo $productividad->periodo_id . "-". $productividad->tipo_prod_id . "-". $productividad->monto . "<br>";
//                }
//                echo "--------------------------------<br>";
//            }
            
            
            //se carga la productividad de las personas
            //en el formato de salida
            $detalle_productividad = array();
            
            
            foreach($this->centros_costos as $centro_costo){
                //echo $centro_costo . ":" . count($centro_costo->prodTipoProductividads)."<br>";
                $tipo_prod_salida = array();
                
                
                foreach($centro_costo->prodTipoProductividads as $tipo_productividad){
                    
                    if(($tipo_productividad->acadamico && $this->es_academico) ||
                       ($tipo_productividad->no_academico && !$this->es_academico)){  
                        
                        $monto=0;                        
                        $estado_id=ProdEstado::PENDIENTE;
                        
                        $observaciones = "";
                        foreach($prod_persona as $productividad){     
                            
                            //si existe la prodcutvidad para el tipo se carga el monto
                            if(($productividad->tipo_prod_id==$tipo_productividad->tipo_prod_id) &&
                               ($productividad->cc_id == $centro_costo->cc_id )     
                              ){
                                                            
                                $monto = $monto + $productividad->monto;
                                $estado_id= $productividad->estado_id;   
                                
                                if(!in_array($estado_id, $estados)){
                                    array_push($estados, $estado_id);
                                }
    
                                $observaciones = "Centro Costo: " . $productividad->cc .  "\nTipo Productividad: " . $productividad->tipoProd . "\nMonto: $" . number_format($productividad->monto,0) . "\nObservaciones: " . $productividad->observaciones ."\n";
                                
                                
                            }
                            
                                                
                        }
                        
                        

                        //se genera el registro salida
                        $salida = array("cc_id"=>$centro_costo->cc_id,
                                        "tipo_prod_id"=>$tipo_productividad->tipo_prod_id,
                                        "observaciones"=>$observaciones,
                                        "monto"=>$monto,
                                        "estado_id"=>$estado_id,
                                        
                                      );
                        //se carga al listado
                        array_push($detalle_productividad, $salida);   
                        
                        //totales columna 
                        
                        
                        
                    }
//                    echo "-----------------";
                }
            }
            //FIN RECORRE PROD
            
            if(in_array(ProdEstado::PENDIENTE, $estados)){
                $estado_global_id=  ProdEstado::PENDIENTE;
            }
            
            if(!in_array(ProdEstado::PENDIENTE, $estados) && in_array(ProdEstado::APROBADO_ENCARGADO, $estados)  ){
                $estado_global_id=ProdEstado::APROBADO_ENCARGADO;
            }
            
            if(in_array(ProdEstado::APROBADO, $estados) && !in_array(ProdEstado::PENDIENTE, $estados)&& in_array(ProdEstado::APROBADO_ENCARGADO, $estados)){
                $estado_global_id=ProdEstado::APROBADO;
            }
            
 
            //SI ES VALIDACION O INGRESO SOLO MUESTRO LOS QUE TIENEN DATOS
            if(!$this->es_consulta){
                $total_persona=0;
                foreach($detalle_productividad as $detalle){
                    $total_persona = $total_persona + $detalle["monto"];
                }
            }else{
                //SI ES CONSULTA MUESTRA TODO EL LISTADO
                //INDEPENDIENTE SI TIENEN O NO PRODUCTIVIDADES
                $total_persona=1;
            }
 
            if($total_persona>0){
            
                $array_persona_formateado = array("persona"=>$persona, 
                                                  "listado_productividades"=>$detalle_productividad,
                                                  "estado_global_id"=>$estado_global_id);
                array_push($salida_personas, $array_persona_formateado);
            }
            
        }
        //FIN RECORRE PERSONAS
        
        
//        //OBTIENE TOTALES
//        $totales_columna = array();
//
//        foreach($this->centros_costos as $centro_costo){
//            //echo $centro_costo . ":" . count($centro_costo->prodTipoProductividads)."<br>";
//            $tipo_prod_salida = array();                
//
//
//            foreach($centro_costo->prodTipoProductividads as $tipo_productividad){
//                if(($tipo_productividad->acadamico && $this->es_academico) ||
//                   ($tipo_productividad->no_academico && !$this->es_academico)){  
//
//                    $monto_total_columna=0;
//                    foreach($salida_personas as $salida){  
////                        $prod_persona = array();
//
////                        foreach($this->productividades  as $productividad){
////                            if($productividad->persona_id==$persona->persona_id){
////                                array_push($prod_persona, $productividad);
////                            }
////                        }
//
//                        foreach($salida["listado_productividades"] as $productividad){ 
//                            //si existe la prodcutvidad para el tipo se carga el monto
//                            if(($productividad["tipo_prod_id"]==$tipo_productividad->tipo_prod_id) &&
//                               ($productividad["cc_id"] == $centro_costo->cc_id )     
//                              ){
//                                $monto_total_columna = $monto_total_columna + $productividad["monto"];                                
//                            }
//                       }
//                   }
//
//                   array_push($totales_columna, $monto_total_columna);
//                }
//            }
//        }
      
        
        return array("salida_personas"=>$salida_personas,"totales_columna"=>array());
    }
    
    
    public function generaArregloSalidaDetalleCentroCosto($personas){
        $salida_personas = array();
        $rol_id = Yii::app()->user->rolId;
        //recorro el listado de personas        
        foreach($personas as $persona){            
            $prod_persona = array();
            $estado_global_id=0;
            $estados = array();
            
            //filtro del total de productividades las productividades de la persona
            foreach($this->productividades  as $productividad){
                if($productividad->persona_id==$persona->persona_id){
                    array_push($prod_persona, $productividad);
                }
            }
            
            
            
//            if($persona->persona_id==10){
//                echo "--------------------------------<br>";
//                foreach($prod_persona as $productividad){   
//                    echo $productividad->periodo_id . "-". $productividad->tipo_prod_id . "-". $productividad->monto . "<br>";
//                }
//                echo "--------------------------------<br>";
//            }
            
            
            //se carga la productividad de las personas
            //en el formato de salida
            $detalle_productividad = array();
            
            
            foreach($this->centros_costos as $centro_costo){
                //echo $centro_costo . ":" . count($centro_costo->prodTipoProductividads)."<br>";
                $tipo_prod_salida = array();


                $monto=0;                        
                $estado_id=ProdEstado::PENDIENTE;
                
                $observaciones ="";

                foreach($prod_persona as $productividad){     

                    //si existe la prodcutvidad para el tipo se carga el monto
                    if(($productividad->cc_id == $centro_costo->cc_id )     
                      ){

                        $monto = $monto + $productividad->monto;
                        $estado_id= $productividad->estado_id;   

                        if(!in_array($estado_id, $estados)){
                            array_push($estados, $estado_id);
                        }


                        
                        


                        $observaciones = $observaciones . "Centro Costo: " . $productividad->cc .  "\nTipo Productividad: " . $productividad->tipoProd . "\nMonto: $" .number_format($productividad->monto,0)."\nObservaciones: " . $productividad->observaciones . "\n\n";
                    }


                }



                //se genera el registro salida
                $salida = array("cc_id"=>$centro_costo->cc_id,
                                "monto"=>$monto,
                                "estado_id"=>$estado_id,
                                "observaciones"=>$observaciones,
                              );
                //se carga al listado
                array_push($detalle_productividad, $salida);   

                //totales columna 

            }
            //FIN RECORRE PROD
            
            
            
//            print_r($estados);
            
            if(in_array(ProdEstado::PENDIENTE, $estados)){
                $estado_global_id=  ProdEstado::PENDIENTE;
            }
            
            if(!in_array(ProdEstado::PENDIENTE, $estados) && in_array(ProdEstado::APROBADO_ENCARGADO, $estados)  ){
                $estado_global_id=ProdEstado::APROBADO_ENCARGADO;
            }
            
            if(in_array(ProdEstado::APROBADO, $estados) && !in_array(ProdEstado::PENDIENTE, $estados)&& !in_array(ProdEstado::APROBADO_ENCARGADO, $estados)){
                $estado_global_id=ProdEstado::APROBADO;
            }
            
            $total_persona=0;
            foreach($detalle_productividad as $detalle){
                $total_persona = $total_persona + $detalle["monto"];
            }

            
            //SI ES VALIDACION O INGRESO SOLO MUESTRO LOS QUE TIENEN DATOS
            if($this->es_consulta){
                //SI ES CIONSULTA DE ENCARGADO Y VALIDADOR MUESTRO SOLO LOS MAYORES A CERO
                if($this->es_consulta && ($rol_id == ProdRol::ROL_ENCARGADO || $rol_id == ProdRol::ROL_INGRESO_DATOS) ){
                   $total_persona = $total_persona; 
                }else{
                    if(!$this->mostrar_positivos){
                        $total_persona=1;
                    }
                }
            }
            
            
 
            if($total_persona>0){
                $array_persona_formateado = array("persona"=>$persona, 
                                                  "listado_productividades"=>$detalle_productividad,
                                                  "estado_global_id"=>$estado_global_id);

                array_push($salida_personas, $array_persona_formateado);
            }
        }
        //FIN RECORRE PERSONAS
        
        
//        //OBTIENE TOTALES
//        $totales_columna = array();
//
//        foreach($this->centros_costos as $centro_costo){
//            //echo $centro_costo . ":" . count($centro_costo->prodTipoProductividads)."<br>";
//            $tipo_prod_salida = array();                
//
//            $monto_total_columna=0;
//            foreach($salida_personas as $salida){  
//
//                foreach($salida["listado_productividades"] as $productividad){ 
//                    //si existe la prodcutvidad para el tipo se carga el monto
//                    if(($productividad["cc_id"] == $centro_costo->cc_id )     
//                      ){
//                        $monto_total_columna = $monto_total_columna + $productividad["monto"];                                
//                    }
//               }
//           }
//
//           array_push($totales_columna, $monto_total_columna);
//        }
        
      
        
        return array("salida_personas"=>$salida_personas,"totales_columna"=>array());
    }

    
    public function actionExportarExcel(){
        
        $form = Yii::app()->getSession()->get('form_exportar');
  
        $periodos = ProdPeriodo::getPeriodosCargados();            

        foreach($periodos as $periodo){
            $periodo_inicio_id = $periodo->periodo_id;
            break;
        }

        $valido=false;
        if(!is_null($form))
        {
            $valido=true;                
            $periodo_inicio_id = $form->periodo_id;
            $periodo_fin_id = 0;
            
            if($form->tipo_personas==1){
                $this->es_academico=true;
            }else{
                $this->es_academico=false;
            }

        }
        
        if($form instanceof ConsultasForm){
            $tipo_consulta_id = $form->tipo_consulta;
            
            if($tipo_consulta_id==1){
                $periodo_inicio_id = $form->anio_id*100 + 1;
                $periodo_fin_id = $form->anio_id*100 + 12;
            }
            
            if($tipo_consulta_id==2){
                $periodo_inicio_id = $form->anio_mes_id*100 + $form->mes_id;
                $periodo_fin_id = 0;
            }
            
            if($tipo_consulta_id==3){
                $periodo_inicio_id = $form->anio_desde_id*100 + $form->mes_desde_id;
                $periodo_fin_id = $form->anio_hasta_id*100 + $form->mes_hasta_id;
            }
        }

        $this->periodo_id = $periodo_inicio_id;
        $this->periodo_fin_id = $periodo_fin_id;
        $this->detalle_por_tipo_productividad = true;
        $this->es_consulta = true;
        
        $jqgrid = new JqgridParameterForm;       
        $jqgrid->sidx = "apellido_paterno";
        $jqgrid->sord = "asc";
        
        $rol_id = Yii::app()->user->rolId;

        if($rol_id == ProdRol::ROL_ENCARGADO || $rol_id == ProdRol::ROL_INGRESO_DATOS){
                        
            //FILTRAR POR AREA
            $this->area_id = Yii::app()->user->areaId;
        }
        
        $salida_personas = $this->preparaDatos($jqgrid);
            
        //OBTIENE TOTALES
        $totales_columna = array();

        foreach($this->centros_costos as $centro_costo){
            //echo $centro_costo . ":" . count($centro_costo->prodTipoProductividads)."<br>";
            $tipo_prod_salida = array();                


            foreach($centro_costo->prodTipoProductividads as $tipo_productividad){
                if(($tipo_productividad->acadamico && $this->es_academico) ||
                   ($tipo_productividad->no_academico && !$this->es_academico)){  

                    $monto_total_columna=0;
                    foreach($salida_personas as $salida){  
//                        $prod_persona = array();

//                        foreach($this->productividades  as $productividad){
//                            if($productividad->persona_id==$persona->persona_id){
//                                array_push($prod_persona, $productividad);
//                            }
//                        }

                        foreach($salida["listado_productividades"] as $productividad){ 
                            //si existe la prodcutvidad para el tipo se carga el monto
                            if(($productividad["tipo_prod_id"]==$tipo_productividad->tipo_prod_id) &&
                               ($productividad["cc_id"] == $centro_costo->cc_id )     
                              ){
                                $monto_total_columna = $monto_total_columna + $productividad["monto"];                                
                            }
                       }
                   }

                   array_push($totales_columna, $monto_total_columna);
                }
            }
        }            
            
        $objPHPExcel = new PHPExcel();

        $styleHeaderArray = array(
            'borders' => array(
                'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb'=>'E1E0F7'),
            ),
            'font' => array(
                'bold' => true,
                'size' => 8
            )   ,
            'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )          
        );

        $styleHeaderCCArray = array(
            'borders' => array(
                            'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                            ) 
                        ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb'=>'E1E0F7'),
            ),
            'font' => array(
                'bold' => true,
                'size' => 8
            )            ,
            'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
        );

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'font' => array(
                'size' => 8
            )  
        );

        $styleTotalArray = array(
            'borders' => array(
                'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'font' => array(
                'bold' => true,
                'size' => 8
            )  
        );

        $currencyFormat =  iconv("ISO-8859-1", "UTF-8", '$ #,##0');


        $objPHPExcel->setActiveSheetIndex(0);

        $subtitulo = "Período Consulta" ;

        if($this->periodo_id!=0 && $this->periodo_fin_id!=0){
            $periodo_inicio = ProdPeriodo::getPeriodo($this->periodo_id);  
            $periodo_termino = ProdPeriodo::getPeriodo($this->periodo_fin_id);  

            $subtitulo = $subtitulo . " desde " . $periodo_inicio->nombrePeriodo . " a " . $periodo_termino->nombrePeriodo;
        }else{
            if($this->periodo_id!=0){
                $periodo_inicio = ProdPeriodo::getPeriodo($this->periodo_id);  
                $subtitulo = $subtitulo . " " . $periodo_inicio->nombrePeriodo;
            }

        }

        if($this->es_academico){
            $titulo = 'Resultados Academicos';
            $titulo1 = "Consulta Productividades Globales Academicos";
        }else{
            $titulo = 'Resultados No Academicos';
            $titulo1 = "Consulta Productividades Globales No Academicos";
        }
        $objPHPExcel->getActiveSheet()->setTitle($titulo);

        $row = 2; 

        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$titulo1 );

        $row++;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$subtitulo );
        $row++;
        $row++;

        $col=0;
        if($this->es_academico){
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "ACADEMICO");  
            //$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
            $col++;
        }else{
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "NO ACADEMICO");  
            //$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
            $col++;
        }


        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Centro Costos");  
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
        $col++;
        foreach($this->centros_costos as $centro_costo){
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $centro_costo->nombre_completo_cc);  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);

            $cuenta = count($centro_costo->prodTipoProductividads);

            if($cuenta>1){
                $merge = $this->getRangoAscii($col,$cuenta , $row);                        
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells($merge);
                $objPHPExcel->getActiveSheet()->getStyle($merge)->applyFromArray($styleHeaderArray);
            }

            $col=$col+$cuenta;
            //$col++;
        }

        $row++;

        $col=0;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Persona");  
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
        $col++;

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Total");  
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);
        $col++;


        $tipo_prod_ant= new ProdTipoProductividad();
        foreach($this->centros_costos as $centro_costo){        

            foreach($centro_costo->prodTipoProductividads as $tipo_productividad){
                //if($tipo_productividad->tipo_prod_id!=$tipo_prod_ant->tipo_prod_id){
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $tipo_productividad->descripcion);  
                    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleHeaderArray);


                    //$cuenta = count($tipo_productividad->prodCentroCostos);
                    //$merge = $this->getRangoAscii($col,$cuenta , $row);
                    //$objPHPExcel->setActiveSheetIndex(0)->mergeCells($merge);

                    $col++;


                //}
                //$tipo_prod_ant = $tipo_productividad;
            }
        }

        $row++;

        foreach($salida_personas as $persona){

            $col=0;

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,  $persona["persona"]->nombre_completo());  
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
            $col++;

            $total=0;
            foreach($persona["listado_productividades"] as $detalle_productividad){
                $total=$total+$detalle_productividad["monto"];
            }

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$total);  
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            $col++;

            foreach($persona["listado_productividades"] as $detalle_productividad){
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$detalle_productividad["monto"]);  
                $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
                $col++;   
            }

            $row++;


        }

        $col=0;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, "Totales");  
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
        $col++;

        $total_totales=0;
        foreach($totales_columna as $total){
             $total_totales = $total_totales + $total;

        }

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $total_totales);  
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
        $col++;

        foreach($totales_columna as $total){

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $total);  
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($styleTotalArray);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            $col++;

        }


        ob_end_clean();
        ob_start();

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="consultas_productividades_'.$this->periodo_id.'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

        Yii::app()->end();



    }
    
    public function getRangoAscii($col_desde,$cuenta,$row){
                    
        //LETRA ASSCCI
        $col_desde = $col_desde + 65;
        $col_desde_ascii = chr($col_desde);
        
        $col_hasta = $col_desde + ($cuenta-1);
        $col_hasta_ascii = chr($col_hasta);
        
        if($col_desde>90){
            $col_desde = ($col_desde - 91) + 65;            
            $col_desde_ascii = chr(65) . chr($col_desde);
        }
        
        if($col_hasta>90){
            $col_hasta = ($col_hasta - 91) + 65;            
            $col_hasta_ascii = chr(65) . chr($col_hasta);
        }
        
        
        return $col_desde_ascii .''.$row.':'. $col_hasta_ascii .''. $row;
            
    }
    
    
    
}

?>
