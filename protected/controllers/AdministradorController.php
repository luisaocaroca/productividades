<?php
/**
 * Controlador del módulo de administración (mantenedores).
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.controller
 */
class AdministradorController extends SecureController
{
        
        /**
        * Control de acceso del módulo
        * En este caso solo el usuario administrador puede entrar al módulo
        * 
        * @return array permisos de usuario al módulo
        */
	public function accessRules()
        {
            //SOLO EL USUARIO ADMINISTRADOR PUEDE ACCEDER
            return array(
                array('allow',
                      'roles' => array('admin'),
                ),                
                array('deny',
                    'users'=>array('*'),
                ),
            );
        }   
        
        /**
        * Index del módulo, se redirigue a actionMantenedores
        * 
        */
        public function actionIndex(){
            $this->actionMantenedores();
        }
        
        /**
        * Menu principal del módulo
        * @return string despliega la vista administrador/mantenedores 
        * 
        */    
	public function actionMantenedores()
	{
            
            $this->menu=array(
                array('label'=>Yii::t('app', 'Áreas')               , 'url'=>array('/prodArea/admin')),
                array('label'=>Yii::t('app', 'Centros Costo')       , 'url'=>array('/prodCentroCosto/admin')),
                array('label'=>Yii::t('app', 'Roles')               , 'url'=>array('/prodRol/admin')),
                array('label'=>Yii::t('app', 'Menu')                , 'url'=>array('/prodMenu/admin')),
                array('label'=>Yii::t('app', 'Sub Menu')            , 'url'=>array('/prodSubMenu/admin')),
                array('label'=>Yii::t('app', 'Tipo Productividad')  , 'url'=>array('/prodTipoProductividad/admin')),
                

            );

            $this->render('mantenedores');
	}

	
}