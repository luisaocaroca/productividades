<?php
/**
 * Backup
 * 
 * Yii module to backup, restore databse
 * 
 * @version 1.0
 * @author Shiv Charan Panjeta <shiv@toxsl.com> <shivcharan.panjeta@outlook.com>
 */
class BackupModule extends CWebModule
{
	public $path = null;
        
        /**
        * Inicializa el modulo respaldo
        * 
        * @return array includes
        */        
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'backup.models.*',
			'backup.components.*',
		));
	}

        /**
         * Se ejecuta antes de ejecutar una accion en un controlador del modulo
         * 
         * @param string $controller Nombre del controller
         * @param string $action Nombre de la accion
         * @return bool 
        */            
	public function beforeControllerAction($controller,$action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
