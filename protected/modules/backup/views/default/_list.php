<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'install-grid',
	'dataProvider' => $dataProvider,
	'columns' => array(
                array(
                    'header' => 'Nombre',
                    'value' => '$data["name"]',
                ),
                array(
                    'header' => 'Tamaño',
                    'value' => '$data["size"]',
                ),
                array(
                    'header' => 'Fecha Creación',
                    'value' => 'Yii::app()->dateFormatter->formatDateTime($data["create_time"],"full")',
                ),
		array(
			'class' => 'CButtonColumn',
			'template' => ' {restore}',
			  'buttons'=>array
			    (
			        'restore' => array
			        (
                                    'label'=>'Recuperar',
                                    'imageUrl'=>Yii::app()->baseUrl.'/images/Site-Backup-And-Restore-32.png',
                                    'options'=>array('title'=>'Recuperar Respaldo','width'=>'20px'),
			            'url'=>'Yii::app()->createUrl("backup/default/restore", array("file"=>$data["name"]))',
                                    'click'=>'function(){ if(window.confirm("¿Esta seguro que desea recuperar el respaldo, los datos actuales serán reemplazados?")){return true;}else{return false;} }',

				),
                              
			    ),	
                    

		),
                array(
			'class' => 'CButtonColumn',
			'template' => ' {download} ',
			  'buttons'=>array
			    (
			        'download' => array
			        (
                                    'label'=>'Dowload',
                                    'imageUrl'=>Yii::app()->baseUrl.'/images/Download-32.png',
			            'url'=>'Yii::app()->createUrl("backup/default/download", array("file"=>$data["name"]))',
			        ),

                              
			    ),	
                    

		),
		array(
			'class' => 'CButtonColumn',
			'template' => '{delete}',
			  'buttons'=>array
			    (

			        'delete' => array
			        (
                                    'imageUrl'=>Yii::app()->baseUrl.'/images/Delete_32.png',
			            'url'=>'Yii::app()->createUrl("backup/default/delete", array("file"=>$data["name"]))',
			        ),
                              
			    ),		
		),
	),
)); ?>