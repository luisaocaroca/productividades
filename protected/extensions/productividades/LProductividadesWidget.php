<?php

/**
 * Extension que imprime el listado de productividade de las personas en pantalla
 * con el formato de la planilla Excel que usaban en proceso de control
 * de las productividades
 * 
 * La información en pantalla varía dependiendo de si se está consultando, ingresando o
 * validando las productividades.
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.extensions.productividades
 */
class LProductividadesWidget extends CWidget{
    
    public $tipos_productividades=array();
    public $centros_costos=array();
    public $es_academico=true;
    public $productividades=array();
    public $filtro = array();
    public $periodo_id = 0;
    public $periodo_fin_id = 0;
    public $es_consulta=false;
    public $es_validacion = false;
    public $es_validacion_encargado= false;
    public $es_ingreso = false;
    public $es_informe = false;
    public $model;
    public $width=1000;
    public $exportar_excel = false;
    public $periodo_cerrado = false;
    public $detalle_por_tipo_productividad = false;
    public $area_id = 0;
    public $mostrar_positivos =0;

    
    public $columnas_detalles = array();
    public $columnas_nombres = array();
    public $columnas_cabeceras = array();
    
    public $data_grid = "";
    
    public $form;
    public $titulo = "Listado Productividades";
    
    public function init()
    {
        
        //print_r($this->filtro);
        //echo "periodo:" . $this->periodo_id . " : <br>";
        $this->centros_costos=array();
                
        $persona_id = Yii::app()->user->personaId;
        $rol_id = Yii::app()->user->rolId;
        $centros_costos = array();
        
        
        if($this->es_consulta || $this->es_validacion ){
            if($this->es_consulta && ($rol_id == ProdRol::ROL_ENCARGADO || $rol_id == ProdRol::ROL_INGRESO_DATOS) ){
                
                $listado_cc_tipos_prod_rol = ProdAreaTipoPodCc::getCentrosCostosTiposProductividadesArea($this->area_id, true);

                foreach($listado_cc_tipos_prod_rol as $cc_tipos_prod_rol){
                    if(!in_array($cc_tipos_prod_rol->cc,$centros_costos)){
                        array_push($centros_costos, $cc_tipos_prod_rol->cc);
                    }
                }
            }else{
                $centros_costos = ProdCentroCosto::model()->findAll();
            }
        }else{
            //SI ES INGRESO
                                    
            $listado_cc_tipos_prod_rol = ProdAreaTipoPodCc::getCentrosCostosTiposProductividadesArea($this->area_id, true);

            foreach($listado_cc_tipos_prod_rol as $cc_tipos_prod_rol){
                if(!in_array($cc_tipos_prod_rol->cc,$centros_costos)){
                    array_push($centros_costos, $cc_tipos_prod_rol->cc);
                }
            }

        }
        
        foreach($centros_costos as $centro_costo){
            $tipo_prod_salida = array();
            $relacionesCentroCostoTipoProductividad = ProdCentroCosto::getTiposProducividades($centro_costo->cc_id,$this->periodo_id, $this->periodo_fin_id,$this->es_academico);
            
            foreach($relacionesCentroCostoTipoProductividad as $tipo_productividad){
                
                array_push($tipo_prod_salida, $tipo_productividad);
            }
            
            sort($tipo_prod_salida );
            $centro_costo->prodTipoProductividads = $tipo_prod_salida;
            
            if(count($centro_costo->prodTipoProductividads)>0){
                array_push($this->centros_costos, $centro_costo);
            }
            
        }
        
        //FILTRO APLICADO
        $this->filtro = array();
        $mostrar_filtro=false;
        $detalle_filtro="";
            
        if(isset($this->model)){

            if(isset($this->model->persona_id) && !empty($this->model->persona_id)){
                array_push($this->filtro,array("key"=>"persona_id","value"=>$this->model->persona_id));
                
                $personaFiltrada = ProdPersona::getPersona($this->model->persona_id);
                $detalle_filtro = " por persona " .  $personaFiltrada;
                $mostrar_filtro=true;
            }
            
            if(isset($this->model->persona_carga_id) && !empty($this->model->persona_carga_id)){
                array_push($this->filtro,array("key"=>"persona_carga_id","value"=>$this->model->persona_carga_id));
                $personaFiltrada = ProdPersona::getPersona($this->model->persona_carga_id);
                $detalle_filtro = " por persona ingresa " .  $personaFiltrada;
                
                $mostrar_filtro=true;
            }
            
            if(isset($this->model->monto) && $this->model->monto !=""){
                array_push($this->filtro,array("key"=>"monto","value"=>$this->model->monto));
                $detalle_filtro = " monto mayor que $ " . $this->model->monto;
                $mostrar_filtro=true;
            }
        }
        //FIN FILTRO        
        
        
        $this->llena_columnas_detalle();
        
        $personas_cargan = ProdProductividad::getPersonasCargan($this->periodo_id,$this->periodo_fin_id);
        
        $data = array(
                        "persona_id"=>$persona_id,                       
                        'model'=>$this->model,
                        'form'=>$this->form,
                        "personas_cargan"=>$personas_cargan,
                        "mostrar_filtro"=>$mostrar_filtro,
                        "titulo"=>$this->titulo,
                        "detalle_filtro"=>$detalle_filtro
                       );   

           //echo $this->viewPath;

        $this->render("listadoProductividades",$data);
    }
 
    public function run()
    {
        
    }
    
    
    
    
    function llena_columnas_detalle(){
        
        
        if($this->es_academico){
            $id_link = "aprobar_todo_academico";
        }else{
            $id_link = "aprobar_todo_no_academico";
        }
        
        $hide_validar=true;
        if($this->es_validacion || $this->es_validacion_encargado){
            $hide_validar=false;
        }
        
        
        if(!$this->detalle_por_tipo_productividad){
            $titulo_personas="<br>Personas<br>";
        }else{
            $titulo_personas="<br>Personas<br><br>";
        }
        
 
        
        $imghtml=CHtml::image(Yii::app()->baseUrl."/images/Ok-32.png","Aprobar Todo",array("id"=>$id_link,"width"=>20));
        
        $validar =$imghtml;// "<a id='$id_link' href='$id_link') alt='Aprobar Todo'>" . $imghtml . "</a>";
        
//        $validar = CHtml::link(
//                                $imghtml,
//                                "",
//                                array(  'id'=>$id_link,
//                                        'style'=>'cursor: pointer; text-decoration: underline;',
//                                )
//                        );
        
        $this->columnas_nombres = array('id','Ver',$validar,'Apellido Paterno', 'Apellido Materno','Nombres','Total','Total Number');
        $this->columnas_cabeceras = array( array(
                                                    'startColumnName'=>'apellido_paterno',
                                                    'numberOfColumns'=>3,
                                                    'titleText'=>$titulo_personas
                                                )
                                            );
        
        array_push($this->columnas_detalles, array(
                   'name'=>'id',
                   'index'=>'id',
                   'width'=>'40',
                   'hidden'=>true,
                   'sortable'=>false,
                   'search'=>false,
                   'frozen'=>true,
                    'align'=>'center',
                   'classes'=>'tregistropersona .ui-state-highlight'
                   ));
        
        array_push($this->columnas_detalles, array(
                   'name'=>'detalle',
                   'index'=>'detalle',
                   'width'=>'40',
                   'hidden'=>false,
                   'sortable'=>false,
                   'search'=>false,
                   'frozen'=>true,
                    'align'=>'center',
                  // 'classes'=>'tregistropersona .ui-state-highlight'
                   ));
        
        array_push($this->columnas_detalles, array(
                   'name'=>'validar',
                   'index'=>'validar',
                   'width'=>'40',
                   'hidden'=>$hide_validar,
                   'sortable'=>false,
                   'search'=>false,
                   'frozen'=>true,
                   'align'=>'center',
                   'classes'=>'tregistropersona tregistroselected'
                   ));        
        
        array_push($this->columnas_detalles, array(
                   'name'=>'apellido_paterno',
                   'index'=>'apellido_paterno',
                   'width'=>'130',
                   'hidden'=>false,
                   'key'=>true,
                   'sortable'=>true,
                   'search'=>false,
                   'frozen'=>true,
                   'classes'=>'tregistropersona tregistroselected',
                   ));
        
        array_push($this->columnas_detalles, array(
                   'name'=>'apellido_materno',
                   'index'=>'apellido_materno',
                   'width'=>'130',
                   'sortable'=>true,
                   'search'=>false,
                   'frozen'=>true,
                  'classes'=>'tregistropersona tregistroselected'
               ));
        
        array_push($this->columnas_detalles, array(
                   'name'=>'nombres',
                   'index'=>'nombres',
                   'width'=>'130',
                   'sortable'=>true,
                   'search'=>false,
                   'frozen'=>true,
                   'classes'=>'tregistropersona tregistroselected'
               ));
        
        array_push($this->columnas_detalles, array(
                   'name'=>'total',
                   'index'=>'total',
                   'width'=>'100',
                   'sortable'=>false,
                   'search'=>false,
                   'frozen'=>false,
                   'align'=>'right',
                   'classes'=>'tregistrototal'
               ));
        
        array_push($this->columnas_detalles, array(
                   'name'=>'total_number',
                   'index'=>'total_number',
                   'width'=>'100',
                   'sortable'=>false,
                   'search'=>false,
                   'frozen'=>false,
                   'hidden'=>true
               ));
        
        $largo_minimo = 90;
        
        if(!$this->detalle_por_tipo_productividad){
        
            $i=4;
            foreach($this->centros_costos as $centro_costo){

                $nombres = explode(" ", $centro_costo->nombre_cc) ;
                
                $nombre_salto = "";
                $i=0;
                foreach($nombres as $nombre){
                    if($i==0){
                        $nombre_salto .= $nombre."<br>";
                    }else{
                        $nombre_salto .= ' ' . $nombre;
                    }
                    
                    $i++;
                }
                
                $nombres = explode("<br>", $nombre_salto) ;
                $i=0;
                foreach($nombres as $nombre){
                    if($i!=0){
                        $largo1 = strlen($nombre);
                    }else{
                        $largo2 = strlen($nombre);
                    }
                    
                    $i++;
                }
                
                if($largo1>$largo2){
                    $largo = $largo1*9;
                }else{
                    $largo = $largo2*9;
                }
                
                if($largo<$largo_minimo){
                    $largo = $largo_minimo;
                }
                
                
                array_push($this->columnas_detalles, array(
                       'name'=>"cc_" . $centro_costo->cc_id,
                       'index'=>"cc_" . $centro_costo->cc_id,
                       'width'=>$largo,
                       'sortable'=>false,
                       'search'=>false,
                       'align'=>'right',
                    //   'classes'=>'tregistro'
                   ));

                array_push($this->columnas_detalles, array(
                       'name'=>"cc_" . $centro_costo->cc_id . "_number",
                       'index'=>"cc_" . $centro_costo->cc_id. "_number",
                       'width'=>$largo,
                       'sortable'=>false,
                       'search'=>false,
                       'hidden'=>true
                   ));
                
                
                array_push($this->columnas_nombres, $centro_costo->cc_id);
                array_push($this->columnas_nombres, $centro_costo->cc_id. "_number");

                
                
                array_push($this->columnas_cabeceras, array(
                                                        'startColumnName'=>"cc_" . $centro_costo->cc_id,
                                                        'numberOfColumns'=>1,
                                                        'titleText'=>$nombre_salto,
                                                        'align'=>'center'
                                                    ));

                $i++;
            }
        
        }else{
            //LLENA DETALLE POR TIPO PROD
            $i=4;
            
            $cc_id_ant=0;
            foreach($this->centros_costos as $centro_costo){
                $nombres = explode(" ", $centro_costo->nombre_cc) ;
                
                $nombre_salto = "";
                $i=0;
                foreach($nombres as $nombre){
                    if($i==0){
                        $nombre_salto .= $nombre."<br>";
                    }else{
                        $nombre_salto .= ' ' . $nombre;
                    }
                    
                    $i++;
                }
                
                $nombres = explode("<br>", $nombre_salto) ;
                $i=0;
                foreach($nombres as $nombre){
                    if($i!=0){
                        $largo1 = strlen($nombre);
                    }else{
                        $largo2 = strlen($nombre);
                    }
                    
                    $i++;
                }
                
                if($largo1>$largo2){
                    $largoCC = $largo1*9;
                }else{
                    $largoCC = $largo2*9;
                }
                
                $numeroTP = count($centro_costo->prodTipoProductividads);
                
                $largoTipos = 0;
                foreach($centro_costo->prodTipoProductividads as $tipo_productividad){
                    $largoTipos = $largoTipos + strlen($tipo_productividad->descripcion)*9; 
                }
                
                $usa_largo_cc = false;
                if($largoCC>$largoTipos){
                    $usa_largo_cc = true;
                }
                
                
                foreach($centro_costo->prodTipoProductividads as $tipo_productividad){
                    
                    
                    $largoTp = strlen($tipo_productividad->descripcion)*9; 
                            
                    if($usa_largo_cc){
                        $largo = $largoCC/$numeroTP;
                    }else{
                        $largo = $largoTp;
                    }
                    
                    if($largo<$largo_minimo){
                        $largo = $largo_minimo;
                    }
                    
                    
                    if($cc_id_ant!=$centro_costo->cc_id){
                        array_push($this->columnas_cabeceras, array(
                                                            'startColumnName'=>"cc_".$centro_costo->cc_id."_prod_" . $tipo_productividad->tipo_prod_id,
                                                            'numberOfColumns'=>count($centro_costo->prodTipoProductividads)*2,
                                                            'titleText'=>$centro_costo->cc_id . "<br>" . $nombre_salto  ,
                                                            'align'=>'center'
                                                        ));
                    }
                    

                            
                    array_push($this->columnas_detalles, array(
                       'name'=>"cc_".$centro_costo->cc_id."_prod_" . $tipo_productividad->tipo_prod_id,
                       'index'=>"cc_".$centro_costo->cc_id."_prod_" . $tipo_productividad->tipo_prod_id,
                       'width'=>$largo,
                       'sortable'=>false,
                       'search'=>false,
                       'align'=>'right',
                       'classes'=>'tregistro'
                    ));

                    array_push($this->columnas_detalles, array(
                       'name'=>"cc_".$centro_costo->cc_id."_prod_" . $tipo_productividad->tipo_prod_id . "_number",
                       'index'=>"cc_".$centro_costo->cc_id."_prod_" . $tipo_productividad->tipo_prod_id. "_number",
                       'width'=>'140',
                       'sortable'=>false,
                       'search'=>false,
                       'hidden'=>true
                    ));

                    array_push($this->columnas_nombres, $tipo_productividad->descripcion);
                    array_push($this->columnas_nombres, "cc_".$centro_costo->cc_id."_prod_" . $tipo_productividad->tipo_prod_id. "_number");                            
                    
                    $cc_id_ant = $centro_costo->cc_id;
                            
                    $i++;
                }
            }
        }
    }
    

    
    
}
?>


