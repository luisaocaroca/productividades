<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//echo $persona_id;
$ancho_pagina = Yii::app()->session['ancho_pagina']-140;

if(!$mostrar_filtro){
    Yii::app()->clientScript->registerScript('search', "
        $('.search-form').toggle();
        $('.search-button').click(function(){
                $('.search-form').toggle();
                return false;
        });
    ");
}
                
?>

<style>
/*    tr.highlight {
    background-color: red;
    }*/


.ui-th-column-header
{
  text-align: center;
}


.ui-state-highlight { background: #ffb703 !important; border-color:#ffb703 !important; background-color: #ffb703 !important; }
</style>

<script type="text/javascript">
/*<![CDATA[*/
jQuery(function($) {
    $("[data-toggle='tooltip']").tooltip();
    
    $("a").live("click",function(){
        
        var referencia= $(this).attr("href");
        var id = $(this).attr('id');  
        var name = $(this).attr('name');  
        
        //alert(referencia);
        
        if(name==="detalle_persona"){

            //alert(referencia);
            //alert(id);
            mostrarLoader();
            var data = JSON.parse(id);
            
            $('#modalDetalle').html("");

            jQuery.ajax({'type':'POST',
                         'data':data,
                         //'complete':afterAjax,
                         'url':referencia,
                         'cache':false,
                         'success':function(html){
                             $('#modalDetalle').html(html).modal();
                             //jQuery("#preview").html(html);
                             $('#loading-indicator').hide();
                         }

                        });
        
            return false;
        }
    
    });

    

  
    
            
});

//SE DEBE DEFINIR EN LA PAGINA PARA QUE APLIQUE LOS FILTROS DEL LISTADO
function actualizaTipoPersona(){
    mostrarLoader();
    <?php if($this->es_ingreso){?>
            $('#periodo_id').val();
    <?php }?>
    
    $('form#<?php echo $this->form->htmlOptions["id"];?>').submit();
}


function filtrar(){
    mostrarLoader();
    $('form#<?php echo $this->form->htmlOptions["id"];?>').submit();
}

function quitarfiltro(){
    mostrarLoader();
    //alert("quitarfiltro");
    $("#persona_id").val(null);
    $("#nombre_persona").val(null);
    $("#persona_carga_id").val(null);
    $("#monto").val(null);
    $('form#<?php echo $this->form->htmlOptions["id"];?>').submit();
}
/*]]>*/
</script>

<script>

$(document).ready(function(){
    
    $('#ValidacionForm_mostrar_detalle_tipo_productividad').change(function () {
            var name = $(this).val();
            var check = $(this).attr('checked');
            
            if(check){
                //alert("Chequeado");
                $('form#ValidacionForm').submit();
            }else{
                //alert("No Chequeado");   
                $('form#ValidacionForm').submit();
            }
            
        });
        
        $('#ValidacionForm_mostrar_positivos').change(function () {
            var name = $(this).val();
            var check = $(this).attr('checked');
            
            if(check){
                //alert("Chequeado");
                $('form#ValidacionForm').submit();
            }else{
                //alert("No Chequeado");   
                $('form#ValidacionForm').submit();
            }
            
        });
        
      
      
    $("#boton-academico").click(function() {
        mostrarLoader();
        <?php if($this->es_ingreso){?>
                $('#periodo_id').val();
        <?php }?>
            
        <?php if($this->es_ingreso){?>
                $("#ConsultasForm_area_id").val("<?php echo $this->area_id?>");
        <?php }?>
                
        <?php if($this->es_validacion_encargado){?>
                $("#ConsultasForm_area_id").val("<?php echo $this->area_id?>");
        <?php }?>        
            
        $("#tipo_personas").val("1");
        $('form#<?php echo $this->form->htmlOptions["id"];?>').submit();
    });
    
    $("#boton-noacademico").click(function() {
        mostrarLoader();
        <?php if($this->es_ingreso){?>
                $('#periodo_id').val();
        <?php }?>
            
        <?php if($this->es_ingreso){?>
                $("#ConsultasForm_area_id").val("<?php echo $this->area_id?>");
        <?php }?>
                
                
        <?php if($this->es_validacion_encargado){?>
                $("#ConsultasForm_area_id").val("<?php echo $this->area_id?>");
        <?php }?>   
                
            
        $("#tipo_personas").val("2");
        $('form#<?php echo $this->form->htmlOptions["id"];?>').submit();
    });

    //SE ACTIVA AL HACER CLICK EN LA COLUMNA "Validar" de la grilla JQUERY
    $("#ValidacionForm_validar_productividades").click(function() {
        var checked = $(this).is(':checked');
        
        $("input[type=checkbox]").each(function() { 

            if($(this).attr("name").indexOf("JqgridParameterForm")>=0){


                var str = $(this).attr("name");
                //alert(str);
                
                var n=str.split("[");
                var tipo = n[1].substr(0, n[1].length-1);

                var disabled = $(this).attr("disabled");
                
                if(!disabled){

                    if(checked){
                        $(this).attr('checked', true);
                    }else{
                        $(this).attr('checked', false);
                    }

                
                }
                //}
            }
        });
    });

        $("#productividades").jqGrid('navGrid','#pager',
                        {edit:false, add:false, del:false, search:false},
                        {}, {}, {}, 
                        { 	// search
                                sopt:['cn', 'eq', 'ne','le','ge','lt', 'gt', 'bw', 'ew'],
                                closeOnEscape: true, 
                                multipleSearch: false, 
                                closeAfterSearch: true,
                                overlay:true,
                                recreateFilter:true
                        }
            );

                
        $("#productividades").jqGrid('setGroupHeaders', {
          useColSpanStyle: false, 
          groupHeaders:	<?php  echo  json_encode($this->columnas_cabeceras) ?>
        }); 
        
        
        jQuery("#productividades").jqGrid('setFrozenColumns');
        jQuery("#productividades").triggerHandler("jqGridAfterGridComplete");    

    });
  
function clickedVal(object){

    var str = object.name;
    //alert(str);
    var n=str.split("[");
    var tipo= n[1].substr(0, n[1].length-1);
    var id = n[2].substr(0, n[2].length-1);
    
    $("#ValidacionForm_"+tipo+"_" + id + "_rechaza").removeAttr("checked");
        
    
}

function carga_totales(){
    //alert('loadComplete');
    var grid = $('#productividades');
    
    var colSum = grid.jqGrid('getCol', 'total_number', false, 'sum');
    colSum = number_format(colSum);
    //alert(colSum);
    grid.jqGrid('footerData', 'set', { total: "$ " + colSum });
    
    <?php
        if($this->detalle_por_tipo_productividad){
            foreach($this->centros_costos as $centro_costo){
                foreach($centro_costo->prodTipoProductividads as $tipo_productividad){
                ?>
                  
                  colSum = grid.jqGrid('getCol', '<?php echo "cc_".$centro_costo->cc_id."_prod_" . $tipo_productividad->tipo_prod_id . "_number"?>', false, 'sum');
                  colSum = number_format(colSum);
                  grid.jqGrid('footerData', 'set', { <?php echo "cc_".$centro_costo->cc_id."_prod_" . $tipo_productividad->tipo_prod_id?>: "$ " + colSum });
                <?php 
                }
            }
        }else{
            foreach($this->centros_costos as $centro_costo){
                ?>
                  
                  colSum = grid.jqGrid('getCol', '<?php echo "cc_" . $centro_costo->cc_id . "_number"?>', false, 'sum');
                  colSum = number_format(colSum);
                  grid.jqGrid('footerData', 'set', { <?php echo "cc_" . $centro_costo->cc_id?>: "$ " + colSum });
                <?php 
            }
        }
    ?>
            

}


function seleccion_fila(id){
    //alert(id);
   //  var rowData = $(this).getRowData(id);
     // if(id!==lastSel){ 
            $(this).find('.ui-state-highlight').css({background:'#80BFFF !important'});
            lastSel=id; 
            
            $('table tr').find('.ui-state-highlight').css('background', '#ffffff');
            $(this).find('.ui-state-highlight').addClass('ui-state-hover');
            
            
      
       //}
}


</script>
<div class="modal" id="myModal">
	<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title"><span class="glyphicon glyphicon-filter"></span> Filtro Avanzado</h4>
          
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="col-md-5">
                   Persona 
                </div>   
                <div class="col-md-7">
                    <?php 
                            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(                       
                                  'attribute'=>'nombre_persona',
                                  'model'=>$model,
                                  'sourceUrl'=>array('validacionProductividades/personaList'),
                                  'name'=>'nombre_persona',
                                  'id'=>'nombre_persona', 
                                  'options'=>array(
                                    'minLength'=>'1',
                                    'showAnim'=>'fold',
                                    'select'=>"js: function(event, ui) {
                                        $('#persona_id').val(ui.item['persona_id']);
                                    }",
                                    'change'=>"js: function(event, ui) {
                                                    if (!ui.item) {
                                                       $('#persona_id').val('');
                                                       $('#nombre_persona').val('');
                                                    }
                                                  }",
                                  ),
                                  'htmlOptions'=>array(
                                    'size'=>40,
                                    'maxlength'=>45,
                                    'class'=>'form-control'
                                  ),

                            )); ?>
                            <?php echo $form->hiddenField($model,'persona_id',array('id'=>'persona_id','type'=>"hidden")); ?>
                </div>  
            </div>
            
            <div class="row">
                <div class="col-md-5">
                   Persona ingresó productividad
                </div>   
                <div class="col-md-7">
                    <?php echo $form->dropDownList( $model,
                                                            'persona_carga_id', 
                                                            CHtml::listData($personas_cargan,'persona_carga_id','personaCarga'),                                                    
                                                            array("id"=>"persona_carga_id",'empty' => 'Seleccionar Persona..','class'=>'form-control')
                                                            );?>
                </div>  
            </div>
            
            <div class="row">
                <div class="col-md-5">
                   Montos mayores a
                </div>   
                <div class="col-md-7">
                   <?php echo $form->textField($model,'monto',array("id"=>"monto",'class'=>'form-control')); ?>
                </div>  
            </div>
            
            
                              

        </div>
        <div class="modal-footer">
          <a href="#" data-dismiss="modal" class="btn">Cerrar</a>
          <button id="quitar" type="button" class="btn btn-danger" onclick="quitarfiltro()"><span class="glyphicon glyphicon-remove"></span> Quitar</button>
          <button id="filtroPersona" type="button" class="btn btn-success" onclick="filtrar(this)"><span class="glyphicon glyphicon-filter"></span> Filtrar</button>
       
        </div>
      </div>
    </div>
</div>

<div id="salida" style="display:none"></div>
    
<?php
    $this->beginWidget('bootstrap.widgets.BsPanel', array(
    'title' => '<span class="glyphicon glyphicon-list"></span> ' . $titulo,
    'type' => BsHtml::PANEL_TYPE_PRIMARY,
    //'htmlOptions'=>array('class' => 'gray-dark')
));
    ?>

<div class="row">
    <div class="col-md-12">




        <?php echo $form->hiddenField($model,'tipo_personas',array('id'=>'tipo_personas','type'=>"hidden",'size'=>2,'maxlength'=>1)); ?>

        <div class="row">
            <div class="col-md-4">
                <div class="buttons">
                    <button id="boton-academico" class="btn <?php if($this->es_academico ){?>btn-primary<?php }else{?>btn-default<?php }?>" type="button">
                        Académico
                    </button>

                    <button id="boton-noacademico"  class="btn <?php if(!$this->es_academico ){?>btn-primary<?php }else{?>btn-default<?php }?>" type="button">
                        No Académico
                    </button>
                </div>



            </div>
            
         
            <div class="col-md-4 text-right">
                <?php if($this->es_validacion || $this->es_validacion_encargado){?>
                    <span class="glyphicon glyphicon-check"></span> Aprobar Todos
                    <?php echo $form->checkBox($model,'validar_productividades'); ?>
                    <br>
                <?php }?>
                <?php if(!$this->es_ingreso && !$this->es_informe){?>
                    <a href="#" style=" text-decoration: none; color: #000 " title="Si está marcado Despliega el resultado de la lista detallando las columnas por tipo de productividad" alt="Si está marcado Despliega el resultado de la lista detallando las columnas por tipo de productividad">
                    <span class="glyphicon glyphicon-edit"></span> Mostrar detalle Tipo Productividad</a>
                    <?php echo $form->checkBox($model,'mostrar_detalle_tipo_productividad'); ?>
                <?php }?>
                    
                <?php if($this->es_consulta){?>
                    <br>
                    <a href="#" style=" text-decoration: none; color: #000 " title="Mostrar solo valores mayores que cero" alt="Mostrar solo valores mayores que cero">
                    <span class="glyphicon glyphicon-plus"></span> Mostrar solo valores mayores a cero</a>
                    <?php echo $form->checkBox($model,'mostrar_positivos'); ?>
                <?php }?>
            </div>

            <div class="col-md-4 text-right">
                <?php if($mostrar_filtro){?>
                    <a data-toggle="modal" href="#myModal" class="btn btn-danger"><span class="glyphicon glyphicon-filter"></span> Filtrado por <?php echo $detalle_filtro?></a>
                <?php }else{?>
                    <a data-toggle="modal" href="#myModal" class="btn btn-primary"><span class="glyphicon glyphicon-filter"></span> Filtro Avanzado</a>
                <?php }?>

                
                
            </div>

        </div>

        <br>

        <div class="row">
            <div id="grilla" class="col-md-12">    

        <?php 
              $this->widget('ext.jqgrid.JqGrid', array(
               'id'=>'productividades',
               'language'=>'es',
               'options'=>array(
                   'url'=>Yii::app()->createUrl('grillaProductividades/getDatosGrilla'),
                   'postData'=>array("periodo_id"=>$this->periodo_id,
                                     "periodo_fin_id"=>$this->periodo_fin_id,
                                     "es_academico"=>$this->es_academico,
                                     "es_consulta"=>$this->es_consulta,
                                     "es_validacion"=>$this->es_validacion,
                                     "es_validacion_encargado"=>$this->es_validacion_encargado,
                                     "es_ingreso"=>$this->es_ingreso,
                                     "periodo_cerrado"=>$this->periodo_cerrado,
                                     "area_id"=>$this->area_id,
                                     "detalle_por_tipo_productividad"=>$this->detalle_por_tipo_productividad,
                                     "mostrar_positivos"=>$this->mostrar_positivos,
                                     "filtro"=>  json_encode($this->filtro)
                                    ),
                   'datatype'=>'json',
                   'mtype'=>'POST',
                   'colNames'=>$this->columnas_nombres,
                   'colModel'=>$this->columnas_detalles,
                   'pager'=> '#pager',
                   'rowNum'=>'100',
                   'height'=>'400',
                   //'width'=>'2500',
                   'sortname'=>'apellido_paterno',
                   'sortorder'=>'asc',
                   'viewrecords'=>true,
                   'scrolling'=>true,
                   'autowidth'=>true,
                   'shrinkToFit'=>false,
                   'ignoreCase'=>true,
                   'footerrow'=>true,
                   //'altRows'=>true,
                   'userDataOnFooter'=>true,           
                   //'multiselect'=>true,
                   'colTotals'=>true,
                   'navBarOptions' => array( 'search' => true), 
                   'loadComplete'=> 'function(){ carga_totales() }  ',
                   'onSelectRow'=> 'function(id){ seleccion_fila(id)}'

               ) 
           ));

           ?>
            </div>
        </div>

  
    </div>
</div>
<?php
$this->endWidget();
?>