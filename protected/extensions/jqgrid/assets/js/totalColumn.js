(function($) {
    $.jgrid.extend({
        showTotals: function(options) {
            var o = $.extend({
                total: "Totals",
                totalRow: undefined,
                includeRows: []                
            }, options || {})
            return this.each(function() {
                if (!this.grid) {
                    return;
                }
                var self = this;
                var showTotals = function() {
                    var totals = {};
                    $.each(self.p.colModel, function(index, data) {
                        if (o.totalRow && o.totalRow == data.index) {
                            totals[data.index] = o.total;
                        } else {
                            var sum = NaN;
                            if (o.includeRows.length) {
                                if ($.inArray(data.index, o.includeRows) > -1) {
                                    sum = $(self).jqGrid('getCol', data.index, false, 'sum');
                                }
                            } else {
                                var maxPresision = 0;
                                $.each($(self).jqGrid('getCol', data.index, false), function(index, value) {
                                    if (isNaN(value)) {
                                        sum = NaN;
                                        return false;
                                    }
                                    if (value === "") {
                                        return;
                                    }
                                    if (!sum) {
                                        sum = 0;
                                    }
                                    sum += parseFloat(value);
                                    var pointPosition = value.indexOf('.'), valuePrecision = 0;
                                    if (pointPosition > -1) {
                                        valuePrecision = value.length - pointPosition - 1;
                                    }
                                    if (valuePrecision > maxPresision) {
                                        maxPresision = valuePrecision;
                                    }
                                });
                                if (maxPresision > 0 && sum) {
                                    sum = sum.toFixed(maxPresision);
                                }
                            }
                            if (!isNaN(sum)) {
                                totals[data.index] = sum;
                            }
                        }
                    });
                    $(self).jqGrid('footerData', 'set', totals);
                };
                $(this).bind('jqGridAfterLoadComplete', showTotals);
                //Already loaded?
                if (this.rows.length) {
                    showTotals();
                }
            });
        }
    });
})(jQuery);