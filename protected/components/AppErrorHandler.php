<?php
/**
 * Clase que toma el control cuando se produce un Exception en los controladores
 * 
 * Actualmente no se esta usando, pero puede ser util para el control de excepciones
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.components
 */
class AppErrorHandler extends CErrorHandler {

    /**
     * 
     * @param string $exception Exception producida
     * @return string
     */
    protected function handleException($exception) {
        //Exception example: 
        /* CDbCommand failed to execute the SQL statement: SQLSTATE[40001]:
        * Serialization failure: 1213 Deadlock found when trying to get lock;
        * try restarting transaction. The SQL statement executed was:
        * INSERT INTO `table_name` (`id`, `name`) VALUES (:yp0, :yp1)
        */
        //echo $exception->getMessage();
        //Yii::app()->user->setFlash('error',$e->getMessage());
        return parent::handleException($exception);
    }
}
?>
