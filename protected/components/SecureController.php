<?php
/**
 * Controlador base usado para que se ejecuten las verificaciones de seguridad
 * de todos los controladores del sistema
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.components
 */
class SecureController extends Controller
{
    
        /**
        * Define la función de control de acceso que tiene cada controlador
        * del sistema
        * 
        * @return array funciones que realizan el control de acceso
        */    
        public function filters()
        {
            return array(
                'accessControl',
            );
        }
    
}