<?php

/**
 * Controlador base que define el layout de la aplicación
 * y instancia la variable que contiene el menú principal del sistema
 * 
 * Todos los controladores de la aplicación extienden de este controlador
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.components
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
}