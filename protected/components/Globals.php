<?php

/**
 * Clase que define la forma como se encriptan los campos relacionados
 * a dinero en las coordinaciones y productividades
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.components
 */
class Globals {
    const KEY = '197912091400';

   /**   
    * Función para descencriptar un valor de la base de datos
    * es compatible con AES_DECRYPT de mysql
    * @param int $val Valor a desencriptar
    * @param string $key ='null' Clave para desencriptar el valor
    * @return string AES-decrypt $val.   
   */
   public static function aesDecrypt($val,$key=null) {
      if ($key == null) $key = self::KEY;
      $mode = MCRYPT_MODE_ECB;
      $enc = MCRYPT_RIJNDAEL_128;
      $dec = @mcrypt_decrypt($enc, $key, $val, $mode, @mcrypt_create_iv( @mcrypt_get_iv_size($enc, $mode), 
                  MCRYPT_DEV_URANDOM ) );
      return rtrim( $dec, ( ( ord(substr( $dec, strlen( $dec )-1, 1 )) >= 0 and ord(substr( $dec, strlen( $dec )-1, 1 ) ) <= 16 ) ? chr(ord(substr( $dec, strlen( $dec )-1, 1 ))): null) );
   }

    /**
     * Función para descencriptar un valor de la base de datos
     * es compatible con AES_ENCRYPT de mysql
     * @param int $val Valor a desencriptar
     * @param string $key ='null' Clave para desencriptar el valor
     * @return string AES-encrypted  $val.   
    */   
      public static function aesEncrypt($val, $key=null) {
         if ($key == null) $key = self::KEY;
         $mode=MCRYPT_MODE_ECB;
         $enc=MCRYPT_RIJNDAEL_128;
         $val=str_pad($val, (16*(floor(strlen($val) / 16)+(strlen($val) % 16==0?2:1))), chr(16-(strlen($val) % 16)));
          return @mcrypt_encrypt($enc, $key, $val, $mode, mcrypt_create_iv( mcrypt_get_iv_size($enc, $mode), MCRYPT_DEV_URANDOM));
        }
    }

?>
