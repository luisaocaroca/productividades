<?php
/**
 * Clase que representa la informacion que se necesita para identificar 
 * un usuario del sistema
 * 
 * Esta clase contiene las funciones para realizar la autenticación 
 * de un usuario en el sistema.
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.components
 */
class UserIdentity extends CUserIdentity
{

        private $id;
        private $model;  
        public $rut;
        public $drut;
        public $email;
        public $telefono;
        public $apellido_paterno;
        public $apellido_materno;
        public $nombre;
        public $nombre_rol;

    
        /**
        * Obtiene el ID del usuario
        * 
        * @return int ID del usuario
        */        
        public function getId()
        {
            return $this->id;
        }
        
      
 
        /**
        * Obtiene el model que representa a un usuario
        * 
        * @return array Model que representa a un usuarios
        */            
        public function getModel()
        {
            return $this->model;
        }

        /**
         * Constructor
         * @param string $username Nombre de usuario
        */ 
        public function __construct($username)
        {
            //cargo el username a la clase padre
            parent::__construct($username,$username);
        }



        /**
         * Authenticates a user.
         * The example implementation makes sure if the username and password
         * are both 'demo'.
         * In practical applications, this should be changed to authenticate
         * against some persistent user identity storage (e.g. database).
         * @return bool whether authentication succeeds.
         */
        public function authenticate()
        {

                $persona = ProdPersona::model()->findByAttributes(array('username'=>CHtml::encode($this->username)));

                if(is_null($persona)){    
                    
                    //si no se encuentra el usuario por el login se busca por rut
                    //y luego se actualiza el login en la BD
                    $persona = ProdPersona::model()->findByAttributes(array('rut'=>CHtml::encode($this->rut)));
                    
                    if(!is_null($persona)){   
                        $persona->username = $this->username;
                        $this->actualiza_usuario($persona);                        
                    }else{
                        $this->crearUsuario();
                    }
                    //revisa si el usuario se puede crear
//                    $this->errorCode=self::ERROR_USERNAME_INVALID;
//                    $this->errorMessage="El usuario no existe en la base de datos: rut " . $this->username;
                    

                }else{

                    $this->actualiza_usuario($persona);
                    $this->model= $persona;
                    $this->id= $persona->persona_id;
                    //SETEA EL PERMISO QUE TIENE SOBRE EL SISTEMA
                    $this->setWebRol($persona->rol_id);

                    $this->errorCode=self::ERROR_NONE; 
                }
                

    //		if(!isset($users[$this->username]))
    //			$this->errorCode=self::ERROR_USERNAME_INVALID;
    //		else if($users[$this->username]!==$this->password)
    //			$this->errorCode=self::ERROR_PASSWORD_INVALID;
    //		else
    //			$this->errorCode=self::ERROR_NONE;

                return !$this->errorCode;
        }

        /**
        * define los permisos que tiene cada rol de usuario sobre el sistema
        * Este Rol se usa en los filtros de cada controller
        * Para definir si el usuario puede o no acceder al controller
        * @param int $rol_id Rol del usuario que hace el login
        */           
        private function setWebRol($rol_id){

            switch ($rol_id){
                case 1:
                    $this->setState('roles','validador');          
                    break;
                case 2:
                    $this->setState('roles','encargado');          
                    break;   
                case 3:
                    $this->setState('roles','ingreso');          
                    break; 
                case 6:
                    $this->setState('roles','director');          
                    break; 
                case 7:
                    $this->setState('roles','admin');          
                    break;      
                case 8:
                    $this->setState('roles','consulta');          
                    break;           
            }
        }


        /**
         * Valida que el RUT del usuario sea correcto
         * 
         * @param int $rut_entrada Rut del usuario
         * @param int $dv_entrada Dígito verificador del usuario
         * @return bool Define si el rut es o no correcto.
        */  
        public function valida_rut($rut_entrada,$dv_entrada){

            //TODO SACAR PARA EL CASO DE DATOS REALES
            //return true;

            $x=2;
            $s=0;
            for ( $i=strlen($rut_entrada)-1;$i>=0;$i-- )
            {
                if ( $x >7 )
                {
                        $x=2;
                }
                $s += $rut_entrada[$i]*$x;
                $x++;
            }
            $dv=11-($s%11);
            if ( $dv==10 )
            {
                    $dv='K';
            }
            if ( $dv==11 )
            {
                    $dv='0';
            }
            if ( $dv==$dv_entrada )
            {
                    return true;
            }
            else
            {
                    return false;
            }
        }

       /**
        * Crea un nuevo usuario
        * 
        * Se usa la metodología REST (tipo de web services) para crear a un usuario
        * Esto se usa cuando un usaurio entra al sistema a través de la INTRANET del DCC
        * y no está creado en el sistema de productividades.
        * A través de esta función el usuario se crea automáticamente
        * 
        * @return int Codigo de la operación formato HTML
       */         
       private function crearUsuario(){

            
            $persona = new ProdPersona();
            $persona->username = $this->username;
            $persona->rut = $this->rut;
            $persona->dv = $this->drut;
            $persona->nombres = $this->nombre;
            $persona->apellido_paterno = $this->apellido_paterno;
            $persona->apellido_materno =$this->apellido_materno;
            $persona->email = $this->email;
            $persona->telefono =$this->telefono;
            
            $rol = ProdRol::model()->findByAttributes(array('rol_intranet'=>CHtml::encode($this->nombre_rol)));
            $persona->rol_id = $rol->rol_id;
           
            $persona->insert();
            

            $this->model= $persona;
            $this->id= $persona->persona_id;
            //SETEA EL PERMISO QUE TIENE SOBRE EL SISTEMA
            $this->setWebRol($persona->rol_id);
            $this->errorCode=self::ERROR_NONE; 

                

//            $url = Yii::app()->request->getBaseUrl(true) ."/index.php/api/";
//            $client = new RestClient($url, '', '');
//
//            //$response = $client->get('persona/1');
//            //
//            //print_r($response);
//
//            $response = $client->post('persona', array('nombres'=>$nombre,
//                                                       'apellido_paterno'=>$apellido_paterno,
//                                                       'apellido_materno'=>$apellido_materno,
//                                                       'rut'=>$rut, 
//                                                       'dv' =>$drut, 
//                                                       'email'=>$email,
//                                                       'rol_id'=>8
//                                                        ));
//            //SI SE PRODUCE UN ERROR AL CREAR EL USUSARIO AUTOMATICAMENTE
//            //DEVUELVE EL ERROR
//            if($client->getHttpCode()==500){
//                //devuelve arreglo errores para 
//                $mensaje = "El usuario no se puede crear automáticamente: ";
//                foreach($response["errors"] as $a){                
//                    foreach($a as $b){
//                        $mensaje = $mensaje . $b. ",". PHP_EOL. PHP_EOL;
//                    }
//                }
//
//    //             
//    //
//    //            echo "<pre>";
//    //            print_r($response["errors"]);
//    //            echo "</pre>";
//    //            
//    //            echo get_class($response["errors"]);
//
//                $this->errorCode=500;
//                $this->errorMessage=$mensaje;
//
//            }else{
//                //SINO HACE EL CAST DEL USUARIO
//                $persona = new ProdPersona();
//                $persona->attributes = $response;
//
//                $this->model= $persona;
//                $this->id= $persona->persona_id;
//                //SETEA EL PERMISO QUE TIENE SOBRE EL SISTEMA
//                $this->setWebRol($persona->rol_id);
//
//                $this->errorCode=self::ERROR_NONE; 
//
//
//            }



       }
       
       function actualiza_usuario(ProdPersona $persona){
           
           $persona->nombres = $this->nombre;
           $persona->apellido_materno = $this->apellido_materno;
           $persona->apellido_paterno = $this->apellido_paterno;
           $persona->email = $this->email;
           $persona->telefono = $this->telefono;
           
           
           $rol = ProdRol::model()->findByAttributes(array('rol_intranet'=>CHtml::encode($this->nombre_rol)));
           
           $persona->rol = $rol;
           $persona->rol_id = $rol->rol_id;
           
           $persona->save();
           
           
       }
}