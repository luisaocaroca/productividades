<?php
/**
 * Clase que representa a un usuario autenticado del sistema
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.components
 */
class WebUser extends CWebUser {
 
        /**
        * Overrides a Yii method that is used for roles in controllers (accessRules).
        *
        * @param string $operation Name of the operation required (here, a role).
        * @param mixed $params (opt) Parameters for this operation, usually the object to access.
        * @return bool Permission granted?
        */
       public function checkAccess($operation, $params=array())
       {

   //        if(isset(Yii::app()->session['items'])){
   //            //SE ACABO LA SESSION
   //            echo "session time out";
   //            $this->logout();
   //        }

           //echo $operation;
           if (empty($this->id)) {
               // Not identified => no rights
               return false;
           }
           $role = $this->getState("roles");
           if ($role === 'admin') {
               return true; // admin role has access to everything
           }


           // allow access if the operation request is the current user's role
           return ($operation === $role);
    }
    
        /**
        * Ejecuta el login de un usuarios
        * @param WebUser $identity Clase que identifica al usuario
        * @param int $duration Duracion de la session del usuario
        * 
        */         
        public function login($identity, $duration=0)
        {
            parent::login($identity, $duration);
            Yii::app()->getSession()->add('model', $identity->getModel());
            
            $persona = $identity->getModel();
            //SE OBTIENEN LOS DATOS DEL MENU SEGÚN EL ROL
            $this->getMenus($persona->rol_id);
                            
        }
        

        /**
        * Ejecuta el logout de un usuario
        * @param bool $destroySession Boolean que define si se va o no a destruir la session
        * 
        */              
        public function logout($destroySession= true)
        {
            // I always remove the session variable model.
            Yii::app()->getSession()->remove('model');
            Yii::app()->getSession()->destroy();
        }

        /**
        * Obtiene la clase que representa al usuario
        * @return WebUser Clase que representa al usuario
        * 
        */                 
        public function getModel()
        {
            return Yii::app()->getSession()->get('model');
        }

        /**
        * Obtiene al usuario
        * @return WebUser Clase que representa al usuario
        * 
        */   
        protected function getUser()
        {
          return Yii::app()->getSession()->get('model');
        }


        /**
        * Obtiene el nombre completo de un usuario
        * @return string Nombre completo del usuario
        * 
        */   
        function getNombreCompleto(){
            $user = $this->getUser();
            return $user->nombres . " " . $user->apellido_paterno .  " " . $user->apellido_materno;
        }

        /**
        * Obtiene el nombre del perfil de un usuario
        * @return string Nombre del perfil del usuario
        * 
        */           
        function getPerfil(){
          $user = $this->getUser();
          return $user->rol;
        }

        /**
        * Obtiene el RUT completo de un usuario
        * @return string RUT completo del usuario
        * 
        */            
        function getRut(){
          $user = $this->getUser();
          return $user->rut . "-" . $user->dv;
        }


        /**
        * Obtiene el EMAIL de un usuario
        * @return string EMAIL del usuario
        * 
        */         
        function getMail(){
          $user = $this->getUser();
          return $user->email;
        }

        /**
        * Obtiene el ID de un usuario
        * @return int ID del usuario
        * 
        */          
        function getPersonaId(){
          $user = $this->getUser();
          return $user->persona_id;
        }

        /**
        * Obtiene el ROL ID de un usuario
        * @return int ROL ID del usuario
        * 
        */         
        function getRolId(){
          $user = $this->getUser();
          return $user->rol_id;
        }
        
        
        /**
        * Obtiene el tipo persona ACADEMICO/NO ACADEMICO
        * @return int Tipo persona del usuario => 1 ACADEMICO, 0 NO ACADEMICO
        * 
        */         
        function getTipoPersonaId(){
          $user = $this->getUser();
          return $user->bo_academico;
        }
        
        /**
        * Define si el usuario es un administraor
        * @return int entrega si el usuario es o no administrador
        * 
        */  
        function isAdmin(){
          $user = $this->getUser();
          return intval($user->role) == 1;
        }
        
        /**
        * Obtiene el Area ID de un usuario
        * @return int Area ID del usuario
        * 
        */         
        function getAreaId(){
          $user = $this->getUser();
          return $user->area_id;
        }
        
        /**
        * A partir del rol del usuario se genera el menú horizontal de la aplicación.
         * Este menú se construye a partir de la relación de los {@link ProdSubMenu} 
         * y  {@link ProdRol} =>  {@link ProdSubMenuRol} 
        * 
        * @param int $rol_id Rol del usuario
        */
        protected function getMenus($rol_id){
            $items = array();

            $submenus= ProdSubMenuRol::model()->with(array('subMenu'=>array('condition'=>'rol_id='.$rol_id)))->findAll();



            foreach($submenus as $submenuRol){
                
                

                $submenu = $submenuRol->subMenu;
                $menu = $submenu->menu; 
                
                if($menu->bo_activo == 1){

                    if($menu->menu_id >1){

                        $menu_visible = true;

                        if(Yii::app()->user->isGuest){
                            $menu_visible = false; 
                        }

                        $returnarray = array(   'label' => $menu->nombre_menu, 
                                                //'url' => array( $menu->link,'id' => $menu->menu_id),
                                                'url' => array( $menu->link),
                                                'visible'=>$menu_visible

                                            );


                        $subitems = array();

                        foreach($menu->prodSubMenus as $submenu){
                            
                            if($submenu->bo_activo==1){
                                $submenu_visible = false;                        

                                foreach($submenu->prodRols as $rol){
                                    if($rol->rol_id==$rol_id){
                                        $submenu_visible = true;                        
                                    }

                                    if(Yii::app()->user->isGuest){
                                        $submenu_visible = false; 
                                    }
                                }       

                                $subitem = array(  'label' => $submenu->nombre_menu, 
                                                    //'url' => array( $submenu->link,'id' => $submenu->sub_menu_id),
                                                    'url' => array( $submenu->link),
                                                    'visible'=>$submenu_visible
                                                );

                                array_push($subitems, $subitem);
                            }

                        }

                        if(count($subitems)>1){                    
                            $returnarray = array_merge($returnarray, array('items' => $subitems)); 
                        }


                        if(!in_array($returnarray, $items)){
                            array_push($items, $returnarray);
                        }

                    }
                }
            }

            
            //$returnarray = array('label'=>'Salir ('.(Yii::app()->user->nombreCompleto).')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest);

            //array_push($items, $returnarray);

            //setea el menu en una variable session
            Yii::app()->session['items'] = $items;
            
            
        }


}
?>