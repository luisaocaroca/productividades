<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CronCommand
 *
 * @author luisao
 */
class CronCommand extends CConsoleCommand{
    //put your code here
    
    
//    public function run()
//    {
//        error_reporting(E_ERROR);
//        echo "ok";
//        print("hola");
//    }
    
    public function actionActualizaCoordinaciones() { 
        
        Yii::log('Cron: inicio actionActualizaCoordinaciones', CLogger::LEVEL_INFO, 'application.commands.CronCommand');
        
        try{
            $administrador = ProdPersona::getAdministrador();

            Yii::import('application.controllers.CoordinacionesController'); 
            $controllerCoordinaciones = new CoordinacionesController("coordinaciones");
            $controllerCoordinaciones->aplicarCoordinacionesPeriodosAbiertos($administrador->persona_id);

            Yii::log("Cron: fin actionActualizaCoordinaciones", CLogger::LEVEL_INFO, "application.commands.CronCommand");        

            return 1;
        }catch (Exception $exp){
            Yii::log("Cron: error actionActualizaCoordinaciones". $exp->getMessage(), CLogger::LEVEL_ERROR, "application.commands.CronCommand");        

            return 0;
        }
        
                
        
        
    }
    
    /**
     * Verifica si hay que abrir el periodo y si hay que cerrar los periodos anteriores
     * 
     * @param int $persona_id Persona que aplica el cambio.
     */  
    public function actionAdministradorPeriodos() { 
        
        Yii::log('Cron: inicio actionAdministradorPeriodos', CLogger::LEVEL_INFO, 'application.commands.CronCommand');
        
        try{
            date_default_timezone_set("America/Santiago");
                        
            //CREAR PERIODO ACTUAL EN CASO DE QUE NO EXISTA
            $periodo_actual_id = date("Ym"); 
            ProdPeriodo::verifica_crear_periodo($periodo_actual_id);
            //APLICAR COORDINACIONES
            $this->actionActualizaCoordinaciones();
            
            //VERIFICAR LOS PERIODOS ANTERIORES QUE ESTAN ABIERTOS Y PONERLOS EN ESTADO VALIDACION o CERRARLOS
            $periodosAnterioresAbiertos = ProdPeriodo::getPeriodosAnterioresAbiertos($periodo_actual_id);
            
            foreach($periodosAnterioresAbiertos as $periodo){
                $dia_actual = date("d"); 
                
                //SI ESTAMOS EN LOS PRIMEROS X (ProdPeriodo::DIAS_PARA_VALIDACION) DIAS DEL MES, EL PERIODO ANTERIOR SE DEJA EN ESTADO
                //DE VALIDACION
                //SINO SE CIERRA
                if($dia_actual > ProdPeriodo::DIAS_PARA_VALIDACION){
                    $periodo->estado_periodo_id=  ProdPeriodo::PERIODO_CERRADO;
                }else{
                    $periodo->estado_periodo_id=  ProdPeriodo::PERIODO_VALIDACION;
                }
                                
                $periodo->save();
            }

            
            Yii::log("Cron: fin actionAdministradorPeriodos", CLogger::LEVEL_INFO, "application.commands.CronCommand");        

            return 1;
        }catch (Exception $exp){
            Yii::log("Cron: error actionCerrarPeriodo". $exp->getMessage(), CLogger::LEVEL_ERROR, "application.commands.CronCommand");        

            return 0;
        }
        
        
    }
    
    public function actionRespaldoDiario(){
        
        Yii::log('Cron: inicio actionRespaldoDiario', CLogger::LEVEL_INFO, 'application.commands.CronCommand');
        
        try{
            Yii::import('application.modules.backup.controllers.DefaultController'); 
            $myController = new DefaultController("default");
            $myController->createRestore();

            Yii::log("Cron: fin actionRespaldoDiario", CLogger::LEVEL_INFO, "application.commands.CronCommand");        

            return 1;
        }catch (Exception $exp){
            Yii::log("Cron: error actionRespaldoDiario". $exp->getMessage(), CLogger::LEVEL_ERROR, "application.commands.CronCommand");        
            return 0;
        }
    }
    
    public function actionEnviaReporteMail() { 
        
        Yii::log('Cron: inicio actionEnviaReporteMail', CLogger::LEVEL_INFO, 'application.commands.CronCommand');
        
        try{
            $administrador = ProdPersona::getAdministrador();

            Yii::import('application.controllers.SiteController'); 
            $controllerSite = new SiteController("mail");
            $controllerSite->ActionSendMail();

            Yii::log("Cron: fin actionEnviaReporteMail", CLogger::LEVEL_INFO, "application.commands.CronCommand");        

            return 1;
        }catch (Exception $exp){
            Yii::log("Cron: error actionEnviaReporteMail". $exp->getMessage(), CLogger::LEVEL_ERROR, "application.commands.CronCommand");        

            return 0;
        }
        
                
        
        
    }
    
    public function actionRegistrarCronTab(){
        
        //echo Yii::getPathOfAlias('application');
    
        $cron = new Crontab('my_crontab','/u/g/gsochoa/intranet/docs_intranet/sis_prod/productividades/protected'); // my_crontab file will store all added jobs
        $cron->eraseJobs(); // erase all previous jobs in my_crontab
        
        //MIN // HORA
        $job = new CronApplicationJob('yiic', 'cron', array("administradorPeriodos"), '00', '00'); // run every day
        $cron->add($job);
        
        $job = new CronApplicationJob('yiic', 'cron', array("respaldoDiario"), '00', '00'); // run every day
        $cron->add($job);
        
        $job = new CronApplicationJob('yiic', 'cron', array("enviaReporteMail"), '00', '00', '1', '*', '*'); // Correr una vez al mes
        $cron->add($job);
        
        $job = new CronApplicationJob('yiic', 'cron', array("enviarMailPendientes"), '00', '12', '*', '*', '0'); // Correr una vez al mes
        $cron->add($job);
        
        $job = new CronApplicationJob('yiic', 'cron', array("enviarMailPendientesJefeAdministrativo"), '00', '12', '*', '*', '0'); // Correr una vez al mes
        $cron->add($job);
        
        
        $jobs_obj = $cron->getJobs(); // previous jobs saved in my_crontab

        foreach($jobs_obj as $job)
            echo $job->getCommand() . PHP_EOL;
        
        $cron->saveCronFile(); // save to my_crontab cronfile
 
        $cron->saveToCrontab(); // adds all my_crontab jobs to system (replacing 
    }
    
    
    public function actionEnviarMailPendientes() { 
        
        Yii::log('Cron: inicio actionMailProductividadesPendientes', CLogger::LEVEL_INFO, 'application.commands.CronCommand');
        
        try{

            Yii::import('application.controllers.SiteController'); 
            $controllerSite = new SiteController("mail");
            $controllerSite->ActionEnviaMailProductividadesPendientes();

            Yii::log("Cron: fin actionMailProductividadesPendientes", CLogger::LEVEL_INFO, "application.commands.CronCommand");        

            return 1;
        }catch (Exception $exp){
            Yii::log("Cron: error actionMailProductividadesPendientes". $exp->getMessage(), CLogger::LEVEL_ERROR, "application.commands.CronCommand");        

            return 0;
        }
        
                
        
        
    }
    
    
    public function actionEnviarMailPendientesJefeAdministrativo() { 
        
        Yii::log('Cron: inicio actionEnviarMailProductividadesPendientesJefeAdministrativo', CLogger::LEVEL_INFO, 'application.commands.CronCommand');
        
        try{

            Yii::import('application.controllers.SiteController'); 
            $controllerSite = new SiteController("mail");
            $controllerSite->ActionEnviaMailProductividadesPendientesJefeAdministrativo();

            Yii::log("Cron: fin actionEnviarMailProductividadesPendientesJefeAdministrativo", CLogger::LEVEL_INFO, "application.commands.CronCommand");        

            return 1;
        }catch (Exception $exp){
            Yii::log("Cron: error actionEnviarMailProductividadesPendientesJefeAdministrativo". $exp->getMessage(), CLogger::LEVEL_ERROR, "application.commands.CronCommand");        

            return 0;
        }
        
                
        
        
    }
}

?>
