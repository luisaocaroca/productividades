<?php

return array(
        'Create'=>'Crear',
        'Save'=>'Guardar',
        'Search'=>'Buscar',
        'field_with'=>'Campos Con',
        'are required' => 'son requeridos',
        'Manage'=>'Administrador',
        'List'=>'Lista',
        'Create'=>'Crear',
        'Advanced Search'=>'Busqueda Avanzada',
        'View'=>'Vista',
        'Update'=>'Actualizar',
        'Delete'=>'Borrar',
        )
?>