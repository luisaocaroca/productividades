<?php

/**
 * Description of ProdPersonaTest
 *
 * @author luisao
 */
class ProdPersonaTest extends WebTestCase {

    public function testCrearPersonaTest1() {
            //ejecuta el login
            $this->open('/productividades/ingreso.php');
            
            
            //PRUEBA 1            
            $this->open('prodPersona/crearPersona');
            //$this->assertTextPresent('Crea Persona');
            $this->assertElementPresent('name=ProdPersona[rut]');
            $this->type('name=ProdPersona[rut]','13635010');
             
            $this->click("//input[@value='Crear']");
            $this->waitForTextPresent('no puede ser nulo');
            

            
            
        }        
        
        public function testCrearPersonaTest2() {
            //ejecuta el login
            $this->open('/productividades/ingreso.php');
            
            //PRUEBA 2
            $this->open('prodPersona/crearPersona');
            //$this->assertTextPresent('Crea Persona');
            $this->assertElementPresent('name=ProdPersona[rut]');
            $this->type('name=ProdPersona[rut]','13635010');
            $this->type('name=ProdPersona[dv]','2');
            $this->type('name=ProdPersona[nombres]','Luis');
             
            $this->click("//input[@value='Crear']");
            $this->waitForTextPresent('no puede ser nulo');
            
            
        }   
        
        public function testCrearPersonaTest3() {
            //ejecuta el login
            $this->open('/productividades/ingreso.php');
            
            //PRUEBA 2
            $this->open('prodPersona/crearPersona');
            //$this->assertTextPresent('Crea Persona');
            $this->assertElementPresent('name=ProdPersona[rut]');
            $this->type('name=ProdPersona[rut]','13635010');
            $this->type('name=ProdPersona[dv]','2');
            $this->type('name=ProdPersona[nombres]','Luis');
            $this->type('name=ProdPersona[apellido_paterno]','Toledo');
             
            $this->click("//input[@value='Crear']");
            $this->waitForTextPresent('El Rut ya fue ingresado');
            
            
        }   
        
        public function testCrearPersonaTest4() {
            //ejecuta el login
            $this->open('/productividades/ingreso.php');
            
            //PRUEBA 2
            $this->open('prodPersona/crearPersona');
            //$this->assertTextPresent('Crea Persona');
            $this->assertElementPresent('name=ProdPersona[rut]');
            $this->type('name=ProdPersona[rut]','54345454');
            $this->type('name=ProdPersona[dv]','2');
            $this->type('name=ProdPersona[nombres]','Maestro');
            $this->type('name=ProdPersona[apellido_paterno]','Rope');
             
            $this->click("//input[@value='Crear']");
            $this->waitForTextPresent('Persona Creada correctamente');
            
            
        }   

}

?>