<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InformacionPersonaForm
 *
 * @author Luisao
 */
class InformacionPersonaForm extends CFormModel{
    //put your code here
        
        public $mail;
        
        
        public function attributeLabels()
        {
                return array(
                    'mail'=>Yii::t('app', 'Mail')
                    );
                
                    
        }
        
         public function rules()
        {
            return array(
                array('mail', 'length', 'max'=>200),
            );
        }
}

?>
