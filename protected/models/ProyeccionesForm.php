<?php

/**
 * Form usado para consultar las proyecciones.
 * Form usado para consultar las proyecciones.
 * 
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */

class ProyeccionesForm extends CFormModel
{
        public $tipo_proyeccion;
        public $base_calculo_mensual;
        public $base_calculo_anual;
        public $exportar_excel;
    
        /**
         * Obtiene el label de los campos del formulario
         * 
         * @return array
         */      
        public function attributeLabels()
        {
                return array(
                    'tipo_proyeccion'=>'Tipo Proyeccion',
                    'base_calculo_mensual'=>'Selecciona Base Cálculo Mensual',
                    'base_calculo_anual'=>'Selecciona Base Cálculo Anual',
                    'exportar_excel'=>'Exportar Excel',
                );
        }

        /**
         * Reglas de validación de los campos del formulario
         * 
         * @return array
         */         
        public function rules()
        {
            return array(
                array('base_calculo_mensual', 'numerical'),
                array('base_calculo_anual', 'numerical'),
                array('exportar_excel', 'numerical'),
                array('tipo_proyeccion', 'verificar_consulta'),

            );
        }

        /**
         * Reglas de validación para la ejecución de la consulta
         * @param array $attribute atributos
         * @param array $params parametros
         * @return array
         */            
        public function verificar_consulta($attribute,$params) {
            //echo $this->rut; 

            if($this->tipo_proyeccion==1){
                if($this->base_calculo_mensual==0){
                    $this->addError("base_calculo_mensual", 'Debe seleccionar la base del cálculo mensual');
                }        
            }

            if($this->tipo_proyeccion==2){
                if($this->base_calculo_anual==0){
                    $this->addError("base_calculo_anual", 'Debe seleccionar la base del cálculo anual');
                } 
            }

        }
}
?>
