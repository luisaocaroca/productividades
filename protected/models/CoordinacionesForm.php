<?php

/**
 * Form usado para la carga de coordinaciones
 * Este formulario se usa cuando se cargan coordinaciones al sistema
 * 
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */

class CoordinacionesForm extends CFormModel
{
	public $persona_id;
        public $nombre_persona;
        public $tipo_prod_id;
        public $cc_id;
        public $monto;
        public $actualiza = false;
        public $descripcion;

        /**
         * Reglas de validación de los campos del formulario
         * 
         * @return array
         */     
	public function rules()
	{
		return array(
			array('persona_id,tipo_prod_id,cc_id,monto,nombre_persona', 'required','message'=>'Debe ingresar {attribute}'),
			array('monto', 'numerical','integerOnly'=>true),
                        array('monto', 'verificar_monto'),
                        array('actualiza', 'length', 'max'=>10),
                        array('descripcion','length', 'max'=>500)
                    

		);
	}

        /**
         * Obtiene el label de los campos del formulario
         * 
         * @return array
         */  
	public function attributeLabels()
	{
		return array(
                    'persona_id'=>'Persona',
                    'nombre_persona'=>'Persona',
                    'tipo_prod_id'=>'Tipo Productividad',
                    'cc_id'=>'Centro Costo',
                    'monto'=>'Monto',
                    'descripcion'=>'Indicar a que corresponde la Coordinación'
		);
	}
        
        /**
         * Reglas de validación para el monto
         * @param array $attribute atributos
         * @param array $params parametros
         * @return array
         */          
        public function verificar_monto($attribute,$params) {
            //echo $this->rut; 
            
            if($this->monto<=0){
                $this->addError($attribute, 'El monto debe ser<br>mayor que cero');
            }
        }
        

//        
//        public function toString(){
//            
////            echo "<pre>";
////            print_r($this);
////            echo "</pre>";
//                
////            echo "periodo_id    :" . $this->periodo_id . "<br>";
////            echo "persona_id    :" . $this->persona_id . "<br>";
////            echo "nombre_persona:" . $this->nombre_persona . "<br>";
////            echo "tipo_prod_id  :" . $this->tipo_prod_id . "<br>";
////            echo "cc_id         :" . $this->cc_id . "<br>";
////            echo "monto         :" . $this->monto . "<br>";
////            echo "numero_pagos  :" . $this->numero_pagos . "<br>";
//        }

	
}
