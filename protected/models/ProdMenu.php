<?php

Yii::import('application.models._base.BaseProdMenu');

/**
 * Model que realiza el mapeo de la tabla ProdMenu
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProdMenu extends BaseProdMenu
{
        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return BaseProdMenu::model
         */    
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}