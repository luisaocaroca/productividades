<?php

Yii::import('application.models._base.BaseProdCentroCosto');

/**
 * Model que realiza el mapeo de la tabla ProdCentroCosto
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProdCentroCosto extends BaseProdCentroCosto
{
        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return BaseProdCentroCosto::model
         */    
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        
       /**
         * Consulta el listado de centros de costo
         * 
         * @return array {@link ProdCentroCosto} listado de centros de costo
         */           
        public static function getCentrosCosto(){
            $criteria = new CDbCriteria();
            $criteria->addCondition("bo_activo=1");
            $criteria->order='cc_id ASC';
            
            $centroscosto = ProdCentroCosto::model()->findAll($criteria);
            return $centroscosto;
        }
                
        /**
         * Consulta a la base de datos de los centros de costo asociados a un tipo de productividad
         * 
         * @param int $tipo_prod_id tipo de productividad
         * @return array Listado de centros de costo asociada a los tipo de productividads
         */          
        public static function getCentrosCostosTipoProductividad($tipo_prod_id){
            $centros_costos = array();
            
            $centros_costo_tipos_prod=ProdTipoProdCc::model()->findAll('tipo_prod_id=:tipo_prod_id',array(':tipo_prod_id'=>(int) $tipo_prod_id));

            foreach($centros_costo_tipos_prod as $centro_costo){
                //echo $tipo_productividad->tipoProductividad;
                
                if($centro_costo->centroCosto->bo_activo = 1){
                    array_push($centros_costos, $centro_costo->centroCosto);
                }
            }
            
            
            return $centros_costos;            

        }
        
        
        public static function getCentroCosto($cc_id){
            $centro_costo  = self::model()->findByPk($cc_id);
            return $centro_costo;
        }
        
        
        public static function getTiposProducividades($cc_id,$periodo_id,$periodo_fin_id,$es_academico){
            $centro_costo=self::getCentroCosto($cc_id);            
            
            $relacionesCentroCostoTipoProductividad = $centro_costo->prodTipoProductividads;
                
            $relacionesCargadasProductividades = ProdProductividad::getRelacionesCentroCostoTipoProductividadPeriodo($periodo_id, $periodo_fin_id, $centro_costo->cc_id);

            foreach($relacionesCargadasProductividades as $tipoProd){
                $existe = false;
                foreach($relacionesCentroCostoTipoProductividad as $tipoProdRelacion){
                    if($tipoProdRelacion->tipo_prod_id == $tipoProd["tipo_prod_id"]){
                        $existe = true;
                        break;
                    }
                }

                if(!$existe){
                    $tipo_productividad = ProdTipoProductividad::getTipoProductividad($tipoProd["tipo_prod_id"]);
                    array_push($relacionesCentroCostoTipoProductividad, $tipo_productividad);
                }
            }
            
            $salida = array();
            
            foreach($relacionesCentroCostoTipoProductividad as $tipo_productividad){
                if(($tipo_productividad->acadamico && $es_academico) ||
                    ($tipo_productividad->no_academico && !$es_academico)){  
                    
                    array_push($salida, $tipo_productividad);
                }
            }
            
            return $salida;
        }
        
        public static function getTiposProducividadesTodos($cc_id,$periodo_id,$periodo_fin_id){
            $centro_costo=self::getCentroCosto($cc_id);            
            
            $relacionesCentroCostoTipoProductividad = $centro_costo->prodTipoProductividads;
                
            $relacionesCargadasProductividades = ProdProductividad::getRelacionesCentroCostoTipoProductividadPeriodo($periodo_id, $periodo_fin_id, $centro_costo->cc_id);

            foreach($relacionesCargadasProductividades as $tipoProd){
                $existe = false;
                foreach($relacionesCentroCostoTipoProductividad as $tipoProdRelacion){
                    if($tipoProdRelacion->tipo_prod_id == $tipoProd["tipo_prod_id"]){
                        $existe = true;
                        break;
                    }
                }

                if(!$existe){
                    $tipo_productividad = ProdTipoProductividad::getTipoProductividad($tipoProd["tipo_prod_id"]);
                    array_push($relacionesCentroCostoTipoProductividad, $tipo_productividad);
                }
            }

            
            return $relacionesCentroCostoTipoProductividad;
        }
}