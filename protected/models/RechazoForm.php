<?php

/**
 * Formulario para rechazar productividades
 * Actualmente esta obsoleto por cambio de funcionalidads
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class RechazoForm extends CFormModel
{
        public $observaciones;
        public $periodo_id;
        public $persona_id;
        public $aprueba;
    //    public $rechaza;
        public $productividad;
    

        /**
         * Reglas de validación de los campos del formulario
         * 
         * @return array
         */      
        public function rules()
        {
                return array(                    
                        array('periodo_id', 'numerical'),
                        array('persona_id', 'numerical'),
                        array('productividad', 'length', 'max'=>500),

                );
        }

    
}
?>
