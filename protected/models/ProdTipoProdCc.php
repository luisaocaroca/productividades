<?php

Yii::import('application.models._base.BaseProdTipoProdCc');

/**
 * Model que realiza el mapeo de la tabla ProdTipoProdCc
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProdTipoProdCc extends BaseProdTipoProdCc
{
        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return BaseProdTipoProdCc::model
         */       
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public static function getTiposProductividadesCentroCosto($cc_id){
            
            $relacion_cc_tipo_prod = self::model()->findAll('cc_id=:cc_id',array(':cc_id'=>(int) $cc_id));
            
            return $relacion_cc_tipo_prod;
                
        }
}