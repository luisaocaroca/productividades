<?php

Yii::import('application.models._base.BaseProdEstado');

/**
 * Model que realiza el mapeo de la tabla ProdEstado
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProdEstado extends BaseProdEstado
{
        /**
         * @var Constante que define el valor APROBADO de una productividad
         */    
        const APROBADO = 2;
        /**
         * @var Constante que define el valor APROBADO de una productividad
         */    
        const APROBADO_ENCARGADO = 1;
        /**
         * @var Constante que define el valor PENDIENTE de una productividad
         */   
        const PENDIENTE = 0;
        /**
         * @var Constante que define que se recuperaran todos los valores
         */   
        const TODOS = -1;
    
        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return BaseProdEstado::model
         */    
        public static function model($className=__CLASS__) {
                return parent::model($className);
        }
        
        /**
         * Busca al administrador del sistemas
         * 
         * @return ProdPersona Administrador del sistema
         */             
        public static function getEstados(){
            $criteria = new CDbCriteria();            
            $criteria->order= 'estado_id ASC';

            $estados = self::model()->findAll($criteria);
            return $estados;        
        }
}