<?php

Yii::import('application.models._base.BaseProdSubMenuRol');

/**
 * Model que realiza el mapeo de la tabla ProdSubMenuRol
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProdSubMenuRol extends BaseProdSubMenuRol
{
        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return BaseProdSubMenuRol::model
         */       
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}