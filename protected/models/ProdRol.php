<?php

Yii::import('application.models._base.BaseProdRol');

/**
 * Model que realiza el mapeo de la tabla ProdRol
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProdRol extends BaseProdRol
{
        /**
         * @var Constante que define el valor del rol JEFE_ADMINISTRATIVO
         */        
        const ROL_JEFE_ADMINISTRATIVO       = 1;
        
        /**
         * @var Constante que define el valor del rol ROL_ENCARGADO
         */            
        const ROL_ENCARGADO                 = 2;
        
        /**
         * @var Constante que define el valor del rol ROL_INGRESO_DATOS
         */            
        const ROL_INGRESO_DATOS             = 3;
        
    
        /**
         * @var Constante que define el valor del rol ROL_DIRECTOR
         */            
        const ROL_DIRECTOR                  = 6;
        
        /**
         * @var Constante que define el valor del rol ROL_ADMINISTRDOR
         */            
        const ROL_ADMINISTRDOR              = 7;
        
        /**
         * @var Constante que define el valor del rol ROL_ACADEMICO
         */            
        const ROL_CONSULTA                 = 8;
        
       
        
        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return BaseProdRol::model
         */        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        /**
         * Consulta el listado de Roles
         * 
         * @return array {@link ProdArea} listado de roles
         */           
        public static function getRoles(){
            $criteria = new CDbCriteria();
            $criteria->addCondition("bo_activo=1");
            
            $roles = ProdRol::model()->findAll($criteria);
            return $roles;
        }
        
        
}