<?php

/**
 * Form usado para la carga de productividades
 * Este formulario se usa cuando se cargan productividades al sistema
 * 
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class JqgridParameterForm extends CFormModel
{
        public $page;
        /**
         * @var number 
         */    
        public $rows;
        /**
         * @var string 
         */    
        public $sidx;
        /**
         * @var string 
         */      
        public $sord;
        /**
         * @var boolean 
         */      
        public $_search;
        /**
         * @var string 
         */      
        public $filters;

        /**
         * @var string 
         */      
        public $filtersSql;    

        /**
         * @var number 
         */      
        public $total;
        /**
         * @var number 
         */      
        public $records;
        
        /**
         * @var number 
         */      
        public $start;


        /**
         * @var number 
         */      
        public $limit;           
        
        //LISTADO DE CHECKBOX PARA LAS VALIDACIONES
        public $academicos = array();
        
        public $no_academicos = array();


    
    /**
         * Obtiene el label de los campos del formulario
         * 
         * @return array
         */         
        public function attributeLabels()
        {
                return array(
                    'page'=>Yii::t('app', 'Pagina'),
                    'rows'=>Yii::t('app', 'Filas'),
                    'sidx'=>Yii::t('app', 'Orden Por'),
                    'sord'=>Yii::t('app', 'Orden Tipo'),
                    '_search'=>Yii::t('app', 'Busqueda ?'),
                    'filters'=>Yii::t('app', 'Filtros'),
                    'limit'=>Yii::t('app', 'Limite'),
                    'academicos'=>Yii::t('app', 'Listado De check de academicos'),
                    'no_academicos'=>Yii::t('app', 'Listado De check de no academicos'),
                    );
        }
        
        
        /**
         * Reglas de validación de los campos del formulario
         * 
         * @return array
         */           
        public function rules()
        {
            return array(
                array('page', 'numerical'),
                array('limit', 'numerical'),
                array('rows', 'length', 'max'=>500),
                array('sidx', 'length', 'max'=>500),
                array('sord', 'length', 'max'=>500),
                array('_search', 'length', 'max'=>500),
                array('filters', 'length', 'max'=>500),
                array('academicos', 'length', 'max'=>500),
                array('no_academicos', 'length', 'max'=>500),
                );
                
        }
        
        
        public function calculaPaginas($count){
            $this->limit = $this->rows;
                    //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$this->limit);
            } else {
                $total_pages = 0;
            }
            if ($this->page > $total_pages)
                $this->page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            if( $count >0 ) {
                $this->start = $this->limit*$this->page - $this->limit;    
            }else{
                $this->start = 0;
            }  

            // Se agregan los datos de la respuesta del servidor
            $this->total = $total_pages;
            $this->records = $count;

        }
}
