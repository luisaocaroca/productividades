<?php

Yii::import('application.models._base.BaseProdCoordinacion');

/**
 * Model que realiza el mapeo de la tabla ProdCoordinacion
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProdCoordinacion extends BaseProdCoordinacion
{
        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return BaseProdCoordinacion::model
         */    
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
       /**
         * Consulta a la base de datos del listado de coordinaciones
         * 
         * @param bool $orderByNombre = 'false' define si se quiere ordenar o no las coordinaciones spor nombres de personas
         * @return array Listado de centros de coordinaciones
         */         
        public static function getCoordinaciones($orderByNombre=false){
            
            if($orderByNombre){
                $criteria_coord = new CDbCriteria();
                $criteria_coord->join = "JOIN prod_persona on prod_persona.persona_id = t.persona_id";
                $criteria_coord->order= 'prod_persona.nombres ASC';
                $coordinaciones = self::model()->findAll($criteria_coord);
            }else{
                $coordinaciones = self::model()->findAll();            
            }
            return $coordinaciones;
        }
        
       /**
         * Consulta a la base de datos del listado de coordinaciones asociadas a una persona
         * 
         * @param int $persona_id persona de las coordinaciones a consultars
         * @return array Listado de centros de coordinaciones
         */          
        public static function getCoordinacionesPersona($persona_id){
            $criteria_coord = new CDbCriteria();
            $criteria_coord->compare('persona_id', $persona_id);
            return self::model()->findAll($criteria_coord);
                    
        }
        
       /**
         * Consulta a la base de datos del listado de coordinaciones asociadas a una persona y a un tipo de coordinacion s
         * 
         * @param int $persona_id persona de las coordinaciones a consultar
         * @param int $cc_id centro de costo a consultar
         * @param int $tipo_prod_id tipo de productividad a consultar
         * @return array Listado de centros de coordinaciones
         */           
        public static function getCoordinacionesPersonaTipo($persona_id,$cc_id,$tipo_prod_id){
            $criteria_coord = new CDbCriteria();
            $criteria_coord->compare('persona_id', $persona_id);
            $criteria_coord->compare('cc_id', $cc_id);
            $criteria_coord->compare('tipo_prod_id', $tipo_prod_id);

            return self::model()->find($criteria_coord);
                    
        }
        
        
}