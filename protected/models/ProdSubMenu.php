<?php

Yii::import('application.models._base.BaseProdSubMenu');

/**
 * Model que realiza el mapeo de la tabla ProdSubMenu
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProdSubMenu extends BaseProdSubMenu
{
        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return BaseProdSubMenu::model
         */     
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}