<?php

Yii::import('application.models._base.BaseProdProductividad');

/**
 * Model que realiza el mapeo de la tabla ProdProductividad
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProdProductividad extends BaseProdProductividad
{
        public $total;
        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return BaseProdProductividad::model
         */     
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
       /**
         * Consulta el listado de productividades de una persona para un cierto estado, y periodo de tiempos
         * 
        * 
         * @param int $persona_id periodo ID
         * @param int $estado_id ='-1' estado ID
         * @param int $periodo_inicio = '' periodo inicio de la consulta
         * @param int $periodo_fin = '' periodo fin de la consulta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getProductividadesIndividuales($persona_id,$area_id=0,$estado_id=ProdEstado::TODOS,$periodo_inicio="",$periodo_fin=""){
            
            $criteria_prod = new CDbCriteria();
            $criteria_prod->compare('persona_id', $persona_id);
            
            if($estado_id<>ProdEstado::TODOS){
                $criteria_prod->compare('estado_id', $estado_id);
            }
            
            if($area_id!=0){
                $criteria_prod->compare('area_id', $area_id);
            }
            
            if(!empty($periodo_inicio)){
                if(empty($periodo_fin)){
                    $criteria_prod->compare('periodo_id', $periodo_inicio);
                    $criteria_prod->order= 'periodo_id DESC';
                }else{
                    $criteria_prod->addBetweenCondition('periodo_id',$periodo_inicio,$periodo_fin);
                    $periodo_actual_id = date("Ym");  
                    $criteria_prod->compare('periodo_id <',$periodo_actual_id);
                    $criteria_prod->order= 'periodo_id DESC,tipo_prod_id DESC';
                }
            }  else {
                $periodo_actual_id = date("Ym");  
                $criteria_prod->compare('periodo_id <',$periodo_actual_id);
                $criteria_prod->order= 'periodo_id DESC,tipo_prod_id DESC';
            }
            
            
            return self::model()->findAll($criteria_prod);
            
        }
        
        /**
         * Consulta el listado de productividades de una persona para un cierto estado, y periodo de tiempos
         * El resultado es un dataprovider
         * 
        * 
         * @param int $persona_id periodo ID
         * @param int $estado_id ='-1' estado ID
         * @param int $periodo_inicio = '' periodo inicio de la consulta
         * @param int $periodo_fin = '' periodo fin de la consulta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getProductividadesIndividualesDataProvider(ProdProductividad $model,$periodo_inicio="",$periodo_fin="",$route="",$order=true,$paginacion = 10,$area_id=0){
            
            $criteria_prod = new CDbCriteria();
            $criteria_prod->compare('persona_id',$model->persona_id);
            
            if($model->estado_id<>ProdEstado::TODOS){
                $criteria_prod->compare('estado_id', $model->estado_id);
            }
            
            if(!empty($periodo_inicio)){
                if(empty($periodo_fin)){
                    $criteria_prod->compare('periodo_id', $periodo_inicio);
                    //$criteria_prod->order= 'periodo_id DESC';
                }else{
                    $criteria_prod->addBetweenCondition('periodo_id',$periodo_inicio,$periodo_fin);
                    $periodo_actual_id = date("Ym");  
                    $criteria_prod->compare('periodo_id <',$periodo_actual_id);
                    //$criteria_prod->order= 'periodo_id DESC,tipo_prod_id DESC';
                }
            }  else {
                $periodo_actual_id = date("Ym");  
                $criteria_prod->compare('periodo_id <',$periodo_actual_id);
               // $criteria_prod->order= 'periodo_id DESC,tipo_prod_id DESC';
            }
            
            if($area_id!=0){
                $criteria_prod->compare('area_id', $area_id);
            }
            
            // need to clone the criteria to compute sum ?
            $amountCriteria = clone($criteria_prod);
            $amountCriteria->select = "SUM(AES_DECRYPT(monto, '".Globals::KEY ."')) AS total";
            $amountCriteria->compare('estado_id',  ProdEstado::APROBADO);
            $model->total = self::model()->find($amountCriteria)->total;
            
            if(is_null($model->total)){
                $model->total=0;
            }
        
            $attributes = array(
                            'periodo_id',     
                            'tipo_prod_id',  
                            'estado_id',   
                            'cc_id',   
                            'monto'=>
                                    array(
                                            'asc'=>"AES_DECRYPT(monto, '".Globals::KEY ."') DESC",
                                            'desc'=>"AES_DECRYPT(monto, '".Globals::KEY ."') ASC",
                                    ),
                        );
            
            if(!$order){
                $attributes = array(
                            'periodo_id');
            }
            
            //$criteria_prod->
            
            return new CActiveDataProvider($model, array(
                    'criteria' => $criteria_prod,
                    'sort'=>array(
                        'route'=> $route,
                        'defaultOrder'=>array(
                            'periodo_id'=>true
                          ),
                        'attributes'=>$attributes,
                    ),
                    'pagination'=>array(
                        'pageSize'=>$paginacion,
                    ),
            ));
            
        }
        
       /**
         * Consulta el listado de productividades para un estado y un periodo de tiempos
         * 
         * 
         * @param int $estado_id estado ID
         * @param int $periodo_inicio = '' periodo inicio de la consulta
         * @param int $periodo_fin = '' periodo fin de la consulta
         * @param bool $order_nombre_persona = 'false' define si se va o no a ordenar por nombre de persona
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getProductividadesGlobales($estado_id,$periodo_inicio="",$periodo_fin="",$order_nombre_persona = false){
            
            $criteria_prod = new CDbCriteria();
            $criteria_prod->compare('estado_id', $estado_id);
            
            if(empty($periodo_fin)){
                if(!empty($periodo_inicio)){
                    $criteria_prod->compare('periodo_id', $periodo_inicio);
                }
            }else{
                $criteria_prod->addBetweenCondition('periodo_id',$periodo_inicio,$periodo_fin);
                $periodo_actual_id = date("Ym");  
                $criteria_prod->compare('periodo_id <',$periodo_actual_id);
            }
            if($order_nombre_persona){
                $criteria_prod->join = "JOIN prod_persona on prod_persona.persona_id = t.persona_id";
                $criteria_prod->order= 'prod_persona.apellido_paterno ASC';
            }else{
                $criteria_prod->order= 't.periodo_id DESC, t.tipo_prod_id';
            }
            
            return self::model()->findAll($criteria_prod);
            
        }
        
       /**
         * Verifica si las productividades del período han sido validadas o no
         * 
         * @param int $periodo_id periodo a consultar
         * @return int cantidad de registros no aprobados
         */         
        public static function getEstadoValidacionPeriodo($periodo_id){
            $criteria_prod = new CDbCriteria();
            $criteria_prod->compare('periodo_id',$periodo_id);
            $criteria_prod->compare('estado_id !', ProdEstado::APROBADO);
                       
            $registros = self::model()->count($criteria_prod);
            
            return $registros;            
            
        }
        
        /**
         * Verifica si las productividades del período han sido validadas o no para el encargado
         * 
         * @param int $periodo_id periodo a consultar
         * @return int cantidad de registros no aprobados
         */         
        public static function getEstadoValidacionPeriodoArea($periodo_id,$area_id){
            $criteria_prod = new CDbCriteria();
            $criteria_prod->compare('periodo_id',$periodo_id);
            $criteria_prod->compare('area_id',$area_id);
            $criteria_prod->compare('estado_id !', ProdEstado::APROBADO);
                       
            $registros = self::model()->count($criteria_prod);
            
            return $registros;            
            
        }
        
       /**
         * Consulta el listado de productividades de un período que han sido obtenidas a partir de las coordinaciones
         * 
         * @param int $periodo_id periodo a consultar
         * @return array {@link ProdProductividad} listado productividades encontradas
         */         
        public static function getProductividadesObtenidasDeCoordinaciones($periodo_id){
            //CABIAR AL FLAG DE LA TABLA TIPOS PRODUCTIVIDADDES
            $criteria_prod = new CDbCriteria();
            $criteria_prod->join = "JOIN prod_coordinacion on prod_coordinacion.persona_id      = t.persona_id 
                                                          and prod_coordinacion.tipo_prod_id    = t.tipo_prod_id
                                                          and prod_coordinacion.cc_id           = t.cc_id";
            //TODO:aca quedaría por definir si se pueden cargar productividades a partir de tipos_prod que corresponden a coordinaciones
            //en caso que se tenga que cargar productividades de coordinaciones temporales (por ejemplo)
            //entonces habría que agregar un filtro para marcar que dicha productividad viene de una coordinacion (quizas otro campo en la tabla).             
            $criteria_prod->join = "JOIN prod_tipo_productividad on prod_tipo_productividad.tipo_prod_id      = t.tipo_prod_id 
                                                                and es_coordinacion = 1";
            $criteria_prod->compare('periodo_id', $periodo_id);
            
            
            return self::model()->findAll($criteria_prod);
            
            
            
        }
        
        /**
         * Consulta el listado de productividades de una persona por periodo, centro costo y tipo productividad
         * 
         * @param int $persona_id Persona ID
         * @param int $periodo_id Periodo ID
         * @param int $cc_id Centro Costo ID
         * @param int $tipo_prod_id Tipo Productividad ID
         * 
         * @return ProdProductividad productividad encontrada
        */    
        public static function buscarProductividadUsuario($persona_id,$periodo_id,$cc_id,$tipo_prod_id,$persona_carga_id){
            
            
            $criteria_prod = new CDbCriteria();
            $criteria_prod->compare('persona_id', $persona_id);              
            $criteria_prod->compare('periodo_id', $periodo_id);
            $criteria_prod->compare('cc_id', $cc_id);            
            $criteria_prod->compare('tipo_prod_id', $tipo_prod_id);
            $criteria_prod->compare('persona_carga_id', $persona_carga_id);

            return self::model()->find($criteria_prod);
            
        }
        
       /**
         * Obtiene el listado de personas que han cargado productividades en el sistema
         * 
         * @param int $periodo_id periodo a consultar
         * @return array personas que han cargado productividades
         */               
        public static function getPersonasCargan($periodo_id,$periodo_fin_id=0){
            
            $criteria=new CDbCriteria;
            $criteria->distinct = true;
            $criteria->select = array(
                    't.persona_carga_id',
            );
            
            if($periodo_fin_id==0){
                $criteria->compare('periodo_id', $periodo_id);
            }else{
                $criteria->addBetweenCondition('periodo_id',$periodo_id,$periodo_fin_id);
            }
          
            return self::model()->findAll($criteria);
        }
        
        /**
         * Obtiene el listado de personas que han cargado productividades en el sistema
         * 
         * @param int $periodo_id periodo a consultar
         * @return array personas que han cargado productividades
         */               
        public static function getPersonasCarganArea($periodo_id,$area_id,$periodo_fin_id=0){
            
            $criteria=new CDbCriteria;
            $criteria->distinct = true;
            $criteria->select = array(
                    't.persona_carga_id',
            );
            
            $criteria->compare('area_id', $area_id);
            if($periodo_fin_id==0){
                $criteria->compare('periodo_id', $periodo_id);
            }else{
                $criteria->addBetweenCondition('periodo_id',$periodo_id,$periodo_fin_id);
            }
          
            return self::model()->findAll($criteria);
        }
        
       /**
         * Consulta el listado de productividades agrupadas por estado
         * 
         * 
         * @param int $periodo_id periodo de consulta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getEstadoValidacionProductividades($periodo_id,$area_id=0){
            
            
            if($area_id==0){
                $sql = "SELECT CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END as tipo,
                               p.estado_id, 
                               e.nombre_estado,
                               case when p.estado_id = 0 then 'red' else 'normal' end as color,
                               SUM(CONVERT( AES_DECRYPT(monto, '".Globals::KEY."') , decimal))  as monto
                        FROM prod_db.prod_productividad p,
                                 prod_db.prod_persona u,
                             prod_db.prod_estado e 
                        WHERE p.periodo_id = $periodo_id
                          AND p.persona_id = u.persona_id
                          AND p.estado_id  = e.estado_id
                        group by CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END,
                                 p.estado_id,
                                 e.nombre_estado
                        order by tipo asc , estado_id asc
                        ;";
            }else{
                $sql = "SELECT CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END as tipo,
                               p.estado_id, 
                               e.nombre_estado,
                               case when p.estado_id = 0 then 'red' else 'normal' end as color,
                               SUM(CONVERT( AES_DECRYPT(monto, '".Globals::KEY."') , decimal))  as monto
                        FROM prod_db.prod_productividad p,
                             prod_db.prod_persona u,
                             prod_db.prod_estado e 
                        WHERE p.periodo_id = $periodo_id
                          AND p.persona_id = u.persona_id
                          AND p.estado_id  = e.estado_id
                          AND p.area_id    = $area_id
                        group by CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END,
                                 p.estado_id,
                                 e.nombre_estado
                        order by tipo asc , estado_id asc
                        ;";
            }
            
            $list= Yii::app()->db->createCommand($sql)->queryAll();

            
            return $list;
            
        }  
        
        
        /**
         * Consulta el listado de productividades agrupadas por estado
         * 
         * 
         * @param int $periodo_id periodo de consulta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getEstadoValidacionProductividadesPersona($persona_id,$periodo_id){
            
            $sql = "SELECT CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END as tipo,
                           p.estado_id, 
                           e.nombre_estado,
                           case when p.estado_id = 0 then 'red' else 'normal' end as color,
                           SUM(CONVERT( AES_DECRYPT(monto, '".Globals::KEY ."') , decimal)) as monto
                    FROM prod_db.prod_productividad p,
                             prod_db.prod_persona u,
                         prod_db.prod_estado e 
                    WHERE p.periodo_id = $periodo_id
                      AND p.persona_id = u.persona_id
                      AND p.estado_id  = e.estado_id
                      AND p.persona_id = $persona_id
                    group by CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END,
                             p.estado_id,
                             e.nombre_estado
                    order by tipo asc , estado_id asc
                    ;";
            
            $list= Yii::app()->db->createCommand($sql)->queryAll();

            
            return $list;
            
        }
        
        /**
         * Consulta el listado de productividades agrupadas por centro costo
         * 
         * 
         * @param int $periodo_id periodo de consulta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getResumenCentroCosto($periodo_id){
            
            $sql = "SELECT c.cc_id,
                           CONCAT( convert(c.cc_id,char), '-',c.nombre_cc) as nombre_cc,  
                           SUM(CONVERT( AES_DECRYPT(monto, '".Globals::KEY ."') , decimal)) as monto
                    FROM prod_db.prod_productividad p,
                         prod_db.prod_centro_costo c,
                         prod_db.prod_estado e 
                    WHERE p.periodo_id = $periodo_id
                      AND p.estado_id  = e.estado_id
                      AND p.cc_id  = c.cc_id
                    group by c.cc_id,
                             c.nombre_cc
                    order by c.cc_id asc 
                    ;";
            
            $list= Yii::app()->db->createCommand($sql)->queryAll();

            
            return $list;
            
        }  
        
        /**
         * Consulta el listado de productividades agrupadas por centro costo
         * 
         * 
         * @param int $periodo_id periodo de consulta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getResumenCentroCostoRango($periodo_id,$periodo_fin,$area_id=0,$estado_id=0){
            
            $sql = "SELECT c.cc_id,CONCAT( convert(c.cc_id,char), '-',c.nombre_cc) as nombre_cc,  
                           CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END as tipo,
                           SUM(CONVERT( AES_DECRYPT(monto, '".Globals::KEY ."') , decimal)) as monto
                     FROM prod_db.prod_productividad p,
                          prod_db.prod_centro_costo c,
                          prod_db.prod_estado e ,
                          prod_db.prod_persona u
                    WHERE p.estado_id  = e.estado_id
                      AND p.cc_id  = c.cc_id
                      and p.persona_id = u.persona_id
                    ";
                          
                     

                            
            if($periodo_fin!=0){
                 $sql = $sql . " AND p.periodo_id >= $periodo_id
                                AND p.periodo_id <= $periodo_fin";
            }else{
                $sql = $sql . " AND p.periodo_id = $periodo_id";
            }
            
            if($estado_id!=0){
                $sql = $sql . " AND p.estado_id = $estado_id";
            }
            
            if($area_id!=0){
                $sql = $sql . " AND p.area_id = $area_id";
            }
            
            $sql = $sql . " group by c.cc_id,
                                       c.nombre_cc,
                                       CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END
                              order by c.cc_id asc ;";
            
            
            $list= Yii::app()->db->createCommand($sql)->queryAll();
            
            
            $array_tipo=array("academico","no_academico");
            
            $centros_costos = array();
            
            foreach($list as $registro){
                if(!in_array($registro["cc_id"], $centros_costos)){
                    array_push($centros_costos,$registro["cc_id"]);
                }
            }

            $salida = array();
            
            foreach($centros_costos as $centro_costo){
                
                $total_academico=0;
                $total_no_academico=0;
                $total=0;
                $nombre_cc="";

                foreach($array_tipo as $tipo){
                    
                    foreach($list as $registro){
                        if($registro["cc_id"] == $centro_costo && $registro["tipo"]==$tipo) {
                            if($tipo=="academico"){
                                $total_academico = $total_academico+$registro["monto"]; 
                            }else{
                                $total_no_academico = $total_no_academico+$registro["monto"]; 
                            }
                            
                            $nombre_cc = $registro["nombre_cc"];
                           
                        }
                        
                        
                    } 
                }
                
                $total = $total_academico+$total_no_academico; 
                
                array_push($salida, array("cc_id"=>$centro_costo,"nombre_cc"=>$nombre_cc,"monto_academico"=>$total_academico,"monto_no_academico"=>$total_no_academico,"monto_total"=>$total));
            }
            
            return $salida;
            
        }  
        
        
        /**
         * Consulta totales productividades por año y tipo para ser graficadas
         * 
         * 
         * @param int $periodo_id periodo de consulta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getProductividadesAnioTipo($periodo_id,$cc_id=0){
            
            $sql = "select round(a.periodo_id/100,0) as anio, 
                           CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END as tipo,
                            sum(aes_decrypt(a.monto,'".Globals::KEY ."')) as monto
                 from prod_db.prod_productividad a,
                      prod_db.prod_persona u
                 where a.persona_id = u.persona_id
                   and a.estado_id = " . ProdEstado::APROBADO ;
            
            if($cc_id!=0){
                $sql .= "   and a.cc_id = " . $cc_id;
            }
            
            $sql .= "   and a.periodo_id >= " . $periodo_id .
                        " group by round(a.periodo_id/100,0),
                                   CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END
                            ;";
            
            $list= Yii::app()->db->createCommand($sql)->queryAll();

            
            return $list;
            
        }
        
        /**
         * Consulta totales productividades por año y tipo para ser graficadas
         * 
         * 
         * @param int $periodo_desde_id periodo desde
         * @param int $periodo_hasta_id periodo hasta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getProductividadesDetalleAnio($periodo_desde_id,$periodo_hasta_id,$cc_id=0,$area_id=0){
            
            $sql = "select  a.periodo_id, 
                            CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END as tipo,
                            sum(aes_decrypt(a.monto,'".Globals::KEY ."')) as monto
                 from prod_db.prod_productividad a,
                      prod_db.prod_persona u
                 where a.persona_id = u.persona_id
                   and a.estado_id = " . ProdEstado::APROBADO ;
            
            if($cc_id!=0){
                $sql .= "   and a.cc_id = " . $cc_id;
            }
            
            if($area_id!=0){
                $sql .= "   and a.area_id = " . $area_id;
            }
            
             $sql .= "   and a.periodo_id >= " . $periodo_desde_id .
                        "   and a.periodo_id <= " . $periodo_hasta_id .
                        " group by a.periodo_id,
                                   CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END
                            ;";

            $list= Yii::app()->db->createCommand($sql)->queryAll();

            
            return $list;
            
        }
        
        
        /**
         * Consulta totales productividades por año y tipo para ser graficadas
         * 
         * 
         * @param int $periodo_desde_id periodo desde
         * @param int $periodo_hasta_id periodo hasta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getProductividadesDetalleAnioPendientes($periodo_desde_id,$periodo_hasta_id,$cc_id=0,$area_id=0){
            
            $sql = "select  a.periodo_id, 
                            sum(aes_decrypt(a.monto,'".Globals::KEY ."')) as monto
                 from prod_db.prod_productividad a,
                      prod_db.prod_persona u
                 where a.persona_id = u.persona_id
                   and a.estado_id <> " . ProdEstado::APROBADO ;
            
            if($cc_id!=0){
                $sql .= "   and a.cc_id = " . $cc_id;
            }
            
            if($area_id!=0){
                $sql .= "   and a.area_id = " . $area_id;
            }
            
             $sql .= "   and a.periodo_id >= " . $periodo_desde_id .
                        "   and a.periodo_id <= " . $periodo_hasta_id .
                        " group by a.periodo_id
                            ;";

            $list= Yii::app()->db->createCommand($sql)->queryAll();

            
            return $list;
            
        }
        
        
        /**
         * Consulta totales productividades por año y tipo para ser graficadas
         * 
         * 
         * @param int $periodo_desde_id periodo desde
         * @param int $periodo_hasta_id periodo hasta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getProductividadesDetalleAnioPersona($persona_id,$periodo_desde_id,$periodo_hasta_id){
            
            $sql = "select  a.periodo_id, 
                            CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END as tipo,
                            sum(aes_decrypt(a.monto,'".Globals::KEY ."')) as monto
                 from prod_db.prod_productividad a,
                      prod_db.prod_persona u
                 where a.persona_id = u.persona_id
                   and a.estado_id = " . ProdEstado::APROBADO .
                "   and a.periodo_id >= " . $periodo_desde_id .
                "   and a.periodo_id <= " . $periodo_hasta_id .
                "   and a.persona_id  = " . $persona_id .
                " group by a.periodo_id,
                           CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END
                    ;";
            
            $list= Yii::app()->db->createCommand($sql)->queryAll();

            
            return $list;
            
        }
        
        /**
         * Consulta totales productividades por año y tipo para ser graficadas
         * 
         * 
         * @param int $periodo_desde_id periodo desde
         * @param int $periodo_hasta_id periodo hasta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getProductividadesDetalleCentroCosto($periodo_desde_id,$periodo_hasta_id,$area_id=0){
            
            $sql = " select   periodo_id, 
                              cc_id,   
                              CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END as tipo,
                              sum(aes_decrypt(a.monto,'".Globals::KEY ."')) as monto
                     from prod_db.prod_productividad a,
                          prod_db.prod_persona u
                     where a.persona_id = u.persona_id
                       and a.estado_id = " . ProdEstado::APROBADO .
                      " and a.periodo_id >=" . $periodo_desde_id .
                      " and a.periodo_id <=" . $periodo_hasta_id;
            
            
            if($area_id!=0){
                $sql .= " and a.area_id = " . $area_id;
            }
           
            $sql .= " group by periodo_id,
                                CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END,
                                cc_id
                       order by cc_id ASC;";
            
            $list= Yii::app()->db->createCommand($sql)->queryAll();

            
            return $list;
            
        }
        
        /**
         * Consulta totales productividades por año y tipo para ser graficadas
         * 
         * 
         * @param int $periodo_desde_id periodo desde
         * @param int $periodo_hasta_id periodo hasta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getProductividadesDetalleCentroCostoPersona($persona_id,$periodo_desde_id,$periodo_hasta_id){
            
            $sql = " select   periodo_id, 
                              cc_id,
                              CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END as tipo,
                              sum(aes_decrypt(a.monto,'".Globals::KEY ."')) as monto
                     from prod_db.prod_productividad a,
                          prod_db.prod_persona u
                     where a.persona_id = u.persona_id
                       and a.estado_id = " . ProdEstado::APROBADO .
                      " and a.periodo_id >=" . $periodo_desde_id .
                      " and a.periodo_id <=" . $periodo_hasta_id .
                      " and a.persona_id  =" . $persona_id .
                     " group by periodo_id,
                                CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END,
                                cc_id;";
            
            $list= Yii::app()->db->createCommand($sql)->queryAll();

            
            return $list;
            
        }
        
        
        /**
         * Consulta totales productividades por año y tipo para ser graficadas
         * 
         * 
         * @param int $periodo_desde_id periodo desde
         * @param int $periodo_hasta_id periodo hasta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getProductividadesDetalleCentroCostoTipoProductividad($periodo_desde_id,$periodo_hasta_id,$cc_id){
            
            $sql = " select   periodo_id, 
                              tipo_prod_id,
                              CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END as tipo,
                              sum(aes_decrypt(a.monto,'".Globals::KEY ."')) as monto
                     from prod_db.prod_productividad a,
                          prod_db.prod_persona u
                     where a.persona_id = u.persona_id
                       and a.estado_id = " . ProdEstado::APROBADO .
                      " and a.periodo_id >=" . $periodo_desde_id .
                      " and a.periodo_id <=" . $periodo_hasta_id .
                      " and a.cc_id =" . $cc_id .
                     " group by periodo_id,
                                tipo_prod_id,
                                CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END;";
            
            $list= Yii::app()->db->createCommand($sql)->queryAll();

            
            return $list;
            
        }
        
        
        public static function getDetalleTipoProductividadDataProvider(ProdProductividad $model,$cc_id,$tipo_prod_id,$vl_periodo,$vl_anio,$route){
            
            $criteria_prod = new CDbCriteria();
            $criteria_prod->compare('tipo_prod_id',$tipo_prod_id);
            $criteria_prod->compare('cc_id',$cc_id);
            $criteria_prod->compare('estado_id',ProdEstado::APROBADO);
            
            if($vl_periodo!=0){
                $criteria_prod->compare('periodo_id',$vl_periodo);
            }else{
                $periodo_inicio = $vl_anio."01";
                $periodo_fin = $vl_anio."12";
                $criteria_prod->addBetweenCondition('periodo_id',$periodo_inicio,$periodo_fin);
            }
            
            
            
            // need to clone the criteria to compute sum ?
            $amountCriteria = clone($criteria_prod);
            $amountCriteria->select = "SUM(AES_DECRYPT(monto, '".Globals::KEY ."')) AS total";
            $amountCriteria->compare('estado_id',  ProdEstado::APROBADO);
            $model->total = self::model()->find($amountCriteria)->total;
            
            if(is_null($model->total)){
                $model->total=0;
            }
        
            $attributes = array(
                            'persona_id',  
                            'periodo_id',     
                            'tipo_prod_id',  
                            'estado_id',   
                            'cc_id',   
                            'monto'=>
                                    array(
                                            'asc'=>"AES_DECRYPT(monto, '".Globals::KEY ."') DESC",
                                            'desc'=>"AES_DECRYPT(monto, '".Globals::KEY ."') ASC",
                                    ),
                        );
  
            
            //$criteria_prod->
            
            return new CActiveDataProvider($model, array(
                    'criteria' => $criteria_prod,
                    'sort'=>array(
                        'route'=> $route,
                        'defaultOrder'=>array(
                            'persona_id'=>'asc',
                          ),
                        'attributes'=>$attributes,
                    ),
                    'pagination'=>array(
                        'pageSize'=>100000,
                    ),
            ));
            
        }
        
        
        /**
         * Consulta el listado de productividades pendientes por área y estado
         * 
         * 
         * @param int $periodo_id periodo de consulta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getProductividadesPendientes($estado_id,$area_id=0){
            
            $sql = " SELECT periodo_id,
                            pers.nombres,
                            pers.apellido_paterno,
                            pers.apellido_materno,
                            pers.rut,
                            pers.dv,
                            prod.cc_id,
                        cc.nombre_cc,
                        prod.tipo_prod_id,
                        tp.nombre_tipo_prod,
                        area.nm_area
                   FROM prod_db.prod_productividad prod,
                        prod_db.prod_persona pers,
                            prod_db.prod_area area,
                        prod_db.prod_centro_costo cc,
                        prod_db.prod_tipo_productividad  tp
                  WHERE prod.persona_id 		= pers.persona_id
                    AND prod.area_id    		= area.area_id
                    AND prod.cc_id      		= cc.cc_id
                    AND prod.tipo_prod_id               = tp.tipo_prod_id";
             
            if($area_id!=0){
                    $sql .= " AND prod.area_id    		= $area_id";
            }
            
            $sql .= " AND prod.estado_id  		= $estado_id
                    ORDER BY pers.nombres, pers.apellido_paterno;";
            
            $list= Yii::app()->db->createCommand($sql)->queryAll();

            
            return $list;
            
        }  
        
        
        /**
         * Consulta el listado de productividades de una persona para un cierto estado, y periodo de tiempos
         * El resultado es un dataprovider
         * 
        * 
         * @param int $persona_id periodo ID
         * @param int $estado_id ='-1' estado ID
         * @param int $periodo_inicio = '' periodo inicio de la consulta
         * @param int $periodo_fin = '' periodo fin de la consulta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getProductividadesPendientesDataProvider(ProdProductividad $model,$route="",$order=true,$paginacion = 1000){
            
            $criteria_prod = new CDbCriteria();
            $criteria_prod->compare('area_id',$model->area_id);
            $criteria_prod->compare('estado_id', $model->estado_id);
          
            // need to clone the criteria to compute sum ?
            $amountCriteria = clone($criteria_prod);
            $amountCriteria->select = "SUM(AES_DECRYPT(monto, '".Globals::KEY ."')) AS total";
            $amountCriteria->compare('estado_id', $model->estado_id);
            $model->total = self::model()->find($amountCriteria)->total;
            
            if(is_null($model->total)){
                $model->total=0;
            }
        
            $attributes = array(
                            'periodo_id', 
                            'persona_id',
                            'estado_id',   
                            'tipo_prod_id',
                            'cc_id',   
                            'monto'=>
                                    array(
                                            'asc'=>"AES_DECRYPT(monto, '".Globals::KEY ."') DESC",
                                            'desc'=>"AES_DECRYPT(monto, '".Globals::KEY ."') ASC",
                                    ),
                            'persona_carga_id', 
                            'observaciones'
                        );
            
            if(!$order){
                $attributes = array(
                            'persona_id');
            }
            
            //$criteria_prod->
            
            return new CActiveDataProvider($model, array(
                    'criteria' => $criteria_prod,
                    'sort'=>array(
                        'route'=> $route,
                        'defaultOrder'=>array(
                            'periodo_id'=>true
                          ),
                        'attributes'=>$attributes,
                    ),
                    'pagination'=>array(
                        'pageSize'=>$paginacion,
                    ),
            ));
            
        }
        
        
        /**
         * Consulta totales productividades comparando centro costos por año
         * 
         * 
         * @param int $periodo_id periodo de consulta
         * @return array {@link ProdProductividad} listado productividades encontradas
         */          
        public static function getProductividadesComparaCCAnioTipo($periodo_id,$cc_id=0){
            
            $sql = "select round(a.periodo_id/100,0) as anio, 
                           cc_id,
                           CASE WHEN u.bo_academico = 1 THEN 'academico' ELSE 'no_academico' END as tipo,
                           sum(aes_decrypt(a.monto,'".Globals::KEY ."')) as monto
                 from prod_db.prod_productividad a,
                      prod_db.prod_persona u
                 where a.persona_id = u.persona_id
                   and a.estado_id = " . ProdEstado::APROBADO ;
            
            if($cc_id!=0){
                $sql .= "   and a.cc_id = " . $cc_id;
            }
            
            $sql .= "   and a.periodo_id >= " . $periodo_id .
                        " group by round(a.periodo_id/100,0),
                                   cc_id,
                                   tipo
                          order by cc_id asc,anio desc, tipo, monto asc
                            ;";
            
            $list= Yii::app()->db->createCommand($sql)->queryAll();

            
            return $list;
            
        }
        
        /**
         * Consulta relaciones tipos_productividades - centro costo de un período ingresado
         * 
         * 
         * @param int $periodo_id periodo de consulta
         * @param int $cc_id Centro Costo
         * @return array {@link ProdTipoProductividad} listado tipos productividades encontradas
         */          
        public static function getRelacionesCentroCostoTipoProductividadPeriodo($periodo_id,$periodo_fin_id,$cc_id){
            
            if($periodo_fin_id==0){
                $sql = "SELECT tipo_prod_id, count(*) as cuenta
                          FROM prod_db.prod_productividad p
                         WHERE p.periodo_id = $periodo_id
                           AND p.cc_id      = $cc_id  
                        group by tipo_prod_id
                        order by tipo_prod_id asc
                        ;";
            }else{
                $sql = "SELECT tipo_prod_id, count(*) as cuenta
                          FROM prod_db.prod_productividad p
                         WHERE p.periodo_id >= $periodo_id
                           AND p.periodo_id <= $periodo_fin_id   
                           AND p.cc_id      = $cc_id  
                        group by tipo_prod_id
                        order by tipo_prod_id asc
                        ;";
            }
            
            $list= Yii::app()->db->createCommand($sql)->queryAll();

            
            return $list;
            
        }
}