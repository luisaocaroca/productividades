<?php

Yii::import('application.models._base.BaseProdLogProductividad');

class ProdLogProductividad extends BaseProdLogProductividad
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        /**
         * Consulta el listado de productividades de una persona para un cierto estado, y periodo de tiempos
         * 
        * 
         * @param int $persona_id periodo ID
         * @param int $periodo_id periodo inicio de la consulta
         * @return array {@link ProdLogProductividad} listado losgs productividades encontradas
         */          
        public static function getLogsModificaciones($persona_id,$periodo_id,$productividad_id=0){
            
            $criteria_prod = new CDbCriteria();
            $criteria_prod->compare('persona_id', $persona_id);
            $criteria_prod->compare('periodo_id', $periodo_id);   
            if($productividad_id!=0){
                $criteria_prod->compare('productividad_id', $productividad_id);   
            }
            $criteria_prod->order= 'fecha_modificacion DESC';
            
            return self::model()->findAll($criteria_prod);
            
        }
}