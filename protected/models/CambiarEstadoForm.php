<?php
/**
 * Formulario para cambiar estado de una productividads
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class CambiarEstadoForm extends CFormModel
{
        public $periodo_id;
        public $persona_id;
        public $es_academico;
    
        /**
         * Reglas de validación de los campos del formulario
         * 
         * @return array
         */         
        public function rules()
        {
                return array(                    
                        array('periodo_id', 'numerical'),
                        array('persona_id', 'numerical'),
                        array('es_academico', 'numerical'),
                );
        }
   
}
?>
