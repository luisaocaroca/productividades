<?php

Yii::import('application.models._base.BaseProdPersona');
Yii::import('ext.wrest.behaviors.WRestModelBehavior');

/**
 * Model que realiza el mapeo de la tabla ProdPersona
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProdPersona extends BaseProdPersona
{
    
        const TIPO_PERSONA_ACADEMICO = 1;
        const TIPO_PERSONA_NO_ACADEMICO = 0;
        
        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return BaseProdPersona::model
         */    
        public static function model($className=__CLASS__) {
                return parent::model($className);
        }

       /**
         * Consulta el detalle de una persona cargado en la base de datos
         * 
         * @param int $persona_id persona ID 
         * @return ProdPersona persona encontrada
         */            
        public static function getPersona($persona_id){
            $persona = self::model()->findByPk($persona_id);
            return $persona;        
        }

       /**
         * Busca al administrador del sistemas
         * 
         * @return ProdPersona Administrador del sistema
         */             
        public static function getAdministrador(){
            $criteria = new CDbCriteria();
            $criteria->compare('rol_id', ProdRol::ROL_ADMINISTRDOR);
            $criteria->compare('bo_activo', 1);

            $persona = self::model()->find($criteria);
            return $persona;        
        }
        
        /**
         * Busca al administrador del sistemas
         * 
         * @return ProdPersona Administrador del sistema
         */             
        public static function getPersonas(){
            $criteria = new CDbCriteria();      
            $criteria->compare('bo_activo', 1);
            $criteria->order= 'nombres ASC';

            $personas = self::model()->findAll($criteria);
            return $personas;        
        }
        
        
        /**
         * Busca personas encargadas de un area
         * 
         * @return ProdPersona Listado Encargados
         */             
        public static function getEncargadosArea($area_id){
            $criteria = new CDbCriteria();      
            $criteria->compare('bo_activo', 1);
            $criteria->compare('area_id', $area_id);
            $criteria->compare('rol_id', ProdRol::ROL_ENCARGADO);
            
            $criteria->order= 'nombres ASC';

            $personas = self::model()->findAll($criteria);
            return $personas;        
        }

        
        /**
         * Busca personas con rol jefe administrativo
         * 
         * @return ProdPersona Listado Jefes Administrativos
         */             
        public static function getJefesAdministrativos(){
            $criteria = new CDbCriteria();      
            $criteria->compare('bo_activo', 1);
            $criteria->compare('rol_id', ProdRol::ROL_JEFE_ADMINISTRATIVO);
            
            $criteria->order= 'nombres ASC';

            $personas = self::model()->findAll($criteria);
            return $personas;        
        }


        public static function getRolIngresoProductividades($persona){
            
            $tipo_persona = $persona->bo_academico;
            return $tipo_persona;


        }
        
        
        
       /**
         * Hereda el comportamiento REST 
         * Se usa para implementar la creación de personas a través de la metodología REST (web services)
         * 
         */             
        public function behaviors()
        {
            return array(
                'RestModelBehavior' => array(
                    'class' => 'WRestModelBehavior',
                )
            );
        }

       /**
         * Convierte a string los datos de una persona
         * 
         * @return string Devuelve el nombre completo de una personas
         */           
        public function __toString() {

            return $this->nombre_completo();

        }
        
        
}