<?php

Yii::import('application.models._base.BaseProdValidaProd');

/**
 * Model que realiza el mapeo de la tabla ProdValidaProd
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProdValidaProd extends BaseProdValidaProd
{
        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return BaseProdValidaProd::model
         */         
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}