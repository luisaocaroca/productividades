<?php

/**
 * Form usado para consultas específicas.
 * Este formulario se usa en las consultas de productividades individuales, globales,
 * estadisticas e informes.
 * 
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */


/**
 * @property integer $anio_id

*/
class ConsultasForm extends CFormModel
{
        public $anio_id;

        public $mes_id;
        public $anio_mes_id;

        public $anio_desde_id;
        public $mes_desde_id;
        public $anio_hasta_id;
        public $mes_hasta_id;

        public $periodo_id;
        
        public $area_id;

        public $exportar_excel=false;


        public $tipo_consulta;
        public $tipo_salida;
        public $tipo_prod_id;
        public $cc_id;
        public $tipo_grafico;
        
        public $limpiar;
        public $tipo_personas;
        
        //FILTRO AVANZADO
        public $persona_id;
        public $nombre_persona;
        public $persona_carga_id;
        public $monto;
        
        public $mostrar_detalle_tipo_productividad;
        public $mostrar_positivos;
        
        public $filtrar_listado;
        /**
         * Obtiene el label de los campos del formulario
         * 
         * @return array
         */         
        public function attributeLabels()
        {
                return array(
                    'periodo_id'=>Yii::t('app', 'Período'),
                    'area_id'=>Yii::t('app', 'Área'),
                    'anio_id'=>Yii::t('app', 'Seleccionar Año'),
                    'anio_desde_id'=>'Año desde',
                    'mes_desde_id'=>'Mes desde',
                    'anio_hasta_id'=>'Año hasta',
                    'mes_hasta_id'=>'Mes hasta',
                    'tipo_consulta'=>'Tipo Consulta',
                    'tipo_salida'=>'Tipo Salida',
                    'tipo_prod_id'=>'Tipo Productividad',
                    'cc_id'=>'Centro Costo',
                    'tipo_grafico'=>'Tipo Grafico',
                    'tipo_personas'=>'Tipo Personas',
                    'persona_id'=>'Persona',
                    'nombre_persona'=>'Persona',
                    'persona_carga_id'=>'Persona Carga',
                    'monto'=>'Monto',
                    'mostrar_detalle_tipo_productividad'=>'Mostrar Detalle Tipo Productividad',
                    'mostrar_positivos'=>'Mostrar Positivos',
                    'filtrar_listado'=>'Filtro Aplicado'
                    
                );
        }

        /**
         * Reglas de validación de los campos del formulario
         * 
         * @return array
         */           
        public function rules()
        {
            return array(
                array('periodo_id', 'numerical'),
                array('area_id', 'numerical'),
                array('anio_id', 'numerical'),

                array('mes_id', 'numerical'),
                array('anio_mes_id', 'numerical'),

                array('anio_desde_id', 'numerical'),
                array('mes_desde_id', 'numerical'),
                array('anio_hasta_id', 'numerical'),
                array('mes_hasta_id', 'numerical'),

                array('tipo_consulta', 'verificar_consulta'),
                array('tipo_salida', 'numerical'),
                array('cc_id', 'numerical'),
                array('tipo_prod_id', 'numerical'),
                array('tipo_grafico', 'numerical'),
                array('exportar_excel', 'boolean'),
                
                array('limpiar','numerical'),
                array('tipo_personas','numerical'),
                array('persona_id','numerical'),
                array('persona_carga_id','numerical'),
                array('nombre_persona', 'length', 'max'=>500),
                array('monto','numerical'),
                array('mostrar_detalle_tipo_productividad,mostrar_positivos','numerical'),
                array('filtrar_listado','numerical'),
            );
        }

        /**
         * Reglas de validación de los campos del formulario
         * @param array $attribute atributos
         * @param array $params parametros
         * @return array
         */           
        public function verificar_consulta($attribute,$params) {
            //echo $this->rut; 

            if($this->tipo_consulta==2){
                if($this->mes_id==0){
                    $this->addError("mes_id", 'Debe seleccionar el mes');
                }        
            }

            if($this->tipo_consulta==3){
                $falta_mes=false;

                if($this->mes_desde_id==0){
                    $this->addError("mes_desde_id", 'Debe seleccionar el mes desde');
                    $falta_mes=true;
                }

                if($this->mes_hasta_id==0){
                    $this->addError("mes_hasta_id", 'Debe seleccionar el mes hasta');
                    $falta_mes=true;
                }  

                if(!$falta_mes){
                    $inicio = $this->anio_desde_id*100 + $this->mes_desde_id;
                    $fin = $this->anio_hasta_id*100 + $this->mes_hasta_id;

                    if($inicio>$fin){                
                        $this->addError("mes_hasta_id", 'La fecha desde no puede se mayor que la fecha hasta');
                    }
                }
            }

        }
    
}
?>
