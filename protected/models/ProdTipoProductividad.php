<?php

Yii::import('application.models._base.BaseProdTipoProductividad');

/**
 * Model que realiza el mapeo de la tabla ProdTipoProductividad
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProdTipoProductividad extends BaseProdTipoProductividad
{
        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return BaseProdTipoProductividad::model
         */     
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
       /**
         * Consulta el listado de tipo de productividades que son coordinaciones
         * 
         * @return array {@link ProdTipoProductividad} listado tipos productividades que son coordinaciones
         */           
        public static function getTiposCoordinaciones(){
            $criteria_tipo = new CDbCriteria();
            $criteria_tipo->compare('es_coordinacion', 1);
            $criteria_tipo->compare('bo_activo', 1);
            $tipos_productividades = ProdTipoProductividad::model()->findAll($criteria_tipo);
            return $tipos_productividades;
        }
        
        public static function getTipoProductividad($tipo_prod_id){
            $tipo_productividad  = self::model()->findByPk($tipo_prod_id);
            return $tipo_productividad;
        }
        
       
}