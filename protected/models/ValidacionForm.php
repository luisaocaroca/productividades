<?php
/**
 * Formulario para cambiar estado de una productividades de forma masiva
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ValidacionForm extends CFormModel
{
    public $periodo_id;
    public $es_academico;
    public $area_id;
    
    //FILTRO AVANZADO
    public $tipo_personas;
    public $persona_id;
    public $nombre_persona;
    public $persona_carga_id;
    public $monto;    
    public $mostrar_detalle_tipo_productividad;
    public $mostrar_positivos;
    public $validar_productividades;
    
    public $filtrar_listado;
    
/**
         * Obtiene el label de los campos del formulario
         * 
         * @return array
         */         
        public function attributeLabels()
        {
                return array(
                    'periodo_id'=>Yii::t('app', '<span class="glyphicon glyphicon-calendar"></span> Período'),
                    'area_id'=>Yii::t('app', '<span class="glyphicon glyphicon-th-large"></span> Área')
                    );
        }
        
    /**
         * Reglas de validación de los campos del formulario
         * 
         * @return array
         */  
	public function rules()
	{
		return array(
                    array('periodo_id,es_academico,area_id,validar_productividades,mostrar_positivos','numerical'),
                    array('tipo_personas','numerical'),
                    array('persona_id','numerical'),
                    array('persona_carga_id','numerical'),
                    array('nombre_persona', 'length', 'max'=>500),
                    array('monto','numerical'),
                    array('mostrar_detalle_tipo_productividad','numerical'),
                    array('filtrar_listado','numerical')
                    

		);
	}
}
?>
