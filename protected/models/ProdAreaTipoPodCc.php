<?php

Yii::import('application.models._base.BaseProdAreaTipoPodCc');

class ProdAreaTipoPodCc extends BaseProdAreaTipoPodCc
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        
        public static function getRelacionesArea($area_id){
            
            $relacion_cc_tipo_prod = self::model()->findAll('area_id=:area_id',array(':area_id'=>(int) $area_id));
                
            $relaciones = array();
            foreach($relacion_cc_tipo_prod as $relacion){

                if($relacion->cc->bo_activo == 1 && $relacion->tipoProd->bo_activo ==1 ){
                    array_push($relaciones, $relacion);
                }
            }
            
            
            return $relaciones;
                
        }
        
         /**
         * Entrega el listado de centros de costos asociados al area
         * 
         * @param int $area_id area a consultar
         * @param bool $sin_coordinaciones ='true' define si quiere devolver o no el listado de coordinaciones
         * @return array {@link ProdRolTipoProdCC} listado tipos de productividades/CC encontradas
         */           
        public static function getCentrosCostosTiposProductividadesArea($area_id,$sin_coordinaciones=true){
                        
            
            $relacion_cc_tipo_prod = ProdAreaTipoPodCc::model()->findAll('area_id=:area_id order by cc_id',array(':area_id'=>(int) $area_id));
            $salida = array();
            
            if($sin_coordinaciones){
                
                foreach($relacion_cc_tipo_prod as $cc_tipos_prod_rol){
                    $tipoProd = $cc_tipos_prod_rol->tipoProd;
                    if($tipoProd->bo_activo==1){
                        if($tipoProd->es_coordinacion!=1){
                            array_push($salida, $cc_tipos_prod_rol);
                        }
                    }
                }
            }else{
                foreach($relacion_cc_tipo_prod as $cc_tipos_prod_rol){
                    $tipoProd = $cc_tipos_prod_rol->tipoProd;
                    if($tipoProd->bo_activo==1){
                        array_push($salida, $cc_tipos_prod_rol);
                    }
                }
                
            }

            return $salida;
            
            
        }
        
        public static function getAreasCentroCosto($cc_id){
            
            $areas_cc = self::model()->findAll('cc_id=:cc_id',array(':cc_id'=>(int) $cc_id));
                
            $areas = array();
            foreach($areas_cc as $area_cc){

                $area = ProdArea::getArea($area_cc->area_id);
                if(!in_array($area, $areas)){
                    array_push($areas, $area);
                }
            }
            
            
            return $areas;
                
        }
}