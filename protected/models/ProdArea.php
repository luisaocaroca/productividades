<?php

Yii::import('application.models._base.BaseProdArea');

class ProdArea extends BaseProdArea
{
    
        const TODOS = 0;
    
    
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        
        /**
         * Consulta el listado de Áreas
         * 
         * @return array {@link ProdArea} listado de áreas
         */           
        public static function getAreas(){
            $criteria = new CDbCriteria();
            $criteria->addCondition("bo_activo=1");
            $criteria->order = 'area_id ASC';
            
            $areas = ProdArea::model()->findAll($criteria);
            return $areas;
        }
        
        /**
         * Consulta el listado de Áreas
         * 
         * @return array {@link ProdArea} listado de áreas
         */           
        public static function getAreasFiltrada(){
            $criteria = new CDbCriteria();
            $criteria->addCondition("bo_activo=1");
            $criteria->addCondition("area_id<>0");
            $criteria->order = 'area_id ASC';
            
            $areas = ProdArea::model()->findAll($criteria);
            return $areas;
        }
        
        public static function getArea($area_id){
            $area  = self::model()->findByPk($area_id);
            return $area;
        }
}