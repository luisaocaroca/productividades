<?php

Yii::import('application.models._base.BaseProdPeriodo');

/**
 * Model que realiza el mapeo de la tabla ProdPeriodo
 *  
 *  
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProdPeriodo extends BaseProdPeriodo
{
        const PERIODO_ABIERTO = ProdEstadoPeriodo::ABIERTO;
        const PERIODO_VALIDACION = ProdEstadoPeriodo::VALIDACION;
        const PERIODO_CERRADO = ProdEstadoPeriodo::CERRADO;
        
        const DIAS_PARA_VALIDACION = 8;
        
        static $MESES = array( array("id"=>"01","label"=>'Ene'), 
                            array("id"=>"02","label"=>'Feb'), 
                            array("id"=>"03","label"=>'Mar'), 
                            array("id"=>"04","label"=>'Abr'), 
                            array("id"=>"05","label"=>'May'),
                            array("id"=>"06","label"=>'Jun'),
                            array("id"=>"07","label"=>'Jul'),
                            array("id"=>"08","label"=>'Ago'),
                            array("id"=>"09","label"=>'Sep'),
                            array("id"=>"10","label"=>'Oct'),
                            array("id"=>"11","label"=>'Nov'),
                            array("id"=>"12","label"=>'Dic'));        
    
        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return BaseProdPeriodo::model
         */
        public static function model($className=__CLASS__) {
                return parent::model($className);
        }

       /**
         * Consulta a la base de datos de los períodos cargados
         * 
         * @param bool $soloPeriodoAbierto = 'false' define si se quiere mostrar solo los períodos activos 
         * @return array Listado de períodos
         */           
        public static function getPeriodosCargados($soloPeriodoAbierto=false){
            $periodos = array();

            $criteria = new CDbCriteria();
            if($soloPeriodoAbierto){
                $criteria->compare('estado_periodo_id', self::PERIODO_ABIERTO);
            }
            $criteria->order= 'periodo_id DESC';
            //$criteria->addInCondition('id', array(4, 8, 15, 16, 23, 42));
            $periodos_cargados = self::model()->findAll($criteria);

            foreach($periodos_cargados as $periodo){
                $periodo_actual_id = date("Ym");   

                if($periodo_actual_id >= $periodo->periodo_id ){
                    array_push($periodos, $periodo);
                }

            }

            return $periodos;

        }

       /**
         * Consulta a la base de datos de los años cargados
         * 
         * @return array Listado de años
         */   
        public static function getAnios(){
            $anios = array();
            $anio_actual_id = date("Y");  

            $periodos_cargados = self::getPeriodosCargados();

            foreach ($periodos_cargados as $periodo){
                //echo $periodo->anio;                
                if($periodo->anio<=$anio_actual_id){
                    $anio_salida = array("id"=>$periodo->anio,"value"=>$periodo->anio);
                    
                    if(!in_array($anio_salida, $anios)){
                        array_push($anios,$anio_salida);
                    }
                }                
            }

            return $anios;
        }

       /**
         * Consulta el detalle de un período cargado en la base de datos
         * 
         * @param int $periodo_id periodo ID
         * @return ProdPeriodo periodo encontrados
         */          
        public static function getPeriodo($periodo_id){

            $periodo = self::model()->findByPk($periodo_id);

            if(is_null($periodo)){
                $periodo = new ProdPeriodo();
                $periodo->periodo_id = $periodo_id;                
                $anio = substr($periodo_id, 0,4);
                $mes  = substr($periodo_id, 4,2);

                $periodo->nombre_periodo = $mes ."/" . $anio; 
                $periodo->mes =  $mes;  
                $periodo->anio=  $anio;
            }

            return $periodo;
        }

       /**
         * Obtiene los meses del años
         * 
         * @return array {@link ProdPeriodo} con los meses del año
         */         
        public static function getMeses(){
            
            /*
            $meses = array();

            for($i=0;$i<13;$i++){
                $mes = new ProdPeriodo();
                $mes->mes = $i;
                array_push($meses, $mes);                
            }                       

            return $meses;
             * */
             return ProdPeriodo::$MESES;

        }

       /**
         * Verifica si es necesario crear un nuevo período (periodo actual)s
         * 
         * @param int $periodo_id periodo a verificars
         */          
        public static function verifica_crear_periodo($periodo_id){
            $periodo_actual = self::model()->findByPk($periodo_id);           

            if(is_null($periodo_actual)){
                $periodo_actual = new ProdPeriodo();
                $periodo_actual->periodo_id = $periodo_id;

                $anio = substr($periodo_id, 0,4);
                $mes  = substr($periodo_id, 4,2);

                $periodo_actual->nombre_periodo = $mes ."/" . $anio; 
                $periodo_actual->mes =  $mes;  
                $periodo_actual->anio=  $anio;
                $periodo_actual->estado_periodo_id = ProdEstadoPeriodo::ABIERTO;
                $periodo_actual->save();

                //SE CARGAN LAS COORDINACIONES AL NUEVO PERIODO
                $coordinaciones = ProdCoordinacion::model()->findAll();

                foreach($coordinaciones  as $coordinacion){
                    if($coordinacion->monto>0){
                        $productividad = new ProdProductividad();
                        $productividad->periodo_id = $periodo_id;
                        $productividad->tipo_prod_id = $coordinacion->tipo_prod_id;
                        $productividad->persona_id = $coordinacion->persona_id;
                        $productividad->cc_id = $coordinacion->cc_id;
                        $productividad->cuota_actual = 1;
                        $productividad->numero_cuotas = 1;
                        $productividad->monto = $coordinacion->monto;
                        $productividad->total_monto_asignado = $coordinacion->monto;
                        $productividad->estado_id = ProdEstado::APROBADO;
                        $productividad->persona_carga_id = Yii::app()->user->personaId;

                        $productividad->save();

                    }
                }
            }
        }

       /**
         * Consulta a la base de datos para determinar si el período anterior está o no abierto
         * 
         * @param int $periodo_id periodo 
         * @return int numero de periodos abiertos anteriores al periodo consulta
         */         
        public static function buscaPeriodoAnteriorAbierto($periodo_id){
            $criteria = new CDbCriteria();
            $criteria->condition = "periodo_id < ".$periodo_id;
            $criteria->compare('estado_periodo_id',self::PERIODO_ABIERTO);
            //$criteria->addInCondition('id', array(4, 8, 15, 16, 23, 42));
            return self::model()->count($criteria);
        }
        
        
        /**
         * Listado de periodos abiertos
         * 
         * @param int $periodo_id periodo 
         * @return int periodo actual
         */         
        public static function getPeriodosAnterioresAbiertos($periodo_id){
            $criteria = new CDbCriteria();
            $criteria->condition = "periodo_id < ".$periodo_id;
            //$criteria->compare('estado_periodo_id',self::PERIODO_ABIERTO);
            $criteria->addInCondition('estado_periodo_id', array(self::PERIODO_ABIERTO,self::PERIODO_VALIDACION));
            return self::model()->findAll($criteria);
        }

        
        public static function getNumeroMes($mes){
        
            foreach(self::$MESES as $mesObject){
                if($mesObject["label"]==$mes){
                    return $mesObject["id"];
                }
            }
        }

        public static function getNombreMes($mes){

            foreach(self::$MESES as $mesObject){
                if($mesObject["id"]==$mes){
                    return $mesObject["label"];
                }
            }
        }
    
}