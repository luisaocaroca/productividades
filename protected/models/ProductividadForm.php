<?php

/**
 * Form usado para la carga de productividades
 * Este formulario se usa cuando se cargan productividades al sistema
 * 
 * @author Luis Toledo
 * @version 0.1
 * @package application.models
 */
class ProductividadForm extends CFormModel
{
	public $periodo_id;
        public $area_id;
	public $persona_id;
        public $nombre_persona;
        public $tipo_prod_id;
        public $cc_id;
        public $monto;
        public $numero_pagos=1;
        public $actualiza_periodo = false;
        public $editar_productividad = false;
        public $tipo_prod_editar_id = false;
        public $cc_editar_id = false;
        public $persona_carga_id;
        public $tipo_personas=1;
        
        public $mostrar_detalle_tipo_productividad;
        
        public $observaciones;
        

        /**
         * Reglas de validación de los campos del formulario
         * 
         * @return array
         */  
	public function rules()
	{
		return array(
			array('periodo_id, area_id,persona_id,tipo_prod_id,cc_id,monto,nombre_persona,numero_pagos', 'required','on'=>'nuevo','message'=>'Debe ingresar {attribute}'),
                        array('periodo_id,area_id, persona_id,monto,numero_pagos', 'required','on'=>'actualizar','message'=>'Debe ingresar {attribute}'),
			array('monto,numero_pagos', 'numerical','integerOnly'=>true),
                        array('numero_pagos', 'verificar_numero_pagos'),
                        array('monto', 'verificar_monto'),
                        array('actualiza_periodo', 'length', 'max'=>10),
                        array('editar_productividad', 'length', 'max'=>10),
                        array('tipo_prod_editar_id,cc_editar_id,tipo_personas', 'numerical','integerOnly'=>true),
                        array('persona_id,persona_carga_id','numerical'),
                        array('mostrar_detalle_tipo_productividad','numerical','on'=>'nuevo'),
                        array('observaciones','length', 'max'=>500),
                    
                    

		);
	}

        /**
         * Obtiene el label de los campos del formulario
         * 
         * @return array
         */  
	public function attributeLabels()
	{
		return array(
                    'periodo_id'=>'Seleccionar Período',
                    'area_id'=>'Área',
                    'persona_id'=>'Persona',
                    'nombre_persona'=>'Persona',
                    'tipo_prod_id'=>'Tipo Productividad',
                    'cc_id'=>'Centro Costo',
                    'monto'=>'Monto',
                    'numero_pagos'=>'Cuotas',
                    'mostrar_detalle_tipo_productividad'=>'Mostrar Detalle Tipo Productividad',
                    'observaciones'=>'Indicar a que corresponde la productividad'

		);
	}
        
        /**
         * Reglas de validación para los numeros de pagos
         * @param array $attribute atributos
         * @param array $params parametros
         * @return array
         */         
        public function verificar_numero_pagos($attribute,$params) {
            //echo $this->rut; 
            
            if($this->numero_pagos<=0){
                $this->addError($attribute, 'El  numero de pagos debe ser mayor que cero');
            }
        }
        
        /**
         * Reglas de validación para los montos
         * @param array $attribute atributos
         * @param array $params parametros
         * @return array
         */           
        public function verificar_monto($attribute,$params) {
            //echo $this->rut; 
            
            if($this->monto<=0){
                $this->addError($attribute, 'El monto debe ser mayor que cero');
            }
        }
        

//        
//        public function toString(){
//            
////            echo "<pre>";
////            print_r($this);
////            echo "</pre>";
//                
////            echo "periodo_id    :" . $this->periodo_id . "<br>";
////            echo "persona_id    :" . $this->persona_id . "<br>";
////            echo "nombre_persona:" . $this->nombre_persona . "<br>";
////            echo "tipo_prod_id  :" . $this->tipo_prod_id . "<br>";
////            echo "cc_id         :" . $this->cc_id . "<br>";
////            echo "monto         :" . $this->monto . "<br>";
////            echo "numero_pagos  :" . $this->numero_pagos . "<br>";
//        }

	
}
