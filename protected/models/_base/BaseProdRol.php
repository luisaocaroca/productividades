<?php

/**
 * This is the model base class for the table "prod_rol".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ProdRol".
 *
 * Columns in table "prod_rol" available as properties of the model,
 * followed by relations of table "prod_rol" available as properties of the model.
 * @author Luis Toledo
 * @version 0.1
 * @package application.models._base 
 * 
 * @property int $rol_id
 * @property string $nombre_rol
 * @property string $rol_intranet
 * @property int $bo_activo
 *
 * @property ProdPersona[] $prodPersonas
 * @property ProdTipoProductividad[] $prodTipoProductividads
 * @property ProdSubMenu[] $prodSubMenus
 */
abstract class BaseProdRol extends GxActiveRecord {

        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return GxActiveRecord::model
         */    
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

        /**
         * Obtiene el nombre de la tablas
         * 
         * @return string
         */        
	public function tableName() {
		return 'prod_rol';
	}

        /**
         * Recupera el label de un campo de la tabla
         * 
         * @param int $n nivel del label
         * @return string
         */            
	public static function label($n = 1) {
		return Yii::t('app', 'Rol|Roles', $n);
	}

        /**
         * Obtiene el nombre de la columna que representa la tabla
         * 
         * @return string
         */         
	public static function representingColumn() {
		return 'nombre_rol';
	}   

        /**
         * Reglas de validación del objeto que mapea a la tabla
         * 
         * @return array
         */            
	public function rules() {
		return array(
			array('rol_id,bo_activo', 'required'),
			array('rol_id,bo_activo', 'numerical', 'integerOnly'=>true),
			array('nombre_rol', 'length', 'max'=>100),
                        array('rol_intranet', 'length', 'max'=>100),                    
			array('nombre_rol', 'default', 'setOnEmpty' => true, 'value' => null),
			array('rol_id, nombre_rol, rol_intranet', 'safe', 'on'=>'search'),
		);
	}

        /**
         * Define las relaciones que tiene la tabla con el resto de objetos del modelos
         * 
         * @return array
         */             
	public function relations() {
		return array(
			'prodPersonas' => array(self::HAS_MANY, 'ProdPersona', 'rol_id'),
			'prodSubMenus' => array(self::MANY_MANY, 'ProdSubMenu', 'prod_sub_menu_rol(rol_id, sub_menu_id)'),
		);
	}

        /**
         * Define las relaciones que tiene la tabla con el resto de objetos del modelos
         * 
         * @return array
         */             
	public function pivotModels() {
		return array(
			'prodSubMenus' => 'ProdSubMenuRol',
		);
	}

        /**
         * Obtiene el label de los campos de la tablas
         * 
         * @return array
         */            
	public function attributeLabels() {
		return array(
			'rol_id' => Yii::t('app', 'Rol'),
			'nombre_rol' => Yii::t('app', 'Nombre Rol'),
                        'rol_intranet'=>Yii::t('app', 'Identificador Intranet'),
                        'bo_activo' => Yii::t('app', 'Vigente'),
			'prodPersonas' => null,
			'prodSubMenus' => null,
		);
	}

        /**
         * Ejecuta la busqueda de registros de la tabla
         * 
         * @return CActiveDataProvider
         */           
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('rol_id', $this->rol_id);
		$criteria->compare('nombre_rol', $this->nombre_rol, true);
                $criteria->compare('rol_intranet', $this->rol_intranet, true);
                $criteria->compare('bo_activo', $this->bo_activo, true);
		
                return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
        
        
        /**
         * Trasforma a string el valor del campo boolean es activo
         * 
         * @return string Nombre campo booelan activo
         */           
        public function activo(){
            return $this->bo_activo == 1 ? 'Si' : 'No';
        }
        
        /**
         * Trasforma a string el valor del campo boolean es activo
         * 
         * @return string Color campo booelan activo
         */           
        public function color(){
            return $this->bo_activo == 0 ? 'red' : '';
        }
        
}