<?php

/**
 * This is the model base class for the table "prod_sub_menu_rol".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ProdSubMenuRol".
 *
 * Columns in table "prod_sub_menu_rol" available as properties of the model,
 * and there are no model relations.
 * @author Luis Toledo
 * @version 0.1
 * @package application.models._base 
 * 
 * @property int $sub_menu_id
 * @property int $rol_id
 *
 */
abstract class BaseProdSubMenuRol extends GxActiveRecord {

        /**
         * Recupera el objeto que mapea la tabla
         * 
         * @param string $className representa el nombre de la clases
         * @return GxActiveRecord::model
         */    
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

        /**
         * Obtiene el nombre de la tablas
         * 
         * @return string
         */        
	public function tableName() {
		return 'prod_sub_menu_rol';
	}

        /**
         * Recupera el label de un campo de la tabla
         * 
         * @param int $n nivel del label
         * @return string
         */            
	public static function label($n = 1) {
		return Yii::t('app', 'Sub Menu Rol|Sub Menu Roles', $n);
	}

        /**
         * Obtiene el nombre de la columna que representa la tabla
         * 
         * @return string
         */         
	public static function representingColumn() {
		return array(
			'sub_menu_id',
			'rol_id',
		);
	}

        /**
         * Reglas de validación del objeto que mapea a la tabla
         * 
         * @return array
         */            
	public function rules() {
		return array(
			array('sub_menu_id, rol_id', 'required'),
			array('sub_menu_id, rol_id', 'numerical', 'integerOnly'=>true),
			array('sub_menu_id, rol_id', 'safe', 'on'=>'search'),
		);
	}

        /**
         * Define las relaciones que tiene la tabla con el resto de objetos del modelos
         * 
         * @return array
         */             
	public function relations() {
		return array(
                    'subMenu' => array(self::BELONGS_TO, 'ProdSubMenu', 'sub_menu_id'),
                    'rol' => array(self::BELONGS_TO, 'ProdRol', 'rol_id'),
		);
	}

        /**
         * Define las relaciones que tiene la tabla con el resto de objetos del modelos
         * 
         * @return array
         */             
	public function pivotModels() {
		return array(
		);
	}

        /**
         * Obtiene el label de los campos de la tablas
         * 
         * @return array
         */            
	public function attributeLabels() {
		return array(
			'sub_menu_id' => null,
			'rol_id' => null,
		);
	}

        /**
         * Ejecuta la busqueda de registros de la tabla
         * 
         * @return CActiveDataProvider
         */           
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('sub_menu_id', $this->sub_menu_id);
		$criteria->compare('rol_id', $this->rol_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}